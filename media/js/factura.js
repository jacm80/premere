function guardarPago(item){
   //////////////////////////////////////////////////////////////////////////////////////////
   var orden_id      = $('#orden_id').val( );
   var forma_pago    = $('#pago_forma_pago option:selected').text();
   var forma_pago_id = $('#pago_forma_pago').val();
   var pago_banco    = $('#pago_banco option:selected') .text();
   var pago_banco_id = $('#pago_banco').val();
   var pago_numero_comprobante = $('#pago_numero_comprobante')  .val();
   var pago_descripcion = $('#pago_descripcion option:selected').text();
   var factura_id       = $('#pago_descripcion').val( );
   ///////////////////////////////////////////////////////////////////////////////////////////
   $.ajax({
            url: base_url+'/factura/registrarpago',
            type:'post',
            async: true,
            data:{
                  orden_id: orden_id, 
                  forma_pago_id: forma_pago_id, 
                  banco_id: pago_banco_id, 
                  numero_comprobante: pago_numero_comprobante, 
                  descripcion: pago_descripcion,
                  pago_ref: factura_id
            },
            success:function(respuesta){ _toRow(item); },
            error:function(){ alert('No se registro el pago'); }
         });
   ////////////////////////////////////////////////////////////////////////////////////////////
   function _toRow(item){
      $('#agregar').removeClass('hide'); 
      //-----------------------------------------------------------------
      $('#pago_forma_pago_'+item)        .replaceWith('<span>' + forma_pago + '</span>');
      $('#pago_banco_'+item)             .replaceWith('<span>' + pago_banco + '</span>');
      $('#pago_numero_comprobante_'+item).replaceWith('<span>' + pago_numero_comprobante + '</span>');
      $('#pago_descripcion_'+item)       .replaceWith('<span>' + pago_descripcion + '</span>');
      //------------------------------------------------------------------------------------------
      $('#pago_forma_pago')        .remove();
      $('#pago_banco')             .remove();
      $('#pago_numero_comprobante').remove();
      $('#pago_descripcion')       .remove();
      $('#guardar')                .remove();
      alert('Pago Registrado con Exito');
   }
}


$(document).ready(function(){

   $('#agregar').click(function(){
      $.ajax({
            url: base_url+'/factura/addpago',
            type:'post',
            async: true,
            data: { item: i , orden_id:$('#orden_id').prop('value') },   
            dataType:'html',
            success:function(respuesta){
               $(respuesta).insertBefore('#tr_ref');
               $('#agregar').addClass('hide');
               i++;
             },
            error:function(){ alert('fallo'); }
            });
   });


   $('#optCancelar').qtip({
	   content: 'Seleccione el pago asociado con la orden',
      position: { my:'bottom left',at: 'top right'}
   });

   $('#optCancelar').change(function(){
      if ($(this).val()=='1')
      {
         var resta = $('#resta').html( );
         $('#monto_cancelar').html(resta);
      }
      else {
         var iva = $('#iva').html( );
         $('#monto_cancelar').html(iva);
      }
   });   


   $('#BtnOpcionPago').click(function(){
      if ($('#optCancelar').val()==1)
      {
         //alert('pagar orden completa');
         location.href = base_url + '/factura/cancelarOrdenCompleta/id/'+$('#orden_id').val();
      }
   });

   $('input[type=image]').click(function(event){
      var tipo    = event.target.id.split('-')[0];
      var xid     = event.target.id.split('-')[1];
      var numero  = event.target.id.split('-')[2];
      var titulo;
      if (tipo=='BtnView')
      {
         /**/
         titulo='Factura No.: '+zeropad(numero,6);
         $.ajax({
            url:base_url+'/factura/viewFactura',
            async:true,
            type:'post',
            dataType:'html',
            data:{id:xid},
            success:function(resp){ $('#dialog').html(resp); },
            error:function(){ alert('Error al cargar la vista de la factura'); }
         });
         $('#dialog').dialog({
            modal:true,
            title: titulo,
            width: 800, 
            height: 500,
            buttons:{'Cerrar':function(){ $(this).dialog("close"); } }
         });
         /**/
      }
      else if (tipo=='BtnCancelar') 
      {
         location.href = base_url+'/factura/formPagoFactura/id/'+xid;
      }
   });

});

function loadTMPL(motivo){
      var tmpl;
      var output = $('#divMotivo');
      output.html('');
      data = (motivo == 'debito') ? { debito:[true] } : { debito:false };
      $.Mustache.load(base_url+'/media/js/mustache-tmpl/ajuste/motivo.htm')
      .fail(function(){ console.debug('fallo la carga del template mustache motivo'); })
      .done(function(){ output.mustache('motivo',data); });
   }

$(document).ready(function(){
   $('#tipo_ajuste').change(function(){
      if ($(this).val( ) == '1') 
         loadTMPL('debito');
      else 
         loadTMPL('credito');
   });

   $('#content-numero').click(function(){
     $('#viewfactura').fadeToggle('slow'); 
   });

});

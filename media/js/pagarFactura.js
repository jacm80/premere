function _getPagos( ) {
   var _pagos = 0.0;
   $('.forma-pago').each(function(i,elem){
       _pagos += parseFloat($(elem).prop('value'))
   });
   return _pagos;
}


function _reCalcularMontos( )
{
   var _saldo              = parseFloat($('#saldo').html());
   var _resta_orden        = parseFloat($('#resta_orden_ro').val());
   var _total_factura      = parseFloat($('#total_factura').html());
   var _saldo_dispuesto    = parseFloat($('#resta_monto').prop('value'));
   var _pagos              = _getPagos();
   // obtener el saldo
   var _saldo_disponible   = (_saldo-_saldo_dispuesto).toFixed(2);
   // obtener lo que resta
   var _pendiente          = (_total_factura - (_saldo_dispuesto + _pagos)).toFixed(2);
   // lo que queda por cancelar
   var _por_cancelar_orden = (_resta_orden - _pagos).toFixed(2); 

   // salida
   $('#saldo_disponible').html(_saldo_disponible);
   $('#pendiente').html(_pendiente);
   $('#resta_orden').html(_por_cancelar_orden);
   $('#total_pago').html(_pagos.toFixed(2));

   if ((_por_cancelar_orden<0) || (_pendiente>0)) {
      $('#error_cuadre').removeClass('hide');
      $('#BtnProcesar').attr('disabled','disabled');
   }
   else {
      $('#error_cuadre').addClass('hide');
      $('#BtnProcesar').removeAttr('disabled');
   }

   return { pendiente: _pendiente, saldo_disponible:_saldo_disponible, resta_orden: _por_cancelar_orden };
}


function _enabledChkPago( ){
   var _pendiente = parseFloat($('#pendiente').html( ));
   console.debug(_pendiente);
   if (_pendiente > 0)
   {
      $('.forma-pago-chk').each(function(i,elem){
         $(elem).removeAttr('disabled');
      });
   }
   else {
      $('.forma-pago-chk').each(function(i,elem){
         $(elem).attr('disabled','disabled');
      });
   }
}

$(document).ready(function(){
      // para descontar del saldo (del abono)
      $('#usar_saldo').change(function(){
         var _saldo         = parseFloat($('#saldo').html());
         var _total_factura = parseFloat($('#total_factura').html());
         ///////////////////////////////////////////////////////////////////////
         
         if ($(this).attr('checked')=='checked'){
            $('#resta_monto').removeAttr('disabled').val(_total_factura).focus( );
            _reCalcularMontos( );
         }
         else {
            $('#resta_monto').val('').attr('disabled','disabled');
            $('#resta_monto').val('0').focus( );
            _reCalcularMontos( );
         }
         _enabledChkPago( );      
      });
   
      // para actualizar el saldo de la orden
      $('#resta_monto').change(function(){
         _reCalcularMontos( );
         _enabledChkPago( );
      });

      // cuando se haga un check para la forma de pago
      $('.forma-pago-chk').click(function(){
         var _pendiente     = parseFloat($('#pendiente').html()).toFixed(2);
         var _pid = $(this).val( );
         //console.debug(_pid);
         if ($(this).attr('checked')=='checked'){
            $('#monto-'+_pid).removeAttr('disabled').val(_pendiente);
            $('#espNC-'+_pid).removeClass('hide');
            $('#espBC-'+_pid).removeClass('hide');
            _reCalcularMontos( );
         }
         else {
            $('#monto-'+_pid).attr('value','0.00');
            $('#monto-'+_pid).attr('disabled','disabled');
            $('#espNC-'+_pid).addClass('hide');
            $('#espBC-'+_pid).addClass('hide');
            _reCalcularMontos( );
         }
      });
      
      // cuando se modifique el monto en la forma de pago
      $('.forma-pago').change(function(){
         _reCalcularMontos( );
      });

      $('#BtnProcesar').click(function(){
         $('#FormFactura').submit( );
      });

      $('.numero-comprobante').live('blur',function( ){   
         //alert('hola');
         var idx = $(this).prop('id').split('-')[1];
         console.debug(idx);
         $('#espBC-'+idx).fadeOut();
         $('#espNC-'+idx).fadeOut();
      });

});

/*
   existencia_unidad    => 4
   existencia_detallado => 300
   valor_detallado      => 500
   
   cantidad             => 400

   delta = existencia_detallado - cantidad
   delta = (300 - 400) =  - 100               
   
   if (delta<0)
   {
      unidad--;
      cantidad_detallada = valor_detallado-delta
   }

*/



function restar_inventario(zid,cantidad){
   var index = zid.split('-')[1];
   var unid = '#existencia_unidad-'+index;
   var deta = '#existencia_detallado-'+index;
   var unidad    = parseInt($(unid).html());
   var detallado = parseInt($(deta).html());
   if ($('#tipo_cantidad-'+index).prop('value')=='2'){
      var delta = detallado - cantidad;
      if (delta <=0 ){
         unidad--;
         $(unid).html(unidad);
         $('#hd_existencia_unidad-'+index).val(unidad);
         detallado = parseInt( $('#valor_detallado-'+index).val( ) ) - (-delta);
         $(deta).html(detallado);
         $('#hd_existencia_detallado-'+index).val(detallado);
      }
      else {
         detallado-=cantidad;
         $(deta).html(detallado);
      }
   }
   else {
      unidad-=cantidad;
      $(unid).html(unidad);
      $('#hd_existencia_unidad-'+index).val(unidad);
   }
}

function undo(index)
{
   if (confirm('Esta seguro de volver a los valores iniciales?'))
   {
      var unid = '#existencia_unidad-'+index;
      var deta = '#existencia_detallado-'+index;
      var unidOrig = '#orig_existencia_unidad-'+index;
      var detaOrig = '#orig_existencia_detallado-'+index;
      $(unid).html($(unidOrig).val()); 
      $(deta).html($(detaOrig).val());
      $('#cantidad-'+index).val('0'); 
   }
}

function eliminar_item(index)
{
   if (confirm('Seguro de eliminar el item?'))
   {
      $('#tr-'+index).remove( );
   }
}


$(document).ready(function(){
   $.Mustache.options.warnOnMissingTemplates = true;
	$.Mustache.addFromDom();
 
   $('#agregar').click(function( ){
         
         $.ajax({
                  url: base_url + '/materialSalida/getMateriales',
                  type:'post',
                  async: true,
                  dataType: 'html',
                  //data: { entidad: tipo[$('#GastoConcepto_tipo').val()] },
                  success: function(response){ $('#dialog').html(response); },
                  error:   function(){ alert('error'); }
               });

         $('#dialog').dialog({ 
                              modal: true, 
                              title: 'Lista de Materiales',  
                              width: 800, height: 500,
                              buttons:{
                                       'Buscar':function(){
                                                            $.ajax({
                                                                     url: base_url + '/materialSalida/getMateriales',
                                                                     type:'get',
                                                                     async: true,
                                                                     dataType: 'html',
                                                                     data: $('#materialForm').serialize(),
                                                                     success: function(response){ $('#dialog').html(response); },
                                                                     error:   function(){ alert('error'); }
                                                                  });
                                                          },
                                       'Seleccionar': function( ) {

                                          var _ids = [ ];

                                          $('input[type=checkbox]').each(function(i){
                                             if (this.checked == true) { _ids.push(this.value); }
                                          });

                                          $.ajax({
                                                   url: base_url+'/materialSalida/setMateriales',
                                                   type:'get',
                                                   async: true,
                                                   dataType: 'html',
                                                   data: { ids : _ids /*.join()*/ },
                                                   success: function(response){ 
                                                               var materiales = eval(response);
                                                               $.each(materiales,function(index,item){
                                                                  if ($('#material-'+item.id).length>0) { }
                                                                  else {
                                                                        var output  = $('#detalle-content');
                                                                        $.Mustache.load(base_url+'/media/js/mustache-tmpl/materialsalida/item.htm')
                                                                        .fail(function(){ console.debug('fallo la carga del template mustache'); })
                                                                        .done(function(){ output.mustache('item',item); });
                                                                        //$('#detalle').before(_createRow(item));
                                                                  } 
                                                                  //console.debug(item.material);
                                                               });
                                                               //console.debug(_ids);
                                                            },
                                                   error:   function(){ alert('error'); }
                                                 });
                                          $(this).dialog("close");
                                        },
                                       'cancelar': function(){ $(this).dialog("close"); }
                                    }
                             });
      });
   
      
      $('#MaterialSalida_tipo_salida_id').change(function(){
         if ($('#MaterialSalida_tipo_salida_id').prop('value')=='2')
         {
            $('#MaterialSalida_almacen_id').removeClass('hide');
         }
         else
         {
            $('#MaterialSalida_almacen_id').addClass('hide');
         }
      });


      $('#BtnGuardarCompra').click(function(){
         $('#salidaForm').submit( );
      });

      $('#BtnReimprimir').live('click',function(){
         var id = $('#MaterialSalida_id').val();
         location.href = base_url+'/materialSalida/printRecibo?id='+id;
      });

});

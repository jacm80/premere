function firmaValida(firma){
   var ce=0;
   // razon social
   if (firma.razon_social == ''){ 
      $('#error-firma_razon_social').removeClass('hide');
      ce++;
   }
   else $('#error-firma_razon_social').addClass('hide');
   // domicilio fiscal
   if (firma.domicilio_fiscal == ''){ 
      $('#error-firma_domicilio_fiscal').removeClass('hide');
      ce++;
   }
   else $('#error-firma_domicilio_fiscal').addClass('hide');
   // registro de comercio
   if (firma.registro == '') {
      $('#error-firma_registro').removeClass('hide');
      ce++;
   }
   else $('#error-firma_registro').addClass('hide');
   
   return (ce>0) ? false : true;
}

// Gestion de SUCURSALES

// Llamar al form para agregar nueva sucursal

function callFormSucursal(prefix) {
   $('#'+prefix+'_descripcion').val('');
   $('#'+prefix+'_domicilio_fiscal').val('');
   $('#'+prefix+'_telefonos').val('');
   $('#'+prefix+'_sucursales').show( );
   $('#'+prefix+'_conFormSucursal').hide( );
}

// Cancelar el form

function deleteSucursal(xid){
   if (confirm('Esta seguro de eliminar la sucursal?')){
      $.ajax({
         url:base_url +'/cliente/deleteSucursal',
         data:{id:xid},
         async: true,
         type: 'post',
         success:function(){
            $('#sucursal-'+xid).remove();
         },
         error:function(){
            alert('No se puede eliminar la sucursal');
         }
      });
   }
}

function cancelarFormSucursal(prefix) {
   $('#'+prefix+'_error-descripcion').addClass('hide');
   $('#'+prefix+'_error-domicilio_fiscal').addClass('hide');
   $('#'+prefix+'_sucursales').hide( );
   $('#'+prefix+'_conFormSucursal').show( );
}

function sucursalValida(sucursal,prefix) {
   var cs = 0;
   // razon social
   if (sucursal.descripcion == ''){
      $('#'+prefix+'_error-descripcion').removeClass('hide');
      cs++;
   }
   else $('#'+prefix+'_error-descripcion').addClass('hide');
   
   if (sucursal.domicilio_fiscal == ''){
      $('#'+prefix+'_error-domicilio_fiscal').removeClass('hide');
      cs++;
   }
   else $('#'+prefix+'_error-domicilio_fiscal').addClass('hide');
   
   return (cs>0) ? false : true;
}

// Agregar sucursal
function addSucursal(prefix){

   var xid = prefix.split('-')[1];

   var sucursal = {
                     descripcion       : $.trim($('#'+prefix+'_descripcion').val()),
                     firma_id          : xid,
                     domicilio_fiscal  : $.trim($('#'+prefix+'_domicilio_fiscal').val()),
                     telefonos         : $.trim($('#'+prefix+'_telefonos').val()),
                     PATH_SERVER       : base_url
                  };
   
   console.debug(sucursal);

   if (sucursalValida(sucursal,prefix)) { _ajaxGuardarSucursal(sucursal); }

   function _ajaxGuardarSucursal(sucursal){
      $.ajax({
         url: base_url+ '/cliente/addSucursal',
         type: 'post',
         async: true,
         dataType: 'json',
         data: sucursal,
         success:function(resp){
            sucursal.id = resp;
            _sucursalItemMustache(sucursal,prefix);
            $('#'+prefix+'_sucursales').hide( );
            $('#'+prefix+'_conFormSucursal').show( );
         },
         error:function(){ alert('error al cargar al servidor'); }
      });
   }

   function _sucursalItemMustache(data,prefix){
      var output = $('#'+prefix);
      $.Mustache.load(base_url+'/media/js/mustache-tmpl/cliente/item-sucursal.htm')
      .fail(function(){ console.debug('fallo la carga del template mustache'); })
      .done(function(){ output.mustache('item-sucursal',data);});
   }
   
}

$(document).ready(function(){

   $.Mustache.options.warnOnMissingTemplates = true;
	$.Mustache.addFromDom();
 
   $('#Cliente_razon_social').blur(function(){
      if ($('#Cliente_cliente_tipo_id').val() == '1'){
         $('#Cliente_representante_legal').val($(this).val()).attr('readonly','readonly');
      }
   });

   $('#Cliente_cliente_tipo_id').change(function(){
      // totalmente necesario para que coloque el foco donde es
      $('#Cliente_rif').focus();
      if ($(this).val()==1){
         $('#Cliente_rif').val('V-');
         $('#Cliente_registro_de_comercio').val('No Aplica');
         $('#Cliente_registro_de_comercio').attr('readonly','readonly');
         $('#Cliente_archivo_registro_de_comercio').attr('readonly','readonly');
      }
      else { 
         $('#Cliente_rif').val('J-'); 
         $('#Cliente_registro_de_comercio').val('');
         $('#Cliente_registro_de_comercio').removeAttr('readonly');
         $('#Cliente_archivo_registro_de_comercio').removeAttr('readonly');
      }
   });

   $('#btnFormCliente').live('click',function(){
      $.ajax({
         url: base_url+ '/cliente/form',
         type: 'post',
         async: true,
         dataType: 'html',
         data:{ id:$('#hd_cliente_id').val() },
         success:function(resp){
            $('#clienteView').html(resp);
         },
         error:function(){ alert('error al cargar al servidor'); }
      });
   });
   
   function _mustache_cliente(data){
      var output  = $('#clienteView');
      output.html('');
      $.Mustache.load(base_url+'/media/js/mustache-tmpl/cliente/cliente.htm')
      .fail(function(){ console.debug('fallo la carga del template mustache'); })
      .done(function(){ output.mustache('cliente-view',data);});
   }

   $('#btnCancelarCliente').live('click',function(){
      var data = _extractCliente( );
      _mustache_cliente(data);
   });

   // desplegar la vista del cliente

   function _extractCliente( ){
      return {
         cliente_id:           $.trim($('#hd_cliente_id').val()),
         rif:                  $.trim($('#Cliente_rif').val()),
         razon_social:         $.trim($('#Cliente_razon_social').val()),
         representante_legal:  $.trim($('#Cliente_representante_legal').val()),
         domicilio_fiscal:     $.trim($('#Cliente_domicilio_fiscal').val()),
         contribuyente_id:     $.trim($('#Cliente_contribuyente_id').val()),
         cliente_tipo_id:      $.trim($('#Cliente_cliente_tipo_id').val()),
         telefono_celular:     $.trim($('#Cliente_telefono_celular').val()),
         telefono_residencial: $.trim($('#Cliente_telefono_residencial').val()),
         registro_de_comercio: $.trim($('#Cliente_registro_de_comercio').val()), 
         email:                $.trim($('#Cliente_email').val()),
         //----------------------------------------------------------------------------------
         contribuyente:        $('#Cliente_contribuyente_id option:selected').text(),
         cliente_tipo:         $('#Cliente_cliente_tipo_id option:selected').text()
      }
   }

   $('#btnActualizarCliente').live('click',function(){
     var cliente = _extractCliente( );
     cliente.id = $('#hd_cliente_id').val();
     $.ajax({
               url: base_url+ '/cliente/update',
               type: 'post',
               async: true,
               dataType: 'html',
               data: cliente,
               success:function(resp){
                 $('#clienteView').html(''); 
                 $('#clienteView').html(resp); 
               },
               error:function(){ alert('error al actualizar el cliente'); }
            });
   });

   $('#btnGuardarCliente').click(function(){
     $.ajax({
         url: base_url+ '/cliente/create',
         type: 'post',
         async: true,
         dataType: 'json',
         /**/
         data: _extractCliente( ),
         /**/
         //data: test,
         success:function(resp){
            var objJson = eval(resp);
            _mustache_cliente(objJson.view);
            $('#clienteForm').hide( );
            $('#pn_rif').html(objJson.principal.rif);
            $('#pn_razon_social').html(objJson.principal.razon_social);
            $('#lg_cliente_tipo').html( (objJson.view.cliente_tipo_id==1) ? 'Persona Natural':'Firma Juridica' );
            $('#lg_firmas').html( (objJson.view.cliente_tipo_id==1) ? 'Firmas Unipersonales':'Franquicias' );
         },
         error:function(){ alert('error al guardar el cliente en cliente/create'); }
      });
     //$('#yw0').tabs({ disabled:[2,3] });
     $('#yw0').tabs({ disabled:[] });
   });

   // Gestion de FIRMAS

   $('#btnFormFirma').click(function(){
      $('#firma_razon_social').val('');
      $('#firma_domicilio_fiscal').val('');
      $('#firma_telefonos').val('');
      $('#firma_registro').val('');
      $('#divformFirma').show( );
      $('#conBotonForm').hide( );
   });
   
   $('#btnCancelarFirma').click(function(){
      $('#error-firma_razon_social').addClass('hide');
      $('#error-firma_domicilio_fiscal').addClass('hide');
      $('#error-firma_registro').addClass('hide');
      $('#divformFirma').hide( );
      $('#conBotonForm').show( );
   });

   $('#btnGuardarFirma').click(function(){
      // agregar la firma
      var firma = {
                  cliente_id:           $.trim($('#hd_cliente_id').val()),
                  razon_social:         $.trim($('#firma_razon_social').val( )),
                  domicilio_fiscal:     $.trim($('#firma_domicilio_fiscal').val( )),
                  telefonos:            $.trim($('#firma_telefonos').val( )),
                  registro_de_comercio: $.trim($('#firma_registro').val( ))
                 };
   
      console.debug(firma);
 
      if (firmaValida(firma)) {  _ajaxGuardarFirma(firma);  }

      function _ajaxGuardarFirma(firma){
         $.ajax({
                  url: base_url+ '/cliente/addFirma',
                  type: 'post',
                  async: true,
                  dataType: 'json',
                  data: firma,
                  success: function(resp){
                     // retornar el id de la firma agregada
                     firma.firma_id = eval(resp);
                     // agregar el item a la tabla de firmas
                     var output  = $('#firmas');
                     $.Mustache.load(base_url+'/media/js/mustache-tmpl/cliente/item-firma.htm')
                      .fail(function(){ console.debug('fallo la carga del template mustache'); })
                      .done(function(){ output.mustache('item-firma',firma);});
                     // crear la plantilla de sucursales para la firma
                     mustache_sucursal(firma);
                     $('#divformFirma').hide( );
                     $('#conBotonForm').show( );
                     // activar la pestaña sucursal
                     if ($('#historia').html( )=='El cliente no tiene historia.') $('#yw0').tabs({ disabled:[4] });
                     //else $('#yw0').tabs({ disabled:[3] });
                  },
                  error:function(){ alert('error al cargar al servidor'); }
               });
      }
   });

});

function mustache_sucursal(firma) {
   // -----------------------------------------------------------------------
   var sucursal = {
                    firma               : firma.razon_social,
                    firma_id            : firma.firma_id,
                    tag                 : 'id-'+firma.firma_id,        //firma.razon_social.replace(/\s/g,''),
                    principal_domicilio : firma.domicilio_fiscal,
                    principal_telefono  : firma.telefonos
                  };
   var output  = $('#sucursales'); 
   if ($.trim(output.html())=='No hay sucursales.') output.html('');
   $.Mustache.load(base_url+'/media/js/mustache-tmpl/cliente/sucursal.htm')
   .fail(function(){ console.debug('fallo la carga del template mustache'); })
   .done(function(){ output.mustache('sucursal-box',sucursal);});
}

//----------------------------------------------------------
// historia de cliente
function show_orden(index) {
   $('#inf-orden_'+index).fadeIn('slow');
}

function close_orden(index) {
   $('#inf-orden_'+index).fadeOut('slow');
   //return false;
}

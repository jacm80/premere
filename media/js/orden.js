/// console.log() --> FIREBUG

//$('#Orden_firma_id').prop('disabled',false);   
var precios;

function getPrecios( )
{
   $.ajax({
            url: base_url+'/orden/getPrecios',
            async: true,
            dataType:'json',
            success:function(respuesta){ precios = eval(respuesta); },
            error:function(){ alert('fallo'); }
   });
}


/*
   .monto#monto_1 = .monto#cantidad_1 * .monto#precio_unitario_1
   .monto#monto_2 = .monto#cantidad_2 * .monto#precio_unitario_1
   .monto#monto_3 = .monto#cantidad_3 * .monto#precio_unitario_1
   base_imponible = sumatoria(.monto)
   iva = porc_iva * base_imponible
   total_pagar = base_imponible+iva
*/

function sumatoria( )
{
   var baseImponible=0;
   var iva=0;
   var totalPagar=0;

   _getMontoFilas( );
   
   iva = parseFloat(baseImponible*0.12).toFixed(2);
   baseImponible = baseImponible.toFixed(2);
   totalPagar = (parseFloat(baseImponible) + parseFloat(iva)).toFixed(2);
   //alert(totalPagar);   

   // resultados
   $('#baseImponible').html(baseImponible).end();
   $('#iva').html(iva).end();
   $('#iva').html(iva).end();
   $('#totalPagar').html(totalPagar).end();

   function _getMontoFilas( ){
      $('.monto').each(function(i){
         var n=0,cant=0,precio=0;
         n =  $(this).prop('id').split('_')[1];
         cant   = parseFloat ($('#cantidad_'+n).val( ) *1) .toFixed(2); 
         precio = parseFloat ($('#spanPrecio_'+n)  .html( )*1) .toFixed(2);
         //alert(precio);
         $('#spanMonto_'+n).html( cant*precio ).end( );
         $('#monto_'+n).val( cant*precio ).end( );
         baseImponible+=parseFloat($(this).html());
      });   
   }
}

//var vDescuento = precios[n]['precio_unitario'] * (parseFloat($('#descuento_id_'+item).val( ))/100);
function findPrecio(item,n)
{
   var IdDescuento = $('#descuento_id_'+item).val().split('-')[0];
   var vPorcDescuento = ($('#descuento_id_'+item).val().split('-')[1]/100).toFixed(2);
   var vPrecio    = parseFloat(precios[n]['precio_unitario']).toFixed(2);
   var vDescuento = (vPorcDescuento * vPrecio).toFixed(2); 
   //var vDescuento = vDescuento.toFixed(2);
   //console.debug(vPrecio);
   //console.debug(vDescuento);
   /////////////////////////////////////////////////////////////////////////////////////////////
   $('#spanPrecio_'+item).html(vPrecio-vDescuento);
   $('#precio_'+item).val(vPrecio-vDescuento);
   $('#documento_fiscal_id_'+item).val(precios[n]['documento_fiscal_id']);
   $('#cantidad_'+item).val(precios[n]['cantidad_minima']);  

   if (precios[n]['documento_fiscal_id']=='0')  $('#cnum-'+item).addClass('hide');   
   else $('#cnum-'+item).removeClass('hide');
}

function getNumeracion(item)
{
   //////////////////////////////////////////////////////////////////////////////
   // Comprobar si hay numeraciones en el detalle
   var finControl;
   $('.numeracion').each(function(i,elem){
      if ($(elem).prop('value')!='') {
         //console.debug(i+':'+$(elem).prop('id'));
         finControl = $(elem).prop('value');
      }
   });
   console.log(finControl);
   //////////////////////////////////////////////////////////////////////////////
   $.ajax({
            url: base_url+'/orden/getNumeracion',
            type:'post',
            async: true,
            data: { 
                     //documento_fiscal_id : precios[$('#producto_id_'+item) .val()]['documento_fiscal_id'],
                     documento_fiscal_id : $('#producto_id_'+item)         .val(),
                     cliente_id          : $('#Orden_cliente_id')          .val(),
                     serie               : $('#serie_'+item)               .val(),
                     identificador       : $('#identificador_'+item)       .val(),
                     juegos              : $('#juegos_'+item)              .val(),
                     cantidad            : $('#cantidad_'+item)            .val(),
                     ultimo_control      : finControl
                  },   
            dataType:'json',
            success:function(respuesta){
               var numeracion = eval(respuesta); 
               $('#inicio_numero_' +item).val(numeracion['inicio_numero' ]);
               $('#fin_numero_'    +item).val(numeracion['fin_numero'    ]);
               $('#inicio_control_'+item).val(numeracion['inicio_control']);
               $('#fin_control_'   +item).val(numeracion['fin_control'   ]);
            },
            error:function(){ alert('fallo'); }
   });
}

function hide(elem) {
   var item = elem.id.split('-')[1];
   $('#cont_num-'+item).fadeToggle();
   $('#cnumBoxHead-'+item).fadeToggle();
}

function getSedes( )
{
   $.ajax({
            url: base_url+'/orden/getsedes',
            type:'get',
            async: true,
            data: { id : $('#Orden_firma_id').val() },   
            dataType:'html',
            success:function(respuesta){ 
              $('#div_sede').html(respuesta).end(); 
            },
            error:function(){ alert('fallo'); }
      });
}


// NO ES EL AUTOCOMPLETAR OJO
function getCliente(){
   $.ajax({
            url: base_url+'/orden/getfirmas',
            type:'get',
            async: true,
            data: { id : $('#Orden_cliente_id').val() },   
            dataType:'html',
            success:function(respuesta){ 
              $('#div_firma').html(respuesta).end(); 
              $('#div_sede').html('[SELECCIONE LA FIRMA]').end(); 
            },
            error:function(){ alert('fallo'); }
   });
}

function toggle_banco(event) {
   var el = event.target;
   //#pago_forma_pago:selected option:selected
   var selector = '#'+$(el).prop('id') +' option:selected';    
   var textoFormaPago = $(selector).text();
   //
   // activar para escribir el numero de comprobante
   //
   if (textoFormaPago.charAt(0)=='*'){
      $('#Pago_numero_comprobante').removeClass('hide');
   }
   else{
      $('#Pago_numero_comprobante').addClass('hide');
   }
   // activar para seleccionar el banco
   if ( $.inArray($(el).prop('value'),['2','3','4','6','7'])>-1  ) {
      $('#pagoBancoId').removeClass('hide');
   }
   else {
      $('#pagoBancoId').addClass('hide');
   }
}

$(document).ready(function( ){
  

   getPrecios( );
   //var i=1;

   $('#pagoFormaPagoId').change(toggle_banco);

   $('#BtnAgregar').click(function(e){
      e.preventDefault();
      $.ajax({
            url: base_url+'/orden/additem',
            type:'post',
            async: true,
            data: { item : i },   
            dataType:'html',
            success:function(respuesta){
               $('#detalle').before(respuesta);
               i++;
             },
            error:function(){ alert('fallo'); }
            });
   });

      /*
      $.ajax({
            url: base_url+'/orden/getfirmas',
            type:'get',
            async: true,
            data: { id : $('#Orden_cliente_id').val() },   
            dataType:'html',
            success:function(respuesta){ 
              $('#div_firma').html(respuesta).end(); 
              $('#div_sede').html('[SELECCIONE LA FIRMA]').end(); 
            },
            error:function(){ alert('fallo'); }
       });

      */

   $('.ico-delete').live('click',function(event){
      if (confirm('Esta realmente seguro de eliminar el item?')){   
         var item = this.id.split('_')[1];   
         if ($('#factura_item_id-'+item))
         {
            if (!isNewRecord)
            {
               $.ajax({
                        url: base_url+'/orden/eliminar_detalle', 
                        type: 'post',
                        data: { id :$('#factura_item_id-'+item).prop('value') },
                        type:'post',
                        async: true,
                        success:function(resp){ 
                           /*alert('Listo')*/
                           obj = eval ( '(' + resp + ')' );
                           //console.info(obj);
                           $('#baseImponible').text(obj['baseImponible']);
                           $('#iva').text(obj['iva']); 
                           $('#totalPagar').text(obj['totalPagar']); 
                        },
                        error: function(){ }
                     });
            }
         }
         $('#tr_'+item).remove( );
         sumatoria();
      }
   });

   /*
   $('#pruebaTab').click(function(){
      //$( "#yw1" ).tabs('option','disabled',[1]);
      $("#yw1").tabs('option','disabled',[]);
   }); */

   /*
   * TODO no borrar
   $('input,textarea').blur(function(){
      // campos requeridos
      var requiredFields = [
                           '#Orden_cliente_id',
                           '#Orden_fecha_entrega',
                           '#Orden_direcciones_incluidas',
                           '#Orden_telefonos_incluidos',
                           '#Orden_abono'
                           ];
      var canOpen=true;
      $(requiredFields).each(function(i,sel){
         if ($(sel).val( ) == '') { canOpen=false; return false; }
      });
      if (canOpen) $("#yw1").tabs('option','disabled',[]);
   });
   */

   $('input').live('keypress',function(event){
      var elem = $(event.target);
      if (!(elem.prop('id').match(/inicio_(\w+)_(\d+)/))) return;
      else {
         if (event.which==13){
            var ref = elem.prop('id').split('_')[1];
            var item = elem.prop('id').split('_')[2];
            var inicio = parseInt(elem.prop('value'));
            var juegos = parseInt($('#juegos_'+item).prop('value'));
            var cant   = parseInt($('#cantidad_'+item).prop('value'));
            var fin    = (inicio + (juegos*cant)) -1;
            $('#fin_'+ref+'_'+item).val(fin);
         }
      }
   });

   
   $('#BtnListado').click(function(e){
      e.preventDefault();
      location.href = base_url+'/orden/index';
   });

   $('#BtnReimprimir').live('click',function(e){
      e.preventDefault();
      location.href = base_url+'/orden/imprimir?id='+$('#Orden_id').val( );
   });

   $('#BtnGuardar').click(function(){
      if (validate()) alert('listo');
      //$('#OrdenForm').submit( );
   });

});


function changeColorRow(item)
{
   if (($('#serie_'+item).prop('value')!='0') && ($('#producto_'+item).prop('value') !='0'))
   {
      $('#tr_'+item).attr('style','background-color: white');
   }
}


function _chk_productos( ){
   
   var retorno=true;
   
   $('.productos').each(function(i,elem){
      var row = $(elem).prop('id').split('_')[2];
      if ($(elem).prop('value')=='0') {
         alert('El producto no ha sido seleccionado');
         $('#tr_'+row).attr('style','background-color: #FCF7CA');
         retorno=false;
         return false;
      }
      if ($('#serie_'+row).prop('value')=='0'){
         alert('La serie no ha sido seleccionado');
         $('#tr_'+row).attr('style','background-color: #FCF7CA');
         $('#cont_num-'+row).fadeIn();
         $('#cnumBoxHead-'+row).fadeIn();
         retorno=false;
         return false;
      }
   });
   return retorno;
}


function validate( ){
   if (i<=1){
      alert('La orden no tiene productos asignados');
      return false;
   }
   else if (!_chk_productos()) return false;
   else return true;
}

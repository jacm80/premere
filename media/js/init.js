////////////////////////////////////////////////////////////////////////////
//       Variable generales para compartirla por toda la aplicacion

////////////////////////////////////////////////////////////////////////////
var l = window.location;
var base_url = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1];

function zeropad (str, max) {
  return str.length < max ? zeropad("0" + str, max) : str;
}

function toggle_banco(event) {
   var el = event.target;
   //#pago_forma_pago:selected option:selected
   var selector = '#'+$(el).prop('id') +' option:selected';    
   var textoFormaPago = $(selector).text();

   //
   // activar para escribir el numero de comprobante
   //
   if (textoFormaPago.charAt(0)=='*'){
      $('#Pago_numero_comprobante').removeClass('hide');
   }
   else{
      $('#Pago_numero_comprobante').addClass('hide');
   }
   // activar para seleccionar el banco
   if ( $.inArray($(el).prop('value'),['2','3','4','6','7'])>-1  ) {
      $('#Pago_banco_id').removeClass('hide');
   }
   else {
      $('#Pago_banco_id').addClass('hide');
   }
}

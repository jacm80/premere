function calcularUnidad(id){
   var u_sistema = parseInt($('#unidad_sistema-'+id).html());
   var u_real    = parseInt($('#existencia_unidad-'+id).val( ));
   console.debug('sistema: '+u_sistema);
   console.debug('real:'+u_real);
   var diff = -(u_sistema-u_real);
   console.debug('diff:'+diff);
   $('#diferencia_unidad-'+id).html(diff);
   if (diff<0) $('#diferencia_unidad-'+id).addClass('rojo');
}

function calcularDetallado(id){
   var d_sistema = parseInt($('#detallado_sistema-'+id).html());
   var d_real    = parseInt($('#existencia_detallado-'+id).val( ));
   console.debug('sistema: '+d_sistema);
   console.debug('real:'+d_real);
   var diff = -(d_sistema-d_real);
   console.debug('diff:'+diff);
   $('#diferencia_detallado-'+id).html(diff);
   if (diff<0) $('#diferencia_detallado-'+id).addClass('rojo');
}

$(document).ready(function(){
   var cbAlmacen = $('#MaterialConciliacion_almacen_id');
   cbAlmacen.change(function(){
      if (cbAlmacen.val()>0)
      {
         $.ajax({
            url: base_url+'/conciliar/GetMateriales',
            type:'post',
            async:true,
            dataType:'html',
            data: { almacen_id : cbAlmacen.val( ) },
            success:function(resp){   
               $('#viewMateriales').html('');
               $('#viewMateriales').html(resp)
               .fadeOut('fast')
               .fadeIn('fast');
            },
            error:function(){ alert('error en getMateriales'); }
         });
      }
   });

   $('#BtnGuardar').click(function(){
      $('#conciliacionForm').submit( );
   });
   
   $('#BtnListado').click(function(){
      location.href = base_url+'/conciliar';
   });

});

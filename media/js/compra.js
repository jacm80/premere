/*
 *
 * */

var _inicial = [];
var iva = 0.12;
var cpago=1;

// ocultar los campos sengun la forma de pago
function toggle_banco(event) {
   var el = event.target;
   //#pago_forma_pago:selected option:selected
   var selector = '#'+$(el).prop('id') +' option:selected';
   var index = $(el).prop('id').split('-')[1];   
   var textoFormaPago = $(selector).text();
   //
   // activar para escribir el numero de comprobante
   //
   if (textoFormaPago.charAt(0)=='*'){
       $('#pago_numero_comprobante-'+index).removeClass('hide');
   }
   else {
      $('#pago_numero_comprobante-'+index).addClass('hide');
   }
   // activar para seleccionar el banco
   if ( $.inArray($(el).prop('value'),['2','3','4','6','7'])>-1  ) {
      $('#pago_banco_id-'+index).removeClass('hide');
   }
   else {
      $('#pago_banco_id-'+index).addClass('hide');
   }
}

// colocar la cantidad total a cancelar cuando la opcion sea cancelacion
function chk_cancelar(tipo,id){
   
   var index = id.split('-')[1];
   var monto_cancelado = 0;   
   var pago_monto = 0;

   if (tipo=='Cancelacion'){
      $('.montos').each(function(i,elem){ 
         if ($(elem).prop('id')!=id) monto_cancelado += $(elem).prop('value');
      });
      pago_monto = parseFloat($('#totalPagar').html()) - monto_cancelado;
      $('#pago_monto-'+index).val(pago_monto);
   }
}


function eliminarItem(xid){
   if (confirm('Desea eliminar el item?')){
      $('#material-'+xid+'-sup').remove();
      $('#material-'+xid+'-inf').remove();
      _calcularTotales( );
   }
}


function calcularIvaxItem(xid){
   var total_item = 0.0;
   var cantidad = parseInt($('#cantidad-'+xid).val( )); 
   var precio_u = parseFloat($('#precio_unitario-'+xid).val( ));
   // incluye iva
   if ($('#incluye_iva-'+xid).is(':checked')){
      precio_u = Math.round((precio_u / 1.12));
      $('#precio_unitario-'+xid).val(precio_u);
      var iva_item = (precio_u * iva) * cantidad;
      //alert("esta checked");
   }
   // NO incluye iva
   else {
      var iva_item = (precio_u * iva) * cantidad;
      //alert("esta no checked");
   }
   iva_item   = Math.round(iva_item*100)/100;
   precio_u   = Math.round(precio_u*100)/100;
   total_item = Math.round(parseFloat(iva_item)+parseFloat(precio_u*cantidad)*100)/100;
   $('#iva-'+xid).val(iva_item);
   $('#total_item-'+xid).html(total_item);
   _calcularTotales();
}


function _calcularTotales( ){
   var baseImponible=0;
   var montoIva=0;
   $('.precio_unitario').each(function(i,elem){
      if ($(elem).prop('value')!='') {
         baseImponible += (parseFloat($(elem).prop('value')));
         ////////////////////////////////////////////////////
         var id = $(elem).prop('id').split('-')[1];
         montoIva += parseFloat($('#iva-'+id).val( )); 
         ////////////////////////////////////////////////////
      }
   });
   montoIva = (Math.round(montoIva*100)/100).toFixed(2);
   baseImponible  = Math.round(baseImponible*100)/100;
   baseImponible  = baseImponible.toFixed(2);
   var totalPagar = (Math.round((parseFloat(baseImponible)+parseFloat(montoIva))*100)/100).toFixed(2);
   $('#hiddenIva')          .val(montoIva);
   $('#hiddenBaseImponible').val(baseImponible);
   $('#baseImponible')      .html(baseImponible);
   $('#montoIva')           .html(montoIva);
   $('#totalPagar')         .html(totalPagar);
}

function updateGrid(_ids) {
   $.ajax({
         url: base_url+'/compra/setMateriales',
         type:'get',
         async: true,
         dataType: 'html',
         data: { ids : _ids /*.join()*/ },
         success: function(response){ 
                                       var materiales = eval(response);
                                       $.each(materiales,function(index,item) {
                                          if ($('#material-'+item.id).length>0) { }
                                          //else $('#detalle').before(_createRow(item)); 
                                          else {
                                             var tmpData = {
                                                            'id':item.id,
                                                            'material':item.material,
                                                            'barcode':item.barcode, 
                                                            'PATH_SERVER':base_url,
                                                            'almacenes': _inicial['almacenes']
                                                            };
                                             var output  = $('#detalle-compra-body');
                                             $.Mustache.load(base_url+'/media/js/mustache-tmpl/compra/item.htm')
                                                .fail(function(){ console.debug('fallo la carga del template mustache'); })
                                                .done(function(){ output.mustache('item-compra',tmpData);});
                                          }
                                          //console.debug(item.material);
                                       });
                                       //console.debug(_ids);
                                    },
         error:   function(){ alert('error'); }
   });
}


$(document).ready(function(){
	
   $.Mustache.options.warnOnMissingTemplates = true;
	$.Mustache.addFromDom();
   
   // inicializar los valores para la interfaz
   $.ajax({
         url: base_url+'/compra/inicializar',
         type:'get',
         async: true,
         dataType: 'json',
         success: function(response){
            var objJSON = eval(response);
            _inicial = objJSON;
         } 
   });

   // agregar pago
   $('#agregarPago').click(function( ){
      var tmpData = {
                  num: cpago,
                  forma_pago: _inicial['formas_pagos'],
                  fecha: '01/01/2013',
                  bancos: _inicial['bancos'],
                  user:   _inicial['user'],
                  PATH_SERVER: base_url
                 };
      var output  = $('#cpagos');
      $.Mustache.load(base_url+'/media/js/mustache-tmpl/compra/pago.htm')
      .fail(function(){ console.debug('fallo la carga del template mustache'); })
      .done(function(){ output.mustache('item-pago',tmpData);});
      cpago++;
   });
   
   // agregar los items de la compra
   $('#agregar').click(function(){
   
         $.ajax({
            url: base_url + '/compra/getMaterialesProveedor',
            type:'post',
            async: true,
            dataType: 'html',
            data: { proveedor_id: $('#Compra_proveedor_id').val() },
            success: function(response){ $('#dialog').html(response); },
            error:   function(){ alert('error'); }
         });

         $('#dialog').dialog({
                              modal: true,
                              title: 'Lista de Materiales',  
                              width: 800, height: 500,
                              buttons:{ 
                                       'Buscar':function( ){
                                                            $.ajax({
                                                                     url: base_url + '/compra/getCatalogoMateriales',
                                                                     type:'get',
                                                                     async: true,
                                                                     dataType: 'html',
                                                                     data: $('#materialForm').serialize(),
                                                                     success: function(response){ $('#dialog').html(response); },
                                                                     error:   function(){ alert('error'); }
                                                                  });
                                       },
                                       'Seleccionar': function( ) {
                                          var _ids = [ ];
                                          $('input[type=checkbox]').each(function(i){
                                             if (this.checked == true) {
                                                _ids.push(this.value);
                                             }
                                          });
                                          updateGrid(_ids);
                                          $(this).dialog("close");
                                        },
                                       'cancelar': function(){ $(this).dialog("close"); } 
                              }
                           });
   });


   // registrar la compra
   $('#guardarCompra').click(function(){
      $('#compraForm').submit( );
   });

   $('#listarCompra').click(function(){
      location.href = base_url+'/compra/index';
   });

});

function calc_total( ){
   if ($.isNumeric($('#Gasto_monto').val())){

      var porc_iva = ($('#incluye_iva').attr('checked')=='checked') ? parseFloat($('#incluye_iva').val()) : 0.0;
      
      var monto = parseFloat($('#Gasto_monto').val( ));
      var iva   = monto * porc_iva;
      var total = iva + monto;
      
      $('#Gasto_monto').val(monto.toFixed(2));
      $('#Gasto_iva').val(iva.toFixed(2));
      $('#total_gasto').html(total.toFixed(2));
   }
}

function toggle_banco(event) {
   var el = event.target;
   //#pago_forma_pago:selected option:selected
   var selector = '#'+$(el).prop('id') +' option:selected';    
   var textoFormaPago = $(selector).text();
   //
   // activar para escribir el numero de comprobante
   //
   if (textoFormaPago.charAt(0)=='*'){
      $('#Gasto_numero_comprobante').removeClass('hide');
   }
   else{
      $('#Gasto_numero_comprobante').addClass('hide');
   }
   // activar para seleccionar el banco
   if ( $.inArray($(el).prop('value'),['2','3','4','6','7'])>-1  ) {
      $('#Gasto_banco_id').removeClass('hide');
   }
   else {
      $('#Gasto_banco_id').addClass('hide');
   }
}

$(document).ready(function(){      

   $('#Gasto_monto').change(calc_total);

   $('#incluye_iva').change(calc_total);

   $('#Gasto_forma_pago_id').change(toggle_banco);

   $('#BtnGuardarGasto').click(function(){
      $('#gasto-form').submit( );
   });
   
   $('#BtnListado').click(function(){
      location.href = base_url+'/gasto';
   });
   
   $('#BtnNuevo').click(function(){
      location.href = base_url+'/gasto/create';
   });

});

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset; ?>" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/gridview.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/detailview.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pager.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/navi.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tabs.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/notificacion.css" />
   
   <?php HtmlApp::loadjQuery( ) ;?><?php #HtmlApp::loadjQuery( ) ;?>
   <?php HtmlApp::loadJs('init') ;?>
</head>

<body>
<div class="container" id="page">

	<div id="header" style="/*border: 1px solid black;*/">
      <!-- <div id="logo"></div> -->
      <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/banner_litimun.png"/>
	</div>
   <!-- header -->

	<div id="topbar">
      <div id="topnav">
         <ul id="topnav">
         <?php 
           //include(Yii::app()->user->getRole().'.php'):
            /**/
            if (Yii::app()->user->checkAccess('admin'))
               $userMenu = include('ACL/admin.php'); 
            else if (Yii::app()->user->checkAccess('vendedor'))
               $userMenu = include('ACL/vendedor.php'); 
            else if (Yii::app()->user->checkAccess('maquetador'))
               $userMenu = include('ACL/maquetador.php'); 
            else if (Yii::app()->user->checkAccess('cliente'))
               $userMenu = include('ACL/cliente.php'); 
            else $userMenu = include('ACL/guest.php'); 
            /**/
         ?>
		   <?php $this->widget('zii.widgets.CMenu',array('items'=>$userMenu)); ?>	   
         </ul>
      </div>
      <br class="clear" />
   </div>
   <!-- mainmenu -->
   
   <!-- breadcrumbs -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?>
	<?php endif; ?>
   <!-- End breadcrumbs -->

   <!-- flash messages -->
   <div class="info" style="text-align:left;">
      <?php $flashMessages = Yii::app()->user->getFlashes( );
      if ($flashMessages)
      {
        // echo '<ul class="flashes">';   
         foreach($flashMessages as $key=>$msg)
         {
            echo '<div class="flash-'.$key.'">'.$msg.'</div>';
         }  
         //echo '</ul>';
      }
      ?>
   </div>
   <!-- End flash messages -->

	<?php echo $content; ?>

   <div class="clear"></div>

   <!-- -->
   <div id="content-notificacion">
      <div id="content-notificacion-buttons">
    	   <span id="notificacion-numbers">Tiene (2) Mensaje(s)</span>
    	   <span id="maximize" onclick="$('#notificacion').slideDown('slow');"></span>
         <span id="minimize" onclick="$('#notificacion').slideUp('slow');"></span>        
      </div>
	   <div id="notificacion">
         <p id="notificacion-item" class="row">
            <span class="title">Bienvenido a Premere<?php #echo $n['titulo']; ?></span>
   			<span class="article">Te gustara la experiencia<?php #echo $n['descripcion']; ?></span>
         </p>
         <p id="notificacion-item" class="row">
            <span class="title">Puedes empezar viendo el menu</span>
   			<span class="article">Lorem ipsum dolor sit amet, consectetuer 
            adipiscing elit, sed diam nonummy nibh euismod tincidunt 
            ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad 
            minim veniam, quis nostrud exerci tation ullamcorper suscipit 
            lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel 
            eum iriure dolor in hendrerit in vulputate velit esse molestie 
            consequat, vel illum dolore eu feugiat nulla facilisis</span>
         </p>
      </div>
   </div>
   <!-- -->

   <div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> Premere System by CodeBox c.a.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->
</div><!-- page -->
<?php echo Yii::app()->user->ui->displayErrorConsole(); ?>
</body>
</html>
<?php
   Yii::app()->clientScript->registerScript(
      'myHideEffect',
      '$(".info").animate({opacity:1.0},6000).slideUp("slow");',
      CClientScript::POS_READY
   );
?>

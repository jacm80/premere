<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo Yii::app()->language; ?>" lang="<?php echo Yii::app()->language; ?>">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=<?php echo Yii::app()->charset; ?>" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/form.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/gridview.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/detailview.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/pager.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/navi.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/tabs.css" />
   <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/notificacion.css" />
   
   <?php HtmlApp::loadjQuery( ) ;?><?php HtmlApp::loadjQuery( ) ;?>
   <?php HtmlApp::loadJs('init') ;?>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo" style="text-align:right;"><?php echo CHtml::image(Yii::app()->baseUrl."/media/images/premereSystem.jpg"); ?></div>
	</div><!-- header -->

	<div id="topbar">
      <div id="topnav">
      <ul id="topnav">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>Yii::t('app','Home'),'url'=>array('/site/index')),
				//array('label'=>Yii::t('app','About'),'url'=>array('/site/page', 'view'=>'about')),
            array('label'=>Yii::t('app','File'),'url'=>'#','items'=>array(
                     array('label'=>'Clientes','url'=>Yii::app()->request->baseUrl . '/cliente'),
                     array('label'=>'Conceptos de Gastos','url'=>Yii::app()->request->baseUrl.'/GastoConcepto'),
                     )
                  ),
            array('label'=>'Inventario','url'=>'#','items'=>array(
                        array('label'=>'Conciliar','url'=>Yii::app()->request->baseUrl.'/conciliar/create'),
                        array('label'=>'Productos/Manufactura','url'=>Yii::app()->request->baseUrl.'/producto'),
                        array('label'=>'Material/Insumos','url'=>Yii::app()->request->baseUrl.'/material'),
                        array('label'=>'Proveedor','url'=>Yii::app()->request->baseUrl.'/proveedor'),
                        array('label'=>'Catalogo de Material','url'=>Yii::app()->request->baseUrl.'/CatalogoMaterial'), 
                        /*
                        array('label'=>'Catalogo de Precios x Proveedor','url'=>Yii::app()->request->baseUrl.'/catalogoProveedor/create'),
                        */
                        array('label'=>'Marcas','url'=>Yii::app()->request->baseUrl.'/marca'), 
                        array('label'=>'Medidas/Unidades','url'=>Yii::app()->request->baseUrl.'/unidad'), 
                        )),

            array('label'=>'Procesos','url'=>'#','items'=>array(
                                                               array('label'=>'Orden de Trabajo','url'=>Yii::app()->request->baseUrl.'/orden/create'),
                                                               array('label'=>'Facturar','url'=>Yii::app()->request->baseUrl.'/factura'),
                                                               array('label'=>'Ajustar Factura','url'=>Yii::app()->request->baseUrl.'/ajusteFactura'),
                                                               array('label'=>'Salida de Material','url'=>Yii::app()->request->baseUrl.'/materialSalida/create'),
                                                               array('label'=>'Ordenes Pendientes','url'=>Yii::app()->request->baseUrl.'/buzon/pendiente'),
                                                               array('label'=>'Registrar Gasto','url'=>Yii::app()->request->baseUrl.'/gasto/create'),
                                                               array('label'=>'Registrar Compra','url'=>Yii::app()->request->baseUrl.'/compra/create'),
                                                               array('label'=>'Generar Declacion SENIAT','url'=>Yii::app()->request->baseUrl.'/declaracion'),
                                                               )),
            array('label'=>'Consulta','url'=>'#','items'=>array(
                                                               array('label'=>'Consultar Numeracion','url'=>Yii::app()->request->baseUrl.'/consulta/numeracion'),
                                                               array('label'=>'Cuadre de Caja','url'=>Yii::app()->request->baseUrl.'/cuadre'),
                                                               )),
            array('label'=>'Administrar','url'=>'#','items'=>array(
                        array('label'=>'Administrar Usuarios','url'=>Yii::app()->user->ui->userManagementAdminUrl, 'visible'=>!Yii::app()->user->isGuest),
                                                               array('label'=>'Respaldos','url'=>Yii::app()->request->baseUrl.'/respaldo'),
                                                               array('label'=>'Auditoria','url'=>Yii::app()->request->baseUrl.'/auditoria'),
                                                               )),
            /*
            array('label'=>'Contabilidad','url'=>'#','items'=>array(
                                                                     array('label'=>'Catalogo Contable','url'=>Yii::app()->request->baseUrl.'/catalogoContable'),
                                                                     array('label'=>'Plan Unico de Cuentas','url'=>Yii::app()->request->baseUrl.'/planContable'),
                                                                     array('label'=>'Registrar Asiento','url'=>Yii::app()->request->baseUrl.'/planContable'),
                                                                     array('label'=>'Registrar Apertura','url'=>Yii::app()->request->baseUrl.'/planContable'),
                                                                     array('label'=>'Balance General','url'=>Yii::app()->request->baseUrl.'/balanceComprobacion'),
                                                                     array('label'=>'Cierre Fiscal','url'=>Yii::app()->request->baseUrl.'/cierreFiscal'),
                                                                    )),
            */
            array('label'=>Yii::t('app','Contact'), 'url'=>array('/site/contact')),
            
            array('label'=>'Login', 'url'=>Yii::app()->user->ui->loginUrl, 'visible'=>Yii::app()->user->isGuest),
            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>Yii::app()->user->ui->logoutUrl, 'visible'=>!Yii::app()->user->isGuest)
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	   </ul>
      </div>
      <br class="clear" />
      </div>
   <!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif; ?>

	<?php echo $content; ?>

   <div class="clear"></div>

   <div id="content-notificacion">
      <div id="content-notificacion-buttons">
    	   <span id="notificacion-numbers">Tiene (2) Mensaje(s)</span>
    	   <span id="maximize" onclick="$('#notificacion').slideDown('slow');"></span>
         <span id="minimize" onclick="$('#notificacion').slideUp('slow');"></span>        
      </div>
	   <div id="notificacion">
         <p id="notificacion-item" class="row">
            <span class="title">Bienvenido a Premere<?php #echo $n['titulo']; ?></span>
   			<span class="article">Te gustara la experiencia<?php #echo $n['descripcion']; ?></span>
         </p>
         <p id="notificacion-item" class="row">
            <span class="title">Puedes empezar viendo el menu</span>
   			<span class="article">Lorem ipsum dolor sit amet, consectetuer 
            adipiscing elit, sed diam nonummy nibh euismod tincidunt 
            ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad 
            minim veniam, quis nostrud exerci tation ullamcorper suscipit 
            lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel 
            eum iriure dolor in hendrerit in vulputate velit esse molestie 
            consequat, vel illum dolore eu feugiat nulla facilisis</span>
         </p>
      </div>
   </div>

   <div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> Premere System by CodeBox c.a.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->
</div><!-- page -->
<?php echo Yii::app()->user->ui->displayErrorConsole(); ?>
</body>
</html>

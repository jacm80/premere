<?php $this->beginContent('//layouts/main'); ?>
   
   <?php if (Yii::app()->user->hasFlash('success')):?>
      <div class="flash-notice">
         <?php echo Yii::app()->user->getFlash('success')?>
      </div>
   <?php endif?>
   <?php if (Yii::app()->user->hasFlash('error')):?>
      <div class="flash-error">
         <?php echo Yii::app()->user->getFlash('error')?>
      </div>
   <?php endif?>

<div class="span-19">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Operaciones',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>

<?php
   return array(
				array('label'=>Yii::t('app','Home'),'url'=>array('/site/index')),
				array('label'=>'Buzon','url'=>array('/buzon')),

            array('label'=>Yii::t('app','File'),'url'=>'#','items'=>array(
                  array('label'=>'Clientes','url'=>Yii::app()->request->baseUrl . '/cliente'),
                  )),

            array('label'=>'Procesos','url'=>'#','items'=>array(
                  array('label'=>'Generar Declacion SENIAT','url'=>Yii::app()->request->baseUrl.'/declaracion'),
                  )),

            array('label'=>'Consulta','url'=>'#','items'=>array(
                  array('label'=>'Consultar Numeracion','url'=>Yii::app()->request->baseUrl.'/consulta/numeracion'),
                  )),


            array('label'=>'Login', 'url'=>Yii::app()->user->ui->loginUrl, 'visible'=>Yii::app()->user->isGuest),

            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>Yii::app()->user->ui->logoutUrl, 'visible'=>!Yii::app()->user->isGuest)
			);
?>

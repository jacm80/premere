<?php
   return array(
				array('label'=>Yii::t('app','Home'),'url'=>array('/site/index')),
				array('label'=>'Buzon','url'=>array('/buzon')),

            array('label'=>Yii::t('app','File'),'url'=>'#','items'=>array(
                  array('label'=>'Clientes','url'=>Yii::app()->request->baseUrl . '/cliente'),
                  )),

            array('label'=>'Inventario','url'=>'#','items'=>array(
                  array('label'=>'Conciliar','url'=>Yii::app()->request->baseUrl.'/conciliar/create'),
                  array('label'=>'Productos/Manufactura','url'=>Yii::app()->request->baseUrl.'/producto'),
                  array('label'=>'Material/Insumos','url'=>Yii::app()->request->baseUrl.'/material'),
                  array('label'=>'Proveedor','url'=>Yii::app()->request->baseUrl.'/proveedor'),
                  array('label'=>'Catalogo de Material','url'=>Yii::app()->request->baseUrl.'/CatalogoMaterial'), 
                  array('label'=>'Marcas','url'=>Yii::app()->request->baseUrl.'/marca'), 
                  array('label'=>'Medidas/Unidades','url'=>Yii::app()->request->baseUrl.'/unidad'), 
                  )),

            array('label'=>'Procesos','url'=>'#','items'=>array(
                  array('label'=>'Orden de Trabajo','url'=>Yii::app()->request->baseUrl.'/orden/create'),
                  array('label'=>'Facturar','url'=>Yii::app()->request->baseUrl.'/factura'),
                  array('label'=>'Ajustar Factura','url'=>Yii::app()->request->baseUrl.'/ajusteFactura'),
                  array('label'=>'Salida de Material','url'=>Yii::app()->request->baseUrl.'/materialSalida/create'),
                  array('label'=>'Ordenes Pendientes','url'=>Yii::app()->request->baseUrl.'/buzon/pendiente'),
                  array('label'=>'Registrar Gasto','url'=>Yii::app()->request->baseUrl.'/gasto/create'),
                  array('label'=>'Registrar Compra','url'=>Yii::app()->request->baseUrl.'/compra/create'),
                  array('label'=>'Generar Declacion SENIAT','url'=>Yii::app()->request->baseUrl.'/declaracion'),
                  )),

            array('label'=>'Consulta','url'=>'#','items'=>array(
                  array('label'=>'Consultar Numeracion','url'=>Yii::app()->request->baseUrl.'/consulta/numeracion'),
                  )),


            array('label'=>'Login', 'url'=>Yii::app()->user->ui->loginUrl, 'visible'=>Yii::app()->user->isGuest),

            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>Yii::app()->user->ui->logoutUrl, 'visible'=>!Yii::app()->user->isGuest)
			);
?>

<?php
   return array(
				array('label'=>Yii::t('app','Home'),'url'=>array('/site/index')), 
				array('label'=>'Catalogo','url'=>array('/site/catalogo')), 
            array('label'=>Yii::t('app','Contact'), 'url'=>array('/site/contact')),
            array('label'=>'Login', 'url'=>Yii::app()->user->ui->loginUrl, 'visible'=>Yii::app()->user->isGuest),
            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>Yii::app()->user->ui->logoutUrl, 'visible'=>!Yii::app()->user->isGuest),
			);
?>

<?php

   class DeclaracionController extends Controller
   {

      private $meses;

      public function filters()
	   {
		   return array(array('CrugeAccessControlFilter'));
	   }

      public function init( )
      {
         $this->meses = array(
                          '0'=> array( 'inicio'=>(date('Y')-1).'-12-01','fin'=>(date('Y')-1).'-12-30'  ),
                          '1'=> array( 'inicio'=> date('Y').'-01-01', 'fin'=>date('Y').'-01-30' ),
                          '2'=> array( 'inicio'=> date('Y').'-02-01', 'fin'=>date('Y').'-02-30' ),
                          '3'=> array( 'inicio'=> date('Y').'-03-01', 'fin'=>date('Y').'-03-30' ),
                          '4'=> array( 'inicio'=> date('Y').'-04-01', 'fin'=>date('Y').'-04-30' ),
                          '5'=> array( 'inicio'=> date('Y').'-05-01', 'fin'=>date('Y').'-05-30' ),
                          '6'=> array( 'inicio'=> date('Y').'-06-01', 'fin'=>date('Y').'-06-30' ),
                          '7'=> array( 'inicio'=> date('Y').'-07-01', 'fin'=>date('Y').'-07-30' ),
                          '8'=> array( 'inicio'=> date('Y').'-08-01', 'fin'=>date('Y').'-08-30' ),
                          '9'=> array( 'inicio'=> date('Y').'-09-01', 'fin'=>date('Y').'-09-30' ),
                         '10'=> array( 'inicio'=> date('Y').'-10-01', 'fin'=>date('Y').'-10-30' ),
                         '11'=> array( 'inicio'=> date('Y').'-11-01', 'fin'=>date('Y').'-11-30' ),
                         '12'=> array( 'inicio'=> date('Y').'-12-01', 'fin'=>date('Y').'-12-30' ),
                        );
      }

      public function actionIndex()
	   {
         //echo HtmlApp::debug($this->meses);
         $this->render('index');
	   }

      private function _search($mes)
      {
         $criteria = new CDbCriteria;
         $criteria->addInCondition('documento_fiscal_id',array(1,2,3,4,5));
         $criteria->addBetweenCondition('fecha_impresion',$this->meses[$mes]['inicio']/*'2012-10-01'*/,$this->meses[$mes]['fin']/*'2012-10-30'*/);
         $ventas = DocumentoControl::model()->findAll($criteria);
         return $ventas;
      }

      public function actionXml()
      {
         $aryVentas = array();
         extract($_POST);
         $ventas = $this->_search($periodo);        
         $rif_empresa = 'G200068868';         
         /////////////////////////////////////////////////////////////////////////////
         $i=0;
         foreach($ventas as $v)
         {
            $aryVentas[$i] = array(
                                    'rif'=>$v->factura->orden->cliente->rif,
                                    'documento_fiscal_id'=> $v->documento_fiscal_id,
                                    'fecha_elaboracion'  => $v->fecha_impresion,
                                    'identificador'      => $v->identificador,
                                    'inicio_control'     => $v->inicio_control,
                                    'fin_control'        => $v->fin_control,
                                    'numero'             => $v->factura->numero,
                                    'base_imponible'     => $v->factura->base_imponible,
                                    'iva'                => $v->factura->base_imponible
                                 );
            if ($v->serie!='N/A') $aryVentas['serie'] = $v->serie;
            if ($v->documento_fiscal_id!=5)
            {
               $aryVentas[$i]['inicio_numero'] = $v->inicio_numero;
               $aryVentas[$i]['fin_numero'   ] = $v->fin_numero;
            }
            $i++;
         }
         /////////////////////////////////////////////////////////////////////////////
         if (count($aryVentas)>0)
            $this->renderPartial('declaracion_xml',array('rif_empresa'=>$rif_empresa,'ventas'=>$aryVentas));
         else $this->render('norecords');
      }
   }

?>

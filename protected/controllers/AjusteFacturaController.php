<?php

class AjusteFacturaController extends Controller
{

	public function actionIndex()
	{
      $model = new Factura('searchAjuste');
      $model->unsetAttributes( );
      if (isset($_GET['Factura']))
      {
         $model->attributes = $_GET['Factura'];
      }
		$this->render('index',array('model'=>$model));
	}

   public function actionAjuste( )
   {
      $id = $_GET['id'];
      $model = Factura::model()->findByPK($id);
      $this->render('ajuste',array('model'=>$model));
   }

}

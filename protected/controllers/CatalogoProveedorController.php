<?php

class CatalogoProveedorController extends Controller
{
	public function actionCreate()
	{
		$this->render('create');
	}

   public function actionAddItem( )
   {
      $item = $_POST['item'];
      $tiposMateriales = MaterialTipo::getListaMaterialTipos( );
      $this->renderPartial('_item',array('tiposMateriales'=>$tiposMateriales,'item'=>$item));
   }

   public function actionGetMaterialxTipo()
   {
      $item    = $_POST['item'];
      $tipo_id = $_POST['tipo'];
      $data = CatalogoMaterial::getCatalogoMaterialxTipo($tipo_id);
      echo CHtml::dropDownList('catalogo_material_id',0,$data,array('id'=>"catalogo_material_id-$item"));
   }

	public function actionDelete()
	{
		$this->render('delete');
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}

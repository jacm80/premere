<?php

class OrdenController extends Controller
{

	public function actionIndex( )
	{
      //echo succDocumento('FACTURA');
      $model = new Orden('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Orden']))
      {
			$model->attributes=$_GET['Orden'];
      }
      //fb(array('uno','dos','tres'),'warn');
		$this->render('index',array('model'=>$model));
	}

   public function actionView($id)
   {
		$this->render('tabOrden',array('model'=>$this->loadModel($id)));
   }

   public function actionUpdate( )
   {
      echo HtmlApp::debug($_POST);
   }

   public function loadModel($id)
	{
      $model = Orden::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


   //echo HtmlApp::debug($_POST);
   public function actionCreate( )
   {
      $model = new Orden;
      if (isset($_POST['Orden']))
		{
         //echo HtmlApp::debug($_POST);
         $model->guardarOrden($_POST);
		   $this->redirect(array('index'));
         
		}
      Yii::app()->clientScript->registerScript('item', "var i = 1;",CClientScript::POS_HEAD);
		$this->render('tabOrden',array('model'=>$model));
   }

   


   /*
   public function actionGuardar( )
   {
      echo HtmlApp::debug($_POST);
   }
   */

   //>> AJAX /////////////////////////////////////////////////////////////////////////////

   public function actionAdditem( )
   {
      $item = $_POST['item'];
      $productos = Orden::getSelectProductos( );
      $this->renderPartial('_itemOrden',array('item'     => $item,
                                             'productos'=> $productos,
                                             'series'   => HtmlApp::getListaSerie()));
   }

   public function actionGetclientes( )
   {
      $x = $_GET['term'];
      echo json_encode(Cliente::model()->getClienteAutoComplete($x));
   }

   public function actionGetfirmas( )
   {
      $id = $_GET['id'];
      $firmas = Firma::getListByCliente($id);
      $this->renderPartial('_dropdown_firma',array('firmas'=>$firmas));
   }

   public function actionGetsedes( )
   {
      $id = $_GET['id'];
      $sedes = Sede::getListByFirma($id);
      $this->renderPartial('_dropdown_sede',array('sedes'=>$sedes));
   }

   public function actionGetPrecios( )
   { 
      echo Producto::getPreciosJSON( ); 
   }

   public function actionGetNumeracion( )
   { 
      echo DocumentoControl::getNumeracionJSON( ); 
   }

   public function actionEliminar_Detalle( )
   {
      extract($_POST);
      $result = FacturaItem::eliminarById($id);
      echo json_encode($result);
   } 

}

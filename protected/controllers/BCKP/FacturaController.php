<?php

   class FacturaController extends Controller
   {
      //Yii::app()->session[“varName”] = $value;
      //$employees = $department->get_employees(array('condition'=>'born<1950'))
      //echo HtmlApp::debug($_facturas);
      /*

      echo CHtml::dropDownList($name, $select, $data, $htmlOptions);

      $name = name that will be used in $_POST when form is send
      $select = string with selected value
      $data = content - interesting think, see bellow
      $htmlOptions = classical array of attributes:
      
      */

      public function filters()
	   {
		   return array(array('CrugeAccessControlFilter'));
	   }


      public function actionIndex()
	   {
		   $model = new Orden('search');
		   $model->unsetAttributes();  // clear any default values
		   if(isset($_GET['Orden']))
         {
			   $model->attributes=$_GET['Orden'];
         }
		   $this->render('index',array('model'=>$model));
	   }

      //echo HtmlApp::debug($_pagos);
      
      public function actionCancelar()
      {
         $id = $_GET['id'];
         $model = Orden::model()->findByPk($id);
         if ($model===null)
			   throw new CHttpException(404,'The requested page does not exist.');
         else {
            $_orden  = $model->getOrden( );
            Yii::app()->getSession()->add('orden_status',$model->orden_status_id);
            $_facturas = Orden::model()->getFacturas($id);
            $_pagos    = Orden::getPagosByOrden($id);
            $item = (count($_pagos) > 0) ? (count($_pagos)+1) : 1;         
            Yii::app()->clientScript->registerScript('item', "var i = $item;",CClientScript::POS_HEAD);
            $this->render('_tabs',array(
                                        'orden'   =>$_orden,
                                        'facturas'=>$_facturas,
                                        'pagos'   =>$_pagos,
                                        ));
         }
      }

      public function actionImprimirFactura()
      {
         $id = $_GET['id'];
         $data = Orden::getFacturaPDF($id);
         $cssPrint = file_get_contents(getcwd().'/media/css/factura_print.css');
         $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
         $mPDF1->SetMargins(2,2,2);
         $mPDF1->WriteHTML($cssPrint,1);
         $mPDF1->WriteHTML( $this->renderPartial('_factura-pdf', array('factura'=>$data), true)  );
         $mPDF1->Output();
      } 
      //////////////////////////////////////////////////////////////////////////////////////////////
      // AJAX
      //////////////////////////////////////////////////////////////////////////////////////////////
      public function actionAddPago()
      {
         $item     = $_POST['item'];
         $orden_id = $_POST['orden_id'];
         $_facturas[0] = 'Cancelar Orden';
         $_bancos      = Banco::getListBancos();
         $_formaPagos  = FormaPago::getListFormaPagos();
         $aryFacturas  = CHtml::ListData(Factura::model()->findAllByAttributes(array('cancelada'=>0,'orden_id'=>$orden_id)),'id','numero');
         $_facturas = array_merge($_facturas,$aryFacturas);
         //HtmlApp::debug($_facturas);
         $this->renderPartial('_pago_item',array('bancos'=>$_bancos,'formaPagos'=>$_formaPagos,'item'=>$item,'facturas'=>$_facturas));
      }

      /*
      array
      (
      'orden_id' => '1'
      'forma_pago_id' => '3'
      'banco_id' => '2'
      'numero_comprobante' => '343'
      'descripcion' => '1001'
      'pago_ref' => '1'
      )      
      */
      
      public function actionRegistrarPago()
      {
         //echo HtmlApp::debug($_POST);
         /**/
         $monto = 0;
         $fecha_pago = date('Y-m-d h:m:i');
         extract($_POST);
         $Pago = new Pago;
         $transaction = $Pago->dbConnection->beginTransaction( );
         try
         {
            $Pago->orden_id      = $orden_id;
            $Pago->forma_pago_id = $forma_pago_id;
            $Pago->fecha         = $fecha_pago;
            $Pago->banco_id      = $banco_id;
            $Pago->numero_comprobante = (isset($numero_comprobante)) ? $numero_comprobante:'';
            $Pago->descripcion   = $descripcion;
            $Pago->usuario_id    = Yii::app()->user->getState('user_id');
            fb($descripcion,'info');
            ///////////////////////////////////////////////////////////////////////////////////
            $Orden = Orden::model()->findByPk($orden_id);
            if ($descripcion=='Cancelar Orden')
            { 
               $monto = $Orden->resta;
               $Orden->resta = 0.00;
               foreach($Orden->facturas as $f) 
               { 
                  $f->fecha_cancelacion = $fecha_pago;
                  $f->cancelada = 1;
                  $f->save(); 
               }
            }
            else 
            {
               $Factura = Factura::model()->findByAttributes(array('numero'=>$descripcion)); 
               $Factura->fecha_cancelacion = $fecha_pago;
               $Factura->cancelada = 1;
               //////////////////////////////////////////////////
               $monto = $Factura->base_imponible + $Factura->iva;
               //////////////////////////////////////////////////
               $Pago->factura_id = $Factura->id;
               fb('F: '.$Factura->id,'warn');
               $Factura->save( );
               $Orden->resta-= ($Factura->base_imponible+$Factura->iva);
               if ( $Orden->resta  < 0 ) $Orden->resta+=$Orden->abono;
               if ( $Orden->resta == 0 ) $Orden->orden_status_id = 3;
            }
            $Pago->monto = $monto;
            fb('Monto: '.$monto,'warn');
            $Pago->save( );
            $Orden->save( );
            //$transaction->rollback();
            fb($_POST,'info');
            $transaction->commit();            
         } 
         catch(Exception $e)
         {
            echo "<pre>" . $e . "</pre>";
            $transaction->rollback();
         }
      }

}
?>

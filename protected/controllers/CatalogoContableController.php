<?php

class CatalogoContableController extends Controller
{

   public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

   private function getParentCodigo( )
   {
      //if (!is_array($_POST['CatalogoCuenta']['parent_codigo'])) return $_POST['CatalogoCuenta']['parent_codigo'];
      foreach($_POST['CatalogoCuenta']['parent_codigo'] as $pc) 
      { 
         if ($pc != '0') $parent_codigo = $pc; 
      }
      return $parent_codigo;
   }

	public function actionCreate()
	{
      /*
      $model = new CatalogoCuenta;
      if (isset($_POST['CatalogoCuenta']))
      {
         $parent_id = $this->getParentCodigo( );
         echo HtmlApp::debug($_POST['CatalogoCuenta']); 
         echo 'parent_id: ' . $parent_id; 
         fb($parent_id,'warn');
      }
      else $this->render('create',array('model'=>$model));
	   */
      $model = new CatalogoCuenta;
      if (isset($_POST['CatalogoCuenta']))
      {
         //////////////////////////////////////
         $parent_id = $this->getParentCodigo( );
         ////////////////////////////////////////////////////////////////////////////////
         $cuentaRoot = CatalogoCuenta::model()->findByPk($parent_id);
         $model->attributes = array(
                                    'codigo'       => $_POST['CatalogoCuenta']['codigo'],
                                    'parent_codigo'=> $parent_id,
                                    'tipo'         => $_POST['CatalogoCuenta']['tipo'],
                                    'name'         => $_POST['CatalogoCuenta']['name'],
                                   );
         /////////////////////////////////////////////////////////////////////////////////
         $model->addAsChildOf($cuentaRoot,NULL,FALSE,null);
         $model->save( );
			$this->redirect(array('catalogoContable/index'));
      }
      else $this->render('create',array('model'=>$model));
   }

   public function actionDisplayCuenta( )
   {
      $chijos=0;
      $cuenta_id = $_POST['cuenta_id'];
      $cuenta = CatalogoCuenta::model()->findByPk($cuenta_id);
      if (!($cuenta===null))
      {
         $nivel  = $cuenta->getLevel();
         $chijos = $cuenta->getChildCount( );
      }
      if ($chijos > 0)
      {
         $cuentas = CatalogoCuenta::ListaCatalogoCuenta($cuenta_id);
		   $this->renderPartial('combo',array('cuentas'=>$cuentas,'nivel'=>$nivel));
      }
   }


	public function actionDelete()
	{
      $id = $_GET['id'];
      $cuenta = CatalogoCuenta::model()->findByPK($id);
      $cuenta->deleteNode( );
		$this->redirect(array('catalogoContable/index'));
	}

   private function getRowCuenta($rs)
   {
      return array(
                  'id'     => $rs->id,
                  'codigo' => $rs->codigo,
                  'name'   => $rs->name,
                  'tipo'   => $rs->tipo,
                  'nivel'  => $rs->getLevel( )
                  );
   }

   private function _getRoot($id)
   {
      $arbol = array( );
      $rsActivos = CatalogoCuenta::model()->findByPk($id);
      foreach ($rsActivos->children as $n1)
      {
         $arbol[ ] = $this->getRowCuenta($n1);
         foreach($n1->children as $n2)
         {
            $arbol[ ] = $this->getRowCuenta($n2);
            foreach ($n2->children as $n3)
            {
               $arbol[ ] = $this->getRowCuenta($n3);
               foreach ($n3->children as $n4)
               {
                  $arbol[ ] = $this->getRowCuenta($n4);
                  foreach($n4->children as $n5)
                  {
                     $arbol[ ] = $this->getRowCuenta($n5);
                     foreach($n5->children as $n6)
                     {
                        $arbol[ ] = $this->getRowCuenta($n6);
                     }
                  }
               }
            }
         }
      }
      return $arbol;
   }
   

	public function actionIndex()
	{
      $this->render('index',array(
                                 'activos'=>$this->_getRoot(1),
                                 'pasivos'=>$this->_getRoot(2),
                                 ));
	}

   public function loadModel($id)
	{
		$model=CatalogoCuenta::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'La Pagina Solicitada no existe.');
		return $model;
	}

   
   public function actionUpdate($id)
	{
      $id = $_GET['id'];
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if(isset($_POST['CatalogoCuenta']))
		{
			$model->attributes=$_POST['CatalogoCuenta'];
			if($model->save())
				$this->redirect(array('catalogoContable/index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}



   public function actionIniciarPlanCuenta( )
   {  
      $primer_nivel= array('ACTIVOS','PASIVOS','CAPITAL','INGRESOS NETOS','COSTOS','GASTOS','GANANCIAS Y PERDIDAS','OTROS INGRESOS Y GASTOS');
      $i=1;
      foreach ($primer_nivel as $c)
      { 
         $cuenta = new CatalogoCuenta;
         $cuenta->codigo = $i;
         $cuenta->parent_codigo = '0';
         $cuenta->name = $c;
         $cuenta->tipo = 'G';
         //$cuenta->addAsChildOf($cuentaRoot,$i,FALSE,null);
         $cuenta->addAsRoot(FALSE,null);
         $cuenta->save();
         $i++;
      }      
      echo "bien";
   }


	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}

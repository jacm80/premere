<?php

   class BuzonController extends Controller
   {
    
      public function filters()
	   {
		   return array(array('CrugeAccessControlFilter'));
	   }

      public function actionIndex( )
      { 
         $model = new Orden('search_buzon');
         $model->unsetAttributes( );
         if (isset($_GET['Orden']))
         {
            $model->attributes = $_GET['Orden'];
         }
		   $this->render('index',array('model'=>$model));
      }
   
      public function actionCambiarEstatus( )
      {
         if ( Yii::app()->user->checkAccess('admin') OR Yii::app()->user->checkAccess('vendedor') )
         { 
            $scope = 'impresas';
            $status_id = '3';
         }
         else if (Yii::app()->user->checkAccess('maquetador')) 
         {
            $scope = 'maquetadas';
            $status_id = '2';
         }
    
         // inicializando en unchecked todos los registros
         $scope_user = Orden::model( )->$scope( )->findAll( );   
         foreach ($scope_user as $scp)
         {
            $scp->orden_status_id = ($status_id-1);
            $scp->save( );
         }  
         // guardando los checkeds
         if (isset($_POST['ids']))
         {
            foreach($_POST['ids'] as $id)
            {          
               $model = Orden::model( )->findByPK($id);
               $model->orden_status_id = $status_id;
               $model->save( );
            }
         }
         echo $scope;
      }

      public function actionListado( )
      {
         $scope = $_GET['scope'];
         //////////////////////////////////////////////////////////////
         $cssPrint = file_get_contents(getcwd().'/media/css/form.css');
         $mPDF1 = Yii::app()->ePdf->mpdf('','A4');
         $mPDF1->SetMargins(2,2,2);
         $mPDF1->WriteHTML($cssPrint,1);
         $mPDF1->WriteHTML( $this->renderPartial( '_listado',array( 'model' => Orden::model()->$scope( )->findAll(),'scope'=>$scope ), true)  );
         $mPDF1->Output();
      }


      /*
      public function actionGetDetalle( )
      {
       $id = $_POST['id'][0];
       $data  = Orden::model( )->getProductos($id);
       $this->renderPartial('_detalle_orden',array('trabajos'=>$data));
      } 

      public function actionOrden()
      {
         $id = $_GET['id'];
         $_orden = Orden::getOrden($id);
         $_orden['items'] = Orden::getProductos($id);
         $this->render('displayOrden',array('orden'=>$_orden));
      }
      */
   }

?>

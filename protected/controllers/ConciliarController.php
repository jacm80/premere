<?php

class ConciliarController extends Controller
{
	public function actionCreate()
	{
      $model = new MaterialConciliacion;
		if (isset($_POST['MaterialConciliacion']))
      {
         $model->guardar( );
      }
      else $this->render('create',array('model'=>$model));
	}

   public function actionGetMateriales()
   {
      $almacen_id = $_POST['almacen_id'];
      $model = Material::model()->findAllByAttributes(array('almacen_id'=>$almacen_id));
      $this->renderPartial('_materiales',array('model'=>$model));
   }

	public function actionIndex()
	{
      $model = new MaterialConciliacion('search');
      $model->unsetAttributes( );
      if (isset($_GET['MaterialConciliacion']))
      {
         $model->attributes = $_GET['MaterialConciliacion']; 
      }
      $this->render('index',array('model'=>$model));
	}

   private function loadModel($id)
	{
      $model = MaterialConciliacion::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	public function actionView($id)
   {
      $model = $this->loadModel($id);
      $this->render('create',array('model'=>$model));
   }

}

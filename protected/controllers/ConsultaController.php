<?php

   class ConsultaController extends Controller
   {

      public function filters()
	   {
		   return array(array('CrugeAccessControlFilter'));
	   }

      public function actionNumeracion( )
      {
         $this->render('search_rif');
      }
  
      public function actionSearchNumeracion( )
      {
         $cliente = Cliente::model()->findByAttributes(array('rif'=>$_POST['rif']));
         $razon_social  = $cliente->razon_social;

         $criteria = new CDbCriteria;
         $criteria->addInCondition('cliente_id',array($cliente->id));
         $criteria->order = 'id DESC';
         $dc = DocumentoControl::model( )->find($criteria);
         $ultimo_control = $dc->fin_control;
         $ultimo_identif = $dc->identificador;

         $params = array('rif' => $_POST['rif']);
         foreach( array('numero','documento','identificador','inicio_numero','fin_numero','inicio_control','fin_control') as $p  )
         {
            if (isset($_POST[$p])) $params[$p] = $_POST[$p];
         }
         $model = new Factura('searchNumByRif');
         $model->unsetAttributes( );
         $model->attributes = $params;
         $this->renderPartial('_numeracion',array(
                                                  'model'         => $model,
                                                  'razon_social'  => $razon_social,
                                                  'ultimo_control'=> $ultimo_control,
                                                  'ultimo_identif'=> $ultimo_identif
                                                 ));
      }

   }

?>

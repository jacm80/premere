<?php

class GastoController extends Controller
{
	
   public function filters( )
	{
		return array(array('CrugeAccessControlFilter'));
	}

   
   private function loadModel($id)
	{
      $model = Gasto::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
   
   public function actionView($id)
	{
      $model = $this->loadModel($id);
      if (isset($_POST['Gasto']))
      {
         $_POST['fecha_registro'] = date('Y-m-d h:m:i');
         $model->attributes = $_POST['Gasto'];
         if ($model->save()) $this->redirect(array('index'));
      }
		$this->render('create',array('model'=>$model));
	}

   public function actionIndex()
	{
		$dataProvider = new Gasto('search');
		$this->render('index',array('model'=>$dataProvider));
	}

   public function actionCreate( )
	{
      $model = new Gasto;
      if (isset($_POST['Gasto']))
      {
         $_POST['fecha_registro'] = date('Y-m-d h:m:i');
         $model->attributes = $_POST['Gasto'];
         if ($model->save( )) $this->redirect(array('index'));
      }
      else
      {
         $this->render('create',array('model'=>$model));
	   }
   }

   /*public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (isset($_POST['Gasto']))
		{
         $_POST['fecha_registro']=date('Y-m-d h:m:i');
			$model->attributes=$_POST['Gasto'];
			if($model->save()) $this->redirect(array('index'));
		}
		$this->render('update',array('model'=>$model));
	}*/


	public function actionDelete( )
	{
		$this->render('delete');
	}

}

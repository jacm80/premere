<?php

class ClienteController extends Controller
{
   private $model;   

   public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}


   protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='clienteForm')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

   public function actionIndex()
   { 
      $model = new Cliente('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Cliente']))
      {
			$model->attributes=$_GET['Cliente'];
      }
		$this->render('index',array('model'=>$model));
   }

   public function actionCreate()
   {
      $this->model = new Cliente;
		$this->performAjaxValidation($this->model);
      if ($_POST)
      {
         $this->_actionGuardar( );
      }
      else $this->render('create',array('model'=>$this->model,'historia'=>array()));
   }

   private function _actionGuardar( )
   {
      $this->model->guardar( );
      $view = array( 
                     'cliente_id'           => $this->model->id,
                     'rif'                  => $this->model->rif,
                     'razon_social'         => $this->model->razon_social,
                     'representante_legal'  => $this->model->representante_legal,
                     'domicilio_fiscal'     => $this->model->domicilio_fiscal,
                     'contribuyente'        => $this->model->contribuyente->descripcion,
                     'cliente_tipo'         => $this->model->clienteTipo->descripcion,
                     'cliente_tipo_id'      => $this->model->cliente_tipo_id,
                     'telefono_residencial' => $this->model->telefono_residencial,
                     'telefono_celular'     => $this->model->telefono_celular,
                     'email'                => $this->model->email,
                     'registro_de_comercio' => $this->model->registro_de_comercio                     
                   );
      $principal = array( 
                        'rif'          => $this->model->rif,
                        'razon_social' => $this->model->razon_social
                        );
      echo json_encode(array('view'=>$view,'principal'=>$principal));
   }

   public function actionUpdate( )
   {
      unset($_POST['contribuyente']);
      unset($_POST['cliente_tipo']);
      $cliente = Cliente::model( )->findByPk($_POST['id']);
      $cliente->attributes = $_POST;
      $cliente->save( );
      $this->renderPartial('_viewCliente',array('model'=>$cliente));
   }
   
   public function actionAddFirma( )
   {
     echo Firma::addFirma($_POST);
   }

   public function actionAddSucursal( ) 
   { 
      echo Sede::addSede($_POST);
   }

   public function loadModel($id)
   {
      $model = Cliente::model()->findByPk($id);
      if ($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
      return $model;
   }

   public function actionView($id)
   {
      $this->render('create',array('model'=>$this->loadModel($id),'historia'=>$this->_historia($id) ));
   }

   public function actionForm( )
   {
      $id = $_POST['id'];
      $model = $this->loadModel($id);
      $this->renderPartial('_formCliente',array('model'=>$model));
   }

   public function actionDeleteSucursal( )
   {
      $id = $_POST['id'];
      $sucursal = Sede::model( )->findByPk($id);
      $sucursal->delete( );
   }

   public function _historia($cliente_id)
   {
      $historia = array( );
      $i=0;
      $ordenes = Orden::model()->findAllbyAttributes(array('cliente_id'=>$cliente_id));
      foreach($ordenes as $o)
      {
         $contribuyente = $o->cliente->contribuyente->descripcion;
         $historia['ordenes'][$i] = array(
                                          'id'              => $o->id,
                                          'fecha_entrada'   => $o->fecha_entrada,
                                          'firma'           => $o->firma->razon_social,
                                          'sucursal'        => $o->sede->descripcion,
                                          'numeros_facturas'=>''
                                          );
         $j=0;
         $nfacturas = array();
         foreach($o->facturas as $f)
         {
            $historia['ordenes'][$i]['facturas'][$j] = array(
                                                            'id'            => $f->id,
                                                            'numero'        => $f->numero,
                                                            'base_imponible'=> $f->base_imponible,
                                                            'iva'           => $f->iva
                                                            );
            $nfacturas[] = $f->numero;
            $k=0;
            foreach($f->facturaItems as $item)
            {
               $historia['ordenes'][$i]['facturas'][$j]['items'][$k] = array(
                                                                             'producto'=>$item->producto->descripcion,
                                                                             'cantidad'=>$item->cantidad,
                                                                             'numeracion'=>''
                                                                            );
               $tipo_doc = $item->producto->documento_fiscal_id;               
               if ($tipo_doc!=0)
               {
                  $n = $f->documentoControls;
                  $serie          = $n->serie;
                  $identificador  = $n->identificador;
                  $inicio_numero  = $n->inicio_numero;
                  $fin_numero     = $n->fin_numero;
                  $fin_control    = $n->fin_control;
                  $inicio_control = $n->inicio_control;
                  $historia['ordenes'][$i]['facturas'][$j]['items'][$k]['numeracion']= "<br />Contribuyente: $contribuyente, Serie: <b>$serie</b>,";
                  if ($tipo_doc!=5)
                     $historia['ordenes'][$i]['facturas'][$j]['items'][$k]['numeracion'].= "<br />Numero de Factura del: $inicio_numero Hasta: $fin_numero,<br />";
                  $historia['ordenes'][$i]['facturas'][$j]['items'][$k]['numeracion'].= "Numero de Control del: $inicio_control Hasta: $fin_control.";
               }
               $k++;
            } 
            $j++;
         }
         $historia['ordenes'][$i]['numeros_facturas'] = implode(',',$nfacturas);
         $i++;
      }
      return $historia;
   }

}

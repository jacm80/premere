<?php

   class CuadreController extends Controller
   {

      private $ordenesNuevas;
      private $ordenesPagos;
      private $gastos;

      private $oNuevas_montoRecibidoEF=0;
      private $oNuevas_montoRecibidoCH=0;
      private $oNuevas_montoRecibidoBC=0;
      private $oNuevas_montoRecibidoTT=0;
      private $oNuevas_montoRecibidoAB=0;
      private $oNuevas_montoPendiente =0;
      private $oNuevas_cantidad=0;
      //---------------------------------
      private $oPagos_montoRecibidoEF=0;
      private $oPagos_montoRecibidoCH=0;
      private $oPagos_montoRecibidoBC=0;
      private $oPagos_montoRecibidoTT=0;
      private $oPagos_montoPendiente =0;
      private $oPagos_cantidad=0;
      //--------------------------------

      private $montoEfectivo;
      private $montoBanco;

      private $tipoPago;

      public function filters()
	   {
		   return array(array('CrugeAccessControlFilter'));
	   }


      public function init()
      {
         $this->tipoPago = array(
                            1=>'EF',
                            2=>'CH',
                            3=>'BC',
                            4=>'BC',
                            5=>'DO',
                            6=>'BC',
                            7=>'BC'
                           );
      }
      
      private function _getOrdenesNuevas()
      {
         $data = array();
         $criteria = new CDbCriteria;
         $fecha = date('Y-m-d');
         $intervalo1 = $fecha.' 00:00:00';
         $intervalo2 = $fecha.' 23:59:59';
         $criteria = $criteria->addBetweenCondition('fecha_entrada',$intervalo1,$intervalo2);
         $Ordenes = Orden::model()->findAll($criteria);
         foreach($Ordenes as $o)
         {
            $data[] = array(
                           'id'         => $o->id,
                           'cliente'    => $o->cliente->razon_social,
                           'total'      => number_format($o->base_imponible + $o->iva,2,',','.'),
                           'abono'      => number_format($o->abono,2,',','.').' ('.$this->tipoPago[$o->abonoFormaPagoId].')',
                           'forma_pago' => $o->abonoFormaPagoId,
                           'resta'      => number_format($o->resta,2,',','.'),
                         );
            $this->oNuevas_montoRecibidoAB+= $o->abono;
            $this->oNuevas_montoRecibidoTT+= $o->base_imponible+$o->iva;
            $this->oNuevas_montoPendiente += $o->resta;
            if  ($o->abonoFormaPagoId==1) $this->oNuevas_montoRecibidoEF += $o->abono;
            if  ($o->abonoFormaPagoId==2) $this->oNuevas_montoRecibidoCH += $o->abono;
            if (($o->abonoFormaPagoId!=1) AND 
                ($o->abonoFormaPagoId!=2) AND 
                ($o->abonoFormaPagoId!=5)) $this->oNuevas_montoRecibidoBC += $o->abono;
            
            $this->oNuevas_cantidad++;
         }
         return $data;
      }
      
      private function _getOrdenesPagos()
      {
         $data = array();
         $fecha = date('Y-m-d');
         $intervalo1  = $fecha.' 00:00:00';
         $intervalo2  = $fecha.' 23:59:59';
         $criteria = new CDbCriteria;
         $criteria->with   = array('orden');
         $criteria->addNotInCondition('descripcion',array('Abono Orden'));
         $criteria->addBetweenCondition('fecha',$intervalo1,$intervalo2);
         $criteria->alias = 't';
         $criteria->compare('o.fecha_entrada','<',$intervalo2);
         $criteria->join   = 'LEFT JOIN orden o ON (o.id = t.orden_id)';
         $OrdenesPagos = Pago::model()->findAll($criteria);
         foreach($OrdenesPagos as $op)
         {
            $data[] = array(
                              'id'         => $op->id,
                              'cliente'    => $op->orden->cliente->razon_social,
                              'total'      => number_format($op->orden->base_imponible+$op->orden->iva,2,',','.'),
                              'resta'      => number_format($op->orden->resta,2,',','.'),
                              'cancelado'  => number_format($op->monto,2,',','.').' ('.$this->tipoPago[$op->forma_pago_id].')',
                              'saldo'      => number_format($op->orden->resta,2,',','.')
                           );
            $this->oPagos_montoRecibidoTT+= $op->monto;
            if  ($op->forma_pago_id==1) $this->oPagos_montoRecibidoEF += $op->monto;
            if  ($op->forma_pago_id==2) $this->oPagos_montoRecibidoCH += $op->monto;
            if (($op->forma_pago_id!=1) AND 
                ($op->forma_pago_id!=2) AND 
                ($op->forma_pago_id!=5)) $this->oPagos_montoRecibidoBC += $op->monto;
            $this->oPagos_cantidad++;
         }
         return $data;
      }


      private function _getGastos( )
      {
         $tipo = array(1=>'F',2=>'R');
         $data = array( );
         $fecha = date('Y-m-d');
         $gastos = Gasto::model()->findAllByAttributes(array('fecha_documento'=>$fecha));
         //$gastos = Gasto::model()->findAll();
         foreach($gastos as $g)
         {
            $data[ ] = array(
                              'numero'  => $tipo[$g->tipo].':'.$g->numero,
                              'concepto'=> $g->gastoConcepto->descripcion,
                              'monto'   => $g->monto,
                              'iva'     => $g->iva,
                              'deuda'   => '' //$g->deuda
                            );
         }
         return $data;
      }

      private function _setCierreValues(&$cierre)
      {
         $cierre->attributes = array(
            'fecha'                  => date('Y-m-d'),
            'ordenesNuevas'          => json_encode($this->ordenesNuevas),
            'ordenesPagos'           => json_encode($this->ordenesPagos),
            'gastos'                 => json_encode($this->gastos),
            //--------------------------------------------------------
            // ORDENES NUEVAS
            'oNuevas_montoRecibidoAB'=> $this->oNuevas_montoRecibidoAB,
            'oNuevas_montoRecibidoTT'=> $this->oNuevas_montoRecibidoTT,
            'oNuevas_montoRecibidoEF'=> $this->oNuevas_montoRecibidoEF,
            'oNuevas_montoRecibidoCH'=> $this->oNuevas_montoRecibidoCH,
            'oNuevas_montoRecibidoBC'=> $this->oNuevas_montoRecibidoBC,
            'oNuevas_montoPendiente' => $this->oNuevas_montoPendiente,  
            'oNuevas_cantidad'       => $this->oNuevas_cantidad,
            // ORDENES VIEJAS
            'oPagos_montoRecibidoTT' => $this->oPagos_montoRecibidoTT,
            'oPagos_montoRecibidoEF' => $this->oPagos_montoRecibidoEF,
            'oPagos_montoRecibidoCH' => $this->oPagos_montoRecibidoCH,
            'oPagos_montoRecibidoBC' => $this->oPagos_montoRecibidoBC,
            'oPagos_montoPendiente'  => $this->oPagos_montoPendiente,  
            'oPagos_cantidad'        => $this->oPagos_cantidad,
            // GASTOS

            // RESUMEN
            'montoEfectivo'         => $this->montoEfectivo,
            'montoBanco'            => $this->montoBanco,
            'usuario_id'            => Yii::app()->user->getState('user_id'),
            'fecha_actualizacion'   => date('Y-m-d h:m:i')
         );
      }
      
      public function _guardarCierreDiario( )
      {
         $cierre = CierreDiario::model( )->findByAttributes(array('fecha'=> date('Y-m-d')));
         if ($cierre===null)
         {
            $cierre = new CierreDiario('cierre');
         }
         $this->ordenesNuevas = $this->_getOrdenesNuevas( );
         $this->ordenesPagos  = $this->_getOrdenesPagos( );
         $this->gastos        = $this->_getGastos( );
         //-----------------------------------------------------------------------------------
         $this->_setCierreValues(&$cierre);
         //-----------------------------------------------------------------------------------
         $this->montoEfectivo =  $this->oNuevas_montoRecibidoEF + $this->oPagos_montoRecibidoEF;
         $this->montoBanco    = ($this->oNuevas_montoRecibidoCH + $this->oNuevas_montoRecibidoBC) + 
                                ($this->oPagos_montoRecibidoCH  + $this->oPagos_montoRecibidoBC );
         //-----------------------------------------------------------------------------------
         $cierre->save( );
      }
      
      private function _extractCierreDiario($fecha)
      {
         $cierre = CierreDiario::model( )->findByAttributes(array('fecha'=>$fecha));
         foreach ($cierre->attributes as $k=>$v)
         {
            if (in_array($k, array('id','fecha','usuario_id','fecha_actualizacion'))  ) continue;
            if (in_array($k, array('ordenesNuevas','ordenesPagos','gastos'))) $this->$k = json_decode($v,true);
            else $this->$k = $v;
         }
         //echo HtmlApp::debug($this->ordenesNuevas);
      }
 
      public function actionCierre( )
      {
         extract($_POST);
         if ($fecha == date('Y-m-d')) $this->_guardarCierreDiario( );
         else $this->_extractCierreDiario($fecha);
         $this->_guardarCierreDiario( );
         $this->_extractCierreDiario($fecha);
         //fb($ordenesPagos,'info');       
         $this->renderPartial('_cierreDiario',array(
                                    'fecha'                 => $fecha,
                                    // ARRAYS
                                    'ordenesNuevas'         => $this->ordenesNuevas,
                                    'ordenesPagos'          => $this->ordenesPagos,
                                    'gastos'                => $this->gastos,  
                                    // ORDENES NUEVAS
                                    'oNuevas_montoRecibidoAB'=> $this->oNuevas_montoRecibidoAB,
                                    'oNuevas_montoRecibidoTT'=> $this->oNuevas_montoRecibidoTT,
                                    'oNuevas_montoRecibidoEF'=> $this->oNuevas_montoRecibidoEF,
                                    'oNuevas_montoRecibidoCH'=> $this->oNuevas_montoRecibidoCH,
                                    'oNuevas_montoRecibidoBC'=> $this->oNuevas_montoRecibidoBC,
                                    'oNuevas_montoPendiente' => $this->oNuevas_montoPendiente,  
                                    'oNuevas_cantidad'       => $this->oNuevas_cantidad,
                                    // ORDENES VIEJAS
                                    'oPagos_montoRecibidoTT'=> $this->oPagos_montoRecibidoTT,
                                    'oPagos_montoRecibidoEF'=> $this->oPagos_montoRecibidoEF,
                                    'oPagos_montoRecibidoCH'=> $this->oPagos_montoRecibidoCH,
                                    'oPagos_montoRecibidoBC'=> $this->oPagos_montoRecibidoBC,
                                    'oPagos_montoPendiente' => $this->oPagos_montoPendiente,  
                                    'oPagos_cantidad'       => $this->oPagos_cantidad,
                                    // RESUMEN
                                    'montoEfectivo'         => $this->montoEfectivo,
                                    'montoBanco'            => $this->montoBanco,
                                    ));

      }

      public function actionIndex()
	   {
         $this->render('index');
      }
   }

?>

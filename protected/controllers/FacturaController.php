<?php

   class FacturaController extends Controller
   {
      
      public function filters()
	   {
		   return array(array('CrugeAccessControlFilter'));
	   }


      private function _pagos($orden_id)
      {
         $monto = 0;
         $Pagos = Pago::model( )->findAllByAttributes(array('orden_id'=>$orden_id));
         foreach($Pagos as $p)
         {
            $monto += $p->monto;
         }
         return $monto; 
      } 

      public function actionIndex()
	   {
		   $model = new Orden('search');
		   $model->unsetAttributes();  // clear any default values
		   if(isset($_GET['Orden']))
         {
			   $model->attributes=$_GET['Orden'];
         }
		   $this->render('index',array('model'=>$model));
	   }

      
      private function loadModel($id)
	   {
         $model = Orden::model()->findByPk($id);
		   if($model===null)
			   throw new CHttpException(404,'La Orden no existe');
		   return $model;
	   }

      
      public function actionViewFactura( )
      {
         $id = $_POST['id'];
         $model = Factura::model()->findByPk($id);
         $this->renderPartial('_facturaView',
                                             array('factura' => $model->getFacturaView() ));
      }

      public function actionImprimirFactura()
      {
         $id = $_GET['id'];
         $data = Orden::getFacturaPDF($id);
         $cssPrint = file_get_contents(getcwd().'/media/css/factura_print.css');
         $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
         $mPDF1->SetMargins(2,2,2);
         $mPDF1->WriteHTML($cssPrint,1);
         $mPDF1->WriteHTML( $this->renderPartial('_factura-pdf', array('factura'=>$data), true)  );
         $mPDF1->Output();
      } 

      public function actionAddPago()
      {
         $item     = $_POST['item'];
         $orden_id = $_POST['orden_id'];
         $_facturas[0] = 'Cancelar Orden';
         $_bancos      = Banco::getListBancos();
         $_formaPagos  = FormaPago::getListFormaPagos();
         $aryFacturas  = CHtml::ListData(Factura::model()->findAllByAttributes(array('cancelada'=>0,'orden_id'=>$orden_id)),'id','numero');
         $_facturas = array_merge($_facturas,$aryFacturas);
         $this->renderPartial('_pago_item',array('bancos'=>$_bancos,'formaPagos'=>$_formaPagos,'item'=>$item,'facturas'=>$_facturas));
      }

      public function actionFormPagoFactura($id)
      {
         $model = Factura::model( )->findByPk($id);
         $this->render('pagarFactura',array('model'=>$model));
      }

      
      public function actionView($id)
      {
         $model = $this->loadModel($id);
         $pagos = Pago::model()->findAllByAttributes(array('orden_id'=>$id));
         //-----------------------------------------------------------------------------
         $resumen['base_imponible'] = $model->base_imponible;
         $resumen['iva'           ] = $model->iva;
         $resumen['total'         ] = $model->base_imponible + $model->iva;
         $resumen['resta'         ] = $model->resta;
         $resumen['pagos'         ] = $this->_pagos($model->id);
         //------------------------------------------------------------------------------
         Yii::app()->getSession()->add('orden_status',$model->orden_status_id);
         $item = (count($pagos) > 0) ? (count($pagos)+1) : 1;
         Yii::app()->clientScript->registerScript('item', "var i = $item;",CClientScript::POS_HEAD);         
         //------------------------------------------------------------------------------
         $this->render('tabs',array('model'=>$model,'resumen'=>$resumen,'pagos'=>$pagos));
      }
      
      public function actionCancelarOrdenCompleta($id)
      {
         //echo "esto es una mierda<br />";
         //echo "id: {$id}--<br />";
         $model = $this->loadModel($id);
         $this->render('cancelarOrden',array('model'=>$model));
      }

      public function actionPagarFactura( )
      {
         echo HtmlApp::debug($_POST);
         /*
         $factura = Factura::model()->findByPk($_POST['id']);
         $user_id = Yii::app()->user->um->id;
         
         if ($_POST['usar_saldo'] == '1')
         {
            $_pago = new Pago;
            $_pago->orden_id      = $factura->orden->id;
            $_pago->fecha = date('Y-m-d h:m:i');
            $_pago->forma_pago_id = 8;
            $_pago->descripcion   = 'Pago de Factura, Disposicion de Saldo (Abono Orden)';
            $_pago->usuario_id  = $user_id;
            $_pago->monto = $_POST['saldo_dispuesto'];
         }

         foreach ($_POST['forma_pago_id'] as $fpid) 
         {
            $_pago = new Pago;
            $_pago->orden_id      = $factura->orden->id;
            $_pago->forma_pago_id = $fpid;
            $_pago->fecha = date('Y-m-d h:m:i');
            $_pago->banco_id = $_POST['banco_id'][$i];
            $_pago->numero_comprobante = $_POST['numero_comprobante'][$i];
            $_pago->usuario_id  = $user_id;
            $_Pago->descripcion = "Pago de Factura";
            $_Pago->monto = $_POST['monto'];
            $_pago->save( );
            $i++;
         }
         */
      }

            
      public function actionRegistrarPago()
      {
         $monto = 0;
         $fecha_pago = date('Y-m-d h:m:i');
         extract($_POST);
         $Pago = new Pago;
         $transaction = $Pago->dbConnection->beginTransaction( );
         try
         {
            $Pago->orden_id      = $orden_id;
            $Pago->forma_pago_id = $forma_pago_id;
            $Pago->fecha         = $fecha_pago;
            $Pago->banco_id      = $banco_id;
            $Pago->numero_comprobante = (isset($numero_comprobante)) ? $numero_comprobante:'';
            $Pago->descripcion   = $descripcion;
            $Pago->usuario_id    = Yii::app()->user->getState('user_id');
            fb($descripcion,'info');
            ///////////////////////////////////////////////////////////////////////////////////
            $Orden = Orden::model()->findByPk($orden_id);
            if ($descripcion=='Cancelar Orden')
            { 
               $monto = $Orden->resta;
               $Orden->resta = 0.00;
               foreach($Orden->facturas as $f) 
               { 
                  $f->fecha_cancelacion = $fecha_pago;
                  $f->cancelada = 1;
                  $f->save(); 
               }
            }
            else 
            {
               $Factura = Factura::model()->findByAttributes(array('numero'=>$descripcion)); 
               $Factura->fecha_cancelacion = $fecha_pago;
               $Factura->cancelada = 1;
               //////////////////////////////////////////////////
               $monto = $Factura->base_imponible + $Factura->iva;
               //////////////////////////////////////////////////
               $Pago->factura_id = $Factura->id;
               fb('F: '.$Factura->id,'warn');
               $Factura->save( );
               $Orden->resta-= ($Factura->base_imponible+$Factura->iva);
               if ( $Orden->resta  < 0 ) $Orden->resta+=$Orden->abono;
               if ( $Orden->resta == 0 ) $Orden->orden_status_id = 3;
            }
            $Pago->monto = $monto;
            fb('Monto: '.$monto,'warn');
            $Pago->save( );
            $Orden->save( );
            //$transaction->rollback();
            fb($_POST,'info');
            $transaction->commit();            
         } 
         catch(Exception $e)
         {
            echo "<pre>" . $e . "</pre>";
            $transaction->rollback();
         }
      }

   }
?>

<?php

class CompraController extends Controller
{

   public $iva;
   public $baseImponible;
   public $total;

   public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

   /*
      Al registrar la compra buscar (catalogo_material_id,proveedor_id) en proveedor_material
      y actualizar precio_unitario y fecha_actualizacion = actual
      Si no insertar proveedor_id,catalogo_material_id, precio_unitario, fecha_actualizacion
   */

   private function loadModel($id)
	{
      $model = Compra::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

   
	public function actionCreate()
	{
      $model = new Compra;
      if (isset($_POST['Compra']))
      {
         $model->guardar( );
         //$this->redirect(array('index'));
         ///echo HtmlApp::debug($_POST['Compra']);
         //return;
      }
      else 
      {
         Yii::app()->clientScript->registerScript('item', "var i = 1;",CClientScript::POS_HEAD);
		   $this->render('create',array('model'=>$model));
      }
	}

   public function actionView($id)
   {
      $model = $this->loadModel($id);
      if (isset($_POST['Compra']))
		{
         $model->guardar( );
		   $this->redirect(array('index'));
		}
      $this->render('create',array('model'=>$model));
   }


   public function actionIndex( )
   {
      $model = new Compra('search');
      $model->unsetAttributes( );
      if (isset($_GET['Compra']))
      {
         $model->attributes = $_GET['Compra']; 
      }
      $this->render('index',array('model'=>$model));
   }


   public function actionGetProveedor()
   {
      $x = $_GET['term'];
      echo json_encode(Proveedor::getAutoComplete($x));      
   }

   /*
      - Si hay actualizar precio_unitario y fecha_actualizacion
      - De lo contrario desplegar todo lo del catalogo de material 
   */

   public function actionGetMaterialesProveedor( )
   {
      $proveedor_id = $_POST['proveedor_id'];
      $criteria = new CDbCriteria;
      $criteria->condition = 'proveedor_id=:proveedor_id';
      $criteria->params    = array(':proveedor_id'=>$proveedor_id);
      $criteria->order     = 'catalogo_material_id DESC';
      $materiales = ProveedorMaterial::model( )->findAll($criteria);
      //////////////////////////////////////////////////////////////////////////////////////////////////  
      /*
      if (!empty($materiales))
      {
         foreach ($materiales as $m)
         {
            $data[] = array(
                        'catalogo_material_id' => $m->catalogo_material_id,
                        'marca'                => $m->catalogo_material->marca->descripcion,
                        'precio_unitario'      => $m->precio_unitario
                       ); 
         }
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////  
      else */ {
            $materiales = new CatalogoMaterial('search');
            /*
            foreach($materiales as $m)
            $data[] = array(
                              'catalogo_material_id' => $m->id,
                              'marca'                => $m->marca->descripcion,
                              'precio_unitario'      => 0
                           );   
            */
            $columns = array(
                              'id',
                              array(
                                    'type'   =>'raw',
                                    'header' =>'Material',
                                    'name'   =>'descripcion',
                                    'value'  =>'$data->descripcion'
                                    ),
                              array('class' => 'CCheckBoxColumn')
                           );
      }
      //////////////////////////////////////////////////////////////////////////////////////////////////  
      $this->renderPartial('_materiales',array('model'=>$materiales,'columns'=>$columns));
   }
   
   public function actionGetCatalogoMateriales( )
   {
      $columns = array(
                        'id',
                        array(
                              'type'   =>'raw',
                              'header' =>'Material',
                              'name'   =>'descripcion',
                              'value'  =>'$data->descripcion'
                             ),
                        array('class' => 'CCheckBoxColumn')
                     );
      $model = new CatalogoMaterial('search');
      if (isset($_GET['CatalogoMaterial']))
      {
       $model->unsetAttributes( );
       $model->attributes = $_GET['CatalogoMaterial'];  
      }
      $model->findAll(/*array('condition'=>'existencia > 0')*/);
      $this->renderPartial('_materiales',array('model'=>$model,'columns'=>$columns));
   }

   public function actionSetMateriales( )
   {
      $ids = $_GET['ids'];
      $data = array( );
      foreach($ids as $id)
      {
         $m = CatalogoMaterial::model( )->findByPk($id);
         $data[ ] = array(
                           'id'              => $m->id,
                           'material'        => $m->descripcion,
                           'barcode'         => $m->barcode,
                           'precio_unitario' => 0,
                           'iva'             => 0,
                           'total'           => 0,
                           //'almacen'    => $m->almacen->descripcion,
                           //'existencia' => $m->existencia
                         );           
      }
      echo json_encode($data); 
   }

	public function actionDelete()
	{
		$this->render('delete');
	}
   
   public function actionInicializar( )
   {
      $_init = array(
                     'user'        => Yii::app()->user->name,
                     'almacenes'   => Almacen::getListAlmacenesAry( ),
                     'formas_pagos'=> FormaPago::getListFormaPagosAry( ),
                     'bancos'      => EmpresaBanco::getListBancosAry( )
                     );
      echo json_encode($_init);
   }


}

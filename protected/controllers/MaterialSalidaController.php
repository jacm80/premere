<?php

class MaterialSalidaController extends Controller
{
	
   public function filters()
	{
		return array(array('CrugeAccessControlFilter'));
	}

   public function actionCreate()
	{
      $model = new MaterialSalida;
      if (isset($_POST['MaterialSalida']))
      {
         $model->guardar( );   
		   $this->redirect(array('printRecibo','id'=>$model->id));
      }
      else
      {
		   $this->render('create',array('model'=>$model));
      }
	}

   public function actionView($id)
   {
	   $this->render('create',array('model'=>$this->loadModel($id)));      
   }

   private function loadModel($id)
	{
      $model = MaterialSalida::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

   public function actionPrintRecibo( )
   {
      $id = $_GET['id'];
      $model = $this->loadModel($id);
      $TMPLrecibo = ($model->tipo_salida_id<2) ? '_reciboSalida' : '_reciboSalidaAC';
      $cssPrint = file_get_contents(getcwd().'/media/css/recibo_salida.css');
      $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
      $mPDF1->SetMargins(2,2,2);
      $mPDF1->WriteHTML($cssPrint,1);
      $mPDF1->WriteHTML( $this->renderPartial($TMPLrecibo, array('model'=>$model), true)  );
      $mPDF1->Output();
   }

   public function actionGetMateriales( )
   {
      $model = new Material('search');
      if (isset($_GET['Material']))
      {
       $model->unsetAttributes( );
       $model->attributes = $_GET['Material'];  
      }
      $model->findAll(array('condition'=>'existencia_unidad > 0'));
      $this->renderPartial('_materiales',array('model'=>$model));
   }

   public function actionSetMateriales( )
   {
      $ids = $_GET['ids'];
      $data = array( );
      foreach($ids as $id)
      {
         $m = Material::model( )->findByPk($id);
         $data[ ] = array(
                           'id'                   => $m->id,
                           'material'             => $m->catalogoMaterial->descripcion,
                           'barcode'              => $m->catalogoMaterial->barcode,
                           'almacen'              => $m->almacen->descripcion,
                           'existencia_unidad'    => $m->existencia_unidad,
                           'existencia_detallado' => $m->existencia_detallado,
                           'maneja_detallado'     => ($m->catalogoMaterial->maneja_detallado==1) ? TRUE: array( ),
                           'valor_detallado'      => $m->catalogoMaterial->unidad->valor_detallado,
                           'descripcion_detallado'=> $m->catalogoMaterial->unidad->descripcion_detallado,
                           'PATH_SERVER'          => Yii::app()->baseUrl
                         );           
      }
      echo json_encode($data); 
   }

	public function actionDelete()
	{
		$this->render('delete');
	}

	public function actionIndex()
	{  
      $model = new MaterialSalida('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MaterialSalida']))
      {
			$model->attributes=$_GET['MaterialSalida'];
      }
		$this->render('index',array('model'=>$model));
	}

}

<?php
   #echo "PROBANDO QUE ESTE LLAMANDO BIEN A GLOBALS.PHP<br />";
   # Clase para mostrar mensajes de depuaracion en fireBug
   require_once('FirePHPCore/FirePHP.class.php');

   /*
   FB::log(‘Mensaje de log’);
   FB::info(‘Mensaje de informacion, con icono azul’);
   FB::warn(‘Esto sería un warning, con icono amarillo’);
   FB::error(‘Mensaje de error, con icono rojo’);
   */

   function fb($var,$tipo='log')
   {
      if ($tipo == 'trace') echo Yii::trace(CVarDumper::dumpAsString($var),'vardump');
      $firephp = FirePHP::getInstance(true);
      $firephp->$tipo($var);
   }

   function succDocumento($tipo)
   {
         $num = EmpresaNumeracion::model( )->findByAttributes(array('descripcion'=>$tipo));
         $num->correlativo++;
         $num->save( );
         return $num->correlativo;
   }

   function getIVA( )
   {
         $criteria = new CDbCriteria;
         $criteria->order = 't.id DESC';
         $criteria->limit = 1;
         $iva = Impuesto::model( )->find($criteria);
         return ($iva->porcentaje/100);
   }

   function limpia_espacios($cadena){
    $cadena = str_replace(' ', '', $cadena);
    return $cadena;
   }
   
   function limpia_cadena($cadena) {
     return (ereg_replace('[^A-Za-z0-9_-ñÑ]', '', $cadena));
   }

?>

<?php
$this->breadcrumbs=array(
	'Empresa Bancos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' EmpresaBanco', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' EmpresaBanco', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' EmpresaBanco', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' EmpresaBanco', 'url'=>array('admin')),
);
?>

<h1>Update EmpresaBanco <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
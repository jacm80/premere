<?php
$this->breadcrumbs=array(
	'Empresa Bancos',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' EmpresaBanco', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' EmpresaBanco', 'url'=>array('admin')),
);
?>

<h1>Empresa Bancos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

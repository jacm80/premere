<?php
$this->breadcrumbs=array(
	'Empresa Bancos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' EmpresaBanco', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' EmpresaBanco', 'url'=>array('admin')),
);
?>

<h1>Crear EmpresaBanco</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Empresa Bancos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' EmpresaBanco', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' EmpresaBanco', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' EmpresaBanco', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' EmpresaBanco', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' EmpresaBanco', 'url'=>array('admin')),
);
?>

<h1>View EmpresaBanco #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'banco_id',
		'agencia',
		'numero',
		'fecha_apertura',
		'saldo',
	),
)); ?>

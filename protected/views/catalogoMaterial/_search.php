<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'barcode'); ?>
		<?php echo $form->textField($model,'barcode',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'material_tipo_id'); ?>
		<?php echo $form->textField($model,'material_tipo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'marca_id'); ?>
		<?php echo $form->textField($model,'marca_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unidad_id'); ?>
		<?php echo $form->textField($model,'unidad_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'maneja_detallado'); ?>
		<?php echo $form->textField($model,'maneja_detallado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
$this->breadcrumbs=array(
	'Catalogo Materials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' CatalogoMaterial', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' CatalogoMaterial', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' CatalogoMaterial', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' CatalogoMaterial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' CatalogoMaterial', 'url'=>array('admin')),
);
?>

<h1>Vista CatalogoMaterial #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'barcode',
		'descripcion',
		'material_tipo_id',
		'marca_id',
		'unidad_id',
		'maneja_detallado',
	),
)); ?>

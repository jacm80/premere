<?php
$this->breadcrumbs=array(
	'Catalogo Materials'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' CatalogoMaterial', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' CatalogoMaterial', 'url'=>array('admin')),
);
?>

<h1>Crear CatalogoMaterial</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
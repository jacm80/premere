<?php
$this->breadcrumbs=array(
	'Catalogo Materials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' CatalogoMaterial', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' CatalogoMaterial', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' CatalogoMaterial', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' CatalogoMaterial', 'url'=>array('admin')),
);
?>

<h1>Update CatalogoMaterial <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
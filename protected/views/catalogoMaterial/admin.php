<?php
$this->breadcrumbs=array(
	'Catalogo Materials'=>array('index'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Listar CatalogoMaterial', 'url'=>array('index')),
	array('label'=>'Crear CatalogoMaterial', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('catalogo-material-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Catalogo Materials</h1>

<p>
Opcionalmente puedes utilizar operadores de comparaci&oacute;n (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
&oacute; <b>=</b>) al comienzo de cada filtro de la lista.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'catalogo-material-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'barcode',
		'descripcion',
		array(
            'type'   => 'raw',
            'header' => 'Tipo',
            'name'   => 'material_tipo_id',
            'value'  => '$data->materialTipo->descripcion',
            'filter' => CHtml::listData(MaterialTipo::model()->findAll(),'id','descripcion')
           ),
      array(
            'type'  => 'raw',
            'header'=> 'Marca',
            'name'  => 'marca_id',
            'value' => '$data->marca->descripcion',
            'filter' => CHtml::listData(Marca::model()->findAll(),'id','descripcion')
          ),
      array(
            'type'  => 'raw',
            'header'=> 'Unidad',
            'name'  => 'unidad_id',
            'value' => '$data->unidad->descripcion',
            'filter'=> CHtml::listData(Unidad::model()->findAll(),'id','descripcion')
            ),
      array(
		      'type'   => 'raw',
            'header' => 'Detallado?',
            'name'   => 'maneja_detallado',
            'value'  => '($data->maneja_detallado==1)?"Si":"No"',
            'filter' => array(1=>'Si','0'=>'No')
         ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

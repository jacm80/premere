<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'catalogo-material-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'barcode'); ?>
		<?php echo $form->textField($model,'barcode',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'barcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'material_tipo_id'); ?>
		<?php echo $form->textField($model,'material_tipo_id'); ?>
		<?php echo $form->error($model,'material_tipo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'marca_id'); ?>
		<?php echo $form->textField($model,'marca_id'); ?>
		<?php echo $form->error($model,'marca_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unidad_id'); ?>
		<?php echo $form->textField($model,'unidad_id'); ?>
		<?php echo $form->error($model,'unidad_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maneja_detallado'); ?>
		<?php echo $form->textField($model,'maneja_detallado'); ?>
		<?php echo $form->error($model,'maneja_detallado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

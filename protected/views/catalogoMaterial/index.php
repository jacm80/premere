<?php
$this->breadcrumbs=array(
	'Catalogo Materials',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' CatalogoMaterial', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' CatalogoMaterial', 'url'=>array('admin')),
);
?>

<h1>Catalogo Materials</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

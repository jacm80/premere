<div id="layout_factura" style="margin:0 auto;width:80%">
   <table class="header">
      <tr>
         <td colspan="2" class="label">Fecha Emision</td>
         <td colspan="2"><?php echo (isset($factura['fecha_cancelacion'])) ? $factura['fecha_cancelacion'] : 'Por Cancelar'; ?></td>
      </tr>
      <tr>
         <td colspan="4" class="label">Nombre y Apellido o Razon Social del Comprador</td>
      </tr>
      <tr>
         <td colspan="4"><?php echo $factura['firma']; ?></td>
      </tr>
      <tr>
         <td colspan="2" class="label">No. RIF o CED No.</td>
         <td colspan="2"><?php echo $factura['rif']; ?></td>
      </tr>
      <tr>
         <td class="label">Forma de Pago</td>
         <td><?php echo $factura['forma_pago']; ?></td>
         <td class="label">Explique</td>
         <td>MONEDA DE CURSO LEGAL</td>
      </tr>
   </table>
   <br />
   <table class="data">
   <thead>
      <tr>
         <th>CANTIDAD</th>
         <th>DESCRIPCION</th>
         <th>PRECIO UNITARIO</th>
         <th>MONTO</th>
         <th>&nbsp;</th>
      </tr>
   </thead>
   <tbody>
   <?php foreach ($factura['items'] as $item): ?>
      <tr>
         <td style="text-align:center;"><?php echo $item['cantidad'];         ?></td>
         <td><?php echo $item['producto'];?></td>
         <td class="monto"><?php echo number_format($item['precio_unitario'],2,',','.');  ?></td>
         <td class="monto"><?php echo number_format($item['monto'],2,',','.');            ?></td>
         <td><input type="checkbox" name="item[]" value="<?php echo $item['id']; ?>"></td>
      </tr>
   <?php endforeach; ?>
   </tbody>
   </table>
   <br />
   <table class="footer">
      <tr>
         <td style="width: 80%;" class="label-summary">Base Imponible segun Alicuota 12 %</td>
         <td class="value-summary"><?php echo number_format($factura['base_imponible'],2,',','.'); ?></td>
      </tr>
      <tr>
         <td class="label-summary">Monto Total del Impuesto segun Alicuota 12 %</td>
         <td class="value-summary"><?php echo number_format($factura['iva'],2,',','.'); ?></td>
      </tr>
      <tr>
         <td class="label-summary">MONTO TOTAL DE LA VENTA</td>
         <td class="value-summary"><?php echo number_format(($factura['base_imponible']+$factura['iva']),2,',','.'); ?></td>
      </tr>
   </table>
</div>

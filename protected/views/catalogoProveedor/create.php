<?php
/* @var $this CatalogoProveedorController */

$this->breadcrumbs=array(
	'Catalogo Proveedor'=>array('/catalogoProveedor'),
	'Create',
);
?>

<style>
   table 
   {
      border: 1px solid black;
   }
   thead th {
      background-color: #e3e3e3;
      border: 1px solid #f3f3f3;
   }
   tbody td {
      border: 1px solid #f3f3f3;
   }
</style>

<script type="text/javascript">
   var i=1; //TODO

   $(document).ready(function(){

      $('#addItem').click(function(){
         $.ajax({
            url: base_url+'/catalogoProveedor/additem',
            type:'post',
            async: true,
            data: { item : i },
            dataType:'html',
            success:function(respuesta){
               $('#detalle').before(respuesta);
               i++;
             },
            error:function(){ alert('fallo'); }
            });
         });

      
      $('select.MaterialTipo').live('change',function(event){
         //alert($(event.target).prop('value'));
         var _item = $(event.target).prop('id').split('-')[1];
         var _tipo = $(event.target).prop('value');
         $.ajax({
            url: base_url+'/catalogoProveedor/getMaterialxTipo',
            type: 'post',
            async: true,
            data: { item: _item, tipo: _tipo },
            success: function(respuesta){
               $('#catalogoMaterial-'+_item).html(respuesta);
            },
            error: function () { alert('fallo'); }
         });
         //alert('hola');
      });

   });

</script>


<h1 style="text-align:center">Catalogo de Precios</h1>

<table id="catalogo">
   <thead>
      <tr>
         <th>Tipo</th>
         <th>Descripcion</td>
         <th>Unidad</th>
         <th>Precio de Compra</th>
         <th>Ultima Compra</th>
         <th>Operacion</th>
      </tr>
   </thead>
   <tbody>
      <tr id="detalle" class="hide">
         <td>Tinta Litografica</td>
         <td>Tinta Azul Reflejo Starligth</td>
         <td>Starligth</td>
         <td>Lata 500 grm.</td>
         <td><?php echo CHtml::TextField('precio','56,72'); ?></td>
         <td>10/12/2012 No. 13351</td>
         <td>Actualizar</td>
      </tr>
      <tfoot>
         <td colspan="8" style="text-align:right;">
            <button id="addItem">Agregar Material</button>
            <button id="guardarCatalogo">Guardar Catalogo</button>
         </td>
      </tfoot>
   </tbody>
</table>

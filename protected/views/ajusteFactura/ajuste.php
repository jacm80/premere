<?php
   HtmlApp::loadCss('form');
   HtmlApp::loadCss('ajuste');
   HtmlApp::loadCss('factura_print');
   HtmlApp::loadJs('mustache-master/mustache');
   HtmlApp::loadJs('jquery-Mustache/jquery.mustache');
   HtmlApp::loadJs('ajuste');
?>

<div id="content-numero">
  <div id="numero-factura">
     <div id="BtnFactura"><?php echo CHtml::image(HtmlApp::imageUrl('icons/tabla.png')); ?></div>
     <div style="top:8px;position:relative;">Ajustar Factura No.:
         <strong><?php echo sprintf('%0d6',$model->numero); ?></strong>
     </div>
     <div style="clear:both;"></div>
  </div>
</div>
<br />

<div id="viewfactura" class="hide">
   <?php $this->renderPartial('_factura',array('factura'=>$model->getFacturaView( ))); ?>
</div>

<br />
<table class="table-form">
   <tr>
      <td>Tipo de Ajuste</td>
      <td>
         <select name="Ajuste[tipo]" id="tipo_ajuste">
            <option value="0">Seleccione..</option>
            <option value="1">Monto a Debitar</option>
            <option value="2">Monto a Acreditar</option>
         </select>
      </td>
   </tr>
   <tr>
      <td>Motivo del Ajuste</td>
      <td>
         <div id="divMotivo"></div>
      </td>
   </tr>
   <tr>
      <td>Observacion</td>
      <td><textarea name="Ajuste['observacion']"></textarea></td>
   </tr>
   <tr>
      <td colspan="2" style="text-align:center"><button>Aceptar</button></td>
   </tr>
</table>

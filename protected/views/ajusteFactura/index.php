<?php $this->breadcrumbs=array('Ajuste Factura'=>array('ajusteFactura'),'index'); ?>
<h1>Lista de Facturas</h1>
<?php 
   $columns = array( 
                     'numero',
                     array
                     (
                        'type'=>'raw',
                        'header'=>'Fecha',
                        'name'=>'fecha_entrada',
                        'value'=>'$data->orden->fecha_entrada',
                     ),
                     array
                     (
                        'type'=>'raw',
                        'header'=>'Cliente',
                        'name'=>'firma_id',
                        'value'=>'$data->orden->firma->razon_social',
                     ),
                     'base_imponible',
                     'iva'
                   );
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'=>'ajuste-grid',
                                                      'filter'=>$model,
                                                      'dataProvider'=>$model->search( ),
                                                      'columns'=> $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/ajusteFactura/ajuste/id/"+xid;
                                                      }'
                                                   ));
?>

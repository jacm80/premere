<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('abreviatura')); ?>:</b>
	<?php echo CHtml::encode($data->abreviatura); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion_detallado')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion_detallado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('valor_detallado')); ?>:</b>
	<?php echo CHtml::encode($data->valor_detallado); ?>
	<br />


</div>

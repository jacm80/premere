<?php
$this->breadcrumbs=array(
	'Unidads',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' Unidad', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' Unidad', 'url'=>array('admin')),
);
?>

<h1>Unidads</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

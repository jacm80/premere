<?php
$this->breadcrumbs=array(
	'Unidads'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Unidad', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Unidad', 'url'=>array('admin')),
);
?>

<h1>Crear Unidad</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Unidads'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Unidad', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Unidad', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Unidad', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Unidad', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Unidad', 'url'=>array('admin')),
);
?>

<h1>Vista Unidad #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'abreviatura',
		'descripcion_detallado',
		'valor_detallado',
	),
)); ?>

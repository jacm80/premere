<?php
$this->breadcrumbs=array(
	'Unidads'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Unidad', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Unidad', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Unidad', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Unidad', 'url'=>array('admin')),
);
?>

<h1>Update Unidad <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
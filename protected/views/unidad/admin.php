<?php
$this->breadcrumbs=array(
	'Unidads'=>array('index'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Listar Unidad', 'url'=>array('index')),
	array('label'=>'Crear Unidad', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('unidad-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Unidads</h1>

<p>
Opcionalmente puedes utilizar operadores de comparaci&oacute;n (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
&oacute; <b>=</b>) al comienzo de cada filtro de la lista.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'unidad-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'descripcion',
		'abreviatura',
		'descripcion_detallado',
		'valor_detallado',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<div style="padding: 8px;text-align:center;">
   <div style="width:400px; border:1px solid black; margin:0 auto; border: 2px solid #e3e3e3;">
   <h3 style="text-align:center; padding: 8px; background-color: #f1f1f1;">Declaracion Fiscal</h3>
   <form method="post" action="<?php echo Yii::app()->request->baseUrl?>/declaracion/xml">
   <div class="row">
      <label for="periodo">Periodo</label>
      <?php echo CHtml::dropDownList('periodo',NULL,
      array(
             '0'=>"Diciembre-".(date('Y')-1),
             '1'=>"Enero",
             '2'=>"Febrero",
             '3'=>"Marzo",
             '4'=>"Abril",
             '5'=>'Mayo',
             '6'=>'Junio',
             '7'=>'Julio',
             '8'=>'Agosto',
             '9'=>'Septiembre',
            '10'=>'Octubre',
            '11'=>'Noviembre',
            '12'=>'Diciembre',
            )); ?>
   <button type="submit">Generar XML</button>
   <br />
   <br />
   </div>
   </form>
   </div>
</div>

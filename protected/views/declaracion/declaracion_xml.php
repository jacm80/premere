<?php header("Content-type: text/xml"); ?>
<?php echo "<?xml version='1.0' encoding='ISO-8859-1' ?>\n"; ?>
<Imprenta Rif_imprenta="<?php echo $rif_empresa; ?>" Periodo_declaracion="<?php echo date('Y-d'); ?>">
   <?php foreach($ventas as $v): ?>  
   <Ventas>
    <Rif_usuario><?php echo $v['rif']; ?></Rif_usuario>
    <Tipo_documento><?php echo $v['documento_fiscal_id']; ?></Tipo_documento>
    <Fecha_elaboracion><?php echo $v['fecha_elaboracion'];  ?></Fecha_elaboracion>
    <Identificador><?php echo $v['identificador']; ?></Identificador>
    <Inicio_numero_secuencial><?php echo $v['inicio_control']; ?></Inicio_numero_secuencial>
    <Fin_numero_secuencial><?php echo $v['fin_control']?></Fin_numero_secuencial>
   <?php if (isset($v['inicio_numero'])): ?>
    <Inicio_numero_documento><?php echo $v['inicio_numero']; ?></Inicio_numero_documento>
    <Fin_numero_documento><?php echo $v['fin_numero']; ?></Fin_numero_documento>
   <?php endif; ?>
    <Numero_factura_venta><?php echo $v['numero']?></Numero_factura_venta>
    <Base_imponible_venta><?php echo $v['base_imponible']; ?></Base_imponible_venta>
    <Monto_iva_venta><?php echo $v['iva']; ?></Monto_iva_venta>
   </Ventas>
   <?php endforeach; ?>
</Imprenta>

<?php
/* @var $this CatalogoContableController */
HtmlApp::loadCss('cuentas_display');
$this->breadcrumbs=array('Catalogo Contable',);
?>
<h2>Catalogo de Cuentas Contables</h2>
<br />
<?php echo CHtml::link('Nueva Cuenta',array('catalogoContable/create')); ?>
<br />
<br />
<table>
   <thead>
   <tr>
      <th>Codigo</th>
      <th>Nombre</th>
      <th>Nivel</th>
      <th>Tipo</th>
      <th>&nbsp;</th>
   </tr>
   </thead>
   <tbody>
      <tr>
         <th colspan="5" class="cuenta-root">ACTIVOS</th>
      </tr>
      <?php foreach ($activos as $a): ?>
         <tr class='nivel-<?php echo $a['nivel']; ?> <?php echo $a['tipo']; ?>'>
            <td><?php echo $a['codigo'];  ?></td>
            <td><?php echo $a['name'];    ?></td>
            <td><?php echo $a['nivel'];   ?></td>
            <td><?php echo $a['tipo'];    ?></td>
            <td style="text-align:center;">
               <?php echo CHtml::image(Yii::app( )->request->baseUrl.'/media/images/icons/update.png',NULL,
                                          array('onclick'=>"location.href='".Yii::app()->request->baseUrl."/catalogoContable/update/id/{$a['id']}'")); ?>
               <?php echo CHtml::image(Yii::app( )->request->baseUrl.'/media/images/icons/delete.png',NULL,
               array('onclick'=>"if (!confirm('Esta Seguro?')) return false; else location.href='".Yii::app()->request->baseUrl."/catalogoContable/delete/id/{$a['id']}'")); ?>
            </td>
         </tr>
      <?php endforeach; ?>
      <tr>
         <th colspan="5" class="cuenta-root">PASIVOS</th>
      </tr>
      <?php foreach ($pasivos as $p): ?>
         <tr class='nivel-<?php echo $p['nivel']; ?> <?php echo $p['tipo']; ?>'>
            <td><?php echo $p['codigo'];  ?></td>
            <td><?php echo $p['name'];    ?></td>
            <td><?php echo $p['nivel'];   ?></td>
            <td><?php echo $p['tipo'];    ?></td>
            <td style="text-align:center;">
               <?php echo CHtml::image(Yii::app( )->request->baseUrl.'/media/images/icons/update.png',NULL,
                                          array('onclick'=>"location.href='".Yii::app()->request->baseUrl."/catalogoContable/update/id/{$p['id']}'")); ?>
               <?php echo CHtml::image(Yii::app( )->request->baseUrl.'/media/images/icons/delete.png',NULL,
                                          array('onclick'=>"location.href='".Yii::app()->request->baseUrl."/catalogoContable/delete/id/{$p['id']}'")); ?>
            </td>
         </tr>
      <?php endforeach; ?>
   </tbody>
</table>
<?php echo CHtml::link('Nueva Cuenta',array('catalogoContable/create')); ?>
<br />

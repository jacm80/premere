<script type="text/javascript">
   $(document).ready(function(){
      $('.parents').live('change',function(event){
         $.ajax({
            url: base_url+'/catalogoContable/displayCuenta',
            type:'post',
            data: { cuenta_id:$(event.target).prop('value') },
            async:true,
            datatype:'html',
            success:function(response)
            {
               var nivel = $(event.target).prop('id').split('-')[1];
               if (nivel=='R') nivel=0;
               else nivel++;
               for (var i=nivel ; i<5 ; i++) {
                  //console.debug(i);
                  $('#nivel_'+i).remove( );
               }
               $('#div_childs').before(response);          
            },
            error:function(){}
         });
      });
   });
</script>

<?php $this->breadcrumbs=array('Catalogo Contable'=>array('/catalogoContable'),'Create',); ?>

<h1><?php echo CHtml::link('Catalogo Contable',array('index'));?></h1>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'catalogo-contable',
	'enableAjaxValidation'=>false,
)); ?>


<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row">
	<?php echo $form->labelEx($model,'codigo'); ?>
	<?php echo $form->textField($model,'codigo',array('rows'=>6, 'cols'=>50)); ?>
	<?php echo $form->error($model,'codigo'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'name'); ?>
	<?php echo $form->textArea($model,'name',array('rows'=>2, 'cols'=>50)); ?>
	<?php echo $form->error($model,'name'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'parent_codigo'); ?>
	<?php echo $form->dropDownList($model,'parent_codigo',
                                  CatalogoCuenta::listaRoots(),
                                  array('id'=>'parent_codigo-R','class'=>'parents','name'=>'CatalogoCuenta[parent_codigo][]')); ?>
	<?php echo $form->error($model,'parent_codigo'); ?>
</div>

<div id="div_childs"></div>

<div class="row">
	<?php echo $form->labelEx($model,'tipo'); ?>
	<?php echo $form->dropDownList($model,'tipo',array('G'=>'GRUPO','M'=>'MOVIMIENTO')); ?>
	<?php echo $form->error($model,'tipo'); ?>
</div>

<br />
<br />

<button type="submit">Crear Cuenta</button>

<?php $this->endWidget(); ?>

</div>

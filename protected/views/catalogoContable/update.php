<div class="form">

<?php $this->breadcrumbs=array('Catalogo Contable'=>array('/catalogoContable'),'Update',); ?>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'catalogo-contable',
	'enableAjaxValidation'=>false,
)); ?>

<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

<?php echo $form->errorSummary($model); ?>

<div class="row">
	<?php echo $form->labelEx($model,'codigo'); ?>
	<?php echo $form->textField($model,'codigo',array('rows'=>6, 'cols'=>50,'disabled'=>'disabled')); ?>
	<?php echo $form->error($model,'codigo'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'name'); ?>
	<?php echo $form->textArea($model,'name',array('rows'=>2, 'cols'=>50)); ?>
	<?php echo $form->error($model,'name'); ?>
</div>

<div class="row">
	<?php echo $form->labelEx($model,'tipo'); ?>
	<?php echo $form->dropDownList($model,'tipo',array('G'=>'GRUPO','M'=>'MOVIMIENTO')); ?>
	<?php echo $form->error($model,'tipo'); ?>
</div>

<button type="submit">Actualizar Cuenta</button>

<?php $this->endWidget(); ?>

</div>

<br />

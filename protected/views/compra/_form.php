<?php
$config_autoComplete = array(
                              //'model'=>$model,
                              'attribute'=> 'proveedor_id',
                              'name'=> 'compra_proveedor',
                              'value' => (isset($model->proveedor->razon_social)) ? $model->proveedor->razon_social : '',
                              'sourceUrl' => Yii::app()->request->baseUrl.'/compra/getProveedor',
                              'htmlOptions'=>array('size'=>'30'),
                              'options'=>array(
                                                'showAnim'=>'fold',
                                                'minLenght'=>2,
                                                'select'   =>"js:function(event,ui){\$('#Compra_proveedor_id').val(ui.item['id']);}",
                                                'change'=>"js:function(event,ui){if(!ui.item){\$('#Compra_proveedor_id').val('');} else {getProveedor();}}"
                                                )
                              );

$config_fechaEmision = array(
                              'model'     => $model,
                              'name'      => 'Compra[fecha_emision]',
                              'attribute' => 'fecha_emision',
                              'language'  =>'es',
                              // additional javascript options for the date picker plugin
                              'options'    => array(
                                                   'showAnim'=>'fold',
                                                   'dateFormat' => 'yy-mm-dd',
                                                   'buttonText' => 'Calendario',
                                                   ),
                              'htmlOptions'=> array('style'=>'height:20px;', 'autocomplete'=>'off', 'class'=>'required'),
                              );

$config_fechaVencimiento = array(
                              'model'     => $model,
                              'name'      => 'Compra[fecha_vencimiento]',
                              'attribute' => 'fecha_vencimiento',
                              'language'  =>'es',
                              // additional javascript options for the date picker plugin
                              'options'    => array(
                                                   'showAnim'=>'fold',
                                                   'dateFormat' => 'yy-mm-dd',
                                                   'buttonText' => 'Calendario',
                                                   ),
                              'htmlOptions'=> array('style'=>'height:20px;', 'autocomplete'=>'off', 'class'=>'required'),
                              );

?>

<div class="form">
<?php echo $form->errorSummary($model); ?>
<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>
<table class="table-form">
   <tr>
      <td><?php echo $form->labelEx($model,'proveedor_id'); ?></td>
      <td>
         <?php $this->widget('zii.widgets.jui.CJuiAutoComplete',$config_autoComplete); ?>
         <?php echo $form->hiddenField($model,'proveedor_id'); ?>
		   <?php echo $form->error($model,'proveedor_id'); ?>
      </td> 
      <td><?php echo $form->labelEx($model,'fecha_registro'); ?></td>
      <td>
         <?php echo $form->textField($model,'fecha_registro',array('disabled'=>'disabled','value'=>date('Y-m-d h:m:i'))); ?>
         <?php echo $form->error($model,'fecha_registro'); ?>
      </td>
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'numero_factura'); ?></td>
      <td>
         <?php echo $form->textField($model,'numero_factura'); ?>
         <?php echo $form->error($model,'numero_factura'); ?>
      </td>
      <td><?php echo $form->labelEx($model,'fecha_emision'); ?></td>
      <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',$config_fechaEmision); ?></td> 
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'fecha_vencimiento'); ?></td>
      <td><?php $this->widget('zii.widgets.jui.CJuiDatePicker',$config_fechaVencimiento); ?></td> 
      <td>&nbsp;</td>
      <td>&nbsp;</td>
   </tr>
</table>
</div>

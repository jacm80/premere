<form method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/compra/getMateriales" id="materialForm">
<?php
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'           => 'material-grid',
                                                      'filter'       => $model,
                                                      'dataProvider' => $model->search( ),
                                                      'columns'      => $columns,
                                                   ));
?>
</form>

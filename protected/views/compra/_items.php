<?php 
$i=0; 
?>
<?php foreach ($model->items as $item): ?>
   <tr id="material-<?php echo $item->id; ?>" class="sup">
      <td><?php echo $item->id; ?></td>
      <td><?php echo $item->material->catalogoMaterial->descripcion ?></td>
      <td><?php echo $item->material->catalogoMaterial->barcode;    ?></td>
      <td><?php echo $item->cantidad;        ?></td>
      <td><?php echo $item->precio_unitario; ?></td>
      <td><?php echo $item->iva; ?></td>
      <td><?php echo ($item->precio_unitario * $item->cantidad) + $item->iva; ?></td>
      <td>&nbsp;</td>
   </tr>
   <tr class="inf">
      <td>&nbsp;</td>
      <td>Almacen: <?php echo $item->almacen->descripcion; ?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
   </tr>
   <?php 
      $this->baseImponible +=  ($item->cantidad * $item->precio_unitario); 
      $this->iva   +=  $item->iva;
      $i++;
   ?>
<?php endforeach; 
      $this->total += $this->baseImponible + $this->iva;
?>

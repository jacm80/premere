<table id="detalle-compra">
   <thead>
      <tr>
         <th>ID</th>
         <th>Material</th>
         <th>Barcode</th>
         <th style="width:10%">Cantidad</th>
         <th style="width:12%">Precio Unitario</th>
         <th style="width:10%">IVA</th>
         <th>TOTAL</th>
         <th>&nbsp;</th>
      </tr>
   </thead>
   <tbody id="detalle-compra-body">
      <?php if (!$model->isNewRecord): ?>
            <?php $this->renderPartial('_items',array('model'=>$model)); ?>
      <?php endif; ?>
   </tbody>
   <tfoot>
      <tr>
         <td colspan="8" style="text-align:right;"><button id="agregar" type="button">Agregar</button></td>
      </tr>
   </tfoot>
</table>

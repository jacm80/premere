<?php
/* @var $this CompraController */
$this->breadcrumbs=array('Compra'); ?>

<?php
   $columns = array(
                     array(
                           'type'=>'raw',
                           'header'=>'Proveedor',
                           'name'=>'proveedor_id',
                           'value'=>'$data->proveedor->razon_social'
                           ),
                     'numero_factura',
                     'fecha_emision',
                     'fecha_vencimiento',
                     array(
                           'type'=>'raw',
                           'header'=>'Monto',
                           'name'=>'monto',
                           'value'=>'$data->base_imponible + $data->iva'
                          ),
                     'estatus'
                  );
   /**/
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'          => 'compra-grid',
                                                      'filter'      => $model,
                                                      'dataProvider'=> $model->search( ),
                                                      'columns'     => $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/compra/view/id/"+xid;
                                                      }'
                                                   ));
?>
<div style="text-align:center;">
   <button type="button" onclick="location.href=base_url+'/compra/create'">Nueva Compra</button>
</div>

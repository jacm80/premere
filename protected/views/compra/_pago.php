<table id="pagos">
   <thead>
   <tr>
      <th>id</th>
      <th>Forma de Pago</th>
      <th>Fecha</th>
      <th>Banco</th>
      <th>No. Comprobante</th>
      <th>Registrado por</th>
      <th>Descripcion</th>
      <th>Monto</th>
      <th>&nbsp;</th>
   </tr>
   </thead>
   <tbody id="cpagos">
      <?php if (!$model->isNewRecord): ?>
      <?php foreach($model->pagos as $p): ?>
      <tr class="trBd">
         <td><?php echo $p->id; ?></td>
         <td><?php echo $p->formaPago->descripcion; ?></td>
         <td><?php echo $p->fecha;  ?></td>
         <td><?php echo $p->empresaBanco->banco->descripcion; ?></td>
         <td><?php echo (!empty($p->numero_comprobante)) ? $p->numero_comprobante:'NO APLICA'; ?></td>
         <td><?php echo Yii::app()->user->um->loadUserById($p->user_id,TRUE)->username; ?></td>
         <td><?php echo $p->descripcion;  ?></td>
         <td><?php echo $p->monto; ?></td>
         <td><img src="<?php echo Yii::app()->request->baseUrl; ?>/media/images/icons/cancel.png"  /></td>
      </tr>
      <?php endforeach; ?>
      <?php endif; ?>
      <tr id="tr_ref" style="display:none;"></tr>
      </tbody>
   <tfoot>
      <tr>
         <?php #if ( Yii::app()->getSession()->get('orden_status')!='FACTURADA' ): ?>
            <td colspan="9" style="text-align:center;">
               <button id="agregarPago" type="button">Agregar Pago</button>
            </td>
         <?php #else: ?>
            <!-- <td colspan="9" style="text-align:center;">&nbsp;</td> -->
         <?php #endif; ?>
      </tr>
   </tfoot>
</table>

<?php
// estilos
HtmlApp::loadCss('compra');
HtmlApp::loadCss('form');
HtmlApp::loadCss('pagos_display');
//javascript
HtmlApp::loadJs('compra');
HtmlApp::loadjQueryUI();
HtmlApp::loadJs('jquery.yiigridview');
HtmlApp::loadJs('mustache-master/mustache');
HtmlApp::loadJs('jquery-Mustache/jquery.mustache');

$cfg_form = array(
      'id'=>'compraForm',
      'enableClientValidation'=>TRUE,
      'enableAjaxValidation'=>FALSE,
      'clientOptions'=>array('validateOnSubmit'=>TRUE)
   );

/* @var $this CompraController */

$this->breadcrumbs=array(
	'Compra'=>array('/compra'),
	'Create',
);


$form = $this->beginWidget('CActiveForm',$cfg_form);


$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
         'Compra'   => array('id'=>'Compra', 'content'=>$this->renderPartial('_form'   ,array('model'=> $model,'form'=>$form),true)),
         'Detalle'  => array('id'=>'Detalle','content'=>$this->renderPartial('_detalle',array('model'=> $model),true)),
         'Pagos'    => array('id'=>'Pagos',  'content'=>$this->renderPartial('_pago'   ,array('model'=> $model),true))
    ),
    // additional javascript options for the tabs plugin
   'options' => array('collapsible'=>true,'selected'=>0,/*'disabled'=>array(1)*/), 
   ));
?>

<div id="dialog"></div>

<table class="table-form">
   <tr>
      <td colspan="2" class="summary-label"><div>Base Imponible</div></td>
      <td class="summary-value">Bs.&nbsp;
         <span id="baseImponible"><?php echo !empty($this->baseImponible) ? number_format($this->baseImponible,2,'.',',') : '0,00'; ?></span>
         <input type="hidden" name="Compra[iva]" id="hiddenIva">
      </td>
   </tr>
   <tr>
      <td colspan="2" class="summary-label"><div>IVA 12%</div></td>
      <td class="summary-value">Bs.&nbsp;
               <span id="montoIva"><?php echo !empty($this->iva) ? $this->iva : '0,00'; ?></span>
               <input type="hidden" name="Compra[base_imponible]" id="hiddenBaseImponible">
      </td>
   </tr>
   <tr>
      <td colspan="2"  class="summary-label"><div>TOTAL a Pagar</div></td>
      <td class="summary-value">Bs.&nbsp;<span id="totalPagar"><?php echo !empty($this->total) ? $this->total : '0,00'; ?></span></td>
   </tr>
</table>

<div style="border: 1px solid #e3e3e3; padding: 4px; text-align:center;">
  <div class="row buttons">
		<?php echo CHtml::Button($model->isNewRecord ? 'Registrar' : 'Actualizar',array('id'=>'guardarCompra')); ?>
		<?php echo CHtml::Button('Listado',array('id'=>'listarCompra')); ?>
	</div>
</div>

<?php $this->endWidget( ); ?>

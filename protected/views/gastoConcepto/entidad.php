<form method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/gastoConcepto/updateGrid" id="entidad">
<?php
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'           => 'entidad-grid',
                                                      'filter'       => $model,
                                                      'dataProvider' => $model->search( ),
                                                      'columns'      => $columns,
                                                   ));
?>
</form>

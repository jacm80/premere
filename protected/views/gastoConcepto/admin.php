<?php
$this->breadcrumbs=array(
	'Gasto Conceptos'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Listar GastoConcepto', 'url'=>array('index')),
	array('label'=>'Crear GastoConcepto', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gasto-concepto-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Gasto Conceptos</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'gasto-concepto-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
      array(
      'name' => 'gasto_tipo_id',
      'value'=> '$data->gastoTipo->descripcion'
      ),
		'descripcion',
      array(
         'type'  => 'raw',
		   'name'  => 'tipo',
         'value' => '($data->tipo=="P") ? "Proveedor" : "Beneficiario"' 
      ),
      array(
         'type'=>'raw',
		   'name'=>'entidad_id',
         'value'=>'($data->tipo=="P") ? Proveedor::getDescripcionById($data->entidad_id) : Beneficiario::getDescripcionById($data->entidad_id)'
      ),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

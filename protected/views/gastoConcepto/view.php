<?php
$this->breadcrumbs=array(
	'Gasto Conceptos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' GastoConcepto', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' GastoConcepto', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' GastoConcepto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' GastoConcepto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' GastoConcepto', 'url'=>array('admin')),
);


?>

<h1>Concepto de Gasto: <?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
      array(
         'type'   => 'raw',
		   'name'   => 'gasto_tipo_id',
         'value'  => $model->gastoTipo->descripcion
      ),
		'descripcion',
      array(
         'type'=>'raw',
		   'name'=>'tipo',
         'value'=>($model->tipo=="P") ? "Proveedor":"Beneficiario"
      ),
      array(
         'type'=>'raw',
		   'name'=>'entidad_id',
         'value'=>($model->tipo=="P") ? Proveedor::getDescripcionById($model->entidad_id)  :  Beneficiario::getDescripcionById($model->entidad_id)
      ),
	),
)); ?>

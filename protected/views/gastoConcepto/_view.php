<?php  $tipo = array('P'=>'Proveedor','B'=>'Beneficiario'); ?>

<div class="view">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gasto_tipo_id')); ?>:</b>
	<?php echo CHtml::encode($data->gastoTipo->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tipo')); ?>:</b>
	<?php echo CHtml::encode($tipo[$data->tipo]); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('entidad_id')); ?>:</b>
	<?php echo CHtml::encode(($data->tipo=='P') ? Proveedor::getDescripcionById($data->entidad_id):Beneficiario::getDescripcionById($data->entidad_id)); ?>
	<br />
</div>

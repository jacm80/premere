<?php
$this->breadcrumbs=array(
	'Gasto Conceptos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' GastoConcepto', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' GastoConcepto', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' GastoConcepto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' GastoConcepto', 'url'=>array('admin')),
);
?>

<h1>Update GastoConcepto <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
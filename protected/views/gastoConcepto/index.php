<?php
$this->breadcrumbs=array(
	'Gasto Conceptos',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' GastoConcepto', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' GastoConcepto', 'url'=>array('admin')),
);
?>

<h1>Gasto Conceptos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
$this->breadcrumbs=array(
	'Gasto Conceptos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' GastoConcepto', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' GastoConcepto', 'url'=>array('admin')),
);
?>

<h1>Crear GastoConcepto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

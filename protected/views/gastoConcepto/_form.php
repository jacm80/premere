<?php HtmlApp::loadjQueryUI(); ?>
<?php HtmlApp::loadJs('jquery.yiigridview'); ?>
<!-- <script type="text/javascript" src="/premere/assets/bdd49dc2/gridview/jquery.yiigridview.js"></script>-->

<style>
   .ui-widget{ border: 1px solid #e3e3e3 !important; }
</style>
<script type="text/javascript">
   var tipo      = { P:'Proveedor',   B:'Beneficiario'  };
   var tipoLabel = { P:'Proveedores', B:'Beneficiarios' };
   $(document).ready(function(){
      $('#GastoConcepto_entidad_id').focus(function( ){
                 $.ajax({
                           url: base_url + '/GastoConcepto/getEntidad',
                           type:'post',
                           async: true,
                           dataType: 'html',
                           data: { entidad: tipo[$('#GastoConcepto_tipo').val()] },
                           success: function(response){ $('#dialog').html(response); },
                           error:   function(){ alert('error'); }
                        });
                  $('#dialog').dialog({ 
                              modal: true, 
                              title: 'Lista de '+tipoLabel[$('#GastoConcepto_tipo').val( )],  
                              width: 600,
                              height: 300,
                              buttons:{
                                       'enviar':function(){
                                                           $.ajax({
                                                                     url: base_url + '/GastoConcepto/updateGrid',
                                                                     type:'get',
                                                                     async: true,
                                                                     dataType: 'html',
                                                                     data: $('#entidad').serialize(),
                                                                     success: function(response){ $('#dialog').html(response); },
                                                                     error:   function(){ alert('error'); }
                                                                  });
                                                          },
                                       'cancelar': function(){ $(this).dialog("close"); }
                                    }
                             });
      });

      
   $('body').delegate('#entidad-grid tbody > tr','click',function(){
      var id = $.fn.yiiGridView.getKey('entidad-grid',$('#entidad-grid tbody > tr').index(this));
      var descripcion = $(this).children().eq(2).text() +' '+ $(this).children().eq(3).text() ;
      $('#GastoConcepto_entidad_id').val(id);
      $('#GastoConcepto_entidad_descripcion').html(descripcion);
      $('#dialog').dialog('close');
   });


   });
</script>

<div id="dialog"></div>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gasto-concepto-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'gasto_tipo_id'); ?>
		<?php echo $form->dropDownList($model,'gasto_tipo_id',GastoTipo::ListaTipoGastos()); ?>
		<?php echo $form->error($model,'gasto_tipo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'descripcion'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tipo'); ?>
		<?php echo $form->dropDownList($model,'tipo',array('P'=>'PROVEEDOR','B'=>'BENEFICIARIO')); ?>
		<?php echo $form->error($model,'tipo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'entidad_id'); ?>
		<?php echo $form->textField($model,'entidad_id',array('style'=>'width:50px') ); ?>&nbsp;
            <span id="GastoConcepto_entidad_descripcion">
               <?php if (!empty($model->tipo)): ?>
                  <?php echo ($model->tipo=='P') ? Proveedor::getDescripcionById($model->entidad_id) :  Beneficiario::getDescripcionById($model->entidad_id); ?>
               <?php endif; ?>
            </span>
		<?php echo $form->error($model,'entidad_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

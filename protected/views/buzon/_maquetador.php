<ul>
   <li>Seleccione la fila para mostrar <strong>detalles del trabajo</strong></li>  
   <li>Haga click en la caja de verificacion de los trabajos ya <strong>dise&ntilde;ados</strong></li>
   <li><strong>Guarde</strong> los cambios</li>
</ul>

<?php
   $url = Yii::app()->request->baseUrl;
   $columns= array(
                  array(
                     'type'   => 'raw',
                     'name'   => 'numero',
                     'header' => 'Orden No.',
                     'value'  => 'sprintf("%06d",$data->numero)'
                  ),
                  'rif',
                  'firma_razon_social',
                  'sucursal',
                  'fecha_entrada',
                  'fecha_entrega',
                  array(
                        'header'        => 'Diseñado',
                        'class'         => 'CCheckBoxColumn',
                        'name'          => 'orden_status_id',
                        'selectableRows'=> 100,
                        'checked'       => 'in_array($data->orden_status_id,array(2))',
                        'id'            => 'chk'
                        )
                  );
   
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'           => 'buzon-grid',
                                                      'filter'       => $model,
                                                      'dataProvider' => $model->search_buzon( ),
                                                      'columns'      => $columns,
                                                    ));
?>
<div style="text-align:center"><button id="BtnGuardar">Guardar</button></div>
<div id="detalle_orden"></div>

<ul>
   <li>Seleccione la fila para mostrar <strong>detalles del trabajo</strong></li>  
   <li>Haga click en la caja de verificacion de los trabajos ya <strong>Impresos</strong></li>
   <li><strong>Guarde</strong> los cambios</li>
</ul>

<?php
   $url = Yii::app()->request->baseUrl;
   $columns= array(
                  //array('name'=>'id','header'=>'Numero'),
                  array(
                     'type'   => 'raw',
                     'name'   => 'numero',
                     'header' => 'Orden No.',
                     'value'  => 'sprintf("%06d",$data->numero)'
                  ),
                  'rif',
                  'firma_razon_social',
                  'sucursal',
                  'fecha_entrada',
                  'fecha_entrega',
                  array(
                        'header'        => 'Diseñado',
                        'class'         => 'CCheckBoxColumn',
                        'name'          => 'orden_status_id',
                        'selectableRows'=> 100,
                        'checked'       => 'in_array($data->orden_status_id,array(3))',
                        'id'            => 'chk'
                        )
                  );
   
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'           => 'buzon-grid',
                                                      'filter'       => $model,
                                                      'dataProvider' => $model->search_buzon( ),
                                                      'columns'      => $columns,
                                                      'selectionChanged'=>"js:function(id){
                                                         xid = $.fn.yiiGridView.getSelection(id);
                                                         //alert(xid);
                                                         /*
                                                         $.ajax({
                                                            url: '{$url}/buzon/getDetalle',
                                                            async:true,
                                                            dataType: 'html',
                                                            type: 'post',
                                                            data: { id : xid },
                                                            success:function(response){ 
                                                               $('#detalle_orden').html(response);
                                                            },
                                                            error:function(){ alert('Fallo el servidor'); }
                                                         });
                                                         */
                                                      }",
                                                   ));
?>

<div style="text-align:center"><button id="BtnGuardar">Guardar</button></div>
<div id="detalle_orden"></div>

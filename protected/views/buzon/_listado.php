<style>
th,td { padding:4px;}
</style>
<?php
   $titulo     =  ($scope == 'maquetadas')   ? 'Imprimir' : 'Entregar';
   $titulo_chk =  ($scope == 'maquetadas'  ) ? 'Impreso'  : 'Cancelado';
?>
<body style="font-family: arial;">
<br />
<br />
<h1 style="text-align:center;font-size:14pt;">Lista de Trabajos Por <?php echo $titulo; ?></h1>
<p>Dia: <?php echo date('d/m/Y')?> Hora: <?php echo date('h:m:s')?></p>
<table class="table-form" style="width:100%;">
   <thead>
   <tr>
      <th>RIF</th>
      <th>Firma</th>
      <th>Numero de Orden</th>
      <th>Fecha Entrega</th>
      <th><?php echo $titulo_chk; ?></th>
   </tr>
   </thead>
   <tbody>
     <?php foreach($model as $o ): ?>
         <tr>
            <td><?php echo $o->cliente->rif;        ?></td>
            <td><?php echo $o->firma->razon_social; ?></td>
            <td><?php echo sprintf('%0d6',$o->id);  ?></td>
            <td><?php echo $o->fecha_entrega;       ?></td>
            <td style="text-align:center"><input type="checkbox" id="<?php echo $titulo_chk; ?>"></td>
         </tr>
     <?php endforeach; ?> 
   </tbody>
</table>
</body>

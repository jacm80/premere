<?php HtmlApp::loadJs('jquery.yiigridview'); ?>

<script type="text/javascript">
   $(document).ready(function(){
      $('#BtnGuardar').click(function(){
         idsel   = $.fn.yiiGridView.getChecked('buzon-grid','chk');
         $.ajax({
            url: base_url + '/buzon/cambiarEstatus',
            async: true,
            type: 'post',
            data: { ids:idsel },
            success:function(resp){ 
               location.href = base_url+'/buzon/Listado?scope='+resp;
            },
            error:  function( ){ alert('error al cambiar el estatus') }
         });
      });
   });
</script>

<?php 
   
if (
   Yii::app()->user->checkAccess('admin') 
      OR 
   Yii::app()->user->checkAccess('vendedor')
   ) $tmpl='_admin';

else if (Yii::app()->user->checkAccess('maquetador')) $tmpl='_maquetador';

$this->renderPartial($tmpl,array('model'=>$model));

?>

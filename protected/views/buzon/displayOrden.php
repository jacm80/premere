<style>
   #n-orden
   {
      margin: 0px;
      padding: 4px;
      background: #f1f1f1;
      font-size: 14pt;
      text-align: center;
   }
   #display-orden
   {
      padding: 0px;
      margin: 0px;
      border: 1px solid #e3e3e3;
   }
   #display-orden p
   {
      margin-left: 20px;
      margin-right: 20px;
      line-height: 18pt;
      text-align: justify;
   }
   #display-orden span
   {
      font-weight: bold;
      color: blue;
   }
   #trabajos 
   {
      width: 95%;
      margin: 0 auto;
      border: 1px solid #e3e3e3;   
   }
   #trabajos th 
   {
      background: gray;
      color: #fff;
      border-left: 1px solid white;
   }
   #trabajos td
   {
      border-left: 1px solid #e3e3e3;
   }
   #trabajos tr
   {
      background: #f1f1f1;
   }
   #trabajos tr.blanco
   {
      background: #fff;
   }
</style>
<div id="display-orden">
   <div id="n-orden">No. Orden: <strong> <?php echo $orden['id']; ?></strong></div>
   <br />
   <p>
      <span>Razon Social:</span> <?php echo $orden['razon_social']; ?>
      <br />
      <span>Direcciones Incluidas:</span> <?php echo $orden['direcciones_incluidas']; ?>
      <br />
      <span>Firma:</span> <?php echo $orden['firma']; ?>
      <br />
      <span>Sucursal:</span> <?php echo $orden['sede']; ?>
      <br />
      <span>Sede:</span> <?php echo $orden['sede']; ?>
      <br />
      <span>Telefonos Incluidos:</span> <?php echo $orden['telefonos_incluidos']; ?>
      <br />
      <span>Publicidad Incluida:</span> <?php echo $orden['publicidad_incluida']; ?>
      <br />
      <span>Observaciones:</span> <?php echo $orden['observaciones']; ?>
      <br />
      <span>Status:</span> <?php echo $orden['orden_status']; ?>
   </p>
   <table id="trabajos">
      <thead>
         <tr>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Numeracion</th>
         </tr>
      </thead>
      <tbody>
         <?php $i=0; ?>
         <?php foreach($orden['items'] as $item): ?>
            <tr <?php if ($i % 2 == 0): ?>class="blanco"<?php endif; ?>>
               <td><?php echo $item['producto']; ?></td>
               <td><?php echo $item['cantidad']; ?></td>
               <td><?php echo $item['numeracion']; ?></td>
            </tr>
            <?php $i++; ?>
         <?php endforeach; ?>
      </tbody>
   </table>
   <br />
   <p style="text-align:center;">
      <button>Cambiar el estatus a Diseñado</button>
   </p>
</div>
<?php #echo HtmlApp::debug($orden); ?>

<?php
$this->breadcrumbs=array(
	'Materials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Material', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Material', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Material', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Material', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Material', 'url'=>array('admin')),
);
?>

<h1>View Material #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
      array(
		'name'=>'catalogo_material_id',
      'value'=>$model->catalogoMaterial->descripcion,
      ),
       array(
		'name'=>'almacen_id',
      'value'=>$model->almacen->descripcion,
      ),
		'punto_reorden',
		'existencia_unidad',
      'existencia_detallado'
	),
)); ?>

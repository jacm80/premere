<form method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/material/getCatalogo" id="CatalogoMaterial">
<?php
   $columns = array(
                     'id',
                     'barcode',
                     'descripcion',
                     array(
                        //'label'=> 'tipo',
                        'name' => 'material_tipo_id',
                        'value'=> '$data->materialTipo->descripcion'
                     ),
                     array(
                        //'label' =>'marca',
                        'name'  =>'marca_id',  
                        'value' =>'$data->marca->descripcion'
                     )
                  );

   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'           => 'catalogo-grid',
                                                      'filter'       => $model,
                                                      'dataProvider' => $model->search( ),
                                                      'columns'      => $columns
                                                   ));
?>
</form>

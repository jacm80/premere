<?php
$this->breadcrumbs=array(
	'Materials'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Material', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Material', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Material', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Material', 'url'=>array('admin')),
);
?>

<h1>Update Material <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
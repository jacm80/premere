<?php
$this->breadcrumbs=array(
	'Materials'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Material', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Material', 'url'=>array('admin')),
);
?>

<h1>Crear Material</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Materials'=>array('index'),
	'Gestion',
);

$this->menu=array(
	array('label'=>'Listar Material', 'url'=>array('index')),
	array('label'=>'Crear Material', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('material-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Gestion de Materiales</h1>

<p>
Opcionalmente puedes utilizar operadores de comparacion (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
&oacute; <b>=</b>) al comienzo de cada filtro de la lista.
</p>

<?php echo CHtml::link('Busqueda Avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'material-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
      array(
		   'name'=>'catalogo_material_id',
         'value'=>'$data->catalogoMaterial->descripcion',
         'filter'=>CHtml::listData(CatalogoMaterial::model( )->findAll(),'id','descripcion')
		),
      array(
		   'name'=>'almacen_id',
         'value'=>'$data->almacen->descripcion',
         'filter'=>CHtml::listData(Almacen::model( )->findAll('id>0'),'id','descripcion')
		),
      'punto_reorden',
		'existencia_unidad',
      'existencia_detallado',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('catalogo_material_id')); ?>:</b>
	<?php echo CHtml::encode($data->catalogoMaterial->descripcion); ?>
	<br />

   <b><?php echo CHtml::encode($data->getAttributeLabel('almacen_id')); ?>:</b>
	<?php echo CHtml::encode($data->almacen->descripcion); ?>
	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('punto_reorden')); ?>:</b>
	<?php echo CHtml::encode($data->punto_reorden); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('existencia_unidad')); ?>:</b>
	<?php echo CHtml::encode($data->existencia_unidad); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('existencia_detallado')); ?>:</b>
	<?php echo CHtml::encode($data->existencia_detallado); ?>
	<br />

</div>

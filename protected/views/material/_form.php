<?php HtmlApp::loadjQueryUI(); ?>
<script type="text/javascript" src="/premere/assets/bdd49dc2/gridview/jquery.yiigridview.js"></script>
<script type="text/javascript">
$(document).ready(function(){
   $('#Material_catalogo_material_id').focus(function(){
      //alert('HOLAAAAA');
      $.ajax({
            url: base_url + '/material/getCatalogo',
            //type:'post',
            async: true,
            dataType: 'html',
            //data: { entidad: tipo[$('#GastoConcepto_tipo').val()] },
            success: function(response){ $('#dialog').html(response); },
            error: function(){ alert('error'); }
         });
      
      $('#dialog').dialog({ 
         modal: true, 
         title: 'Catalogo de Materiales',  
         width: 600, height:300,
         buttons:{
                  'enviar':function(){
                                       $.ajax({
                                                url: base_url + '/material/getCatalogo',
                                                type:'get',
                                                async: true,
                                                dataType: 'html',
                                                data: $('#CatalogoMaterial').serialize(),
                                                success: function(response){ $('#dialog').html(response); },
                                                error:   function(){ alert('error'); }
                                              });
                                     },
                  'cancelar': function(){ $(this).dialog("close"); }
                  }
      });

      $('body').delegate('#catalogo-grid tbody > tr','click',function(){
         var id = $.fn.yiiGridView.getKey('catalogo-grid',$('#catalogo-grid tbody > tr').index(this));
         var descripcion = $(this).children().eq(2).text() ;
         $('#Material_catalogo_material_id').val(id);
         $('#CatalogoMaterial_descripcion').html(descripcion);
         $('#dialog').dialog('close');
      });



   });
});
</script>

<div id="dialog"></div>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'material-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'catalogo_material_id'); ?>
		<?php echo $form->textField($model,'catalogo_material_id',array('style'=>'width:60px;')); ?>
         
         <span id="CatalogoMaterial_descripcion"></span>
		
      <?php echo $form->error($model,'catalogo_material_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'almacen_id'); ?>
		<?php echo $form->dropDownList($model,'almacen_id',Almacen::getListAlmacenes()); ?>
		<?php echo $form->error($model,'almacen_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'punto_reorden'); ?>
		<?php echo $form->textField($model,'punto_reorden'); ?>
		<?php echo $form->error($model,'punto_reorden'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'existencia_unidad'); ?>
		<?php echo $form->textField($model,'existencia_unidad'); ?>
		<?php echo $form->error($model,'existencia_unidad'); ?>
	</div>

   <div class="row">
		<?php echo $form->labelEx($model,'existencia_detallado'); ?>
		<?php echo $form->textField($model,'existencia_detallado'); ?>
		<?php echo $form->error($model,'existencia_detallado'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

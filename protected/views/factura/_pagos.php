<?php HtmlApp::loadCss('pagos_display'); ?>
<table id="pagos">
   <thead>
   <tr>
      <th>id</th>
      <th>Forma de Pago</th>
      <th>Fecha</th>
      <th>Banco</th>
      <th>No. Comprobante</th>
      <th>Registrado por</th>
      <th>Descripcion</th>
      <th>Monto</th>
      <th>&nbsp;</th>
   </tr>
   </thead>
   <tbody id="cpagos">
      <?php foreach($pagos as $p): ?>
      <tr class="trBd">
         <td><?php echo $p->id; ?></td>
         <td><?php echo $p->formaPago->descripcion; ?></td>
         <td><?php echo $p->fecha; ?></td>
         <td><?php echo $p->banco->descripcion; ?></td>
         <td><?php echo (!empty($p->numero_comprobante))?$p->numero_comprobante:'NO APLICA'; ?></td>
         <td><?php echo $p->usuario->username; ?></td>
         <td><?php echo $p->descripcion; ?></td>
         <td><?php echo $p->monto; ?></td>
         <td>&nbsp;</td>
      </tr>
      <?php endforeach; ?>
      <tr id="tr_ref" style="display:none;"></tr>
      </tbody>
   <tfoot>
      <tr>
         <?php if ( Yii::app()->getSession()->get('orden_status')!='FACTURADA' ): ?>
            <td colspan="9" style="text-align:center;">
               <button id="agregar">Agregar Pago</button>
            </td>
         <?php else: ?>
            <td colspan="9" style="text-align:center;">&nbsp;</td>
         <?php endif; ?>
      </tr>
   </tfoot>
</table>

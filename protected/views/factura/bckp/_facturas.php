<?php HtmlApp::loadCss('facturas_display'); ?>
<table id="facturas">
   <thead>
      <tr>
         <th>Producto</th>
         <th>Cantidad</th>
         <th>Base Imponible</th>
         <th>Iva</th>
         <th>Total a Pagar</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <th colspan="5" style="text-align:center;" class="title">DOCUMENTOS FISCALES</th>
      </tr>
      <tr>
         <th colspan="5"><b class="n-factura">Factura N. 1001<b></th>
      </tr>
      <tr>
         <td>Talonarios Factura 1/18 Original y Copia Papel Quimico 1 Color</td>
         <td class="amount">6</td>
         <td class="amount">240</td>
         <td class="amount">14,4</td>
         <td class="amount">254,4</td>
      </tr>
      <tr>
         <th colspan="5"><b class="n-factura">Factura N. 1002</b></th>
      </tr>
      <tr>
         <td>Talonarios Factura 1/18 Original y Copia Papel Quimico 1 Color</td>
         <td class="amount">6</td>
         <td class="amount">240</td>
         <td class="amount">14,4</td>
         <td class="amount">254,4</td>
      </tr>
      <tr>
         <th colspan="5" style="text-align:center;" class="title">PAPELERIA EN GENERAL</th>
      </tr>
      <tr>
         <th colspan="5"><b class="n-factura">Factura N. 1003</b></th>
      </tr>
      <tr>
         <td>Tarjetas de Presentacion medida 20x12 pcs. 1 Color</td>
         <td class="amount">300</td>
         <td class="amount">36</td>
         <td class="amount">14,4</td>
         <td class="amount">314,4</td>
      </tr>
      <tr>
         <td>Hojas Membrete Tamaño Carta</td>
         <td class="amount">300</td>
         <td class="amount">36</td>
         <td class="amount">14,4</td>
         <td class="amount">314,4</td>
      </tr>
   </tbody>
</table>

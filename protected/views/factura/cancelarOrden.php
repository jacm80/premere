<?php HtmlApp::loadCss('form'); ?>

<div style="border: 1px solid #e3e3e3;width:20%;margin:0 auto;text-align:center;border-radius:5px;padding:6px;margin-bottom:20px;">
   Orden No:&nbsp;<strong><?php echo sprintf('%06d',$model->id); ?></strong>
</div>


<div style="float:left; width:48%; margin-right:20px;">
<fieldset>
<legend>Detalles de la Orden</legend>
<table class="table-form">
	<thead>
		<tr>
			<th>Sub-Total</th>
			<th>IVA</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $model->base_imponible; ?></td>
			<td><?php echo $model->iva; ?></td>
			<td><?php echo $model->base_imponible+$model->iva; ?></td>
		</tr>
	</tbody>
</table>
<table class="table-form">
	<thead>
		<tr>
			<th>Pagado</th>
			<th>Resta</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php echo $model->saldo; ?></td>
			<td><?php echo $model->resta; ?></td>
		</tr>
	</tbody>
</table>
	<fieldset style="margin: 0 auto;">
    <legend>Facturas</legend>
		<table class="table-form" style="margin:0 auto;">
      		<thead>
      			<tr>
      				<th>Numero</th>
      				<th>Base Imponible</th>
      				<th>IVA</th>
      				<th>Total</th>
      			</tr>
      		</thead>
      		<tbody>
      		<?php foreach($model->facturas as $f): ?>
        		<tr>
        		 	<td><?php echo $f->numero; ?></td>
        		 	<td><?php echo $f->base_imponible; ?></td>
        		 	<td><?php echo $f->iva; ?></td>
        		 	<td><?php echo $f->base_imponible+$f->iva ?></td>
        		</tr>
  	  		<?php endforeach; ?>
  	  		</tbody>
  	  	</table>
	</fieldset>
</fieldset>
<br />
<div style="text-align:center;">
	<button id="BtnProcesar">Procesar</button>
</div>
</div>


<div id="lyt-izq" style="width:48%; float:left;">
<fieldset>
   <legend>Formas de Pago</legend>
   <table class="table-form">
      <thead>
         <tr>
            <th>Descripcion</th>
            <th>Monto</th>
         </tr>
      </thead>
      <tbody>
         <?php $i=0; ?>   
         <?php foreach(FormaPago::model()->findAll( ) as $fp): ?>
               <tr>
                  <td><?php echo CHtml::checkBox('forma_pago_id[]',null,array('value'=>$fp->id,'class'=>'forma-pago-chk')); ?><?php echo $fp->descripcion; ?></td>
                  <td><?php echo CHtml::textField('monto[]','0.00',array('id'=>"monto-{$fp->id}",'class'=>'forma-pago','disabled'=>'disabled')); ?></td>
               </tr>
               <?php if ($fp->requiere_comprobante==1): ?>
                  <tr id="espBC-<?php echo $fp->id; ?>" class="hide">
                     <td style="background-color: #B8E8F4;">Banco</td>
                     <td style="background-color: #B8E8F4;"><?php echo CHtml::dropDownList("banco_id-{$fp->id}",
                                                         null,
                                                        Banco::getlistBancos(),
                                                        array('id'=>"banco_id-{$fp->id}"));?>
                     </td>
                  </tr>
                  <tr id="espNC-<?php echo $fp->id; ?>" class="hide">
                     <td style="background-color: #B8E8F4;">Numero de Comprobante</td>
                     <td style="background-color: #B8E8F4;">
                        <input type="ẗext" name="comprobante" 
                               class="numero-comprobante" 
                               id="numero_comprobante-<?php echo $fp->id;?>">
                     </td>
                  </tr>
                  <?php $i++; ?>
               <?php endif;?>
         <?php endforeach; ?> 
      </tbody>
   </table>
</fieldset>
</div>
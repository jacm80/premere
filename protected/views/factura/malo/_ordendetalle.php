<h1>Documentos Fiscales</h1>
<table>
   <thead>
      <tr>
         <th>ID</th>
         <th>Producto</th>
         <th>Cantidad</th>
         <th>Monto</th>
         <th>Iva</th>
         <th>Total a Pagar</th>
         <th>Factura</th>
      </tr>
      <tbody>
         <?php foreach($fiscal as $item): ?>
            <td><?php $item['id'];       ?></td>
            <td><?php $item['producto']; ?></td>
            <td><?php $item['cantidad']; ?></td>
            <td><?php $item['monto']; ?></td>
            <td><?php $item['iva']; ?></td>
            <td><a href="javascript:alert('EL PDF');"><?php $item['factura_numero']; ?></a></td>
         <?php endforeach; ?>
      </tbody>
   </thead>
</table>
<h1>Papeleria en General</h1>
<h2><a href="javascript:alert('EL PDF GENERAL');">Factura # <?php $factura_general; ?></a></h2>
<table>
   <thead>
      <tr>
         <th>ID</th>
         <th>Producto</th>
         <th>Cantidad</th>
         <th>Monto</th>
         <th>Iva</th>
      </tr>
      <tbody>
         <?php foreach($detalle as $item): ?>
            <tr>
               <td><?php $item['id'];       ?></td>
               <td><?php $item['producto']; ?></td>
               <td><?php $item['cantidad']; ?></td>
               <td><?php $item['monto']; ?></td>
               <td><?php $item['iva']; ?></td>
            </tr>
         <?php endforeach; ?>
      </tbody>
      <tfoot>
         <tr>
            <td>Base Imponible</td>
            <td><? echo $montoTotal; ?></td>
         </tr>
         <tr>
            <td>I.V.A. 12 % </td>
            <td><?php echo $totalIva; ?></td>
         </tr>
         <tr>
            <td>Total a Pagar</td>
            <td><?php echo ?></td>
         </tr>
      </tfoot>
   </thead>
</table>

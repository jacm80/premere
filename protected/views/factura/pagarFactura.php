<?php 
HtmlApp::loadCss('form'); 
HtmlApp::loadJs('pagarFactura');
?>

<form id="FormFactura" action="<?php echo Yii::app()->baseUrl; ?>/factura/pagarFactura" method="post">

<div id="content-numero">
   <div id="numero-factura">
      <div id="BtnFactura"><?php echo CHtml::image(HtmlApp::imageUrl('icons/tabla.png')); ?></div>
      <div style="top:8px;position:relative;">No. de Factura:
         <strong><?php echo sprintf('%06d',$model->numero); ?></strong>
         <?php echo CHtml::hiddenField('id',$model->id); ?>
      </div>
      <div style="clear:both;"></div>
   </div>
</div>

<br />
<table class="table-form">
         <tr>
            <td style="width: 25%;">Total Factura</td>
            <td style="width: 25%;">
               <strong id="total_factura" style="font-size:14pt;">
                  <?php echo number_format($model->base_imponible+$model->iva,2,'.',','); ?>
               </strong>
            </td>
            <td style="width: 25%;">Monto Pagado</td>
            <td style="background: #B7F0C6; width: 25%;"><strong id="saldo" style="font-size:14pt;"><?php echo $model->orden->saldo; ?></strong></td>
         </tr>
         <tr>
            <!-- --> 
            <td>Pendiente</td>
            <td>
               <strong id="pendiente" style="font-size:14pt;">
                  <?php echo number_format($model->base_imponible+$model->iva,2,'.',','); ?>
               </strong>
            </td>
            <!-- -->
            <td style="text-align:right"><?php echo CHtml::checkBox('usar_saldo'); ?>Descontar del saldo</td>
            <td><?php echo CHtml::textField('resta_monto','0.00',array('disabled'=>'disabled')); ?></td>
         </tr>
</table>

<div id="lyt-izq" style="width:45%;float:left;">
<fieldset style="padding:10px;">
   <legend>Formas de Pago</legend>
   <table class="table-form">
      <thead>
         <tr>
            <th>Descripcion</th>
            <th>Monto</th>
         </tr>
      </thead>
      <tbody>
         <?php $i=0; ?>   
         <?php foreach(FormaPago::model()->findAll( ) as $fp): ?>
               <tr>
                  <td><?php echo CHtml::checkBox('forma_pago_id[]',null,array('value'=>$fp->id,'class'=>'forma-pago-chk')); ?><?php echo $fp->descripcion; ?></td>
                  <td><?php echo CHtml::textField('monto[]','0.00',array('id'=>"monto-{$fp->id}",'class'=>'forma-pago','disabled'=>'disabled')); ?></td>
               </tr>
               <?php if ($fp->requiere_comprobante==1): ?>
                  <tr id="espBC-<?php echo $fp->id; ?>" class="hide">
                     <td style="background-color: #B8E8F4;">Banco</td>
                     <td style="background-color: #B8E8F4;"><?php echo CHtml::dropDownList("banco_id-{$fp->id}",
                                                         null,
                                                        Banco::getlistBancos(),
                                                        array('id'=>"banco_id-{$fp->id}"));?>
                     </td>
                  </tr>
                  <tr id="espNC-<?php echo $fp->id; ?>" class="hide">
                     <td style="background-color: #B8E8F4;">Numero de Comprobante</td>
                     <td style="background-color: #B8E8F4;">
                        <input type="ẗext" name="comprobante" 
                               class="numero-comprobante" 
                               id="numero_comprobante-<?php echo $fp->id;?>">
                     </td>
                  </tr>
                  <?php $i++; ?>
               <?php endif;?>
         <?php endforeach; ?> 
      </tbody>
   </table>
</fieldset>
</div>
<div id="lyt-der" style="width:50%;float:right">
   <fieldset style="padding:10px;">
   <legend>Pago</legend>
      <table class="table-form">
         <tr>
            <td>Total Pago</td>
            <td class="amount" style="background: #E6F2FF ; font-size: 14pt; font-weight:bold;"><div id="total_pago">0,00</div></td>
         </tr>
         <tr class="odd">
            <td>
               Resta de la Orden
               <?php echo CHtml::hiddenField('resta_orden_ro',$model->orden->resta); ?>
            </td>
            <td class="amount" style="background: #FCF7CA; font-size: 14pt;"><div id="resta_orden"><?php echo $model->orden->resta; ?></div></td>
         </tr>
         <tr>
            <td>Saldo Disponible</td>
            <td class="amount" style="background: #B7F0C6; font-size: 14pt;"><div id="saldo_disponible"><?php echo $model->orden->saldo; ?></div></td>
         </tr>
      </table>
   </fieldset>
</div>

<div style="text-align:center; /*border:1px solid black;*/ width: 48%; float:right; padding: 8px;">
   <div id="error_cuadre" style="color:red; font-size: 14pt; margin-bottom: 8px;" class="hide">La orden No cuadra</div>
   <button id="BtnProcesar" type="button">Procesar Pago</button>
</div>

</form>

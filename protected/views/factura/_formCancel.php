<table class="table-form">
   <tr>
      <?php echo CHtml::hiddenField('id',$id);?>
      <td>Forma de Pago</td>
      <td><?php echo CHtml::dropDownList('Pago[forma_pago_id]',null,FormaPago::getListFormaPagos(),array('onchange'=>'toggle_banco(event)')); ?></td>
   </tr>
   <tr>
      <td>Banco</td>
      <td><?php echo CHtml::dropDownList('Pago[banco_id]',null,Banco::getListBancos( ),array('class'=>'hide')); ?>
   </tr>
   <tr>
      <td>Numero comprobante</td>
      <td><?php echo CHtml::textField('Pago[numero_comprobante]',null,array('class'=>'hide')); ?></td>
   </tr>
</table>

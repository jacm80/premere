<tr id="tr_<?php echo $item; ?>">
   <td><?php echo $item; ?></td>
   <td>
      <div id="pago_forma_pago_<?php echo $item; ?>">
         <?php echo CHtml::dropDownList('pago[forma_pago]',NULL,$formaPagos,array('onchange'=>'toggle_banco(this)')); ?>
      </div>
   </td>
   <td><?php echo date('d/m/Y'); ?></td>
   <td>
      <div id="pago_banco_<?php echo $item; ?>">
         <?php echo CHtml::dropDownList('pago[banco]',NULL,$bancos,array('class'=>'hide','id'=>'Pago_banco_id')); ?>
      </div>
   </td>
   <td>
      <div id="pago_numero_comprobante_<?php echo $item; ?>">
         <input type="text" id="Pago_numero_comprobante" name="pago[numero_comprobante]" class="hide" /></td>
      </div>
   <td><?php echo Yii::app()->user->name; ?></td>
   <td>
      <div id="pago_descripcion_<?php echo $item; ?>">
         <?php echo CHtml::dropDownList('pago[descripcion]',NULL,$facturas,array('id'=>'Pago_descripcion')); ?>
      </div>
   </td>
   <td><div id="monto">0.00</div></td>
   <td><button id="guardar" onclick="guardarPago('<?php echo $item; ?>');">Guardar</button></td>
</tr>

<?php $this->breadcrumbs=array('Factura'); ?>
<?php
   $columns= array(
                  array(
                     'header'=> 'No Orden',
                     'type'  => 'raw',
                     'name'  => 'id',
                     'value' => 'sprintf("%06d",$data->id)'
                  ),
                  /**/
                  array(
                        'type'   => 'raw',
                        'header' => 'Cliente',
                        'name'   => 'cliente',
                        'value'  => '$data->firma_razon_social'
                        ),
                  /**/
                  'fecha_entrada',
                  'fecha_entrega',
                  array('name'=>'estatus','value'=>'$data->ordenStatus->descripcion'),
                  );
   /**/
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'          => 'orden-grid',
                                                      'filter'      => $model,
                                                      'dataProvider'=> $model->search( ),
                                                      'columns'     => $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/factura/view/id/"+xid;
                                                      }'
                                                   ));
?>

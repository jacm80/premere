<?php #HtmlApp::loadCss('orden_display'); ?>
<table class="table-form">
   <input type="hidden" value="<?php echo $model->id ?>" id="orden_id">
   <tr>
      <td class="label">RIF</td>
      <td><?php echo $model->cliente->rif ?></td>
      <td class="label">Razon Social</td>
      <td><?php echo $model->cliente->razon_social ?></td>
   </tr>
   <tr>
      <td class="label">Firma</td>
      <td><?php echo $model->firma->razon_social ?></td>
      <td class="label">Sucursal</td>
      <td><?php echo $model->sede->descripcion ?></td>
   </tr>
   <tr>
      <td class="label">Fecha Entrada</td>
      <td><?php echo $model->fecha_entrada; ?></td>
      <td class="label">Fecha Entrega</td>
      <td><?php echo $model->fecha_entrega ?></td>
   </tr>
   <tr>
      <td class="label">Direcciones Incluidas</td>
      <td><?php echo $model->direcciones_incluidas ?></td>
      <td class="label">Telefono incluidos</td>
      <td><?php echo $model->telefonos_incluidos ?></td>
   </tr>
   <tr>
      <td class="label">Publicidad incluidos</td>
      <td><?php echo $model->publicidad_incluida ?></td>
      <td class="label">Observaciones</td>
      <td><?php echo $model->observaciones; ?></td>
   </tr>
   <tr>
      <td class="label">Forma de Abono</td>
      <td><?php echo $model->abonoFormaPagoDescripcion; ?></td>
      <td class="label">Banco</td>
      <td><?php echo $model->abonoBancoDescripcion; ?></td>
   </tr>
   <tr>
      <td class="label">Abono</td>
      <td><?php echo $model->abono ?></td>
      <td class="label">Status</td>
      <td><?php echo $model->ordenStatus->descripcion; ?></td>
   </tr>
   <tr>
      <td colspan="3" style="text-align:right;" class="summary-label">Base Imponible</td>
      <td class="summary-value"><div><?php echo $model->base_imponible ?></div></td>
   </tr>
   <tr>
      <td colspan="3" style="text-align:right;" class="summary-label">I.V.A.</td>
      <td class="summary-value"><div><?php echo $model->iva ?></div></td>
   </tr>
   <tr>
      <td colspan="3" style="text-align:right;" class="summary-label">Total a Pagar</td>
      <td class="summary-value"><div><?php echo  $model->base_imponible + $model->iva; ?></div></td>
   </tr>
   </tr>
   <tr>
      <td colspan="3" style="text-align:right;" class="summary-label">Resta</td>
      <td class="summary-value"><div><?php echo $model->resta ?></div></td>
   </tr>
</table>

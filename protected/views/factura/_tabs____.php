<?php HtmlApp::loadJs('factura'); ?>
<?php  
    /**/
    $this->widget('CTabView', array(
    'tabs'=>array(
        'ordenTab'=>array(
            'title'=>'Orden de Trabajo',
            'view' =>'_orden',
            'data' => array('orden'=>$orden)
        ),
        'facturaTab'=>array(
            'title'=>'Facturas',
            'view'=>'_facturas',
            'data' => array('facturas'=>$facturas)
        ),
        'pagoTab'=>array(
            'title'=>'Pagos',
            'view'=>'_pagos',
            'data' => array('pagos'=>$pagos)
         )
    )));
    /**/
?>

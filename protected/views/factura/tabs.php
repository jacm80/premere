<?php 
HtmlApp::loadCss('form');
HtmlApp::loadCss('factura');
HtmlApp::loadCss('factura_print');
HtmlApp::loadJs('factura');
HtmlApp::loadCss('qtip2/jquery.qtip');
HtmlApp::loadJs('qtip2/jquery.qtip'); 
?>

<div id="dialog"></div>

<div style="border: 1px solid #e3e3e3;width:20%;margin:0 auto;text-align:center;border-radius:5px;padding:6px;margin-bottom:20px;">
   Orden No:&nbsp;<strong><?php echo sprintf('%06d',$model->id); ?></strong>
</div>
<?php  
   $this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
         'Orden'    => array('id'=>'Orden',   'content'=>$this->renderPartial('_orden'   ,array('model'=> $model),true)),
         'Facturas' => array('id'=>'Facturas','content'=>$this->renderPartial('_facturas',array(
                                                                                                'facturas' => $model->getFacturas(),
                                                                                                'resumen'  => $resumen
                                                                                                ),true)),
         'Pagos'    => array('id'=>'Pagos',   'content'=>$this->renderPartial('_pagos',array('pagos'=>$pagos),true)),
    ),
    // additional javascript options for the tabs plugin
   'options' => array('collapsible'=>true,'selected'=>0,/*'disabled'=>array(1)*/), 
   ));
?>

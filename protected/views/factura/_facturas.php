<div id="resumen">
   <div id="base-box">
      Base: <div id="base"><?php echo $resumen['base_imponible']; ?></div>
   </div>
   <div id="iva-box">
      I.V.A.: <div id="iva"><?php echo $resumen['iva']; ?></div>
   </div>
   <div id="total-box">
      Total: <div id="total"><?php echo $resumen['total']; ?></div>
   </div>
   <div id="pagado-box">
      Pagado: <div id="pagado"><?php echo $resumen['pagos']; ?></div>
   </div>
   <div id="resta-box">
      Resta: <div id="resta"><?php echo $resumen['resta']; ?></div>
   </div>
</div>
<div id="cancelacion-box">
   <select id="optCancelar">
      <option value="0">Opciones de Orden de Trabajo</option>
      <option value="1">Pagar Orden Completa</option>
      <option value="2">Pagar Retencion de IVA</option>
   </select>
   <div id="cancelacion">
      <strong style="color:black;">Bs:</strong>&nbsp;<span id="monto_cancelar">0,00</span>
      <?php echo CHtml::imageButton(HtmlApp::imageUrl('icons/pagar.png'),array('id'=>'BtnOpcionPago')); ?>
   </div>
</div>
<div style="clear:both;"></div>

<br />

<table class="table-form">
   <thead>
      <tr>
         <th>Producto</th>
         <th>Cantidad</th>
         <th>Precio Unitario</th>
         <th>Base Imponible</th>
         <th>Iva</th>
         <th>Total a Pagar</th>
      </tr>
   </thead>
   <tbody>
      <!-- BEGIN DOCUMENTOS FISCALES -->
      <?php if (isset($facturas['fiscal'])): ?>
      <tr>
         <td colspan="6" style="text-align:center;" class="title">DOCUMENTOS FISCALES</td>
      </tr>
         <?php foreach($facturas['fiscal'] as $k=>$v): ?>
            <tr>
               <td colspan="6">
                     Factura: <strong class="n-factura"><?php echo sprintf('%06d',$v['numero']); ?></strong>
                     <?php echo CHtml::imageButton(HtmlApp::imageUrl('icons/view.png'),
                                 array('id'=>"BtnView-{$v['id']}-{$v['numero']}",'alt'=>'Ver Factura','type'=>'button')); ?>
                     <?php echo CHtml::imageButton(HtmlApp::imageUrl('icons/pagar.png'),
                                 array('id'=>"BtnCancelar-{$v['id']}-{$v['numero']}",'alt'=>'Cancelar Factura','type'=>'buton')); ?>
               </td>
            </tr>
            <?php foreach($v['items'] as $item): ?>
            <tr>
               <td><?php echo $item['producto']?></td>
               <td class="amount"><?php echo $item['cantidad']; ?></td>
               <td class="amount"><?php echo $item['precio_unitario']; ?></td>
               <td class="amount"><?php echo $item['monto'];    ?></td>
               <td class="amount"><?php echo $item['iva'];      ?></td>
               <td class="amount"><?php echo $item['subtotal']; ?></td>
            </tr>
            <?php endforeach; ?>
         <?php endforeach; ?>
      <?php endif; ?>
      <!-- END DOCUMENTOS FISCALES -->
      <!-- BEGIN PAPELERIA EN GENERAL -->
      <?php if (isset($facturas['papeleria'])): ?>
      <tr>
         <td colspan="6" style="text-align:center;" class="title">PAPELERIA EN GENERAL</td>
      </tr>
         <?php foreach($facturas['papeleria'] as $k=>$v): ?>
            <tr>
               <td colspan="6">
                     Factura: <strong class="n-factura"><?php echo sprintf('%06d',$v['numero']); ?></strong>
                     <?php echo CHtml::imageButton(HtmlApp::imageUrl('icons/view.png'),
                                 array('id'=>"BtnView-{$v['id']}-{$v['numero']}",'alt'=>'Ver Factura','type'=>'button')); ?>
                     <?php echo CHtml::imageButton(HtmlApp::imageUrl('icons/pagar.png'),
                                 array('id'=>"BtnCancelar-{$v['id']}-{$v['numero']}",'alt'=>'Cancelar Factura','type'=>'buton')); ?>
               </td>
            </tr>
            <?php foreach($v['items'] as $item): ?>
            <tr>
               <td><?php echo $item['producto']?></td>
               <td class="amount"><?php echo $item['cantidad'];        ?></td>
               <td class="amount"><?php echo $item['precio_unitario']; ?></td>
               <td class="amount"><?php echo $item['monto'];           ?></td>
               <td class="amount"><?php echo $item['iva'];             ?></td>
               <td class="amount"><?php echo $item['subtotal'];        ?></td>
            </tr>
            <?php endforeach; ?>
         <?php endforeach; ?>
      <?php endif; ?>
      <!-- END PAPELERIA EN GENERAL -->
      </tbody>
</table>

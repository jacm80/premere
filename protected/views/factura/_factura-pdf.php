<body>
   <div id="layout_factura">
      <div id="factura-original">
      <div class="imprenta_head" style="heigth"></div>
      <div style="text-align:right; font-weight: bold; font-size: 14pt;">Factura No.: 0000<?php echo $factura['numero']; ?></div>
      <br />
      <table class="header">
      <tr>
         <td colspan="2" class="label">Fecha Emision</td>
         <td colspan="2"><?php echo date('d/m/Y'); ?></td>
      </tr>
      <tr>
         <td colspan="4" class="label">Nombre y Apellido o Razon Social del Comprador</td>
      </tr>
      <tr>
         <td colspan="4"><?php echo $factura['cliente']; ?></td>
      </tr>
      <tr>
         <td colspan="2" class="label">No. RIF o CED No.</td>
         <td colspan="2"><?php echo $factura['rif']; ?></td>
      </tr>
      <tr>
         <td class="label">Forma de Pago</td>
         <td><?php echo $factura['forma_pago']; ?></td>
         <td class="label">Explique</td>
         <td>MONEDA DE CURSO LEGAL</td>
      </tr>
      </table>
      <br />
      <table class="data">
      <thead>
      <tr>
         <th>CANTIDAD</th>
         <th>DESCRIPCION</th>
         <th>PRECIO UNITARIO</th>
         <th>MONTO</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($factura['items'] as $item): ?>
         <tr>
            <td style="text-align:center;"><?php echo $item['cantidad'];         ?></td>
            <td><?php echo $item['producto'];         ?></td>
            <td class="monto"><?php echo number_format($item['precio_unitario'],2,',','.');  ?></td>
            <td class="monto"><?php echo number_format($item['monto'],2,',','.');            ?></td>
         </tr>
      <?php endforeach; ?>
      </tbody>
      </table>
      <br />
      <table class="footer">
         <tr>
            <td style="width: 80%;" class="label-summary">Base Imponible segun Alicuota 12 %</td>
            <td class="value-summary"><?php echo number_format($factura['base_imponible'],2,',','.'); ?></td>
         </tr>
         <tr>
            <td class="label-summary">Monto Total del Impuesto segun Alicuota 12 %</td>
            <td class="value-summary"><?php echo number_format($factura['iva'],2,',','.'); ?></td>
         </tr>
         <tr>
            <td class="label-summary">MONTO TOTAL DE LA VENTA</td>
            <td class="value-summary"><?php echo number_format(($factura['base_imponible']+$factura['iva']),2,',','.'); ?></td>
         </tr>
      </table>
      <br />
      <div class="hoja">ORIGINAL</div>
   </div>
   <div id="factura-copia">
      <div class="imprenta_head" style="heigth"></div>
      <div style="text-align:right; font-weight: bold; font-size: 14pt;">Factura No.: 0000<?php echo $factura['numero']; ?></div>
      <br />
      <table class="header">
      <tr>
         <td colspan="2" class="label">Fecha Emision</td>
         <td colspan="2"><?php echo date('d/m/Y'); ?></td>
      </tr>
      <tr>
         <td colspan="4" class="label">Nombre y Apellido o Razon Social del Comprador</td>
      </tr>
      <tr>
         <td colspan="4"><?php echo $factura['cliente']; ?></td>
      </tr>
      <tr>
         <td colspan="2" class="label">No. RIF o CED No.</td>
         <td colspan="2"><?php echo $factura['rif']; ?></td>
      </tr>
      <tr>
         <td class="label">Forma de Pago</td>
         <td><?php echo $factura['forma_pago']; ?></td>
         <td class="label">Explique</td>
         <td>MONEDA DE CURSO LEGAL</td>
      </tr>
      </table>
      <br />
      <table class="data">
      <thead>
      <tr>
         <th>CANTIDAD</th>
         <th>DESCRIPCION</th>
         <th>PRECIO UNITARIO</th>
         <th>MONTO</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($factura['items'] as $item): ?>
         <tr>
            <td style="text-align:center;"><?php echo $item['cantidad'];         ?></td>
            <td><?php echo $item['producto'];         ?></td>
            <td class="monto"><?php echo number_format($item['precio_unitario'],2,',','.');  ?></td>
            <td class="monto"><?php echo number_format($item['monto'],2,',','.');            ?></td>
         </tr>
      <?php endforeach; ?>
      </tbody>
      </table>
      <br />
      <table class="footer">
         <tr>
            <td style="width: 80%;" class="label-summary">Base Imponible segun Alicuota 12 %</td>
            <td class="value-summary"><?php echo number_format($factura['base_imponible'],2,',','.'); ?></td>
         </tr>
         <tr>
            <td class="label-summary">Monto Total del Impuesto segun Alicuota 12 %</td>
            <td class="value-summary"><?php echo number_format($factura['iva'],2,',','.'); ?></td>
         </tr>
         <tr>
            <td class="label-summary">MONTO TOTAL DE LA VENTA</td>
            <td class="value-summary"><?php echo number_format(($factura['base_imponible']+$factura['iva']),2,',','.'); ?></td>
         </tr>
      </table>
      <br />
      <div class="hoja">COPIA SIN DERECHO A CREDITO FISCAL</div>
   </div>
   </div>
</body>

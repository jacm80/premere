<div class="box-marco">
   <div class="box-gris">Razon Social</div>
   <div class="box-amarillopastel"><?php echo $razon_social; ?></div>
</div>

<br />

<div class="box-marco">
   <div class="box-gris">Ultimo Numero de Control:</div>
   <div class="box-amarillopastel"><?php echo sprintf('%02d',$ultimo_identif) .'-'. sprintf('%06d',$ultimo_control); ?></div>
</div>

<br />
<h3 style="text-align:center;">Documentos Elaborados hasta la fecha <?php echo date('d/m/Y'); ?></h3>

<form method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/consulta/searchNumeracion" id="searchForm">
<?php
   $columns = array(
                     array(
                        'type'=>'raw',
                        'name'=>'numero',
                        'header'=>'Factura No.',
                        'value'=>'sprintf("%06d",$data->numero)',
                     ),
                     'documento',
                     array(
                        'type'=>'raw',
                        'header'=>'Identificador',
                        'name'=>'identificador',
                        'value'=>'sprintf("%02d",$data->identificador)',
                     ),
                     array(
                        'type'=>'raw',
                        'header'=>'Numero Desde',
                        'name'  =>'inicio_numero',
                        'value' =>'sprintf("%06d",$data->inicio_numero)',
                     ),
                     array(
                        'type'=>'raw',
                        'header'=>'Numero Hasta',
                        'name'  =>'fin_numero',
                        'value' =>'sprintf("%06d",$data->fin_numero)',
                     ),
                     array(
                        'type'=>'raw',
                        'header'=>'Control Desde',
                        'name'  =>'inicio_control',
                        'value' =>'sprintf("%06d",$data->inicio_control)',
                     ),
                     array(
                        'type'=>'raw',
                        'header'=>'Control Hasta',
                        'name'  =>'fin_control',
                        'value' =>'sprintf("%06d",$data->fin_control)',
                     ),
                  );
   /**/
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'          => 'numeracion-grid',
                                                      'filter'      => $model,
                                                      'dataProvider'=> $model->searchNumByRif(),
                                                      'columns'     => $columns,
                                                    ));
?>
</form>



<!--
<table class="table-form">
   <thead>
      <tr>
         <th>Factura</th>
         <th>Documento</th>
         <th>Serie</th>
         <th>Identificador</th>
         <th>Numero Inicio</th>
         <th>Numero Hasta</th>
         <th>Control Inicio</th>
         <th>Control Hasta</th>
      </tr>
   </thead>
   <tbody>
      <?php #foreach ($model as $i): ?>
      <tr>
         <td><?php #echo $i->numero; ?></td>
         <td><?php #echo $i->documento; ?></td>
         <td><?php #echo $i->serie; ?></td>
         <td><?php #echo sprintf('%02d',$i->identificador);  ?></td>
         <td><?php #echo sprintf('%06d',$i->inicio_numero);  ?></td>
         <td><?php #echo sprintf('%06d',$i->fin_numero);     ?></td>
         <td><?php #echo sprintf('%06d',$i->inicio_control); ?></td>
         <td><?php #echo sprintf('%06d',$i->fin_control);    ?></td>
      </tr>
      <?php #endforeach; ?>
   </tbody>
</table>
-->

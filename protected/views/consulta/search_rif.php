<?php 
   HtmlApp::loadCss('form');
   HtmlApp::loadjQueryUI();
   HtmlApp::loadJs('jquery.yiigridview');
?>

<style>
   .table-form tbody tr td { text-align:center; }

   #box-rif
   {
      border: 2px solid #001F50;
      padding: 8px; 
      width:30%; 
      margin: 0 auto;
      border-radius: 10px; 
      text-align:center;
      background: #f1f1f1;
   }
   #box-rif label 
   { 
      color:black;
      font-weight:bold; 
   }

   .box-marco 
   { 
      margin: 0 auto;
      width: 80%;
      text-align:center;
      border: 1px solid #e3e3e3; 
      border-bottom-left-radius: 10px; 
      border-bottom-right-radius: 10px; 
   }

   .box-gris
   {
      color: white;
      font-weight:bold;
      padding:4px;
      background: #001F50; 
   }

   .box-amarillopastel
   {
      font-weight:bold;
      font-size:14pt;
      padding:10px; 
      color: #001F50; 
      background: #f1f1f1;
      border-bottom-left-radius: 10px; 
      border-bottom-right-radius: 10px;
   }
</style>

<script type="text/javascript">

   $(document).ready(function(){
      $('input').live('keypress',function(e){
         if (e.which==13){
            
            var params = {
               rif            : $("#rif").val( ),
               numero         : $("input[name='Factura[numero]']")         .val( ),
               documento      : $("input[name='Factura[documento]']")      .val( ),
               identificador  : $("input[name='Factura[identificador]']")  .val( ),
               inicio_numero  : $("input[name='Factura[inicio_numero]']")  .val( ),
               fin_numero     : $("input[name='Factura[fin_numero]']")     .val( ),
               inicio_control : $("input[name='Factura[inicio_control]']") .val( ),
               fin_control    : $("input[name='Factura[fin_control]']")    .val( ),
            }; 
            
            console.debug(params);
            /**/
            $.ajax({
               url:base_url+'/consulta/searchNumeracion',
               type: 'post',
               data: params,
               datatype: 'html',
               async: true,
               success:function(resp){
                  $('#result').html(resp);
               },
               error:function(){ alert('error al procesar buscarxRif'); }
            });
            /**/
         }
      });
   });

</script>

<div id="box-rif">
   <label for="rif">RIF</label>&nbsp;<?php echo CHtml::textField('rif','V-15073339-8'); ?>
</div>

<br />

<div id="result"></div>

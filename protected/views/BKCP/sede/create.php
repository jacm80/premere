<?php
$this->breadcrumbs=array(
	'Sedes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Sede', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Sede', 'url'=>array('admin')),
);
?>

<h1>Crear Sede</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
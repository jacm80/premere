<?php
$this->breadcrumbs=array(
	'Sedes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Sede', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Sede', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Sede', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Sede', 'url'=>array('admin')),
);
?>

<h1>Update Sede <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Sedes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Sede', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Sede', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Sede', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Sede', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Sede', 'url'=>array('admin')),
);
?>

<h1>View Sede #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'firma_id',
		'domicilio_fiscal',
		'telefono',
	),
)); ?>

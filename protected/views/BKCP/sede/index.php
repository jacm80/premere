<?php
$this->breadcrumbs=array(
	'Sedes',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' Sede', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' Sede', 'url'=>array('admin')),
);
?>

<h1>Sedes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

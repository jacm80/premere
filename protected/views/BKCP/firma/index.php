<?php
$this->breadcrumbs=array(
	'Firmas',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' Firma', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' Firma', 'url'=>array('admin')),
);
?>

<h1>Firmas</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

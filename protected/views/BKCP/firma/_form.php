<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'firma-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cliente_id'); ?>
		<?php 
         $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                  //'model'=>$model,
                  'attribute'=>'cliente_razon_social',
                  'name'=>'cliente_razon_social',
                  'value'=> ($model->isNewRecord) ? '' : Cliente::model()->getRazoSocialById($model->cliente_id),
                  'sourceUrl' => Yii::app()->request->baseUrl.'/orden/getclientes',
                  'htmlOptions'=>array(
                                       'size'=>60
                                       /*'disabled'=> $model->isNewRecord ? 'none' : 'disabled'*/
                                       ),
                  'options'=>array(
                                    'minLenght'=>2,
                                    'select'   =>"js:function(event,ui){
                                       $('#Firma_cliente_id').val(ui.item['id']);
                                    }",
                                    'change'=>"js:function(event,ui){
                                          if(!ui.item){
                                             $('#cliente_id').val('');
                                          }
                                    }"
                                    )
                  
               ));
         echo $form->hiddenField($model,'cliente_id'); 
      ?>
		<?php echo $form->error($model,'cliente_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'razon_social'); ?>
		<?php echo $form->textArea($model,'razon_social',array('row'=>6,'cols'=>50)); ?>
		<?php echo $form->error($model,'razon_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'domicilio_fiscal'); ?>
		<?php echo $form->textArea($model,'domicilio_fiscal',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'domicilio_fiscal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'registro_de_comercio'); ?>
		<?php echo $form->textArea($model,'registro_de_comercio',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'registro_de_comercio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono'); ?>
		<?php echo $form->textField($model,'telefono',array('size'=>10,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'telefono'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'archivo_registro_de_comercio'); ?>
		<?php echo $form->textField($model,'archivo_registro_de_comercio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'archivo_registro_de_comercio'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<?php
$this->breadcrumbs=array(
	'Firmas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Firma', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Firma', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Firma', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Firma', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update'); ?> Firma <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

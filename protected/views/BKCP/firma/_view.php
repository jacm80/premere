<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cliente_id')); ?>:</b>
	<?php echo CHtml::encode($data->cliente->razon_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razon_social')); ?>:</b>
	<?php echo CHtml::encode($data->razon_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('domicilio_fiscal')); ?>:</b>
	<?php echo CHtml::encode($data->domicilio_fiscal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('registro_de_comercio')); ?>:</b>
	<?php echo CHtml::encode($data->registro_de_comercio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono')); ?>:</b>
	<?php echo CHtml::encode($data->telefono); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('archivo_registro_de_comercio')); ?>:</b>
	<?php echo CHtml::encode($data->archivo_registro_de_comercio); ?>
	<br />


</div>

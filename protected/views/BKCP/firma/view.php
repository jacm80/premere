<?php
$this->breadcrumbs=array(
	'Firmas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Firma', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Firma', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Firma', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Firma', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Firma', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','View'); ?> Firma #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
      array(
         'name'=>'cliente_id',
         'value'=>$model->cliente->razon_social
      ),
		'razon_social',
		'domicilio_fiscal',
		'registro_de_comercio',
		'telefono',
		'archivo_registro_de_comercio',
	),
)); ?>

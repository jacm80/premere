<?php
$this->breadcrumbs=array(
	'Firmas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Firma', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Firma', 'url'=>array('admin')),
);
?>

<h1>Crear Firma</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Cliente', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Cliente', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Cliente', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Cliente', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Cliente', 'url'=>array('admin')),
);
?>

<h1>View Cliente #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'rif',
		'razon_social',
		'representante_legal',
		'domicilio_fiscal',
		'contribuyente_id',
		'cliente_tipo_id',
		'telefono_celular',
		'telefono_residencial',
		'registro_de_comercio',
		'email',
		'archivo_rif',
		'archivo_registro_de_comercio',
	),
)); ?>

<div class="form">

   <?php $form=$this->beginWidget('CActiveForm', array(
       'id'=>'cliente-form',
       //'enableAjaxValidation'=>false,
       //'enableClientValidation'=>true,
       //'clientOptions'=>array('validateOnSubmit'=>true)
   )); ?>

	<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'rif'); ?>
		<?php echo $form->textField($model,'rif',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'rif'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'razon_social'); ?>
		<?php echo $form->textArea($model,'razon_social',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'razon_social'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'representante_legal'); ?>
		<?php echo $form->textArea($model,'representante_legal',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'representante_legal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'domicilio_fiscal'); ?>
		<?php echo $form->textArea($model,'domicilio_fiscal',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'domicilio_fiscal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contribuyente_id'); ?>
		<?php echo $form->dropDownList($model,'contribuyente_id',$model::getContribuyentes()); ?>
		<?php echo $form->error($model,'contribuyente_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'cliente_tipo_id'); ?>
		<?php echo $form->dropDownList($model,'cliente_tipo_id',$model::getTipos()); ?>
		<?php echo $form->error($model,'cliente_tipo_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_celular'); ?>
		<?php echo $form->textField($model,'telefono_celular',array('size'=>13,'maxlength'=>13)); ?>
		<?php echo $form->error($model,'telefono_celular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_residencial'); ?>
		<?php echo $form->textField($model,'telefono_residencial',array('size'=>13,'maxlength'=>13)); ?>
		<?php echo $form->error($model,'telefono_residencial'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'registro_de_comercio'); ?>
		<?php echo $form->textArea($model,'registro_de_comercio',array('rows'=>3, 'cols'=>50)); ?>
		<?php echo $form->error($model,'registro_de_comercio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'archivo_rif'); ?>
		<?php echo $form->textField($model,'archivo_rif',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'archivo_rif'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'archivo_registro_de_comercio'); ?>
		<?php echo $form->textField($model,'archivo_registro_de_comercio',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'archivo_registro_de_comercio'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rif')); ?>:</b>
	<?php echo CHtml::encode($data->rif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razon_social')); ?>:</b>
	<?php echo CHtml::encode($data->razon_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('representante_legal')); ?>:</b>
	<?php echo CHtml::encode($data->representante_legal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('domicilio_fiscal')); ?>:</b>
	<?php echo CHtml::encode($data->domicilio_fiscal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contribuyente_id')); ?>:</b>
	<?php echo CHtml::encode($data->contribuyente->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cliente_tipo_id')); ?>:</b>
	<?php echo CHtml::encode($data->clienteTipo->descripcion); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_celular')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_residencial')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_residencial); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('registro_de_comercio')); ?>:</b>
	<?php echo CHtml::encode($data->registro_de_comercio); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('archivo_rif')); ?>:</b>
	<?php echo CHtml::encode($data->archivo_rif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('archivo_registro_de_comercio')); ?>:</b>
	<?php echo CHtml::encode($data->archivo_registro_de_comercio); ?>
	<br />

	*/ ?>

</div>

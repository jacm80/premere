<?php
$this->breadcrumbs=array(
	'Clientes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Cliente', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Cliente', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Cliente', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Cliente', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('app','Update') ?> Cliente ID: <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

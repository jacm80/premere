<?php
$this->breadcrumbs=array(
	'Proveedors',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' Proveedor', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' Proveedor', 'url'=>array('admin')),
);
?>

<h1>Proveedors</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

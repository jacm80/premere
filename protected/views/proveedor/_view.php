<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('rif')); ?>:</b>
	<?php echo CHtml::encode($data->rif); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('razon_social')); ?>:</b>
	<?php echo CHtml::encode($data->razon_social); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('representante')); ?>:</b>
	<?php echo CHtml::encode($data->representante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telefono_fijo')); ?>:</b>
	<?php echo CHtml::encode($data->telefono_fijo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('celular')); ?>:</b>
	<?php echo CHtml::encode($data->celular); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('banco_id')); ?>:</b>
	<?php echo CHtml::encode($data->banco_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numero_cuenta')); ?>:</b>
	<?php echo CHtml::encode($data->numero_cuenta); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contribuyente_id')); ?>:</b>
	<?php echo CHtml::encode($data->contribuyente_id); ?>
	<br />

	*/ ?>

</div>

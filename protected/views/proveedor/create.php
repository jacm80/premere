<?php
$this->breadcrumbs=array(
	'Proveedors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Proveedor', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Proveedor', 'url'=>array('admin')),
);
?>

<h1>Crear Proveedor</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
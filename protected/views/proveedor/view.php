<?php
$this->breadcrumbs=array(
	'Proveedors'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Proveedor', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Proveedor', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Proveedor', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Proveedor', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Proveedor', 'url'=>array('admin')),
);
?>

<h1>View Proveedor #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'rif',
		'razon_social',
		'representante',
		'telefono_fijo',
		'celular',
		'email',
		'banco_id',
		'numero_cuenta',
		'contribuyente_id',
	),
)); ?>

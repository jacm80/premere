<script type="text/javascript">
   $(document).ready(function(){
      //alert('listo');
      /*
      $('#GastoConcepto_entidad_id').click(function( ){
         alert('hola pajuo');
      });
      */
   });
</script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'proveedor-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'rif'); ?>
		<?php echo $form->textField($model,'rif',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'rif'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'razon_social'); ?>
		<?php echo $form->textArea($model,'razon_social',array('rows'=>3, 'cols'=>80)); ?>
		<?php echo $form->error($model,'razon_social'); ?>
	</div>

   <div class="row">
		<?php echo $form->labelEx($model,'domicilio_fiscal'); ?>
		<?php echo $form->textArea($model,'domicilio_fiscal',array('rows'=>3, 'cols'=>80)); ?>
		<?php echo $form->error($model,'domicilio_fiscal'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'representante'); ?>
		<?php echo $form->textArea($model,'representante',array('rows'=>3, 'cols'=>80)); ?>
		<?php echo $form->error($model,'representante'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefono_fijo'); ?>
		<?php echo $form->textField($model,'telefono_fijo',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'telefono_fijo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'celular'); ?>
		<?php echo $form->textField($model,'celular'); ?>
		<?php echo $form->error($model,'celular'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>60)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'banco_id'); ?>
		<?php echo $form->dropDownList($model,'banco_id',Banco::getListBancos()); ?>
		<?php echo $form->error($model,'banco_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'numero_cuenta'); ?>
		<?php echo $form->textField($model,'numero_cuenta'); ?>
		<?php echo $form->error($model,'numero_cuenta'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contribuyente_id'); ?>
		<?php echo $form->dropDownList($model,'contribuyente_id',Contribuyente::ListaContribuyentes()); ?>
		<?php echo $form->error($model,'contribuyente_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

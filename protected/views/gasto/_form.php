<h1 style="text-align:center;">Registro de Gasto</h1>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gasto-form',
	'enableAjaxValidation'=>false,
   'enableClientValidation'=>true,
   'clientOptions'=>array('validateOnSubmit'=>TRUE)
   )); 
?>

<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>

<table class="table-form">
   <tr>
      <td>
         <?php echo $form->hiddenField($model,'id'); ?>
         <?php echo $form->labelEx($model,'gasto_concepto_id'); ?>
      </td>
      <td>
         <?php echo $form->dropDownList($model,'gasto_concepto_id',GastoConcepto::getListaConceptos( )); ?>
		   <?php echo $form->error($model,'gasto_concepto_id'); ?>
      </td>
      <td>
         <label class="required">No.
            <?php echo $form->dropDownList($model,'tipo',array(1=>'Factura',2=>'Recibo'))?>
            <span class="required">*</span>
         </label>
      </td>
      <td>
         <?php echo $form->textField($model,'numero'); ?>
         <?php echo $form->error($model,'numero'); ?>
      </td>
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'fecha_documento'); ?></td>
      <td>
         <?php $this->widget('zii.widgets.jui.CJuiDatePicker',$config_fechaDocumento); ?>
         <?php echo $form->error($model,'fecha_documento');     ?>
      </td>
      <td><?php echo $form->labelEx($model,'fecha_registro'); ?></td>
      <td>
         <?php echo $form->textField($model,'fecha_registro',array('disabled'=>'disabled','value'=>date('Y-m-d h:m:i'))); ?>
         <?php echo $form->error($model,'fecha_registro'); ?>
      </td>
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'forma_pago_id'); ?></td>
      <td>
         <?php echo $form->dropDownList($model,'forma_pago_id',FormaPago::getListFormaPagos( ),array('style'=>'width:200px;')); ?>
         <?php echo $form->error($model,'forma_pago_id');     ?>
      </td>
      <td><?php echo $form->labelEx($model,'banco_id'); ?></td>
      <td>
         <?php echo $form->dropDownList($model,'banco_id',Banco::getListBancos( ),array('class'=>'hide')); ?>
         <?php echo $form->error($model,'banco_id'); ?>
      </td>
   </tr> 
   <tr>
      <td><?php echo $form->labelEx($model,'numero_comprobante'); ?></td>
      <td>
         <?php echo $form->textField($model,'numero_comprobante',array('class'=>'hide')); ?>
         <?php echo $form->error($model,'numero_comprobante'); ?>
      </td>
      <td><?php echo $form->labelEx($model,'observacion'); ?></td>
      <td>
         <?php echo $form->textArea($model,'observacion'); ?>
         <?php echo $form->error($model,'observacion'); ?>
      </td>
   </tr>
</table>
<table class="table-form">
   <tr>
      <td class="summary-label"><div>Monto</div></td>
      <td class="summary-value">
         <?php echo $form->textField($model,'monto',array('style'=>'text-align:right')); ?>
         <?php echo $form->error($model,'monto'); ?>
      </td>
   </tr>
   <tr>
      <td class="summary-label"><div>IVA&nbsp;<input id="incluye_iva" type="checkbox" value="0.12" checked="checked"/></div></td>
      <td class="summary-value">
         <?php echo $form->textField($model,'iva',array('readonly'=>'readonly','style'=>'text-align:right')); ?>
         <?php echo $form->error($model,'iva');     ?>
      </td>
   </tr>
   <tr>
      <td class="summary-label"><div>TOTAL PAGADO</div></td>
      <td class="summary-value"><div id="total_gasto">0,00</div></td>
   </tr>
</table>
<?php $this->endWidget(); ?>
</div>

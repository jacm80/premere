<?php
/* @var $this GastoController */

$this->breadcrumbs=array('Gasto','index'); ?>

<?php
   $columns = array(
                     array(
                           'type'   => 'raw',
                           'header' => 'Tipo',
                           'name'   => 'tipo',
                           'value'  => '$data->getTipo()'
                           ),
                     'numero',
                     array(
                           'type'   => 'raw',
                           'header' => 'Concepto',
                           'name'   => 'gasto_concepto_id',
                           'value'  => '$data->gastoConcepto->descripcion'
                           ),
                     'fecha_documento',
                     array(
                           'type'   => 'raw',
                           'header' => 'Forma de Pago',
                           'name'   => 'forma_pago_id',
                           'value'  => '$data->formaPago->descripcion'),
                     array(
                           'type'   =>'raw',
                           'header' =>'Monto',
                           'name'   =>'monto',
                           'value'  =>'$data->monto + $data->iva'
                          ),
                  );
   /**/
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'          => 'gasto-grid',
                                                      'filter'      => $model,
                                                      'dataProvider'=> $model->search( ),
                                                      'columns'     => $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/gasto/view/id/"+xid;
                                                      }'
                                                   ));
?>
<div style="text-align:center;">
   <button type="button" onclick="location.href=base_url+'/gasto/create'">Nuevo Gasto</button>
</div>

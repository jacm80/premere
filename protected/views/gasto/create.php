<?php
HtmlApp::loadCss('form');
HtmlApp::loadJs('gasto');
$config_fechaDocumento = array(
                              'model'     => $model,
                              'name'      => 'Gasto[fecha_documento]',
                              'attribute' => 'fecha_documento',
                              'language'  =>'es',
                              // additional javascript options for the date picker plugin
                              'options'    => array(
                                                   'showAnim'=>'fold',
                                                   'dateFormat' => 'yy-mm-dd',
                                                   'buttonText' => 'Calendario',
                                                   ),
                              'htmlOptions'=> array('style'=>'height:20px;', 'autocomplete'=>'off', 'class'=>'required'),
                              );

$this->breadcrumbs=array('Gasto'=>array('/gasto'),'Create');
?>

<?php $this->renderPartial('_form',array('model'=>$model,'config_fechaDocumento'=>$config_fechaDocumento)); ?>

<div style="text-align:center;">
  <div class="row buttons">
		<?php echo CHtml::Button($model->isNewRecord ? 'Guardar' : 'Actualizar',array('id'=>'BtnGuardarGasto')); ?>
      &nbsp;
		<?php echo CHtml::Button('Listado',array('id'=>'BtnListado')); ?>
      <?php if (!$model->isNewRecord): ?>
         &nbsp;
		   <?php echo CHtml::Button('Nuevo',array('id'=>'BtnNuevo')); ?>
      <?php endif; ?>
	</div>
</div>

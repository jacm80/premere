<html>
   <head>
      <title>Orden de Trabajo</title>
   </head>
   <body>
      <div class="numeracion">
      <strong>ORDEN DE TRABAJO: No. <?php echo $orden['numero']; ?></strong> 
      </div>
      <br />
      <table class="ordenTrabajo-header">
         <tr>
            <td>Fecha de Entrada</td>
            <td><?php echo $orden['fecha_entrada']; ?></td>
            <td>Fecha de Entrega</td>
            <td><?php echo $orden['fecha_entrega']; ?></td>
         </tr>
         <tr>
            <td>RIF Actualizado</td>
            <td colspan="3"><?php echo $orden['rif']; ?></td>
         </tr>
         <tr>
            <td>Sr(a):</td>
            <td colspan="3"><?php echo $orden['razon_social']; ?></td>
         </tr>
         <tr>
            <td>Razon Social</td>
            <td colspan="3"><?php echo $orden['razon_social']; ?></td>
         </tr>
         <tr>
            <td>Telefono(s) Incluido(s)</td>
            <td colspan="3"><?php echo $orden['telefonos_incluidos']?></td>
         </tr>
         <tr>
            <td>Contribuyente</td>
            <td><?php echo $orden['contribuyente']; ?></td>
            <td>Direccion:</td>
            <td> 
               Principal <input type="checkbox" value="<?php $orden['tipo_direccion']; ?>" 
                                 <? echo ($orden['tipo_direccion']=='Principal') ? 'checked="checked"' : '' ?> />
               &nbsp;
               Sucursal  <input type="checkbox" value="<?php $orden['tipo_direccion']; ?>"
                                 <? echo ($orden['tipo_direccion']=='Sucursal') ? 'checked="checked"' : '' ?> />
            </td>
         </tr>
         <tr>
            <td>Direccion(es) incluida(s)</td>
            <td colspan="3"><?php echo $orden['direcciones_incluidas']; ?></td>
         </tr>
         <tr>
            <td>Publicidad incluida</td>
            <td colspan="3"><?php echo $orden['publicidad_incluida']; ?></td>
         </tr>
         <tr>
            <td>Color de Tinta(s)</td>
            <td colspan="3">Verde</dt>
         </tr>
      </table>
      <br />
      <table class="ordenTrabajo-items">
         <thead>
            <tr>
               <td>Cantidad</td>
               <td>Trabajo</td>
               <td>Precio Unitario</td>
               <td>Valor Bs.</td>
            </tr>
         </thead>
         <tbody>
            <?php foreach($orden['items'] as $item): ?>
            <tr>
               <td><?php echo $item['cantidad'];        ?></td>
               <td><?php echo $item['producto'];     ?></td>
               <td><?php echo $item['precio_unitario']; ?></td>
               <td><?php echo $item['monto'];        ?></td>
            </tr>
            <?php endforeach; ?>
         </tbody>
         <tfoot>
            <tr>
               <td colspan="3">SubTotal</td>
               <td><?php echo $orden['subtotal']; ?></td>
            </tr>
            <tr>
               <td colspan="3">IVA</td>
               <td><?php echo $orden['iva']; ?></td>
            </tr>
            <tr>
               <td colspan="3">TOTAL A PAGAR</td>
               <td><?php echo $orden['total']; ?></td>
            </tr>
         </tfoot>
      </table>

      <br />
      <table class="ordenTrabajo-abono">
         <tr>
            <td width="20%">Valor del Trabajo</td>
            <td><?php echo $orden['total']; ?></td>
            <td width="20%">Abono</td>
            <td><?php echo $orden['abono']; ?></td>
            <td width="20%">Resta</td>
            <td><?php echo $orden['resta']; ?></td>
         </tr>
         <tr>
            <td>Tipo de Pago</td>
            <td><?php echo $orden['forma_pago']; ?></td>
            <td>Banco</td>
            <td colspan="2"><?php echo $orden['banco']?></td>
            <td><?php echo $orden['numero_comprobante']; ?></td>
         </tr>
      </table>
      <br />
      <br />
      <div class="observaciones">
      <p>Observaciones: <?php echo $orden['observaciones'] ;?></p>
      </div>
      <div class="firma">
         <br />
         <br />
         <br />
         <br />
         <br />
         <hr />
         <p style="text-align:center">CLIENTE<p>
      </div>
      <div class="firma">
         <br />
         <br />
         <br />
         <br />
         <br />
         <hr />
         <p style="text-align:center">EMPRESA<p>
      </div>
   </body>
</html>

<?php 
$dc = "DocumentoControl";
$do = "DetalleOrden";

$series = HtmlApp::getListaSerie();

if (isset($factura->documentoControls))
{
   $inicio_numero  = $factura->documentoControls->inicio_numero;
   $fin_numero     = $factura->documentoControls->fin_numero;
   $inicio_control = $factura->documentoControls->inicio_control;
   $fin_control    = $factura->documentoControls->fin_control; 
   $serie          = $factura->documentoControls->serie; 
   $identificador  = $factura->documentoControls->identificador;
   $juegos         = $factura->documentoControls->juegos; 
   $cantidad       = $factura->documentoControls->cantidad;
   $disabled='disabled';
}
else {
   $inicio_numero  = '';
   $fin_numero     = '';
   $inicio_control = '';
   $fin_control    = '';
   $serie          = 'S/N';
   $identificador  = '00';
   $juegos         = '50'; 
   $cantidad       = 6;    
   $disabled='';
}

$optHtml = array
(
   'juegos'          => array('id'=>"juegos_$item",'class'=>'short','disabled'=>$disabled),
   'cantidad'        => array('id'=>"cantidad_$item",'class'=>'short','onchange'=>"getNumeracion($item);sumatoria();",'disabled'=>$disabled),
   'identificador'   => array('id'=>"identificador_$item",'class'=>'short','disabled'=>$disabled),
   'serie'           => array('id'=>"serie_$item",'class'=>'series','onchange'=>"getNumeracion($item); changeColorRow($item);",'disabled'=>$disabled),
   'inicio_numero'   => array('id'=>"inicio_numero_$item",'class'=>'middle','disabled'=>$disabled),
   'fin_numero'      => array('id'=>"fin_numero_$item",'class'=>'middle','disabled'=>$disabled),
   'inicio_control'  => array('id'=>"inicio_control_$item",'class'=>'middle','disabled'=>$disabled),
   'fin_control'     => array('id'=>"fin_control_$item",'class'=>'middle numeracion','disabled'=>$disabled)
);
?>

<?php if (!isset($factura)) : ?>
Cantidad: <?php echo CHtml::textField("{$dc}[cantidad][]",$cantidad,$optHtml['cantidad'] ); ?>
<?php endif; ?>

<div class="cnum-box-head hide" id="cnumBoxHead-<?php echo $item; ?>">
   Juegos: <?php echo CHtml::textField("{$dc}[juegos][]",$juegos,$optHtml['juegos'] ); ?>
   Identificador: <?php echo CHtml::textField("{$dc}[identificador][]",$identificador,$optHtml['identificador']);?>
   Serie: <?php echo CHtml::dropDownList("{$dc}[serie][]",$serie,$series,$optHtml['serie']); ?>&nbsp;
</div>
<div id="cont_num-<?php echo $item ?>" class="cnum-box hide">      
      <br />
      <table class="cnum-box-table">
         <tr>
            <th class="cnum-title">Numeros de Documentos</th>
            <th class="cnum-title">Numeros de Control</th>
         </tr>
         <tr>
            <td>
               Numero Desde: <?php echo CHtml::textField("{$dc}[inicio_numero][]",$inicio_numero,$optHtml['inicio_numero']);   ?>
               Numero hasta: <?php echo CHtml::textField("{$dc}[fin_numero][]"   ,$fin_numero,$optHtml['fin_numero']);   ?>
            </td>
            </td>
            <td>
               Numero Desde: <?php echo CHtml::textField("{$dc}[inicio_control][]",$inicio_control,$optHtml['inicio_control']); ?>
               Numero hasta: <?php echo CHtml::textField("{$dc}[fin_control][]"   ,$fin_control,$optHtml['fin_control']);   ?>
            </td>
            <tr>
      </table>
</div>

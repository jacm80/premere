<?php $i=1; ?>
<?php foreach($model->facturas as $factura): ?>
   <?php foreach ($factura->facturaItems as $item): ?>
      <tr id="tr_<?php echo $i; ?>">
         <td colspan="3" class="row-data">
            <?php echo CHtml::hiddenField("factura_item_id-$i",$item->id); ?>
            <div id="delete_<?php echo $i; ?>" class="ico-delete"><?php echo CHtml::image(HtmlApp::imageUrl('icons/cancel.png')); ?></div>
            <div class="producto" style="clear:both;">
               <?php  echo CHtml::link($item->producto->descripcion,array("/producto/{$item->producto->id}")) ?>
            </div>
            <span class="precio-unitario-label">Precio Unitario:</span>&nbsp;
            <span class="precio-unitario" id="spanPrecio_<?php echo $i; ?>"><?php  echo $item->precio_unitario; ?></span>
            <br style="clear:both;" />
            <div style="text-align:right;">
               <span class="row-subtotal-value monto" id="spanMonto_<?php echo $i; ?>"><?php echo $item->monto;?></span>
               <span class="row-subtotal-label">Monto:</span>
            </div>
         <hr />
            <?php $cantidad = $item->cantidad; 
                  $optCantidad = array('id'       =>"cantidad_$i",
                                       'class'    =>'short',
                                       'disabled'=>'disabled'); ?>
            Cantidad: <?php echo CHtml::textField("DocumentoControl[cantidad][]",$cantidad,$optCantidad); ?>
            <?php if ($item->producto->documento_fiscal_id != 0): ?>
               <a id="cnum-<?php echo $i; ?>" class="cnum" onclick="hide(this);">Numeracion</a>
               <?php echo $this->renderPartial('_numeracion',array('factura'=>$factura,'item'=>$i)); ?>
            <?php endif; ?>
         </td>
         </tr>
      <?php $i++; ?>
   <?php endforeach; ?>
<?php endforeach; ?>
<?php Yii::app()->clientScript->registerScript('item', "var i = $i;",CClientScript::POS_HEAD); ?>

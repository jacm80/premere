<?php
$config_autoComplete = array(
                              //'model'=>$model,
                              'attribute'=> 'cliente_id',
                              'name'=> 'cliente_razon_social',
                              'value' => (isset($model->cliente->razon_social)) ? $model->cliente->razon_social : '',
                              'sourceUrl' => Yii::app()->request->baseUrl.'/orden/getclientes',
                              'htmlOptions'=>array('size'=>'80'),
                              'options'=>array(
                                                'showAnim'=>'fold',
                                                'minLenght'=>2,
                                                'select'   =>"js:function(event,ui){\$('#Orden_cliente_id').val(ui.item['id']);}",
                                                'change'=>"js:function(event,ui){if(!ui.item){\$('#cliente_id').val('');}else {getCliente();}}"
                                                )
                              );
$config_fechaEntrega = array(
                              'model'     => $model,
                              'name'      => 'Orden[fecha_entrega]',
                              'attribute' => 'fecha_entrega',
                              'language'  =>'es',
                              // additional javascript options for the date picker plugin
                              'options'    => array(
                                                   'showAnim'=>'fold',
                                                   'dateFormat' => 'yy-mm-dd',
                                                   'buttonText' => 'Calendario',
                                                   ),
                              'htmlOptions'=> array('style'=>'height:20px;', 'autocomplete'=>'off', 'class'=>'required'),
                              );
?>   
<div class="form">
<?php echo $form->errorSummary($model); ?>
<p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>
<table class="table-form">
   <?php echo $form->hiddenField($model,'id'); ?>
   <tr>
      <td><label class="required">Razon Social <span class="required">*</span></label></td>
      <td colspan="3">
         <?php $this->widget('zii.widgets.jui.CJuiAutoComplete',$config_autoComplete); ?>
         <?php echo $form->hiddenField($model,'cliente_id'); ?>
		   <?php echo $form->error($model,'cliente_id'); ?>
      </td>
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'firma_id'); ?></td>
      <td>
         <div id="div_firma"><?php echo (!empty($model->firma->razon_social)) ? $model->firma->razon_social : '[SELECCIONE EL CLIENTE]'; ?></div>
         <?php echo $form->error($model,'firma_id'); ?>
      </td>
      <td><?php echo $form->labelEx($model,'sede_id'); ?></td>
      <td>
         <div id="div_sede"><?php echo (!empty($model->sede->descripcion)) ? $model->sede->descripcion : '[SELECCIONE LA FIRMA]'; ?></div>
         <?php echo $form->error($model,'sede_id'); ?>
      </td>
      </tr>
      <tr>
         <td><?php echo $form->labelEx($model,'fecha_entrada'); ?></td>
         <td>
            <?php echo $form->textField($model,'fecha_entrada',array('disabled'=>'disabled','value'=>date('Y-m-d h:m:i'))); ?>
            <?php echo $form->error($model,'fecha_entrada'); ?>
         </td>
         <td><?php echo $form->labelEx($model,'fecha_entrega'); ?></td>
         <td>
            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',$config_fechaEntrega); ?>
            <?php echo $form->error($model,'fecha_entrega'); ?>
         </td>
      </tr>
      <tr>
         <td><?php echo $form->labelEx($model,'direcciones_incluidas'); ?></td>
         <td>
		      <?php echo $form->textArea($model,'direcciones_incluidas'); ?>
		      <?php echo $form->error($model,'direcciones_incluidas'); ?>
         </td>
         <td><?php echo $form->labelEx($model,'telefonos_incluidos'); ?></td>
         <td>
		      <?php echo $form->textArea($model,'telefonos_incluidos'); ?>
		      <?php echo $form->error($model,'telefonos_incluidos'); ?>
         </td>
      </tr>
      <tr>
         <td><?php echo $form->labelEx($model,'publicidad_incluida'); ?></td>
         <td>
		      <?php echo $form->textArea($model,'publicidad_incluida'); ?>
		      <?php echo $form->error($model,'publicidad_incluida'); ?>
         </td>
         <td><?php echo $form->labelEx($model,'observaciones'); ?></td>
         <td>
		      <?php echo $form->textArea($model,'observaciones'); ?>
		      <?php echo $form->error($model,'observaciones'); ?>
         </td>
      </tr>
      <tr>
         <td><label>Forma Pago</label></td>
         <td>
            <?php echo CHtml::dropDownList('Pago[forma_pago_id]',
                       $model->abonoFormaPagoId,
                       FormaPago::getListFormaPagos(),
                       array('id'=>'pagoFormaPagoId', 'style'=>'width:60%')); ?>
         </td>
         <td><label>Banco</label></td>
         <td>
            <?php echo CHtml::dropDownList('Pago[banco_id]',
                       $model->abonoBancoId,
                       Banco::getListBancos(),
                       array('id'=>'pagoBancoId','class'=>'hide')); ?>&nbsp;
         </td>
      </tr>
      <tr>
         <td><label>Numero de Comprobante</label></td>
         <td><?php echo  CHtml::textField('Pago[numero_comprobante]',$model->abonoNumeroComprobante,array('class'=>'hide')); ?>&nbsp;</td>
         <td><?php echo $form->labelEx($model,'abono'); ?></td>
         <td>
		      <?php echo $form->textField($model,'abono'); ?>
		      <?php echo $form->error($model,'abono'); ?>
         </td>
      </tr>
</table>
</div>

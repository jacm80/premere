<?php 
   HtmlApp::loadCss('orden_display');
   HtmlApp::loadCss('orden_item');
   HtmlApp::loadjs('orden');
   //----------------------------------------------
   $this->breadcrumbs = array('Orden');
   $form = $this->beginWidget('CActiveForm', array('enableClientValidation'=>TRUE)); 
   $this->widget('CTabView', array(
    'tabs'=>array(
        'ordenTab'=>array(
         //   'id'   =>'orden_tab',
            'title'=>'Datos de la Orden',
            'view' =>'_formTable',
            'data' =>array('model'=> $model,'form'=>$form),
        ),
        'detalleTab'=>array(
         //   'id'   =>'detalle_tab',
            'title'=>'Items',
            'view' =>'_detalleOrden',
            'data' =>array('model'=> $model),
        ),
        'options' => array(
                           //'collapsible'=>true,
                           'selected'=>1
                           ), 
    )));
?>
<div style="border: 1px solid #e3e3e3; padding: 4px; text-align:center;">
  <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear Orden' : 'Guardar Orden'); ?>
	</div>
</div>
<?php   $this->endWidget(); ?>

<div class="form">
<?php   
      $form=$this->beginWidget('CActiveForm', array(
	   //'id'=>'orden-form',
	   'enableAjaxValidation'=>false,
   ));
?>    
   <p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo CHtml::label('Cliente','cliente_razon_social'); ?>
      <?php
         $this->widget('zii.widgets.jui.CJuiAutoComplete', array(
                  //'model'=>$model,
                  'attribute'=>'cliente_razon_social',
                  'name'=>'cliente_razon_social',
                  //'value'=>'id',
                  'sourceUrl' => Yii::app()->request->baseUrl.'/orden/getclientes',
                  'htmlOptions'=>array('size'=>100),
                  'options'=>array(
                                    'showAnim'=>'fold',
                                    'minLenght'=>2,
                                    'select'   =>"js:function(event,ui){
                                       $('#Orden_cliente_id').val(ui.item['id']);
                                    }",
                                    'change'=>"js:function(event,ui){
                                          if(!ui.item){
                                             $('#cliente_id').val('');
                                          }
                                          else {
                                             getCliente();
                                          }
                                    }"
                                    )
                  
               ));
      ?>
		<?php echo $form->hiddenField($model,'cliente_id'); ?>
		<?php echo $form->error($model,'cliente_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'firma_id'); ?>
      <div id="div_firma">[SELECCIONE EL CLIENTE]</div>
		<?php echo $form->error($model,'firma_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sede_id'); ?>
      <div id="div_sede">[SELECCIONE LA FIRMA]</div>
		<?php echo $form->error($model,'sede_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_entrada'); ?>
		<?php echo $form->textField($model,'fecha_entrada',array('disabled'=>'disabled','value'=>date('d/m/Y'))); ?>
		<?php echo $form->error($model,'fecha_entrada'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fecha_entrega'); ?>
      <?php
         $this->widget('zii.widgets.jui.CJuiDatePicker', array('name'=>'Orden[fecha_entrega]',
                                                               'model'=>$model,
                                                               'language'=>'es',
                                                               // additional javascript options for the date picker plugin
                                                               'options'=>array('showAnim'=>'fold',),
                                                               'htmlOptions'=>array('style'=>'height:20px;'),
                                                               ));
      ?>

		<?php #echo $form->textField($model,'fecha_entrega'); ?>
		<?php echo $form->error($model,'fecha_entrega'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'direcciones_incluidas'); ?>
		<?php echo $form->textArea($model,'direcciones_incluidas',array('cols'=>100)); ?>
		<?php echo $form->error($model,'direcciones_incluidas'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'telefonos_incluidos'); ?>
		<?php echo $form->textArea($model,'telefonos_incluidos',array('cols'=>100)); ?>
		<?php echo $form->error($model,'telefonos_incluidos'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'publicidad_incluida'); ?>
		<?php echo $form->textArea($model,'publicidad_incluida',array('cols'=>100)); ?>
		<?php echo $form->error($model,'publicidad_incluida'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'observaciones'); ?>
		<?php echo $form->textArea($model,'observaciones',array('cols'=>100)); ?>
		<?php echo $form->error($model,'observaciones'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'abono'); ?>
		<?php echo $form->textField($model,'abono'); ?>
		<?php echo $form->error($model,'abono'); ?>
	</div>

   <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Crear' : 'Guardar'); ?>
	</div>
</div>
<?php $this->endWidget(); ?>

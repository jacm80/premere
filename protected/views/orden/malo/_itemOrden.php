<tr id="tr_1">
   <td colspan="3" class="row-data">
         <label>Documento:</label>&nbsp;
         <?php echo CHtml::dropDownList("producto_id[$item]",'',$productos,array('onchange'=> "findPrecio('$item',$(this).val()); sumatoria();" ));?>
         <span class="precio-unitario-label">Precio Unitario:</span>&nbsp;
         <span class="precio-unitario" id="precio_<?php echo $item; ?>">&nbsp;</span><br />
         <br />
         <div style="text-align:right;">
            <span class="row-subtotal-label">Monto:</span>
            <span class="row-subtotal-value monto" id="monto_<?php echo $item; ?>"></span>
         </div>
         <hr />
         <a id="cnum-<?php echo $item; ?>" class="cnum" onclick="hide(this);">Numeracion</a>
         <div id="cont_num-<?php echo $item ?>" class="cnum-box hide" class="cnum">
            Juegos: <?php echo CHtml::textField("juegos[$item]",'50',array('class'=>'short')); ?>
            Cantidad: <?php echo CHtml::textField("cantidad[$item]",'6',array('class'=>'short','onchange'=>'sumatoria();')); ?>
            Identificador: <?php echo CHtml::textField("identificador[$item]",'00',array('class'=>'short'));?>
            Serie: <?php echo CHtml::dropDownList("serie_$item",NULL,$series,array('onchange'=>"getNumeracion($item);")); ?>
            <br />
            <?php $this->renderPartial('_numeracion'); ?>
         </div>
   </td>
</tr>

<script type="text/javascript">
   $(document).ready(function(){
      $('#BtnNuevo').click(function(e){
         e.preventDefault( );
         location.href = base_url + '/orden/create';
      });
   });
</script>

<?php

$columns= array(
                  array(
                        'type'   => 'raw',
                        'header' => 'Numero Orden',
                        'name'   => 'id',
                        'value'  => 'sprintf("%06d",$data->id)'
                        ),
                  array(
                        'type'   => 'raw',
                        'header' => 'RIF',
                        'name'   => 'rif',
                        'value'  => '$data->rif'
                        ),
                  array(
                        'type'   => 'raw',
                        'header' => 'Firma',
                        'name'   => 'firma_razon_social',
                        'value'  => '$data->firma_razon_social'
                        ),
                  'fecha_entrada',
                  'fecha_entrega',
                  array(
                        'header' => 'Estatus',
                        'name'   => 'orden_status_id',
                        'value'  => '$data->ordenStatus->descripcion',
                        'filter' =>  OrdenStatus::statusFilter( )
                        ),
                  );
   /**/
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'=>'orden-grid',
                                                      'filter'=>$model,
                                                      'dataProvider'=>$model->search( ),
                                                      'columns'=> $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/orden/view/id/"+xid;
                                                      }'
                                                   ));


?>
<div style="margin:0 auto; text-align:center">
<?php echo CHtml::ImageButton(HtmlApp::imageUrl('icons/new.png'),array('id'=>'BtnNuevo','type'=>'button')); ?>
</div>

<!-- <button id="pruebaTab" type="button">Prueba</button> -->
<?php 
   HtmlApp::loadCss('form');
   HtmlApp::loadCss('orden');
   HtmlApp::loadjs('orden');

   $cfg_form = array(
      'id'=>'OrdenForm',
      'enableClientValidation'=>TRUE,
      'enableAjaxValidation'=>FALSE,
      'clientOptions'=>array('validateOnSubmit'=>TRUE)
   );

   $this->breadcrumbs = array('Orden');
   ?>
  
   <?php if (!$model->isNewRecord): ?> 
   <div id="content-numero">
      <div id="numero-factura">
      <div id="BtnFactura"><?php echo CHtml::image(HtmlApp::imageUrl('icons/tabla.png')); ?></div>
      <div style="top:8px;position:relative;">No. de Orden:
         <strong><?php echo sprintf('%06d',$model->numero); ?></strong>
      </div>
      <div style="clear:both;"></div>
      </div>
   </div>
   <?php endif; ?>
   <br />
   <?php $form = $this->beginWidget('CActiveForm',$cfg_form); 
   $this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
         'Orden'    => array('id'=>'Orden',  'content'=>$this->renderPartial('_form',array('model'=> $model,'form'=>$form),true)),
         'Detalle'  => array('id'=>'Detalle','content'=>$this->renderPartial('_detalle',array('model'=> $model),true))
    ),
    // additional javascript options for the tabs plugin
   'options' => array('collapsible'=>true,'selected'=>0,/*'disabled'=>array(1)*/), 
   ));

  if ($model->isNewRecord) 
  {
    $baseImponible= '0.00';
    $iva          = '0.00';
    $totalPagar   = '0.00';
  }
  else
  {
   $baseImponible = number_format($model->base_imponible,2,',','.');
   $iva           = number_format($model->iva,2,',','.');
   $totalPagar    = number_format($baseImponible + $iva,2,',','.'); 
  }
?>

<table>
   <tr>
      <td colspan="2" class="label-sumatoria">Base Imponible</td>
      <td class="value-sumatoria">Bs.&nbsp;<span id="baseImponible"><?php echo $baseImponible; ?></span></td>
   </tr>
   <tr>
      <td colspan="2" class="label-sumatoria">IVA 12%</td>
      <td class="value-sumatoria">Bs.&nbsp;<span id="iva"><? echo $iva; ?></span></td>
   </tr>
   <tr>
      <td colspan="2"  class="label-sumatoria">TOTAL a Pagar</td>
      <td class="value-sumatoria">Bs.&nbsp;<span id="totalPagar"><?php echo $totalPagar; ?></span></td>
   </tr>
</table>

<div style="text-align:center;">
  <div class="row buttons">
		<?php echo CHtml::ImageButton(HtmlApp::imageUrl('icons/accept2.jpeg'),array('id'=>'BtnGuardar','type'=>'button')); ?>
      &nbsp;
		<?php echo CHtml::ImageButton(HtmlApp::imageUrl('icons/indice.jpeg'), array('id'=>'BtnListado','type'=>'button')); ?>
      <?php if (!$model->isNewRecord): ?>
         &nbsp;
		   <?php echo CHtml::ImageButton(HtmlApp::imageUrl('icons/print.png'), array('id'=>'BtnReimprimir','type'=>'button')); ?>
      <?php endif; ?>
	</div>
</div>

<?php $this->endWidget( ); ?>

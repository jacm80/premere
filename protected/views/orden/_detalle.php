<?php ?>
<table style="border:none;">
      <tbody id="detalle-content">
         <?php if (!$model->isNewRecord): ?>
               <?php $this->renderPartial('_items',array('model'=>$model)); ?>
         <?php endif; ?>
         <tr id="detalle" class="hide"></tr>
      </tbody>
      <tfoot>
         <tr>
            <td colspan="3" style="text-align:right;">
               <?php echo CHtml::ImageButton(HtmlApp::imageUrl('icons/add.png'),array('id'=>'BtnAgregar','type'=>'button')); ?>
               <!-- <button id="agregar" type="button">Agregar</button>-->
            </td>
         </tr>
      </tfoot>
</table>

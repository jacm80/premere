<tr id="tr_<?php echo $item; ?>">
   <td colspan="3" class="row-data">
   <div id="delete_<?php echo $item; ?>" class="ico-delete"><?php echo CHtml::image(HtmlApp::imageUrl('icons/cancel.png')); ?></div>
         <?php echo CHtml::dropDownList("DetalleOrden[producto_id][]",'',$productos,
                                       array('id'=>"producto_id_$item", 'class'=>'productos',
                                             'onchange'=> "findPrecio('$item',$(this).val()); sumatoria(); changeColorRow($item)")
                                       );?>
         <span>Descuento: </span><?php echo CHtml::dropDownList('DetalleOrden[descuento_id][]','0',Descuento::getListDescuento( ),
                                       array('id'=>"descuento_id_$item",'onchange'=>"findPrecio('$item',$('#producto_id_$item').val()); sumatoria();")
                                       );?>
         <br style="clear:both;" />
         <span class="precio-unitario-label">Precio Unitario:</span>&nbsp;
         <span class="precio-unitario" id="spanPrecio_<?php echo $item; ?>">0.00</span>
         <br />
         <?php echo CHtml::hiddenField("DetalleOrden[precio][]",'0',array('id'=>"precio_$item")); ?>
         <?php echo CHtml::hiddenField("DetalleOrden[documento_fiscal_id][]",'0',array('id'=>"documento_fiscal_id_$item")); ?>
         <br />
         <div style="text-align:right;">
            <span class="row-subtotal-value monto" id="spanMonto_<?php echo $item; ?>">0.00</span>
            <span class="row-subtotal-label">Monto:</span>
            <br style="clear:both;"/>
            <?php echo CHtml::hiddenField("DetalleOrden[monto][]",'0',array('id'=>"monto_$item")); ?>
         </div>
         <hr />
         <a id="cnum-<?php echo $item; ?>" class="cnum hide" onclick="hide(this);">Numeracion</a>
         <?php $this->renderPartial('_numeracion',array('item'=>$item,'series'=>$series)); ?>
   </td>
</tr>

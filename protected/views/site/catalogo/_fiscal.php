<h1>Documentos Fiscales</h1>
<table class="table-form">
   <thead>
      <tr>
         <th>Descripcion</th>
         <th>Precio Unitario</th>
         <th>Cantidad</th>
         <th>Sub-Total</th>
         <th>IVA</th>
         <th>TOTAL</th>
      </tr>
   </thead>
   <tbody>
         <?php $cantidades = array(2,4,6,8,10,12,24); ?>
         <?php $i=0; ?>
         <?php foreach($fiscales as $f): ?>
            <?php foreach ($cantidades as $c):?>
            <tr <?php echo ($i%2==0)?'':'class="odd"' ?>>
               <td ><?php echo $f->descripcion; ?></td>
               <td class="amount"><?php echo $f->precio_unitario; ?></td>
               <td class="amount"><?php echo $c;  ?></td>
               <td class="amount"><?php echo $c*$f->precio_unitario; ?></td>
               <td class="amount"><?php echo (($c*$f->precio_unitario)*0.12); ?></td>
               <td class="amount"><?php echo (($c*$f->precio_unitario)*1.12); ?></td>
            </tr>
            <?php $i++; ?>
            <?php endforeach; ?>
      <?php endforeach; ?>
   </tbody>
</table>

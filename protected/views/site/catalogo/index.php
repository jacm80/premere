<?php HtmlApp::loadCss('form'); ?>
<?php  
   $this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
         'Fiscales'          => array('id'=>'Fiscales','content'=>$this->renderPartial('catalogo/_fiscal',array('fiscales'=> $fiscales),true)),
         'Papeleria General' => array('id'=>'General' ,'content'=>$this->renderPartial('catalogo/_general' ,array('general' => $general),true)),
    ),
    // additional javascript options for the tabs plugin
   'options' => array('collapsible'=>true,'selected'=>0,/*'disabled'=>array(1)*/), 
   ));
?>

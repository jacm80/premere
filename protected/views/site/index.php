<div style="margin:12px;">
   <h1><?php echo $empresa['razon_social']; ?></h1>

   <h2>Misi&oacute;n</h2>
   <div style="float:left;margin-right: 15px;"><?php echo CHtml::image(HtmlApp::imageUrl('mision.jpeg')); ?></div>
   <p style="text-align:justify;"><?php echo $empresa['mision']; ?></p>

   <h2>Visi&oacute;n</h2>
   <div style="float:right;margin-left: 15px;"><?php echo CHtml::image(HtmlApp::imageUrl('vision.jpeg')); ?></div>
   <p style="text-align:justify;"><?php echo $empresa['vision']; ?></p>

   <hr />

   <h2>Representantes:</h2>
   <?php echo $empresa['representantes']; ?>
</div>

<table>
   <tr>
      <th colspan="2">INGRESOS</th>
   </tr>
   <tr>
      <td style="width: 50%" class="title">Trabajos Nuevos</td>
      <td><?php echo number_format($oNuevas_montoRecibidoTT,2,',','.'); ?></td>
   </tr>
   <tr>
      <td class="title">Cancelaciones</td>
      <td><?php echo number_format($oPagos_montoRecibidoTT,2,',','.'); ?></td>
   </tr>
   <tr>
      <td class="title">Efectivo en Caja</td>
      <td><?php echo number_format($montoEfectivo,2,',','.'); ?></td>
   </tr>
   <tr>
      <td class="title">Cheques / Banco</td>
      <td><?php echo number_format($montoBanco,2,',','.'); ?></td>
   </tr>
</table>
<br />
<table>
   <tr>
      <th colspan="2">EGRESOS</th>
   </tr>
   <tr>
      <td style="width: 50%" class="title2">Compras</td>
      <td>0.00</td>
   </tr>
   <tr>
      <td class="title2">Adelanto de Prestaciones</td>
      <td>0.00</td>
   </tr>
   <tr>
      <td class="title2">Otros Gastos</td>
      <td>0.00</td>
   </tr>
</table>

<table>
   <tr>
      <th colspan="6">Gastos / Egresos</th>
   </tr>
   <tr>
      <th>No. Factura/Recibo</th>
      <th>Concepto</th>
      <th>Monto</th>
      <th>IVA</th>
      <th>Por Pagar</th>
   </tr>
   <?php if (!empty($gastos)): ?>
   <?php foreach($gastos as $g): ?>   
   <tr>
      <td><?php echo $g['numero'];   ?></td>
      <td><?php echo $g['concepto']; ?></td>
      <td><?php echo $g['monto'];    ?></td>
      <td><?php echo $g['iva'  ];    ?></td>
      <td><?php #echo $g['deuda'];    ?></td>
   </tr>
   <?php endforeach; ?>
   <?php else: ?>
      <tr>
         <td colspan="5" style="text-align:center">No hay registros...</td>
      </tr>
   <?php endif; ?>
   <tr>
      <td colspan="3" class="title">Monto Pagado (Efectivo)</td>
      <td><?php #echo number_format($oGastos_montoEF,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="3" class="title">Monto Pagado (Cheques)</td>
      <td><?php #echo number_format($oGastos_montoCH,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="3" class="title">Monto Pagado (Otros)</td>
      <td><?php #echo number_format($oGastos_montoBC,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="3" class="title">Monto Total en Gastos</td>
      <td><?php #echo number_format($oGastos_montoTT,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="4" class="title">Monto Pendiente por Pagar</td>
      <td><?php #echo number_format($oGastos_montoPendiente,2,',','.'); ?></td>
   </tr>
</table>

<table>
   <tr>
      <th colspan="6">Ordenes Nuevas</th>
   </tr>
   <tr>  
      <th style="width: 10%;">No. Orden</th>
      <th style="width: 40%;">Cliente</th>
      <th>Valor Total</th>
      <th>Abono</th>
      <th>Resta</th>
      <th class="relleno">&nbsp;</th>
   </tr>
   <?php if (!empty($ordenesNuevas)): ?>
   <?php foreach($ordenesNuevas as $on): ?>   
   <tr>
      <td><?php echo $on['id'];      ?></td>
      <td><?php echo $on['cliente']; ?></td>
      <td><?php echo $on['total'];   ?></td>
      <td><?php echo $on['abono'];   ?></td>
      <td><?php echo $on['resta'];   ?></td>
      <th class="relleno">&nbsp;</th>
   <tr>
   <?php endforeach; ?>
      <td><?php echo $oNuevas_cantidad; ?></td>
      <td colspan="6" class="relleno">&nbsp;</td>
   </tr>
   <?php else: ?>
      <tr>
         <td colspan="6" style="text-align:center">No hay registros...</td>
      </tr>
   <?php endif; ?>
   <tr>
      <td colspan="3" class="title">Monto Recibido (Efectivo)</td>
      <td><?php echo number_format($oNuevas_montoRecibidoEF,2,',','.'); ?></td>
      <th class="relleno">&nbsp;</th>
      <th class="relleno">&nbsp;</th>
   </tr>
   <tr>
      <td colspan="3" class="title">Monto Recibido (Cheques)</td>
      <td><?php echo number_format($oNuevas_montoRecibidoCH,2,',','.'); ?></td>
      <th class="relleno">&nbsp;</th>
      <th class="relleno">&nbsp;</th>
   </tr>
   <tr>
      <td colspan="3" class="title">Monto Recibido (Otros)</td>
      <td><?php echo number_format($oNuevas_montoRecibidoBC,2,',','.'); ?></td>
      <th class="relleno">&nbsp;</th>
      <th class="relleno">&nbsp;</th>
   </tr>
   <tr>
      <td colspan="3" class="title">Monto Recibido en Abonos</td>
      <td><?php echo number_format($oNuevas_montoRecibidoAB,2,',','.'); ?></td>
      <th class="relleno">&nbsp;</th>
      <th class="relleno">&nbsp;</th>
   </tr>
   <tr>
      <td colspan="2" class="title">Monto Total</td>
      <td><?php echo number_format($oNuevas_montoRecibidoTT,2,',','.'); ?></td>
      <th colspan="3" class="relleno">&nbsp;</th>
   </tr>
   <tr>
      <td colspan="4" class="title">Monto Pendiente</td>
      <td><?php echo number_format($oNuevas_montoPendiente,2,',','.'); ?></td>
      <th class="relleno">&nbsp;</th>
   </tr>

</table>

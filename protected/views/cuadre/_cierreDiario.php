<h3>Relacion de Ingresos al Dia <?php echo $fecha ?></h3>

<?php $this->renderPartial('_resumen',array(
                                             'oNuevas_montoRecibidoTT' => $oNuevas_montoRecibidoTT,
                                             'oPagos_montoRecibidoTT'  => $oPagos_montoRecibidoTT,
                                             'montoEfectivo' => $montoEfectivo,
                                             'montoBanco'    => $montoBanco
                                           ));?>

<br />

<?php $this->renderPartial('_ordenNuevas',array(
                                                'ordenesNuevas'          => $ordenesNuevas,
                                                'oNuevas_cantidad'       => $oNuevas_cantidad,
                                                'oNuevas_montoRecibidoEF'=> $oNuevas_montoRecibidoEF,
                                                'oNuevas_montoRecibidoCH'=> $oNuevas_montoRecibidoCH,
                                                'oNuevas_montoRecibidoBC'=> $oNuevas_montoRecibidoBC,
                                                'oNuevas_montoRecibidoAB'=> $oNuevas_montoRecibidoAB,
                                                'oNuevas_montoRecibidoTT'=> $oNuevas_montoRecibidoTT,
                                                'oNuevas_montoPendiente' => $oNuevas_montoPendiente
                                                ));?>

<br />


<?php $this->renderPartial('_ordenPagos',array(
                                                'ordenesPagos'          => $ordenesPagos,
                                                'oPagos_cantidad'       => $oPagos_cantidad,
                                                'oPagos_montoRecibidoEF'=> $oPagos_montoRecibidoEF,
                                                'oPagos_montoRecibidoCH'=> $oPagos_montoRecibidoCH,
                                                'oPagos_montoRecibidoBC'=> $oPagos_montoRecibidoBC,
                                                'oPagos_montoRecibidoTT'=> $oPagos_montoRecibidoTT,
                                                'oPagos_montoPendiente' => $oPagos_montoPendiente
                                                ));?>
<br />

<?php $this->renderPartial('_gastos',array(
                                          'gastos'=> $gastos,
                                          ));?>

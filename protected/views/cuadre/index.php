<?php HtmlApp::loadCss('cuadre');
$config_fecha = array(
   'name'      => 'fecha',
   'attribute' => 'fecha_entrega',
   'language'  =>'es',
   // additional javascript options for the date picker plugin
   'options'    => array(
                        'showAnim'=>'fold',
                        'dateFormat' => 'yy-mm-dd',
                        'buttonText' => 'Calendario',
                     ),
                     'htmlOptions'=> array('style'=>'height:20px;', 'autocomplete'=>'off', 'class'=>'required'),
   );
?>
<script type="text/javascript">
   
   $(document).ready(function(){
      $('#btnProcesar').click(function(){
         $.ajax({
                  url: base_url+'/cuadre/cierre',
                  async: true,
                  type: 'post',
                  data: { fecha : $('#fecha').prop('value') },
                  dataType:'html',
                  success:function(respuesta){ 
                                                $('#selFecha').addClass('hide');
                                                $('#div_cierre').html(respuesta); 
                                             },
                  error:function(){ alert('fallo'); }
               });
         });
   });

</script>

<div style="padding: 8px;text-align:center;">
   <div style="width:400px; border:1px solid black; margin:0 auto; border: 2px solid #e3e3e3;" id="selFecha">
      <h3 style="text-align:center; padding: 8px; background-color: #f1f1f1;">Fecha del Cierre</h3>      
      <div>
         <?php $this->widget('zii.widgets.jui.CJuiDatePicker',$config_fecha); ?>
         <br />
         <button id="btnProcesar">Procesar</button>
         <br />
         <br />
      </div>
   </div>
</div>

<div id="div_cierre"></div>

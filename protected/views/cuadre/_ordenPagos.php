<table>
   <tr>
      <th colspan="6">Ordenes Canceladas</th>
   </tr>
   <tr>
      <th>No. Orden</th>
      <th>Cliente</th>
      <th>Valor Total</th>
      <th>Deuda</th>
      <th>Cancelado</th>
      <th>Saldo</th>
   </tr>
   <?php if (!empty($ordenesPagos)): ?>
   <?php foreach($ordenesPagos as $op): ?>   
   <tr>
      <td><?php echo $op['id'       ]; ?></td>
      <td><?php echo $op['cliente'  ]; ?></td>
      <td><?php echo $op['total'    ]; ?></td>
      <td><?php echo $op['resta'    ]; ?></td>
      <td><?php echo $op['cancelado']; ?></td>
      <td><?php echo $op['saldo'    ]; ?></td>
   </tr>
   <?php endforeach; ?>
   <?php else: ?>
      <tr>
         <td colspan="6" style="text-align:center">No hay registros...</td>
      </tr>
   <?php endif; ?>
   <tr>
      <td colspan="4" class="title">Monto Recibido (Efectivo)</td>
      <td><?php echo number_format($oPagos_montoRecibidoEF,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="4" class="title">Monto Recibido (Cheques)</td>
      <td><?php echo number_format($oPagos_montoRecibidoCH,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="4" class="title">Monto Recibido (Otros)</td>
      <td><?php echo number_format($oPagos_montoRecibidoBC,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="4" class="title">Monto Recibido en Pagos</td>
      <td><?php echo number_format($oPagos_montoRecibidoTT,2,',','.'); ?></td>
      <td class="relleno">&nbsp;</td>
   </tr>
   <tr>
      <td colspan="5" class="title">Monto Pendiente</td>
      <td><?php echo number_format($oPagos_montoPendiente,2,',','.'); ?></td>
   </tr>
</table>

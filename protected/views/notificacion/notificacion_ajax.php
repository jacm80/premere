<div id="content-notifications">
	<div id="content-notifications-buttons">
    	<span id="notifications-numbers">
		<?php echo (isset($notificaciones)) ? (count($notificaciones) > 0) ? '' : 'No' : 'No'; ?>
		Tiene 
		<?php echo (isset($notificaciones)) ? (count($notificaciones) > 0) ? count($notificaciones) : '' : '' ; ?> notificaciones</span>
		<span id="maximize" onClick="javascript:document.getElementById('notifications').style.display='block'"></span>
		<span id="minimize" onClick="javascript:document.getElementById('notifications').style.display='none'"></span>        
	</div>
	<div id="notifications">
		<?php if (!empty($notificaciones)): ?>
		<?php foreach ($notificaciones as $n): ?>		
   		<p id="notification-item" class="row">
      		<span class="title"><?php echo $n['titulo']; ?></span>
   			<span class="article"><?php echo $n['descripcion']; ?></span>
   		</p>
		<?php endforeach; ?>
		<?php else: ?>
   		<p id="notification-item" class="row">
      		<span class="title">Informacion</span>
         	<span class="article">No hay notificaciones nuevas por los momentos...</span>
   		</p>
		<?php endif; ?>
	</div>
</div>

<?php $this->breadcrumbs=array('Test'); ?>
<table>
      <thead>
      <tr>
         <th>COD</th>
         <th>NUM_FACT_VENTA</th>
         <th>TIPO</th>
         <th>FECHA_IMPRESION</th>
         <th>SERIE</th>
         <th>IDENTIFICADOR</th>
         <th>INICIO_NUM</th>
         <th>FIN_NUM</th>
         <th>INICIO_CONTROL</th>
         <th>FIN_CONTROL</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($model as $m): ?>
      <tr>
         <td><?php echo $m->COD; ?></td>
         <td><?php echo $m->NUM_FACT_VENTA; ?></td>
         <td><?php echo $m->TIPO; ?></td>
         <td><?php echo $m->FECHA_IMPRESION; ?></td>
         <td><?php echo $m->SERIE ?></td>
         <td><?php echo $m->IDENTIFICADOR; ?></td>
         <td><?php echo $m->INICIO_NUM; ?></td>
         <td><?php echo $m->FIN_NUM; ?></td>
         <td><?php echo $m->INICIO_CONTROL; ?></td>
         <td><?php echo $m->FIN_CONTROL; ?></td>
      </tr>
      <?php endforeach; ?>
      </tbody>
</table>

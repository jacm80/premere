<?php
$this->breadcrumbs=array(
	'Productos'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>Yii::t('app','List')    . ' Producto', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create')  . ' Producto', 'url'=>array('create')),
	array('label'=>Yii::t('app','Update')  . ' Producto', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Delete')  . ' Producto', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>Yii::t('app','Manage')  . ' Producto', 'url'=>array('admin')),
);
?>

<h1>View Producto #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'descripcion',
		'precio_unitario',
		'cantidad_minima',
		'documento_fiscal_id',
	),
)); ?>

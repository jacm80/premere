<?php
$this->breadcrumbs=array(
	'Productos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Producto', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Producto', 'url'=>array('admin')),
);
?>

<h1>Crear Producto</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'descripcion'); ?>
		<?php echo $form->textArea($model,'descripcion',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'precio_unitario'); ?>
		<?php echo $form->textField($model,'precio_unitario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cantidad_minima'); ?>
		<?php echo $form->textField($model,'cantidad_minima'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'documento_fiscal_id'); ?>
		<?php echo $form->textField($model,'documento_fiscal_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
$this->breadcrumbs=array(
	'Productos'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Producto', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Producto', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Producto', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Producto', 'url'=>array('admin')),
);
?>

<h1>Update Producto <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Productos',
);

$this->menu=array(
	array('label'=>Yii::t('app','Create').' Producto', 'url'=>array('create')),
	array('label'=>Yii::t('app','Manage') .' Producto', 'url'=>array('admin')),
);
?>

<h1>Productos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('descripcion')); ?>:</b>
	<?php echo CHtml::encode($data->descripcion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('precio_unitario')); ?>:</b>
	<?php echo CHtml::encode($data->precio_unitario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cantidad_minima')); ?>:</b>
	<?php echo CHtml::encode($data->cantidad_minima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('documento_fiscal_id')); ?>:</b>
	<strong><?php echo CHtml::encode($data->documentoFiscal->descripcion); ?></strong>
	<br />


</div>

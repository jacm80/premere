<?php
$this->breadcrumbs = array('Material Salida');
?>
<h1>Salidas de Material</h1>
<?php 
   $columns = array(
                     array(
                        'type'=>'raw',
                        'header'=>'Numero',
                        'name'=>'id',
                        'value'=>'sprintf("%06d",$data->id)'
                     ),
                     'fecha',
                     array(
                        'type'=>'raw',
                        'header'=>'entrega',
                        'name'=>'entrega',
                        'value'=>'$data->entregado->nombre_completo( )'
                        ),
                     array(
                        'type'=>'raw',
                        'header'=>'entrega',
                        'name'=>'entrega',
                        'value'=>'$data->recibido->nombre_completo( )'
                        )
                   );
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'=>'salida-grid',
                                                      'filter'=>$model,
                                                      'dataProvider'=>$model->search( ),
                                                      'columns'=> $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/materialSalida/view/id/"+xid;
                                                      }'
                                                   ));
?>
<div style="text-align:center;">
   <button type="button" onclick="location.href=base_url+'/materialSalida/create'">Nueva Salida</button>
</div>

<html>
   <head>Recibo Material</head>
   <body>
      <table>
         <tr>
            <td class="label">Entrega</td>
            <td><?php echo $model->entregado->nombre_completo(); ?></td>
         </tr>
         <tr>
            <td class="label">Recibe</td>
            <td><?php echo $model->recibido->nombre_completo(); ?></td>
         </tr>
         <tr>
            <td class="label">Control No.</td>
            <td><?php echo sprintf('%06d',$model->id); ?></td>
         </tr>
      </table>
      <p>
         En la fecha he recibido los <strong>materiales</strong> que se detallan a continuacion:
      </p>
      <table>
         <thead>
            <tr>
               <th>Item</th>
               <th>Descripcion</th>
               <th>Cantidad</th>
            </tr>
         </thead>
         <tbody>
            <?php $i=0;?>
            <?php foreach($model->Items as $item): ?>
            <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $item->catalogoMaterial->descripcion; ?></td>
               <?php if ($item->tipo_cantidad==1): ?>
                  <td><?php echo $item->cantidad. ' '.$item->catalogoMaterial->unidad->descripcion; ?></td>
               <?php else: ?>
                  <td><?php echo $item->cantidad.' '.$item->catalogoMaterial->unidad->descripcion_detallado; ?></td>
               <?php endif ?>
            </tr>
            <?php endforeach; ?>
         </tbody>
      </table>
      <p>
         Se utilizaran para mi beneficio personal, con la idea de devolverlos en un futuro, 
         o cambiar los mismos por horas de trabajo, que justifiquen este egreso a la empresa. 
      </p>
   </body>
</html>

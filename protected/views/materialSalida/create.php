<?php
HtmlApp::loadCss('form');
//HtmlApp::loadCss('materialsalida');
//-----------------------------------------------
HtmlApp::loadjQueryUI();
HtmlApp::loadJs('jquery.yiigridview');
HtmlApp::loadJs('materialsalida');
HtmlApp::loadJs('mustache-master/mustache');
HtmlApp::loadJs('jquery-Mustache/jquery.mustache');

/* @var $this MaterialSalidaController */
$cfg_form = array(
   'id'=>'salidaForm',
   'enableClientValidation'=>TRUE,
   'enableAjaxValidation'=>FALSE,
   'clientOptions'=>array('validateOnSubmit'=>TRUE)
);

$this->breadcrumbs = array('Material Salida'=>array('/materialSalida'),'Create',);
?>

<h1>Registro de Salidas de Material</h1>

<div id="dialog"></div>

<?php 
$form=$this->beginWidget('CActiveForm', $cfg_form);
$this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
         'Salida'   => array('id'=>'Salida' ,'content'=>$this->renderPartial('_form'   ,array('model'=> $model,'form'=>$form),true)),
         'Detalle'  => array('id'=>'Detalle','content'=>$this->renderPartial('_detalle',array('model'=> $model),true))
    ),
    // additional javascript options for the tabs plugin
   'options' => array('collapsible'=>true,'selected'=>0,/*'disabled'=>array(1)*/), 
   ));
?>

<?php $this->endWidget(); ?>

<?php foreach ($model->Items as $i): ?>
   <?php $tipo_cantidad = ($i->tipo_cantidad==1) ? $i->catalogoMaterial->unidad->descripcion : $i->catalogoMaterial->unidad->descripcion_detallado; ?>
   <tr>
      <td><?php echo $i->id; ?></td>
      <td><?php echo $i->catalogoMaterial->barcode;     ?></td>
      <td><?php echo $i->catalogoMaterial->descripcion; ?></td>
      <td><?php echo $model->almacen->descripcion; ?></td>
      <td><?php echo $i->existencia_unidad(); ?></td>
      <td><?php echo $i->existencia_detallado(); ?></td>
      <td style="background-color:yellow"><div><?php echo $i->cantidad  ?></div><?php echo $tipo_cantidad; ?></td>
      <td>&nbsp;</td>
   </tr>
<?php endforeach; ?>

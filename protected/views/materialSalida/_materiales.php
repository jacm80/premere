<?php
   $columns = array(
                     'id',
                     array(
                           'type'   =>'raw',
                           'header' =>'Material',
                           'name'   =>'catalogo_material_id',
                           'value'  =>'$data->catalogoMaterial->descripcion'
                           ),
                     array(
                           'type'   =>'raw',
                           'header' =>'Almacen',
                           'name'   =>'almacen_id',
                           'value'  =>'$data->almacen->descripcion'
                          ),
                     'existencia_unidad',
                     array(
                        'class' => 'CCheckBoxColumn'
                     )
                   );
?>


<form method="get" action="<?php echo Yii::app()->request->baseUrl; ?>/materialSalida/getMateriales" id="materialForm">
<?php
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'           => 'material-grid',
                                                      'filter'       => $model,
                                                      'dataProvider' => $model->search( ),
                                                      'columns'      => $columns,
                                                   ));
?>
</form>

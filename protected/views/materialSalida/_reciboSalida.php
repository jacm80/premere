<html>
   <head>
      <title>Recibo de Material</title>
   </head>
   <body>
      <div>
         {{empresa}}
      </div>
      <table>
         <tr>
            <td class="label">Entrega</td>
            <td><?php echo $model->entregado->nombre_completo(); ?></td>
         </tr>
         <tr>
            <td class="label">Recibe</td>
            <td><?php echo $model->recibido->nombre_completo(); ?></td>
         </tr>
         <tr>
            <td class="label">Control No.</td>
            <td><?php echo sprintf('%06d',$model->id); ?></td>
         </tr>
      </table>
      <p>
         En la fecha he recibido los <strong>materiales</strong> que se detallan a continuacion:
      </p>
      <table>
         <thead>
            <tr>
               <th>Item</th>
               <th>Descripcion</th>
               <th>Cantidad</th>
            </tr>
         </thead>
         <tbody>
            <?php $i=0;?>
            <?php foreach($model->Items as $item): ?>
            <tr>
               <td><?php echo $i; ?></td>
               <td><?php echo $item->catalogoMaterial->descripcion; ?></td>
               <?php if ($item->tipo_cantidad==1): ?>
                  <td><?php echo $item->cantidad. ' '.$item->catalogoMaterial->unidad->descripcion; ?></td>
               <?php else: ?>
                  <td><?php echo $item->cantidad.' '.$item->catalogoMaterial->unidad->descripcion_detallado; ?></td>
               <?php endif ?>
            </tr>
            <?php endforeach; ?>
         </tbody>
      </table>
      <p>y se me ha informado de:</p>
      <ol>
         <li>Forma correcta de usarlo</li>
         <li>Mantenimiento y conservacion</li>
         <li>Necesidad de uso en mi sector de trabajo</li>
      </ol>
      <table class="footer">
         <tr>
            <td class="fecha">Fecha: <?php echo date('d/m/Y h:m:i'); ?></td>
            <td class="firma">Firma</td>
         </tr>
      </table>
   </body>
</html>

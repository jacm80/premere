<div class="form">
<?php echo $form->errorSummary($model); ?>
<p class="note">Campos con <span class="required">*</span> son requeridos.</p>
   
<table class="table-form">
   <tr>
      <td><?php echo $form->labelEx($model,'fecha'); ?></td>
      <td>
         <?php echo $form->hiddenField($model,'id'); ?>
         <?php echo $form->textField($model,'fecha',array('value'=>date('Y-m-d'),'disabled'=>'disabled')); ?>
         <?php echo $form->error($model,'fecha'); ?>
      </td>
      <td><?php echo $form->labelEx($model,'tipo_salida_id'); ?></td>
      <td>
         <?php echo $form->dropDownList($model,'tipo_salida_id',MaterialTipoSalida::getListaMaterialTipoSalidas()); ?>
         <?php echo $form->error($model,'tipo_salida_id'); ?>
      </td>
   </tr>
   <tr>
   <td><?php echo $form->labelEx($model,'almacen_id'); ?></td>
      <td>
         <?php echo $form->dropDownList($model,'almacen_id',Almacen::getListAlmacenes(),array('class'=>'hide')); ?>
         <?php echo $form->error($model,'almacen_id'); ?>
      </td>
      <td><?php echo $form->labelEx($model,'entrega'); ?></td>
      <td>
         <?php echo $form->dropDownList($model,'entrega',Personal::getListPersonalActivo()); ?>
         <?php echo $form->error($model,'entrega'); ?>
      </td>
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'recibe'); ?></td>
      <td>
         <?php echo $form->dropDownList($model,'recibe',Personal::getListPersonalActivo()); ?>
         <?php echo $form->error($model,'recibe'); ?>
      </td>
      <td><?php echo $form->labelEx($model,'observacion'); ?></td>
      <td>
         <?php echo $form->textArea($model,'observacion'); ?>
         <?php echo $form->error($model,'observacion'); ?>
      </td>
   </tr>
</table>
</div>

<div style="/*border: 1px solid #e3e3e3; padding: 4px; */text-align:center;*/">
  <div class="row buttons">
		<?php echo CHtml::Button($model->isNewRecord ? 'Guardar' : 'Actualizar',array('id'=>'BtnGuardarCompra')); ?>
      &nbsp;
		<?php echo CHtml::Button('Listado',array('id'=>'BtnListado','onclick'=>'location.href=base_url+"/materialSalida"')); ?>
      <?php if (!$model->isNewRecord): ?>
         &nbsp;
		   <?php echo CHtml::Button('Recibo',array('id'=>'BtnReimprimir')); ?>
	   <?php endif; ?>
   </div>
</div>

<table class="table-form">
      <thead>
         <tr>
            <th>ID</th>
            <th>Barcode</th>
            <th>Material</th>
            <th>Almacen</th>
            <th style="width:5%;">Existencia</th>
            <th style="width:5%;">Detallado</th>
            <th>Cantidad</th>
            <th>&nbsp;</th>
         </tr>
      </thead>
      <tbody id="detalle-content">
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
         </tr>
         <?php if (!$model->isNewRecord): ?>
               <?php $this->renderPartial('_items',array('model'=>$model)); ?>
         <?php endif; ?>
      </tbody>
      <tfoot>
         <?php if ($model->isNewRecord): ?>
         <tr>
            <td colspan="8"><button id="agregar" type="button">Agregar</button></td>
         </tr>
         <?php else:?>
         <tr>
            <td colspan="8"><button id="reversar" type="button">Reversar Salida</button></td>
         </tr>
         <?php endif; ?>
      </tfoot>
</table>

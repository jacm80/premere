<?php
$this->breadcrumbs=array(
	'Marcas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   .' Marca', 'url'=>array('index')),
	array('label'=>Yii::t('app','Create') .' Marca', 'url'=>array('create')),
	array('label'=>Yii::t('app','View')   .' Marca', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>Yii::t('app','Manage') .' Marca', 'url'=>array('admin')),
);
?>

<h1>Update Marca <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
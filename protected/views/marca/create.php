<?php
$this->breadcrumbs=array(
	'Marcas'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>Yii::t('app','List')   . ' Marca', 'url'=>array('index')),
	array('label'=>Yii::t('app','Manage') . ' Marca', 'url'=>array('admin')),
);
?>

<h1>Crear Marca</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
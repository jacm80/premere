<table class="tabular-data">
   <thead>
      <tr>
         <th style="background:#fff;border:none;">&nbsp;</th>
         <th class="sistema" colspan="2">Sistema</th>
         <th class="real" colspan="2">Real</th>
         <th class="diferencia" colspan="2">Diferencia</th>
      </tr>
      <tr>
         <th style="background:#e3e3e3;width:60%;text-align:center">MATERIALES</th>
         <th>Unidad</th>
         <th>Detalle</th>
         <th>Unidad</th>
         <th>Detalle</th>
         <th>Unidad</th>
         <th>Detalle</th>
      </tr>
   </thead>
   <tbody>
      <?php $i=0; ?>
      <?php foreach ($model as $m): ?>
         <tr>
            <td class="descripcion_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div>
                  <?php echo $m->catalogoMaterial->descripcion; ?>
                  <?php echo CHtml::hiddenField('items[catalogo_material_id][]',$m->id); ?>
               </div>
            </td>
            <!-- sistema -->
            <td class="sistema_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div id="unidad_sistema-<?php echo $m->id; ?>"><?php echo $m->existencia_unidad; ?></div>
               <?php echo CHtml::hiddenField('items[existencia_unidad_sistema][]',$m->existencia_unidad); ?>
            </td>
            <td class="sistema_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div id="detallado_sistema-<?php echo $m->id; ?>"><?php echo $m->existencia_detallado; ?></div>
               <?php echo CHtml::hiddenField('items[existencia_detallado_sistema][]',$m->existencia_detallado); ?>
            </td>
            <!-- real -->
            <td class="real_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo CHtml::textfield("items[existencia_unidad_real][]",$m->existencia_unidad,
                           array('style'=>'width:100%',
                                 'id'=>"existencia_unidad-{$m->id}",
                                 'onchange'=>"calcularUnidad({$m->id})")); ?>
            </td>
            <td class="real_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo CHtml::textfield("items[existencia_detallado_real][]",$m->existencia_detallado,
                           array('style'=>'width:100%',
                                 'id'=>"existencia_detallado-{$m->id}",
                                 'onchange'=>"calcularDetallado({$m->id})")); ?>
            </td>
            <!-- diferencia -->
            <td class="diferencia_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div id="diferencia_unidad-<?php echo $m->id; ?>">0</div>
            </td>
            <td class="diferencia_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div id="diferencia_detallado-<?php echo $m->id; ?>">0</div>
            </td>
         </tr>
         <?php $i++; ?>
      <?php endforeach; ?>
   </tbody>
</table>

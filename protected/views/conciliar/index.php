<?php
/* @var $this ConciliarInventarioController */

$this->breadcrumbs=array('Conciliar Inventario','index');
?>
<h1>Lista de Conciliaciones</h1>
<?php 
   $columns = array( 
                     'id',
                     'fecha',
                     array(
                        'type'=>'raw',
                        'header'=>'Almacen',
                        'name'=>'almacen_id',
                        'value'=>'$data->almacen->descripcion'
                     ),
                     array(
                        'type'=>'raw',
                        'header'=>'Inspecciono',
                        'name'=>'inspecciono',
                        'value'=>'$data->personal->nombres." ".$data->personal->apellidos'
                     )
                   );
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'=>'conciliar-grid',
                                                      'filter'=>$model,
                                                      'dataProvider'=>$model->search( ),
                                                      'columns'=> $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/conciliar/view/id/"+xid;
                                                      }'
                                                   ));
?>

<div style="text-align:center;">
   <button type="button" onclick="location.href=base_url+'/conciliar/create'">Nueva Conciliaci&oacute;n</button>
</div>

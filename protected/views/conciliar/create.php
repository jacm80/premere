<?php
   HtmlApp::loadCss('form');
   HtmlApp::loadCss('conciliacion');
   HtmlApp::loadJs('conciliacion');
   HtmlApp::loadjQueryUI();
   $this->breadcrumbs=array('Conciliar Inventario'=>array('/conciliarInventario'),'Create'); 
?>
<h1 style="text-align:center;">Conciliar Inventario</h1>

<?php $this->renderPartial('_form',array('model'=>$model)); ?>

<div style="margin:0 auto; text-align:center;">
   <?php if ($model->isNewRecord): ?>
      <button id="BtnGuardar" type="button">Guardar</button>
   <?php else: ?>
      <button id="BtnReimprimir" type="button">Reimprimir</button>
   <?php endif; ?>
   &nbsp;
   <button id="BtnListado" type="button">Listado</button>
</div>

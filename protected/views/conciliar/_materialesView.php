<table class="tabular-data">
   <thead>
      <tr>
         <th style="background:#fff;border:none;">&nbsp;</th>
         <th class="sistema" colspan="2">Sistema</th>
         <th class="real" colspan="2">Existencia</th>
         <th class="diferencia" colspan="2">Diferencia</th>
      </tr>
      <tr>
         <th style="background:#e3e3e3;width:60%;text-align:center">MATERIALES</th>
         <th>Unidad</th>
         <th>Detalle</th>
         <th>Unidad</th>
         <th>Detalle</th>
         <th>Unidad</th>
         <th>Detalle</th>
      </tr>
   </thead>
   <tbody>
      <?php $i=0; ?>
      <?php foreach ($model->Items as $item): ?>
         <tr>
            <td class="descripcion_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo $item->catalogoMaterial->descripcion; ?>
            </td>
            <!-- sistema -->
            <td class="sistema_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo $item->existencia_unidad_sistema; ?>
            </td>
            <td class="sistema_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo $item->existencia_detallado_sistema; ?>
            </td>
            <!-- real -->
            <td class="real_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo $item->existencia_unidad_real; ?>
            </td>
            <td class="real_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <?php echo $item->existencia_detallado_real; ?>
            </td>
            <!-- diferencia -->
            <td class="diferencia_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div <?php echo ($item->diferencia_unidad < 0) ? 'class="rojo"' :''; ?>><?php echo $item->diferencia_unidad; ?></div>
            </td>
            <td class="diferencia_<?php echo ($i % 2==0) ? 'light':'dark' ?>">
               <div <?php echo ($item->diferencia_detallado < 0) ? 'class="rojo"' :''; ?>><?php echo $item->diferencia_detallado; ?><div>
            </td>
         </tr>
         <?php $i++; ?>
      <?php endforeach; ?>
   </tbody>
</table>

<div class="form">
   <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'conciliacionForm',
	'enableAjaxValidation'=>false,
   )); ?>
   <?php echo $form->errorSummary($model); ?>   
   <p class="note">Los Campos con <span class="required">*</span> son requeridos.</p>   
   <table class="table-form">
   <tr>
      <td><?php echo $form->labelEx($model,'fecha'); ?></td>
      <td>
         <?php if ($model->isNewRecord): ?>
            <?php echo $form->textField($model,'fecha',array('disabled'=>'disabled','value'=>date('Y-m-d'))); ?>
		      <?php echo $form->error($model,'fecha'); ?>
         <?php else: ?>
            <?php echo $model->fecha; ?>
         <?php endif; ?>
      </td> 
      <td>
         <?php echo $form->labelEx($model,'inspecciono'); ?>
      </td>
      <td>
         <?php if ($model->isNewRecord): ?>
		      <?php echo $form->dropDownList($model,'inspecciono',Personal::getListPersonalActivo()); ?>
            <?php echo $form->error($model,'inspecciono'); ?>
         <?php else: ?>
            <?php echo $model->personal->nombres . ' ' . $model->personal->apellidos; ?>
         <?php endif; ?>
      </td>
   </tr>
   <tr>
      <td><?php echo $form->labelEx($model,'almacen_id'); ?></td>
		<td>
         <?php if ($model->isNewRecord): ?>
            <?php echo $form->dropDownList($model,'almacen_id',Almacen::getListAlmacenes()); ?>
            <?php echo $form->error($model,'almacen_id'); ?>
         <?php else: ?>
            <?php echo $model->almacen->descripcion; ?>
         <?php endif; ?>
      </td>
      <td><?php echo $form->labelEx($model,'observacion'); ?></td>
      <td>
         <?php if ($model->isNewRecord): ?>
		      <?php echo $form->textArea($model,'observacion',array('style'=>'width:95%')); ?>
            <?php echo $form->error($model,'observacion'); ?>
         <?php else: ?>
            <?php echo $model->observacion; ?>
         <?php endif; ?>
      </td>
   </tr>
   </table>
   <?php if (!$model->isNewRecord): ?>
      <?php $this->renderPartial('_materialesView',array('model'=>$model)); ?>
   <?php else: ?>
      <div id="viewMateriales">&nbsp;&nbsp;</div>
   <?php endif; ?>
   <?php $this->endWidget(); ?>
</div>

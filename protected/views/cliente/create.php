<style></style>
<?php
// estilos personalizados

HtmlApp::loadCss('form');
HtmlApp::loadCss('cliente');
// jquery interfaces graficas
HtmlApp::loadjQueryUI();
HtmlApp::loadJs('jquery.yiigridview');
// mustache
HtmlApp::loadJs('mustache-master/mustache');
HtmlApp::loadJs('jquery-Mustache/jquery.mustache');
// js personalizado
HtmlApp::loadJs('cliente');
// ruta
$this->breadcrumbs = array('cliente'=>array('/cliente'), $model->isNewRecord ? 'create':'view',);
?>

<h1>Registro de Cliente</h1>

<?php
   $this->widget('zii.widgets.jui.CJuiTabs', array(
    'tabs'=>array(
         'Cliente'    => array('id'=>'Cliente'   , 'content'=> $this->renderPartial('_cliente', array('model'=> $model),true)),
         'Firmas'     => array('id'=>'Firmas'    , 'content'=> $this->renderPartial('_firma',   array('model'=> $model),true)),
         'Sucursales' => array('id'=>'Sucursales', 'content'=> $this->renderPartial('_sucursal',array('model'=> $model),true)),
         'Historia'   => array('id'=>'Historia'  , 'content'=> $this->renderPartial('_historia',array('historia'=>$historia),true))
    ),
    /*
      si el cliente es nuevo deberia estar desactivadas las demas tabs
    */
   'options' => array('collapsible'=>true,'selected'=>0,'disabled'=> ($model->isNewRecord) ? array(1,2,3) : array() )   
   ));
?>

<div style="text-align:right;">
<?php if ( Yii::app()->user->checkAccess('admin') OR Yii::app()->user->checkAccess('vendedor') ): ?>
   <button type="button" id="btnFormCliente">Editar</button>
<?php endif; ?>
</div>
<div id="border-view">
<input type="hidden" id="hd_cliente_id" value="<?php echo $model->id; ?>"/>
<table id="cliente_view">
   <tr>
      <td>RIF</td>
      <td><?php echo $model->rif; ?></td>
   </tr>
   <tr>
      <td>Razon social</td>
      <td><?php echo $model->razon_social; ?></td>
   </tr>
   <tr>
      <td>Representante Legal</td>
      <td><?php echo $model->representante_legal; ?></td>
   </tr>
   <tr>
      <td>Domicilio fiscal</td>
      <td><?php echo $model->domicilio_fiscal; ?></td>
   </tr>
   <tr>
      <td>Contribuyente</td>
      <td><?php echo $model->contribuyente->descripcion; ?></td>
   </tr>
   <tr>
      <td>Cliente tipo</td>
      <td><?php echo $model->clienteTipo->descripcion; ?></td>
   </tr>
   <tr>
      <td>Telefono Celular</td>
      <td><?php echo $model->telefono_celular; ?></td>
   </tr>
   <tr>
      <td>Telefono residencial</td>
      <td><?php echo $model->telefono_residencial; ?></td>
   </tr>
   <tr>
      <td>Registro de comercio</td>
      <td><?php echo $model->registro_de_comercio; ?></td>
   </tr>
   <tr>
      <td>Email</td>
      <td><?php echo $model->email; ?></td>
   </tr>
</table>
</div>

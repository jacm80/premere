<!-- La Firma Principal cuando el Cliente es una PERSONA NATURAL -->
<fieldset>
   <legend id="lg_cliente_tipo"><?php echo ($model->cliente_tipo_id==1) ? 'Persona Natural' :'Firma Juridica' ?></legend>
   <table class="table-form">
      <thead>
         <tr>
            <th>RIF</th>
            <th>Razon Social</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <?php if ($model->isNewRecord): ?>
               <td><div id="pn_rif"></div></td>
               <td><div id="pn_razon_social"></div></td>
            <?php else: ?>
               <td><div id="pn_rif"><?php echo $model->rif; ?></div></td>
               <td><div id="pn_razon_social"><?php echo $model->razon_social; ?></div></td>
            <?php endif ?>
         </tr>
      </tbody>
   </table>
</fieldset>
<!--      ///////////////////////////////////////////////////     -->

<fieldset>
   <legend id="lg_firmas"><?php echo ($model->cliente_tipo_id ==  1) ? 'Firmas Unipersonales' : 'Franquicias' ?></legend>
   <table class="table-form">
      <thead>
         <tr>
            <th>Razon Social</th>
            <th>Domicilio Fiscal</th>
            <th>Telefonos</th>
            <th>Registro</th>
         </tr>
      </thead>
      <tbody id="firmas">
         <?php if (!$model->isNewRecord): ?>
            <?php $this->renderPartial('_firmaItems',array('model'=>$model)); ?>
         <?php endif; ?>
      </tbody>
      <tfoot>
         <tr>
            <td colspan="4" id="conformFirma">
               <div class="hide" id="divformFirma">
               <table id="formFirma">
                  <tr>
                     <td>Razon Social</td>
                     <td>
                        <input type="text" id="firma_razon_social" style="width:100%;">
                        <div id="error-firma_razon_social" class="cliente-error hide" style="">Razon social es requerido</div>
                     </td>
                  </tr>
                  <tr>
                     <td>Domicilio Fiscal</td>
                     <td>
                        <textarea id="firma_domicilio_fiscal"></textarea>
                        <div id="error-firma_domicilio_fiscal" class="cliente-error hide">Domicilio fiscal es requerido</div>
                     </td>
                  </tr>
                  <tr>
                     <td>Telefonos</td>
                     <td>
                        <textarea id="firma_telefonos"></textarea>
                        <div id="error-firma_telefonos" class="cliente-error hide">Telefono fiscal es requerido</div>
                     </td>
                  </tr>
                  <tr>
                     <td>Registro</td>
                     <td>
                        <input type="text" id="firma_registro" style="width:100%;">
                        <div id="error-firma_registro" class="cliente-error hide">Domicilio fiscal es requerido</div>
                     </td>
                  </tr>
                  <tr>
                     <td colspan="2" style="text-align:center;">
                        <button id="btnGuardarFirma" type="button">Guardar</button>
                        <button id="btnCancelarFirma" type="button">Cancelar</button>
                     </td>
                  </tr>
               </table>
               </div>
            </td>
         </tr>
         <tr id="conBotonForm">
            <td colspan="4" style="text-align:center;">
               <button type="button" id="btnFormFirma">Crear Firma</button>
            </td>
         </tr>
      </tfoot>
   </table>
</fieldset>

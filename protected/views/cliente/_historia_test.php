<style>
   table
   {
      border: 1px solid #e3e3e3;
      margin-bottom:4px;
   }
   .summary td {
      width: 16%;
      text-align: center;
   }
</style>
<div id="historia" class="hide">El cliente no tiene historia.</div>

<div style="margin:0 auto; border: 1px solid #e3e3e3; width: 30%; padding: 8px; border-radius: 8px;">
   <div style="float:left">Ultimo Numero de Control:</div>
   <div style="float:left; font-weight: bold">1200</div>
<div style="clear:both"></div>
</div>
<br />

<table class="tabular-data">
   <thead>
      <tr>
         <th>Orden</th>
         <th>Fecha Entrada</th>
         <th>Facturas</th>
         <th>Firma</th>
         <th>Sucursal</th>
      </tr>
   </thead>
   <tbody>
      <!-- 1 -->
      <tr id="sup-orden_6761">
         <td>6761</td>
         <td>10-02-2013</td>
         <td><div onclick="show_orden('6761');">554,555,558&nbsp;<?php echo CHtml::image(HtmlApp::imageUrl('icons/display.png')); ?></div></td>
         <td>Juan Antonio Canepa Marquez</td>
         <td>Principal</td>
      </tr>
      <tr id="inf-orden_6761" class="hide">
         <td colspan="5">
            <fieldset>
               <legend>Factura: <strong style="color:blue">554</strong></legend>
               <table class="tabular-data">
                  <thead>
                     <tr>
                        <th>Cantidad</th>
                        <th>Trabajo</th>
                        <th>Numeracion</th>
                     </tr>
                  </thead>
                  <tbody>
                  <tr>
                     <td>6</td>
                     <td>Talonarios Factura 1/18 Original y Copia Papel Quimico 1 Color</td>
                     <td><p>
                           Identificador 0, Serie N/A
                           Numero de Documento desde:1 Hasta: 300
                           Numero de Control desde:1 Hasta: 300
                        </p>
                     </td>
                  </tr>
                  </tbody>
               </table>
               <table class="summary">
                  <tr>
                     <td>Base Imponible</td>
                     <td>989823</td>
                     <td>IVA</td>
                     <td>23323</td>
                     <td>Total</td>
                     <td>993443</td>
                  </tr>
               </table>
            </fieldset>
            <fieldset>
               <legend>Factura: <strong style="color:blue">555</strong></legend>
               <table class="tabular-data">
                  <thead>
                     <tr>
                        <th>Cantidad</th>
                        <th>Trabajo</th>
                        <th>Numeracion</th>
                     </tr>
                  </thead>
                  <tbody>
                  <tr>
                     <td>6</td>
                     <td>Talonarios Notas de Credito Tam. 1/18 1 Color papel Quimico</td>
                     <td><p>
                           Identificador 0, Serie N/A
                           Numero de Documento desde:1 Hasta: 300
                           Numero de Control desde:301 Hasta: 600
                        </p>
                     </td>
                  </tr>
                  </tbody>
               </table>
               <table class="summary">
                  <tr>
                     <td>Base Imponible</td>
                     <td>989823</td>
                     <td>IVA</td>
                     <td>23323</td>
                     <td>Total</td>
                     <td>993443</td>
                  </tr>
               </table>
            </fieldset>
            <fieldset>
               <legend>Factura <strong style="color:blue">558</strong></legend>
               <table class="tabular-data">
                  <thead>
                     <tr>
                        <th>Cantidad</th>
                        <th>Trabajo</th>
                        <th>Numeracion</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>6</td>
                        <td>Tarjetas de Presentacion medida 20x12 pcs. 1 Color</td>
                        <td>NO APLICA</td>
                     </tr>
                     <tr>
                        <td>100</td>
                        <td>Hojas Membrete Tamaño Carta</td>
                        <td>NO APLICA</td>
                     </tr>
                  </tbody>
               </table>
               <table class="summary">
                  <tr>
                     <td>Base Imponible</td>
                     <td>989823</td>
                     <td>IVA</td>
                     <td>23323</td>
                     <td>Total</td>
                     <td>993443</td>
                  </tr>
               </table>
            </fieldset>
            <div onclick="close_orden(6761);" style="color:blue;text-align:center;cursor:pointer;">Cerrar Detalle</div>
         </td>
      </tr>
      <!-- end 1 --> 
   </tbody>
</table>

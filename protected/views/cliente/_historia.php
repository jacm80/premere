<?php if (count($historia)>0): ?>

<div id="historia-ultimocontrol">
   <div id="texto">Ultimo Numero de Control:</div>
   <div id="numero"><?php #echo $ultimo_control ?></div>
   <div style="clear:both"></div>
</div>
<br />

<div id="historia">
<table class="table-form">
   <thead>
      <tr>
         <th>Orden</th>
         <th>Fecha Entrada</th>
         <th>Facturas</th>
         <th>Firma</th>
         <th>Sucursal</th>
      </tr>
   </thead>
   <tbody>
      <?php foreach ($historia['ordenes'] as $orden): ?>
         <tr id="sup-orden_<?php echo $orden['id']; ?>">
            <td><?php echo sprintf('%06d',$orden['id']); ?></td>
            <td><?php echo $orden['fecha_entrada']; ?></td>
            <td><div onclick="show_orden('<?php echo $orden['id']; ?>');"><?php echo $orden['numeros_facturas']; ?>
                &nbsp;
               <?php echo CHtml::image(HtmlApp::imageUrl('icons/display.png')); ?></div>
            </td>
            <td><?php echo $orden['firma'];    ?></td>
            <td><?php echo $orden['sucursal']; ?></td>
         </tr>
         <tr id="inf-orden_<?php echo $orden['id']; ?>" class="hide">
            <td colspan="5">
               <?php foreach($orden['facturas'] as $f): ?>   
               <fieldset>
                  <legend>Factura: <strong style="color:blue"><?php echo $f['numero']; ?></strong></legend>
                  <table class="tabular-data">
                     <thead>
                     <tr>
                        <th>Cantidad</th>
                        <th>Trabajo</th>
                        <th>Numeracion</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php foreach($f['items'] as $p): ?>
                        <tr>
                           <td><?php echo $p['cantidad']; ?></td>
                           <td><?php echo $p['producto']; ?></td>
                           <td><p><?php echo $p['numeracion']; ?></p></td>
                        </tr>
                     <?php endforeach; ?>
                     </tbody>
                  </table>
                  <table class="summary">
                     <tr>
                        <td>Base Imponible</td>
                        <td><?php echo $f['base_imponible']; ?></td>
                        <td>IVA</td>
                        <td><?php echo $f['iva']; ?></td>
                        <td>Total</td>
                        <td><?php echo ($f['base_imponible'] + $f['iva']); ?></td>
                     </tr>
                  </table>
               </fieldset>
               <?php endforeach; ?>
               <div onclick="close_orden('<?php echo $orden['id']; ?>');" class="cerrar-historia">Cerrar Detalle</div>               
            </td>
         </tr>
      <?php endforeach; ?>
   <tbody>
</table>
</div>

<?php else: ?>
   <div id="historia">El cliente no tiene historia.</div>
<?php endif; ?>

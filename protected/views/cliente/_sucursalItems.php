<?php $i=0; ?>
<?php foreach($model->firmas as $f): ?>
   <?php $tag="id-{$f->id}" ?>
   <fieldset>
   <legend><?php echo $f->razon_social; ?></legend>
   <table class="table-form">
      <thead>
         <tr>
            <th>Descripcion</th>
            <th>Domicilio</th>
            <th>Telefono</th>
            <th>&nbsp;</th>
         </tr>
      </thead>
      <tbody id="<?php echo $tag; ?>">
         <?php $j=0;?>
         <?php foreach($f->sedes as $s):?>
            <tr id="sucursal-<?php echo $s->id; ?>">
               <td><?php echo $s->descripcion; ?></td>
               <td><?php echo $s->domicilio_fiscal; ?></td>
               <td><?php echo $s->telefonos; ?></td>
               <?php if ($j>0): ?>
               <td><div onclick="deleteSucursal('<?php echo $s->id?>');"><?php echo CHtml::image(HtmlApp::imageUrl('icons/cancel.png')); ?></td>
               <?php endif; ?>
            </tr>
            <?php $j=0;?>
         <?php endforeach; ?>
      </tbody>
      <tfoot>
        <tr id="<?php echo $tag; ?>_sucursales" class="hide">
            <td colspan="4">
               <div>
                  <table id="<?php echo $tag; ?>_sucursalForm" class="tabular-data">
                     <tr>
                        <td>Descripcion</td>
                        <td>
                           <input type="text" id="<?php echo $tag; ?>_descripcion" style="width:100%;">
                           <div id="<?php echo $tag; ?>_error-descripcion" class="cliente-error hide" style="">Descripcion es requerido</div>
                        </td>
                     </tr>
                     <tr>
                        <td>Domicilio Fiscal</td>
                        <td>
                           <textarea id="<?php echo $tag; ?>_domicilio_fiscal"></textarea>
                           <div id="<?php echo $tag; ?>_error-domicilio_fiscal" class="cliente-error hide" style="">Domicilio fiscal es requerido</div>                           
                        </td>
                     </tr>
                     <tr>
                        <td>Telefonos</td>
                        <td><textarea id="<?php echo $tag; ?>_telefonos"></textarea></td>
                     </tr>
                     <tr>
                        <td colspan="4" style="text-align:center;">
                        <button id="<?php echo $tag; ?>_BtnGuardarSucursal" type="button" onclick="addSucursal('<?php echo $tag; ?>');">Guardar</button>
                        <button id="<?php echo $tag; ?>_BtnCancelarSucursal" type="button" onclick="cancelarFormSucursal('<?php echo $tag; ?>');">Cancelar</button> 
                        </td>
                     </tr>
                  </table>
               </div>
            </td>
         </tr> 
         <tr id="<?php echo $tag; ?>_conFormSucursal">
            <?php if (($f->cliente->clienteTipo->id!=1) || ($i>0)): ?>            
                  <td colspan="3" style="text-align:center;">
                     <button id="<?php echo $tag; ?>_btnAddSucursal" type="button" 
                           onclick="callFormSucursal('<?php echo $tag; ?>');">Crear Sucursal</button>
                  </td>
            <?php endif; ?>
         </tr>
      </tfoot>
   </table>
   <?php #echo 'iteracion:'.$i; ?>
   </fieldset>
   <?php $i++; ?>
<?php endforeach; ?>

<?php $this->breadcrumbs = array('cliente','index'); ?>
<h1>Lista de Clientes</h1>
<?php 
   $columns = array( 
                     'id',
                     'rif',
                     'razon_social',
                     array(
                        'type'=>'raw',
                        'header'=>'Contribuyente',
                        'name'=>'contribuyente_id',
                        'value'=>'$data->contribuyente->descripcion'
                     ),
                   );
   $this->widget('zii.widgets.grid.CGridView', array(
                                                      'id'=>'cliente-grid',
                                                      'filter'=>$model,
                                                      'dataProvider'=>$model->search( ),
                                                      'columns'=> $columns,
                                                      'selectionChanged'=>'js:function(id){
                                                         var xid = $.fn.yiiGridView.getSelection(id);
                                                         location.href = base_url+"/cliente/view/id/"+xid;
                                                      }'
                                                   ));
?>

<div style="text-align:center;">
   <button type="button" onclick="location.href=base_url+'/cliente/create'">Nuevo Cliente</button>
</div>

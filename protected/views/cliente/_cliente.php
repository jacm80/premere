<div id="clienteView">
   <?php 
      if ($model->isNewRecord) 
      {
         $this->renderPartial('_formCliente',array('model'=>$model));
      }
      else {
         $this->renderPartial('_viewCliente',array('model'=>$model));
      } 
   ?>
</div>

<?php
   
   class HtmlApp {
      
      public static function loadjQuery( )
      {
         Yii::app()->clientScript->registerCoreScript('jquery');
      }

      public static function loadjQueryUI( )
      {
         Yii::app()->clientScript->registerCoreScript('jquery.ui');
         Yii::app()->clientScript->registerCssFile(Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');
      }

      public static function imageUrl($name)
      {
         return Yii::app()->baseUrl.'/media/images/'.$name;
      }
      
      public static function loadCss($name)
      {
         if (Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/media/css/'.$name.'.css')) return TRUE;
      }
      
      public static function loadJs($name)
      {

         if (Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/media/js/'.$name.'.js')) return TRUE;
      }
      public static function debug($var)
      {
         CVarDumper::dump($var,10,true);
      }

      public static function getListaSerie( )
      {
         return array(
                        '0'=> 'Seleccione...',
                      'N/A'=> 'Sin Serie',
                     '01-A'=> '01-A',
                     '02-B'=> '02-B',
                     '03-C'=> '03-C',
                     '04-D'=> '04-D',
                     '05-E'=> '05-E',
                     '06-F'=> '06-F',
                     '07-G'=> '07-G',
                     '08-H'=> '08-H',
                     '09-I'=> '09-I',
                     '10-J'=> '10-J',
                     '11-K'=> '11-K',
                     '12-L'=> '12-L',
                     '13-M'=> '13-M',
                     '14-N'=> '14-N',
                     '15-O'=> '15-O',
                     '16-P'=> '16-P',
                     '17-Q'=> '17-Q',
                     '18-R'=> '18-R',
                     '19-S'=> '19-S',
                     '20-T'=> '20-T'
                     );
      }
      
      public static function date_mysql($date)
		{
			if (preg_match("/^(\d{2})\/(\d{2})\/(\d{4})$/",$date,$m))
			{
				return $m[3].'-'.$m[2].'-'.$m[1];
			}
			else { return ''; }
		}

   }
?>

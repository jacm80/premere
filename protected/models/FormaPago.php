<?php

/**
 * This is the model class for table "forma_pago".
 *
 * The followings are the available columns in table 'forma_pago':
 * @property integer $id
 * @property string $descripcion
 * @property integer $requiere_comprobante
 *
 * The followings are the available model relations:
 * @property Orden[] $ordens
 * @property Pago[] $pagos
 */
class FormaPago extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FormaPago the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'forma_pago';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, requiere_comprobante', 'required'),
			array('requiere_comprobante', 'numerical', 'integerOnly'=>true),
			array('descripcion', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descripcion, requiere_comprobante', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'pagos'  => array(self::HAS_MANY, 'Pago', 'forma_pago_id'),
			'gastos' => array(self::HAS_MANY, 'Gasto','forma_pago_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'requiere_comprobante' => 'Requiere Comprobante',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('requiere_comprobante',$this->requiere_comprobante);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getListFormaPagos()
   {
      $formaPagos = FormaPago::model( )->findAll();
      foreach ($formaPagos as $fp) $data[$fp->id]= ($fp->requiere_comprobante==1)?'*'.$fp->descripcion:''.$fp->descripcion;
      return $data;
   }

   public static function getListFormaPagosAry()
   {
      $formaPagos = FormaPago::model( )->findAll();
      foreach ($formaPagos as $fp){
            $data[ ]= array(
                  'id'          => $fp->id,
                  'descripcion' => ($fp->requiere_comprobante==1) ? '*'.$fp->descripcion : $fp->descripcion
            );
      }
      return $data;
   }


}

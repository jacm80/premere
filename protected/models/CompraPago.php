<?php

/**
 * This is the model class for table "compra_pago".
 *
 * The followings are the available columns in table 'compra_pago':
 * @property integer $id
 * @property string $fecha
 * @property integer $compra_id
 * @property integer $forma_pago_id
 * @property integer $empresa_banco_id
 * @property string $numero_comprobante
 * @property string $descripcion
 * @property string $monto
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property CrugeUser $user
 * @property FormaPago $formaPago
 * @property EmpresaBanco $empresaBanco
 * @property Compra $compra
 */
class CompraPago extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompraPago the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compra_pago';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('compra_id, empresa_banco_id, descripcion, monto', 'required'),			
         array('compra_id, forma_pago_id, empresa_banco_id, user_id', 'numerical', 'integerOnly'=>true),
			array('numero_comprobante, monto', 'length', 'max'=>10),
			array('descripcion', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha, compra_id, forma_pago_id, empresa_banco_id, numero_comprobante, descripcion, monto, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CrugeUser', 'user_id'),
			'formaPago' => array(self::BELONGS_TO, 'FormaPago', 'forma_pago_id'),
			'empresaBanco' => array(self::BELONGS_TO, 'EmpresaBanco', 'empresa_banco_id'),
			'compra' => array(self::BELONGS_TO, 'Compra', 'compra_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha' => 'Fecha',
			'compra_id' => 'Compra',
			'forma_pago_id' => 'Forma Pago',
			'empresa_banco_id' => 'Empresa Banco',
			'numero_comprobante' => 'Numero Comprobante',
			'descripcion' => 'Descripcion',
			'monto' => 'Monto',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('compra_id',$this->compra_id);
		$criteria->compare('forma_pago_id',$this->forma_pago_id);
		$criteria->compare('empresa_banco_id',$this->empresa_banco_id);
		$criteria->compare('numero_comprobante',$this->numero_comprobante,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('monto',$this->monto,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

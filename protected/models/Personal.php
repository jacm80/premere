<?php

/**
 * This is the model class for table "personal".
 *
 * The followings are the available columns in table 'personal':
 * @property integer $id
 * @property string $cedula
 * @property string $nombres
 * @property string $apellidos
 * @property string $direccion
 * @property string $telefono_fijo
 * @property string $telefono_celular
 * @property integer $activo
 *
 * The followings are the available model relations:
 * @property MaterialSalida[] $materialSalidas
 * @property MaterialSalida[] $materialSalidas1
 */
class Personal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Personal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula, nombres, apellidos, direccion, telefono_fijo, telefono_celular, activo', 'required'),
			array('activo', 'numerical', 'integerOnly'=>true),
			array('cedula, telefono_fijo, telefono_celular', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cedula, nombres, apellidos, direccion, telefono_fijo, telefono_celular, activo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'recibidos'  => array(self::HAS_MANY, 'MaterialSalida', 'entrega'),
			'entregados' => array(self::HAS_MANY, 'MaterialSalida', 'recibe'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cedula' => 'Cedula',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'direccion' => 'Direccion',
			'telefono_fijo' => 'Telefono Fijo',
			'telefono_celular' => 'Telefono Celular',
			'activo' => 'Activo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('telefono_fijo',$this->telefono_fijo,true);
		$criteria->compare('telefono_celular',$this->telefono_celular,true);
		$criteria->compare('activo',$this->activo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getListPersonalActivo( )
   {
      $data = array('0'=>'Seleccione...' );
      $personal = Personal::model()->findAllByAttributes(array('activo'=>1));
      foreach ($personal as $empleado)
      {
         $data[$empleado->id] = $empleado->nombres . ' ' . $empleado->apellidos;
      }
      return $data;
   }

   public function nombre_completo( ) 
   {
      return $this->nombres.' '.$this->apellidos;
   }

}

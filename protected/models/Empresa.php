<?php

/**
 * This is the model class for table "empresa".
 *
 * The followings are the available columns in table 'empresa':
 * @property integer $id
 * @property string $rif
 * @property string $razon_social
 * @property string $domicilio_fiscal
 * @property string $telefonos
 * @property string $representantes
 * @property string $registro_de_comercio
 * @property string $Resolucion_GTIRA
 * @property string $mision
 * @property string $vision
 * @property string $logotipo
 */
class Empresa extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Empresa the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'empresa';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rif, razon_social, domicilio_fiscal, telefonos, representantes, registro_de_comercio, Resolucion_GTIRA, mision, vision, logotipo', 'required'),
			array('rif', 'length', 'max'=>20),
			array('registro_de_comercio', 'length', 'max'=>150),
			array('Resolucion_GTIRA', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, rif, razon_social, domicilio_fiscal, telefonos, representantes, registro_de_comercio, Resolucion_GTIRA, mision, vision, logotipo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rif' => 'Rif',
			'razon_social' => 'Razon Social',
			'domicilio_fiscal' => 'Domicilio Fiscal',
			'telefonos' => 'Telefonos',
			'representantes' => 'Representantes',
			'registro_de_comercio' => 'Registro De Comercio',
			'Resolucion_GTIRA' => 'Resolucion Gtira',
			'mision' => 'Mision',
			'vision' => 'Vision',
			'logotipo' => 'Logotipo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rif',$this->rif,true);
		$criteria->compare('razon_social',$this->razon_social,true);
		$criteria->compare('domicilio_fiscal',$this->domicilio_fiscal,true);
		$criteria->compare('telefonos',$this->telefonos,true);
		$criteria->compare('representantes',$this->representantes,true);
		$criteria->compare('registro_de_comercio',$this->registro_de_comercio,true);
		$criteria->compare('Resolucion_GTIRA',$this->Resolucion_GTIRA,true);
		$criteria->compare('mision',$this->mision,true);
		$criteria->compare('vision',$this->vision,true);
		$criteria->compare('logotipo',$this->logotipo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
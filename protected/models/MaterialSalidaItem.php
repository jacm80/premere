<?php

/**
 * This is the model class for table "material_salida_item".
 *
 * The followings are the available columns in table 'material_salida_item':
 * @property integer $id
 * @property integer $material_salida_id
 * @property integer $catalogo_material_id
 * @property integer $cantidad
 * @property integer $tipo_cantidad
 *
 * The followings are the available model relations:
 * @property MaterialSalida $materialSalida
 * @property CatalogoMaterial $catalogoMaterial
 */
class MaterialSalidaItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaterialSalidaItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'material_salida_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('material_salida_id, catalogo_material_id, cantidad, tipo_cantidad', 'required'),
			array('material_salida_id, catalogo_material_id, cantidad, tipo_cantidad', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, material_salida_id, catalogo_material_id, cantidad, tipo_cantidad', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(

			'almacen' => array(self::BELONGS_TO, 'Almacen', 'almacen_id'),
			'materialSalida' => array(self::BELONGS_TO, 'MaterialSalida', 'material_salida_id'),
			'catalogoMaterial' => array(self::BELONGS_TO, 'CatalogoMaterial', 'catalogo_material_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'material_salida_id' => 'Material Salida',
			'catalogo_material_id' => 'Catalogo Material',
			'cantidad' => 'Cantidad',
			'tipo_cantidad' => 'Tipo Cantidad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('material_salida_id',$this->material_salida_id);
		$criteria->compare('catalogo_material_id',$this->catalogo_material_id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('tipo_cantidad',$this->tipo_cantidad);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function existencia_unidad( )
   {
      $valor = 0;
      $material = Material::model()->findAllByAttributes(array('catalogo_material_id'=>$this->catalogo_material_id));
      foreach($material as $m)
      {
         $valor = $m->existencia_unidad;
      }
      return $valor;
   }

   public function existencia_detallado( )
   {
      $valor = 0;
      $material = Material::model()->findAllByAttributes(array('catalogo_material_id'=>$this->catalogo_material_id));
      foreach($material as $m)
      {
         $valor = $m->existencia_detallado;
      }
      return $valor;
   }

}

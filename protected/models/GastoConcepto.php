<?php

/**
 * This is the model class for table "gasto_concepto".
 *
 * The followings are the available columns in table 'gasto_concepto':
 * @property integer $id
 * @property integer $gasto_tipo_id
 * @property string $descripcion
 * @property string $tipo
 * @property integer $entidad_id
 */
class GastoConcepto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GastoConcepto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gasto_concepto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gasto_tipo_id, descripcion, tipo, entidad_id', 'required'),
			array('gasto_tipo_id, entidad_id', 'numerical', 'integerOnly'=>true),
			array('tipo', 'length', 'max'=>2),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, gasto_tipo_id, descripcion, tipo, entidad_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gastoTipo' => array(self::BELONGS_TO, 'GastoTipo', 'gasto_tipo_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gasto_tipo_id' => 'Gasto Tipo',
			'descripcion' => 'Descripcion',
			'tipo' => 'Tipo',
			'entidad_id' => 'Entidad',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gasto_tipo_id',$this->gasto_tipo_id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('entidad_id',$this->entidad_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getListaConceptos( )
   {
      $data = array('0'=>'Seleccione');
      $rsconceptos = GastoConcepto::model()->findAll();
      foreach($rsconceptos as $c)
      {
         $data[$c->id] = $c->descripcion;
      }
      return $data;
   }

}

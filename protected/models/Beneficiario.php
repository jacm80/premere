<?php

/**
 * This is the model class for table "beneficiario".
 *
 * The followings are the available columns in table 'beneficiario':
 * @property integer $id
 * @property string $cedula
 * @property string $nombres
 * @property string $apellidos
 * @property string $direccion
 * @property string $telefono_fijo
 * @property string $celular
 * @property string $email
 * @property integer $cuenta_contable_id
 * @property integer $banco_id
 * @property string $tipo_cuenta
 * @property integer $numero_cuenta
 * @property integer $contribuyente_id
 * @property string $condicion
 * @property string $fecha_registro
 *
 * The followings are the available model relations:
 * @property Contribuyente $contribuyente
 * @property Banco $banco
 */
class Beneficiario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Beneficiario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'beneficiario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula, nombres, apellidos, direccion, telefono_fijo, celular, email, cuenta_contable_id, banco_id, tipo_cuenta, numero_cuenta, contribuyente_id, condicion, fecha_registro', 'required'),
			array('cuenta_contable_id, banco_id, numero_cuenta, contribuyente_id', 'numerical', 'integerOnly'=>true),
			array('cedula', 'length', 'max'=>12),
			array('nombres, apellidos', 'length', 'max'=>120),
			array('telefono_fijo, celular', 'length', 'max'=>15),
			array('email', 'length', 'max'=>80),
			array('tipo_cuenta', 'length', 'max'=>20),
			array('condicion', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cedula, nombres, apellidos, direccion, telefono_fijo, celular, email, cuenta_contable_id, banco_id, tipo_cuenta, numero_cuenta, contribuyente_id, condicion, fecha_registro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contribuyente' => array(self::BELONGS_TO, 'Contribuyente', 'contribuyente_id'),
			'banco' => array(self::BELONGS_TO, 'Banco', 'banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cedula' => 'Cedula',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'direccion' => 'Direccion',
			'telefono_fijo' => 'Telefono Fijo',
			'celular' => 'Celular',
			'email' => 'Email',
			'cuenta_contable_id' => 'Cuenta Contable',
			'banco_id' => 'Banco',
			'tipo_cuenta' => 'Tipo Cuenta',
			'numero_cuenta' => 'Numero Cuenta',
			'contribuyente_id' => 'Contribuyente',
			'condicion' => 'Condicion',
			'fecha_registro' => 'Fecha Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cedula',$this->cedula,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('direccion',$this->direccion,true);
		$criteria->compare('telefono_fijo',$this->telefono_fijo,true);
		$criteria->compare('celular',$this->celular,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('cuenta_contable_id',$this->cuenta_contable_id);
		$criteria->compare('banco_id',$this->banco_id);
		$criteria->compare('tipo_cuenta',$this->tipo_cuenta,true);
		$criteria->compare('numero_cuenta',$this->numero_cuenta);
		$criteria->compare('contribuyente_id',$this->contribuyente_id);
		$criteria->compare('condicion',$this->condicion,true);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getDescripcionById($id)
   {
      $b = Beneficiario::model()->findByPk($id);
      return $b->apellidos .' '. $b->nombres;
   }

}

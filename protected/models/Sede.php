<?php

/**
 * This is the model class for table "sede".
 *
 * The followings are the available columns in table 'sede':
 * @property integer $id
 * @property string $descripcion
 * @property integer $firma_id
 * @property string $domicilio_fiscal
 * @property string $telefonos
 *
 * The followings are the available model relations:
 * @property Firma $firma
 */
class Sede extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Sede the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'sede';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, firma_id, domicilio_fiscal', 'required'),
			array('firma_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
         array('telefonos','safe'),
         array('id, descripcion, firma_id, domicilio_fiscal, telefonos', 'safe', 'on'=>'search'),
			array('id, descripcion, firma_id, domicilio_fiscal, telefonos', 'safe', 'on'=>'addPrincipal'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'firma' => array(self::BELONGS_TO, 'Firma', 'firma_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'firma_id' => 'Firma',
			'domicilio_fiscal' => 'Domicilio Fiscal',
			'telefonos' => 'Telefonos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('firma_id',$this->firma_id);
		$criteria->compare('domicilio_fiscal',$this->domicilio_fiscal,true);
		$criteria->compare('telefonos',$this->telefonos,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function getListByFirma($firma_id)
   {
      //return CHtml::listData($arySedes ,'id','descripcion');
      $arySedes = Sede::model()->findAll('firma_id=:firmaID',array(':firmaID'=>$firma_id));
      $data = array(0=>'Seleccione');
      foreach($arySedes as $s) $data[$s->id]=$s->descripcion;
      return $data;
   }
   
   public static function addSede($data)
   {
      $sede = new Sede;
      unset($data['PATH_SERVER']);
      $sede->attributes = $data;
      //fb($data,'warn');
      $sede->save( );
      return $sede->id;
   }

}

<?php

/**
 * This is the model class for table "factura".
 *
 * The followings are the available columns in table 'factura':
 * @property integer $id
 * @property integer $numero
 * @property string $fecha_cancelacion
 * @property integer $orden_id
 * @property integer $usuario_id
 * @property integer $cancelada
 * @property integer $fiscal
 *
 * The followings are the available model relations:
 * @property DocumentoControl[] $documentoControls
 * @property Orden $orden
 * @property Usuario $usuario
 * @property FacturaItem[] $facturaItems
 */
class Factura extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Factura the static model class
	 */

   /* especificamente para la consulta de numeracion */
   public $rif;
   public $numero;
   public $documento;
   public $serie;
   public $identificador;
   public $inicio_numero;
   public $fin_numero;
   public $inicio_control;
   public $fin_control;
   public $razon_social;
   /*--------------------------------------------------*/ 

   public $fecha_entrada;
   public $firma_id;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'factura';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('numero, orden_id, usuario_id, cancelada', 'required'),
			array('numero, orden_id, usuario_id, cancelada, fiscal', 'numerical', 'integerOnly'=>true),
			array('fecha_cancelacion, base_imponible, iva', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, numero, fecha_cancelacion, orden_id, usuario_id, cancelada', 'safe', 'on'=>'search'),
			array('id, numero, fecha_cancelacion, orden_id', 'safe', 'on'=>'searchAjuste'),
			array('rif, razon_social, numero, documento, identificador, inicio_numero, fin_numero, inicio_control, fin_control', 'safe', 'on'=>'searchNumByRif'),
		);
	}

   public function scopes( )
   {
      return array(
         'fiscales'  => array('condition' => 'fiscal=1' ),
         'papeleria' => array('condition' => 'fiscal=0' )
      );
   }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documentoControls' => array(self::HAS_ONE, 'DocumentoControl', 'factura_id'),
			'orden' => array(self::BELONGS_TO, 'Orden', 'orden_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
			'facturaItems' => array(self::HAS_MANY, 'FacturaItem', 'factura_id'),
		);
	}
 
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id'         => 'ID',
			'numero'     => 'Numero',
			'fecha_cancelacion' => 'Fecha Cancelacion',
			'orden_id'   => 'Orden',
			'usuario_id' => 'Usuario',
			'cancelada'  => 'Cancelada',
			'fiscal'     => 'Cancelada',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('numero',$this->numero);
		$criteria->compare('fecha_cancelacion',$this->fecha_cancelacion,true);
		$criteria->compare('orden_id',$this->orden_id);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('cancelada',$this->cancelada);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function searchAjuste( )
   {
		$criteria = new CDbCriteria;
		$criteria->compare('numero',$this->numero);
		$criteria->compare('fecha_entrada',$this->fecha_entrada);
		$criteria->compare('firma_id',$this->firma_id);
      $criteria->compare('base_imponible',$this->base_imponible);
      $criteria->compare('iva',$this->iva);
      $criteria->alias = 't';
      $criteria->join  = "LEFT JOIN orden  o ON (o.id = t.orden_id)
                          LEFT JOIN firma  f ON (f.id = o.firma_id)";
		$criteria->with  = array('orden','firma');      
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
   }

   public function searchNumByRif( )
	{
      $rif = $_POST['rif'];
      $criteria = new CDbCriteria;

		$criteria->compare('t.numero',$this->numero,TRUE);
		$criteria->compare('df.descripcion',$this->documento,TRUE);
		$criteria->compare('identificador',$this->identificador,TRUE);
		$criteria->compare('inicio_numero',$this->inicio_numero,TRUE);
		$criteria->compare('fin_numero',$this->fin_numero,TRUE);
		$criteria->compare('inicio_control',$this->inicio_control,TRUE);
		$criteria->compare('fin_control',$this->fin_control,TRUE);
      
      $criteria->alias  = 't';
      $criteria->select = 't.numero,
                           c.rif,
                           c.razon_social,
                           df.descripcion AS documento,
                           dc.serie,
                           dc.identificador,
                           dc.inicio_numero,
                           dc.fin_numero,
                           dc.inicio_control,
                           dc.fin_control';
      $criteria->join = 'LEFT JOIN orden   o ON (o.id = t.orden_id)
                         LEFT JOIN cliente c ON (c.id = o.cliente_id)
                         LEFT JOIN documento_control dc ON (dc.factura_id = t.id)
                         LEFT JOIN documento_fiscal df ON (df.id = dc.documento_fiscal_id)';
      $criteria->addInCondition('c.rif',array($rif));
      $criteria->addInCondition('t.fiscal',array(1));
      $criteria->order = 't.id DESC';         
      return new CActiveDataProvider('Factura', array(
         'criteria' => $criteria,
         'pagination' => array('pageSize' =>10)
      ));
   }


   public function getProducto( ){}

   public function getFacturaView( )
   {
      $factura['numero']            = $this->numero;
      $factura['rif']               = $this->orden->cliente->rif;
      $factura['forma_pago']        = 'Efectivo';
      $factura['base_imponible']    = $this->base_imponible;
      $factura['iva']               = $this->iva;
      $factura['fecha_cancelacion'] = $this->fecha_cancelacion;
      $factura['firma']             = $this->orden->firma->razon_social;
      $contribuyente = $this->orden->cliente->contribuyente->descripcion;
      $factura['items'] = array( ); $i=0;
      foreach ($this->facturaItems as $item)
      {
         $tipo_doc = $item->producto->documento_fiscal_id;
         $factura['items'][$i] = array(
                                    'id'             => $item->id,
                                    'cantidad'       => $item->cantidad,
                                    'producto'       => $item->producto->descripcion,
                                    'precio_unitario'=> $item->precio_unitario,
                                    'monto'          => $item->monto
                                    );
         if ($tipo_doc!=0)
         {
            $n = $this->documentoControls;
            $serie          = $n->serie;
            $identificador  = $n->identificador;
            $inicio_numero  = $n->inicio_numero;
            $fin_numero     = $n->fin_numero;
            $fin_control    = $n->fin_control;
            $inicio_control = $n->inicio_control;
            $factura['items'][$i]['producto'].= "<br />Contribuyente: $contribuyente, Serie: <b>$serie</b>,";
            if ($tipo_doc!=5)
                  $factura['items'][$i]['producto'].= "<br />Numero de Factura del: $inicio_numero Hasta: $fin_numero,<br />";
            $factura['items'][$i]['producto'].= "Numero de Control del: $inicio_control Hasta: $fin_control.";
         }
         $i++;
      }
      return $factura;
   }

}

<?php

/**
 * This is the model class for table "empresa_banco".
 *
 * The followings are the available columns in table 'empresa_banco':
 * @property integer $id
 * @property integer $banco_id
 * @property string $agencia
 * @property string $numero
 * @property string $fecha_apertura
 * @property string $saldo
 *
 * The followings are the available model relations:
 * @property Banco $banco
 */
class EmpresaBanco extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return EmpresaBanco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'empresa_banco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('banco_id, agencia, numero, fecha_apertura, saldo', 'required'),
			array('banco_id', 'numerical', 'integerOnly'=>true),
			array('numero', 'length', 'max'=>30),
			array('saldo', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, banco_id, agencia, numero, fecha_apertura, saldo', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'banco' => array(self::BELONGS_TO, 'Banco', 'banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'banco_id' => 'Banco',
			'agencia' => 'Agencia',
			'numero' => 'Numero',
			'fecha_apertura' => 'Fecha Apertura',
			'saldo' => 'Saldo',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('banco_id',$this->banco_id);
		$criteria->compare('agencia',$this->agencia,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('fecha_apertura',$this->fecha_apertura,true);
		$criteria->compare('saldo',$this->saldo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getListBancosAry( )
   {
      $data = array( );
      $bancos = EmpresaBanco::model( )->findAll( );
      foreach($bancos as $b)
      { 
         $data[ ] = array(
                           'id'         => $b->id,
                           'descripcion'=> $b->banco->descripcion
                          );
      }
      return $data;
   }


}

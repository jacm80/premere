<?php

/**
 * This is the model class for table "documento_control".
 *
 * The followings are the available columns in table 'documento_control':
 * @property integer $id
 * @property integer $documento_fiscal_id
 * @property integer $cliente_id
 * @property string $serie
 * @property integer $identificador
 * @property integer $inicio_numero
 * @property integer $fin_numero
 * @property integer $inicio_control
 * @property integer $fin_control
 * @property integer $juegos,
 * @property integer $cantidad,
 * @property string $fecha_impresion
 * @property integer $factura_id
 *
 * The followings are the available model relations:
 * @property Factura $factura
 */
class DocumentoControl extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DocumentoControl the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'documento_control';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('documento_fiscal_id, cliente_id, serie, identificador, inicio_numero, 
               fin_numero, inicio_control, fin_control, juegos, cantidad, fecha_impresion, factura_id', 'required'),
			array('documento_fiscal_id, cliente_id, identificador, inicio_numero, fin_numero, 
                inicio_control, fin_control, juegos, cantidad, factura_id', 'numerical', 'integerOnly'=>true),
			array('serie', 'length', 'max'=>3),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, documento_fiscal_id, cliente_id, serie, identificador, inicio_numero, fin_numero, 
                inicio_control, fin_control, fecha_impresion, factura_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'factura'   => array(self::BELONGS_TO, 'Factura',   'factura_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'documento_fiscal_id' => 'Documento Fiscal',
			'cliente_id' => 'Cliente',
			'serie' => 'Serie',
			'identificador' => 'Identificador',
			'inicio_numero' => 'Inicio Numero',
			'fin_numero' => 'Fin Numero',
			'inicio_control' => 'Inicio Control',
			'fin_control' => 'Fin Control',
         'juegos'      => 'Juegos',
         'cantidad'    => 'cantidad',
			'fecha_impresion' => 'Fecha Impresion',
			'factura_id'   => 'Factura',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('documento_fiscal_id',$this->documento_fiscal_id);
		$criteria->compare('cliente_id',$this->cliente_id);
		$criteria->compare('serie',$this->serie,true);
		$criteria->compare('identificador',$this->identificador);
		$criteria->compare('inicio_numero',$this->inicio_numero);
		$criteria->compare('fin_numero',$this->fin_numero);
		$criteria->compare('inicio_control',$this->inicio_control);
		$criteria->compare('fin_control',$this->fin_control);
		$criteria->compare('fecha_impresion',$this->fecha_impresion,true);
		$criteria->compare('factura_id',$this->factura_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   /* 
      DocumentoControl::getNumeracion(@params)
      recibe: un mixed
                array(
                  'cantidad'=> '6',
                  'cliente_id'=> '1',
                  'documento_fiscal_id'=> '1',
                  'identificador'=> '00',
                  'juegos'=> '50',
                  'serie'=> 'N/A'
                  )
      retorna: un JSON de la forma: 
            {
            "inicio_numero":501,
            "fin_numero":800,
            "inicio_control":1001,
            "fin_control":1300
            } 

   */

   private function _lastNumDocumento( )
   {
      extract($_POST);
      // consulta de numeros de documentos
      $finNumero = Yii::app()->db->createCommand(array(
            'select'=> 'fin_numero',
            'from'  => 'documento_control',
            'where' => 'cliente_id=:cliente_id AND documento_fiscal_id=:documento_fiscal_id AND serie=:serie AND identificador=:identificador',
            'params'=> array(
                           ':cliente_id'         => $cliente_id,
                           ':documento_fiscal_id'=> $documento_fiscal_id,
                           ':serie'              => $serie,
                           ':identificador'      => $identificador
                  ),
            'order'  => array('id DESC'),
            'limit'  =>1
      ))->queryColumn( );
      return (!empty($finNumero)) ? $finNumero[0] : 0; 
   }

   private function _lastNumcontrol( )
   {
      extract($_POST);
      if (!empty($ultimo_control)) return $ultimo_control;
      // consulta de numeros de control
      $finControl = Yii::app()->db->createCommand(array(
         'select'=> 'fin_control',
         'from'  => 'documento_control',
         'where' => 'cliente_id=:cliente_id AND identificador=:identificador',
         'params'=> array(':cliente_id'=> $cliente_id,':identificador'=>$identificador),
         'order' => array('id DESC'),
         'limit' => 1   
      ))->queryColumn( );
      return (!empty($finControl)) ? $finControl[0] : 0; 
   }

   static public function getNumeracionJSON( )
   {
      extract($_POST);
      $fin_numero  = DocumentoControl::_lastNumDocumento( );
      $fin_control = DocumentoControl::_lastNumcontrol( );
      // arreglo 
      $numeracion=array(
                        'inicio_numero'   => $fin_numero  + 1,
                        'fin_numero'      => $fin_numero  + ($juegos*$cantidad),
                        'inicio_control'  => $fin_control + 1,
                        'fin_control'     => $fin_control + ($juegos*$cantidad)
                        );
      return json_encode($numeracion);
   }
   
}

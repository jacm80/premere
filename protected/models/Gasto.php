<?php

/**
 * This is the model class for table "gasto".
 *
 * The followings are the available columns in table 'gasto':
 * @property integer $id
 * @property integer $gasto_concepto_id
 * @property string $tipo
 * @property string $numero
 * @property string $fecha_documento
 * @property integer $forma_pago_id
 * @property integer $banco_id
 * @property string $numero_comprobante
 * @property string $monto
 * @property string $iva
 * @property string $observacion
 * @property string $fecha_registro
 *
 * The followings are the available model relations:
 * @property Banco $banco
 * @property GastoConcepto $gastoConcepto
 * @property FormaPago $formaPago
 */

class Gasto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Gasto the static model class
	 */
	
   public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gasto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gasto_concepto_id, tipo, numero, fecha_documento, forma_pago_id, banco_id, monto, iva', 'required'),
			array('gasto_concepto_id, forma_pago_id, banco_id', 'numerical', 'integerOnly'=>true),
			array('tipo', 'length', 'max'=>30),
			array('numero', 'length', 'max'=>12),
			array('numero_comprobante', 'length', 'max'=>20),
			array('monto, iva', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, gasto_concepto_id, tipo, numero, fecha_documento, 
               forma_pago_id, banco_id, numero_comprobante, monto, iva, 
               observacion', 'safe'),
			array('id, gasto_concepto_id, tipo, numero, fecha_documento, 
               forma_pago_id, banco_id, numero_comprobante, monto, iva, 
               observacion, fecha_registro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
         'banco' => array(self::BELONGS_TO, 'Banco', 'banco_id'),
         'gastoConcepto' => array(self::BELONGS_TO, 'GastoConcepto', 'gasto_concepto_id'),
         'formaPago' => array(self::BELONGS_TO, 'FormaPago', 'forma_pago_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gasto_concepto_id' => 'Concepto',
			'tipo' => 'Tipo',
			'numero' => 'Numero',
			'fecha_documento' => 'Fecha Documento',
			'forma_pago_id' => 'Forma Pago',
			'banco_id' => 'Banco',
			'numero_comprobante' => 'Numero Comprobante',
			'monto' => 'Monto',
			'iva' => 'Iva',
			'observacion' => 'Observacion',
			'fecha_registro' => 'Fecha Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gasto_concepto_id',$this->gasto_concepto_id);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('numero',$this->numero,true);
		$criteria->compare('fecha_documento',$this->fecha_documento,true);
		$criteria->compare('forma_pago_id',$this->forma_pago_id);
		$criteria->compare('banco_id',$this->banco_id);
		$criteria->compare('numero_comprobante',$this->numero_comprobante,true);
		$criteria->compare('monto',$this->monto,true);
		$criteria->compare('iva',$this->iva,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function getTipo( )
   {
      return ($this->tipo==1) ? 'Factura' : 'Recibo'; 
   }

}

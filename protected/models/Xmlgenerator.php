<?php

/**
 * This is the model class for table "xmlgenerator".
 *
 * The followings are the available columns in table 'xmlgenerator':
 * @property integer $COD
 * @property integer $PERIODO_FISCAL
 * @property integer $COD_CLIENTE
 * @property string $RIF_CLIENTE
 * @property integer $NUM_FACT_VENTA
 * @property integer $TIPO
 * @property string $FECHA_IMPRESION
 * @property integer $SERIE
 * @property integer $IDENTIFICADOR
 * @property integer $INICIO_NUM
 * @property integer $FIN_NUM
 * @property integer $INICIO_CONTROL
 * @property integer $FIN_CONTROL
 * @property integer $CANTIDAD
 * @property double $BASE_IMPONIBLE
 * @property double $MONTO_IVA_VENTA
 */
class Xmlgenerator extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Xmlgenerator the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'xmlgenerator';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('COD, PERIODO_FISCAL, COD_CLIENTE, RIF_CLIENTE, NUM_FACT_VENTA, TIPO, FECHA_IMPRESION, SERIE, IDENTIFICADOR, INICIO_NUM, FIN_NUM, INICIO_CONTROL, FIN_CONTROL, CANTIDAD, BASE_IMPONIBLE, MONTO_IVA_VENTA', 'required'),
			array('COD, PERIODO_FISCAL, COD_CLIENTE, NUM_FACT_VENTA, TIPO, SERIE, IDENTIFICADOR, INICIO_NUM, FIN_NUM, INICIO_CONTROL, FIN_CONTROL, CANTIDAD', 'numerical', 'integerOnly'=>true),
			array('BASE_IMPONIBLE, MONTO_IVA_VENTA', 'numerical'),
			array('RIF_CLIENTE', 'length', 'max'=>30),
			array('FECHA_IMPRESION', 'length', 'max'=>11),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('COD, PERIODO_FISCAL, COD_CLIENTE, RIF_CLIENTE, NUM_FACT_VENTA, TIPO, FECHA_IMPRESION, SERIE, IDENTIFICADOR, INICIO_NUM, FIN_NUM, INICIO_CONTROL, FIN_CONTROL, CANTIDAD, BASE_IMPONIBLE, MONTO_IVA_VENTA', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'COD' => 'Cod',
			'PERIODO_FISCAL' => 'Periodo Fiscal',
			'COD_CLIENTE' => 'Cod Cliente',
			'RIF_CLIENTE' => 'Rif Cliente',
			'NUM_FACT_VENTA' => 'Num Fact Venta',
			'TIPO' => 'Tipo',
			'FECHA_IMPRESION' => 'Fecha Impresion',
			'SERIE' => 'Serie',
			'IDENTIFICADOR' => 'Identificador',
			'INICIO_NUM' => 'Inicio Num',
			'FIN_NUM' => 'Fin Num',
			'INICIO_CONTROL' => 'Inicio Control',
			'FIN_CONTROL' => 'Fin Control',
			'CANTIDAD' => 'Cantidad',
			'BASE_IMPONIBLE' => 'Base Imponible',
			'MONTO_IVA_VENTA' => 'Monto Iva Venta',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('COD',$this->COD);
		$criteria->compare('PERIODO_FISCAL',$this->PERIODO_FISCAL);
		$criteria->compare('COD_CLIENTE',$this->COD_CLIENTE);
		$criteria->compare('RIF_CLIENTE',$this->RIF_CLIENTE,true);
		$criteria->compare('NUM_FACT_VENTA',$this->NUM_FACT_VENTA);
		$criteria->compare('TIPO',$this->TIPO);
		$criteria->compare('FECHA_IMPRESION',$this->FECHA_IMPRESION,true);
		$criteria->compare('SERIE',$this->SERIE);
		$criteria->compare('IDENTIFICADOR',$this->IDENTIFICADOR);
		$criteria->compare('INICIO_NUM',$this->INICIO_NUM);
		$criteria->compare('FIN_NUM',$this->FIN_NUM);
		$criteria->compare('INICIO_CONTROL',$this->INICIO_CONTROL);
		$criteria->compare('FIN_CONTROL',$this->FIN_CONTROL);
		$criteria->compare('CANTIDAD',$this->CANTIDAD);
		$criteria->compare('BASE_IMPONIBLE',$this->BASE_IMPONIBLE);
		$criteria->compare('MONTO_IVA_VENTA',$this->MONTO_IVA_VENTA);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
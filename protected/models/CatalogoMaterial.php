<?php

/**
 * This is the model class for table "catalogo_material".
 *
 * The followings are the available columns in table 'catalogo_material':
 * @property integer $id
 * @property string $barcode
 * @property string $descripcion
 * @property integer $material_tipo_id
 * @property integer $marca_id
 * @property integer $unidad_id
 * @property integer $maneja_detallado
 *
 * The followings are the available model relations:
 * @property Unidad $unidad
 * @property Marca $marca
 * @property MaterialTipo $materialTipo
 * @property Material[] $materials
 */
class CatalogoMaterial extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CatalogoMaterial the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catalogo_material';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('barcode, descripcion, material_tipo_id, marca_id, unidad_id, maneja_detallado', 'required'),
			array('material_tipo_id, marca_id, unidad_id, maneja_detallado', 'numerical', 'integerOnly'=>true),
			array('barcode', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, barcode, descripcion, material_tipo_id, marca_id, unidad_id, maneja_detallado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'unidad' => array(self::BELONGS_TO, 'Unidad', 'unidad_id'),
			'marca' => array(self::BELONGS_TO, 'Marca', 'marca_id'),
			'materialTipo' => array(self::BELONGS_TO, 'MaterialTipo', 'material_tipo_id'),
			'materials' => array(self::HAS_MANY, 'Material', 'catalogo_material_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'barcode' => 'Barcode',
			'descripcion' => 'Descripcion',
			'material_tipo_id' => 'Material Tipo',
			'marca_id' => 'Marca',
			'unidad_id' => 'Unidad',
			'maneja_detallado' => 'Maneja Detallado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('barcode',$this->barcode,true);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('material_tipo_id',$this->material_tipo_id);
		$criteria->compare('marca_id',$this->marca_id);
		$criteria->compare('unidad_id',$this->unidad_id);
		$criteria->compare('maneja_detallado',$this->maneja_detallado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getCatalogoMaterialxTipo($tipo)
   {
      $data = array(0=>'Seleccione...');
      $c_materiales = CatalogoMaterial::model( )->findAllByAttributes(array('material_tipo_id'=>$tipo));
      foreach ($c_materiales as $c) $data[$c->id] = $c->descripcion . " - Marca: ". $c->marca->descripcion;
      return $data;
   }



}

<?php

/**
 * This is the model class for table "material_conciliacion_item".
 *
 * The followings are the available columns in table 'material_conciliacion_item':
 * @property integer $id
 * @property integer $conciliacion_id
 * @property integer $catalogo_material_id
 * @property integer $existencia_unidad_sistema
 * @property integer $existencia_detallado_sistema
 * @property integer $existencia_unidad_real
 * @property integer $existencia_detallado_real
 * @property integer $diferencia_unidad
 * @property integer $diferencia_detallado
 *
 * The followings are the available model relations:
 * @property MaterialConciliacion $conciliacion
 * @property CatalogoMaterial $catalogoMaterial
 */
class MaterialConciliacionItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaterialConciliacionItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'material_conciliacion_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('conciliacion_id, catalogo_material_id, existencia_unidad_sistema, existencia_detallado_sistema, existencia_unidad_real, existencia_detallado_real, diferencia_unidad, diferencia_detallado', 'required'),
			array('conciliacion_id, catalogo_material_id, existencia_unidad_sistema, existencia_detallado_sistema, existencia_unidad_real, existencia_detallado_real, diferencia_unidad, diferencia_detallado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, conciliacion_id, catalogo_material_id, existencia_unidad_sistema, existencia_detallado_sistema, existencia_unidad_real, existencia_detallado_real, diferencia_unidad, diferencia_detallado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'conciliacion' => array(self::BELONGS_TO, 'MaterialConciliacion', 'conciliacion_id'),
			'catalogoMaterial' => array(self::BELONGS_TO, 'CatalogoMaterial', 'catalogo_material_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'conciliacion_id' => 'Conciliacion',
			'catalogo_material_id' => 'Catalogo Material',
			'existencia_unidad_sistema' => 'Existencia Unidad Sistema',
			'existencia_detallado_sistema' => 'Existencia Detallado Sistema',
			'existencia_unidad_real' => 'Existencia Unidad Real',
			'existencia_detallado_real' => 'Existencia Detallado Real',
			'diferencia_unidad' => 'Diferencia Unidad',
			'diferencia_detallado' => 'Diferencia Detallado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('conciliacion_id',$this->conciliacion_id);
		$criteria->compare('catalogo_material_id',$this->catalogo_material_id);
		$criteria->compare('existencia_unidad_sistema',$this->existencia_unidad_sistema);
		$criteria->compare('existencia_detallado_sistema',$this->existencia_detallado_sistema);
		$criteria->compare('existencia_unidad_real',$this->existencia_unidad_real);
		$criteria->compare('existencia_detallado_real',$this->existencia_detallado_real);
		$criteria->compare('diferencia_unidad',$this->diferencia_unidad);
		$criteria->compare('diferencia_detallado',$this->diferencia_detallado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
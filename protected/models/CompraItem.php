<?php

/**
 * This is the model class for table "compra_item".
 *
 * The followings are the available columns in table 'compra_item':
 * @property integer $id
 * @property integer $compra_id
 * @property integer $material_id
 * @property integer $cantidad
 * @property string $precio_unitario
 * @property string $iva
 * @property integer $almacen_id
 *
 * The followings are the available model relations:
 * @property Compra $compra
 * @property Material $material
 */
class CompraItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CompraItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compra_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('compra_id, material_id, cantidad, precio_unitario, iva, almacen_id', 'required'),
			array('compra_id, material_id, cantidad, almacen_id', 'numerical', 'integerOnly'=>true),
			array('precio_unitario, iva', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, compra_id, material_id, cantidad, precio_unitario, iva, almacen_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compra' => array(self::BELONGS_TO, 'Compra', 'compra_id'),
			'material' => array(self::BELONGS_TO, 'Material', 'material_id'),
			'almacen' => array(self::BELONGS_TO, 'Almacen', 'almacen_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'compra_id' => 'Compra',
			'material_id' => 'Material',
			'cantidad' => 'Cantidad',
			'precio_unitario' => 'Precio Unitario',
			'iva' => 'Iva',
			'almacen_id' => 'Almacen',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('compra_id',$this->compra_id);
		$criteria->compare('material_id',$this->material_id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('precio_unitario',$this->precio_unitario,true);
		$criteria->compare('iva',$this->iva,true);
		$criteria->compare('almacen_id',$this->almacen_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

<?php

/**
 * This is the model class for table "firma".
 *
 * The followings are the available columns in table 'firma':
 * @property integer $id
 * @property integer $cliente_id
 * @property integer $razon_social
 * @property string $domicilio_fiscal
 * @property string $registro_de_comercio
 * @property string $telefonos
 * @property string $archivo_registro_de_comercio
 *
 * The followings are the available model relations:
 * @property Cliente $cliente
 */
class Firma extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Firma the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'firma';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cliente_id, razon_social, domicilio_fiscal, registro_de_comercio', 'required'),
			array('cliente_id', 'numerical', 'integerOnly'=>true),
			array('archivo_registro_de_comercio', 'length', 'max'=>30),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
         array('telefonos,registro_de_comercio,archivo_registro_de_comercio','safe'),
			array('id, cliente_id, razon_social, domicilio_fiscal, registro_de_comercio, telefonos, archivo_registro_de_comercio', 'safe', 'on'=>'search'),
			array('id, cliente_id, razon_social, domicilio_fiscal, registro_de_comercio, telefonos, archivo_registro_de_comercio', 'safe', 'on'=>'addPrincipal'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'cliente' => array(self::BELONGS_TO, 'Cliente', 'cliente_id'),
         'sedes'   => array(self::HAS_MANY,'Sede','firma_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cliente_id' => 'Cliente',
			'razon_social' => 'Razon Social',
			'domicilio_fiscal' => 'Domicilio Fiscal',
			'registro_de_comercio' => 'Registro De Comercio',
			'telefonos' => 'Telefono',
			'archivo_registro_de_comercio' => 'Archivo Registro De Comercio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cliente_id',$this->cliente_id);
		$criteria->compare('razon_social',$this->razon_social);
		$criteria->compare('domicilio_fiscal',$this->domicilio_fiscal,true);
		$criteria->compare('registro_de_comercio',$this->registro_de_comercio,true);
		$criteria->compare('telefonos',$this->telefono,true);
		$criteria->compare('archivo_registro_de_comercio',$this->archivo_registro_de_comercio,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function getListByCliente($cliente_id)
   {
      //$ary = CHtml::listData($aryFirmas ,'id','razon_social');
      $aryFirmas = Firma::model()->findAll('cliente_id=:clienteID',array(':clienteID'=>$cliente_id));
      $data = array(0=>'Seleccione');
      foreach($aryFirmas as $f) $data[$f->id]=$f->razon_social;
      return $data;
   }

   public static function addFirma($data)
   {
      $firma = new Firma;
      $transaction = $firma->dbConnection->beginTransaction( );
      try 
      {
         $firma->attributes = $data;
         $firma->save( );
         $sedePrincipal = new Sede();
         $sedePrincipal->attributes = array(
            'firma_id'        => $firma->id,
            'descripcion'     => 'Principal',
            'domicilio_fiscal'=> $firma->domicilio_fiscal,
            'telefonos'       => $firma->telefonos
         ); 
         if ($sedePrincipal->save( )) $transaction->commit( );
         return $firma->id;
      }
      catch(Exception $e)
      {
         $transaction->rollback( );
      }
   }

}

<?php

/**
 * This is the model class for table "producto".
 *
 * The followings are the available columns in table 'producto':
 * @property integer $id
 * @property string $descripcion
 * @property double $precio_unitario
 * @property integer $cantidad_minima
 * @property integer $documento_fiscal_id
 *
 * The followings are the available model relations:
 * @property DocumentoFiscal $documentoFiscal
 */
class Producto extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Producto the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'producto';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, precio_unitario, cantidad_minima, documento_fiscal_id', 'required'),
			array('cantidad_minima, documento_fiscal_id', 'numerical', 'integerOnly'=>true),
			array('precio_unitario', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descripcion, precio_unitario, cantidad_minima, documento_fiscal_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'documentoFiscal' => array(self::BELONGS_TO, 'DocumentoFiscal', 'documento_fiscal_id'),
		);
	}

   public function scopes()
   {
      return array(
                     'fiscales'  =>array('condition'=>'documento_fiscal_id=1'),
                     'general' =>array('condition'=>'documento_fiscal_id=0')
                  );
   }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'precio_unitario' => 'Precio Unitario',
			'cantidad_minima' => 'Cantidad Minima',
			'documento_fiscal_id' => 'Documento Fiscal',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('precio_unitario',$this->precio_unitario);
		$criteria->compare('cantidad_minima',$this->cantidad_minima);
		$criteria->compare('documento_fiscal_id',$this->documento_fiscal_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
   
   static public function getDocumentosFiscales( )
   {
      $data[] = array(''=>'Seleccione...');
      $documentos = DocumentoFiscal::model( )->findAll();
      foreach($documentos as $d) $data[$d->id] = $d->descripcion;
      return $data;
   }

   static public function getPreciosJSON( )
   {
      $data[0]=array(
                     'precio_unitario'     => 0,
                     'cantidad_minima'     => 0,
                     'documento_fiscal'    => 0,
                     'documento_fiscal_id' => 0  
                    );
      $productos = Producto::model()->findAll( );
      foreach($productos as $p)
      {
         $data[$p->id] = array(
                        'precio_unitario'     => $p->precio_unitario,
                        'cantidad_minima'     => $p->cantidad_minima,
                        'documento_fiscal'    => $p->documentoFiscal->descripcion,
                        'documento_fiscal_id' => $p->documento_fiscal_id   
                        );
      }
      return json_encode($data);
   }

}

<?php

/**
 * This is the model class for table "factura_item".
 *
 * The followings are the available columns in table 'factura_item':
 * @property integer $id
 * @property integer $factura_id
 * @property integer $producto_id
 * @property integer $cantidad
 * @property double $monto
 * @property double $iva
 * @property double $descuento_id
 *
 * The followings are the available model relations:
 * @property Factura $factura
 * @property Producto $producto
 * @property Descuento $descuento
 */
class FacturaItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FacturaItem the static model class
	 */
   
   public $numero_orden;
   public $razon_social;
   public $firma_comercial;
   public $sucursal;
   public $fecha_entrada;
   public $fecha_entrega;
   public $trabajo;
   public $serie;
   public $identificador;
   public $rif;
   public $inicio_numero;
   public $fin_numero;
   public $inicio_control;
   public $fin_control;
   public $status;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'factura_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('factura_id, producto_id, cantidad, precio_unitario, monto, iva', 'required'),
			array('factura_id, producto_id, cantidad', 'numerical', 'integerOnly'=>true),
			array('precio_unitario, monto, iva, descuento_id', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.

         /*
         numero_orden, rif,razon_social,firma_comercial,sucursal,fecha_entrada,
         fecha_entrega,trabajo,serie,identificador,inicio_numero,
         fin_numero,inicio_control,fin_control,status
         */

			array('id, factura_id, producto_id, cantidad, monto, iva', 'safe', 'on'=>'search'),
			array('numero_orden,rif,razon_social,firma_comercial,sucursal,fecha_entrada,fecha_entrega,trabajo,
                serie,identificador,inicio_numero,fin_numero,inicio_control,fin_control,status', 'safe', 'on'=>'custom_search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'factura'   => array(self::BELONGS_TO, 'Factura', 'factura_id'),
			'producto'  => array(self::BELONGS_TO, 'Producto', 'producto_id'),
			'descuento' => array(self::BELONGS_TO, 'Descuento', 'descuento_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'factura_id' => 'Factura',
			'producto_id' => 'Producto',
			'cantidad' => 'Cantidad',
			'monto' => 'Monto',
			'iva' => 'Iva',
         'descuento_id'=>'Descuento'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	/*
   public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('factura_id',$this->factura_id);
		$criteria->compare('producto_id',$this->producto_id);
		$criteria->compare('cantidad',$this->cantidad);
		$criteria->compare('monto',$this->monto);
		$criteria->compare('iva',$this->iva);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
   */

   public function search( )
   {
      $criteria = new CDbCriteria;
      /////////////////////////////////////////////////////////////////////////////
		$criteria->compare('o.id',$this->numero_orden,true);
		$criteria->compare('c.rif'         ,   $this->rif,true);
		$criteria->compare('p.descripcion',    $this->trabajo,true);
      $criteria->compare('dc.serie',         $this->serie,true);
		$criteria->compare('o.id'          ,   $this->numero_orden,true);
		$criteria->compare('c.rif'         ,   $this->rif,true);
		$criteria->compare('c.razon_social',   $this->razon_social,true);
		$criteria->compare('z.razon_social',   $this->firma_comercial,true);
		$criteria->compare('s.descripcion',    $this->sucursal,true);
		$criteria->compare('o.fecha_entrada',  $this->fecha_entrada,true);
		$criteria->compare('o.fecha_entrega',  $this->fecha_entrega,true);
      $criteria->compare('dc.serie',         $this->serie,true);
		$criteria->compare('dc.inicio_numero', $this->inicio_numero);
		$criteria->compare('dc.fin_numero',    $this->fin_numero);
		$criteria->compare('dc.identificador', $this->identificador,true);
      $criteria->alias = 'i';
      $criteria->select = 'o.id AS numero_orden, 
                           c.rif,
                           c.razon_social,
                           z.razon_social AS firma_comercial,
                           s.descripcion AS sucursal,
                           o.fecha_entrada,
                           o.fecha_entrega,
                           p.descripcion AS trabajo,
                           dc.serie,
                           dc.identificador,
                           dc.inicio_numero,
                           dc.fin_numero,
                           dc.inicio_control,
                           dc.fin_control,
                           oe.descripcion AS status';
      $criteria->join   = 'LEFT JOIN factura  f ON (f.id = i.factura_id)
                           LEFT JOIN orden    o ON (o.id = f.orden_id)
                           LEFT JOIN cliente  c ON (c.id = o.cliente_id)
                           LEFT JOIN firma    z ON (z.id = o.firma_id)
                           LEFT JOIN sede     s ON (s.id = o.sede_id)
                           LEFT JOIN producto p ON (p.id = i.producto_id)
                           LEFT JOIN orden_status oe ON (oe.id = o.orden_status_id)
                           LEFT JOIN documento_control dc ON (dc.factura_id = f.id)'; 
      /////////////////////////////////////////////////////////////////////////////
		return new CActiveDataProvider($this, array('criteria'=>$criteria));
   }

 
   public static function eliminarById($id)
   {
      $transaction = Yii::app()->db->beginTransaction( );
      try {
            $deltaBaseImponible=0; $deltaIva=0;
            ////////////////////////////////////////////////////////////////
            $Item    = FacturaItem::model()->findByPk($id);
            if ($Item===null) return;
            $Orden   = Orden::model()->findByPk($Item->factura->orden->id);
            $Factura = Factura::model( )->findByPk($Item->factura->id);
            ////////////////////////////////////////////////////////////////
            $tipo_documento = $Item->producto->documento_fiscal_id;
            // si el tipo de documento es fiscal
            if ($tipo_documento!=0) 
               FacturaItem::_deleteItemFiscal  (&$Item,&$Factura,&$deltaBaseImponible,&$deltaIva);
            // si el tipo de documento es NO es fiscal
            else 
               FacturaItem::_deleteItemNofiscal(&$Item,&$Factura,&$deltaBaseImponible,&$deltaIva);
            // actualizar orden -----------------------------------------------------------------
            $Orden->base_imponible  -= $deltaBaseImponible;
            $Orden->iva             -= $deltaIva;
            $Orden->resta           -= ($deltaBaseImponible+$deltaIva);
            $Orden->save( );
            $transaction->commit( );
            //-----------------------------------------------------------------------------------
            return array(
               'baseImponible'=> $Orden->base_imponible,
               'iva'          => $Orden->iva,
               'totalPagar'   => $Orden->base_imponible+$Orden->iva
            );
      }
      catch(Exception $e)
      {
         echo "<pre>" . $e . "</pre>";
         $transaction->rollback( );
      }
   }

   private function _recalcularMontosFactura(&$Factura)
   {
      $baseImponible = 0;
      $iva = 0;
      foreach($Factura->facturaItems as $item)
      {
         $porcentajeDescuento = $item->descuento->porcentaje/100;
         $precioUnitario      = $item->producto->precio_unitario;
         $valorDescuento      = ($precioUnitario * $porcentajeDescuento);
         $baseImponible += ($item->cantidad * ($precioUnitario - $valorDescuento)); 
         $iva += ($baseImponible * getIVA( ));
      }
      //fb('Base Imponible: ' . $baseImponible,'warn');
      //fb('iva: ' . $iva,'warn');
      // actualizar factura
      $Factura->base_imponible = $baseImponible;
      $Factura->iva = $iva;
      $Factura->save( );
   } 
   
   private function _deleteItemNofiscal(&$Item,&$Factura,&$deltaBaseImponible,&$deltaIva)
   {
      // Eliminar el item y Recalcular montos de la factura
      $deltaBaseImponible = $Item->monto;
      $deltaIva           = $Item->iva;
      $Item->delete( );
      // eliminar la factura si no existen mas item x la factura
      $exist = FacturaItem::model()->exists('factura_id=:p1',array(':p1'=>$Factura->id)); 
      if (!$exist) $Factura->delete( );
      // recalcular los montos de la factura
      else FacturaItem::_recalcularMontosFactura(&$Factura);
   }
   

   private function _deleteItemFiscal(&$Item,&$Factura,&$deltaBaseImponible,&$deltaIva)
   {
      //eliminar el item, el control y la factura, 
      $deltaBaseImponible = $Item->monto;
      $deltaIva           = $Item->iva;
      $control = DocumentoControl::model()->findByAttributes(array('factura_id'=>$Factura->id));
      $control ->delete( );
      $Item    ->delete( );
      $Factura ->delete( );
   }

}

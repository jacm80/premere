<?php

/**
 * This is the model class for table "cliente".
 *
 * The followings are the available columns in table 'cliente':
 * @property integer $id
 * @property string $rif
 * @property string $razon_social
 * @property string $representante_legal
 * @property string $domicilio_fiscal
 * @property integer $contribuyente_id
 * @property integer $cliente_tipo_id
 * @property string $telefono_celular
 * @property string $telefono_residencial
 * @property string $registro_de_comercio
 * @property string $email
 * @property string $archivo_rif
 * @property string $archivo_registro_de_comercio
 *
 * The followings are the available model relations:
 * @property Contribuyente $contribuyente
 * @property ClienteTipo $clienteTipo
 * @property Firma[] $firmas
 * @property Orden[] $ordens
 */
class Cliente extends CActiveRecord
{
	public $cliente;
   public $sucursal;
   /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cliente the static model class
	 */
   //public $cliente;
   //public $firma;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cliente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rif, razon_social, representante_legal, domicilio_fiscal, contribuyente_id, 
                cliente_tipo_id, telefono_celular, telefono_residencial, registro_de_comercio, 
                email', 'required'),
			array('contribuyente_id, cliente_tipo_id', 'numerical', 'integerOnly'=>true),
			array('rif', 'length', 'max'=>20),
			array('telefono_celular, telefono_residencial', 'length', 'max'=>13),
			array('email', 'length', 'max'=>100),
			array('archivo_rif, archivo_registro_de_comercio', 'length', 'max'=>30),
         array('razon_social', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
         array('id, rif, razon_social, representante_legal, domicilio_fiscal, contribuyente_id, 
               cliente_tipo_id, telefono_celular, telefono_residencial, registro_de_comercio, 
               email, archivo_rif, archivo_registro_de_comercio', 'safe', 'on'=>'search'),
         array('rif','unique'),
         array('razon_social','unique'),
         array('email','email')
      );
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contribuyente' => array(self::BELONGS_TO, 'Contribuyente', 'contribuyente_id'),
			'clienteTipo' => array(self::BELONGS_TO, 'ClienteTipo', 'cliente_tipo_id'),
			'firmas' => array(self::HAS_MANY, 'Firma', 'cliente_id'),
			'ordens' => array(self::HAS_MANY, 'Orden', 'cliente_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rif' => 'Rif',
			'razon_social' => 'Razon Social',
			'representante_legal' => 'Representante Legal',
			'domicilio_fiscal' => 'Domicilio Fiscal',
			'contribuyente_id' => 'Contribuyente',
			'cliente_tipo_id' => 'Cliente Tipo',
			'telefono_celular' => 'Telefono Celular',
			'telefono_residencial' => 'Telefono Residencial',
			'registro_de_comercio' => 'Registro De Comercio',
			'email' => 'Email',
			'archivo_rif' => 'Archivo Rif',
			'archivo_registro_de_comercio' => 'Archivo Registro De Comercio',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('rif',$this->rif,true);
		$criteria->compare('razon_social',$this->razon_social,true);
		$criteria->compare('representante_legal',$this->representante_legal,true);
		$criteria->compare('domicilio_fiscal',$this->domicilio_fiscal,true);
		$criteria->compare('contribuyente_id',$this->contribuyente_id);
		$criteria->compare('cliente_tipo_id',$this->cliente_tipo_id);
		$criteria->compare('telefono_celular',$this->telefono_celular,true);
		$criteria->compare('telefono_residencial',$this->telefono_residencial,true);
		$criteria->compare('registro_de_comercio',$this->registro_de_comercio,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('archivo_rif',$this->archivo_rif,true);
		$criteria->compare('archivo_registro_de_comercio',$this->archivo_registro_de_comercio,true);
      $criteria->addNotInCondition('id',array(0));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   static function getContribuyentes( )
   {
      return CHtml::listData(Contribuyente::model()->findAll(),'id','descripcion');
   }

   static function getTipos( )
   {
      $data = array('0'=>'Seleccione');
      $tipos = ClienteTipo::model()->findAll( );
      foreach ($tipos as $t) $data[$t->id]=$t->descripcion;
      return $data;
   }

   /*
      Para procesar el iterador como si 
      fueran objetos en vez de arrays
      $dbC = Yii::app()->db->createCommand();
      $dbC->setFetchMode(PDO::FETCH_OBJ);
   */
   static function getClienteAutoComplete($term)
   {
      $data=array();
      $sql="SELECT id,rif,razon_social,representante_legal
               FROM cliente
               WHERE rif like '%$term%'
                     OR razon_social like '%$term%'
                     OR representante_legal like '%$term%'
               ";
      $clientes = Yii::app()->db->createCommand($sql)->queryAll( );
      foreach($clientes as $c)
      {
         $data[ ]=array(
                       'label'=> $c['rif'].' '.$c['razon_social'],
                       'value'=> $c['razon_social'],
                       'id'   => $c['id'],
                       'field'=> $c['representante_legal']
                     );
      }
      return $data; 
   }
   
   static function getRazoSocialById($id)
   {
      $cliente = Cliente::model( )->findByPk($id);
      return $cliente->razon_social;
   }

   private function _guardar_usuario($username,$password,$email)
   {
      $newUser = Yii::app()->user->um->createBlankUser( );
      $newUser->username = $username;
      $newUser->email    = $email;
      Yii::app( )->user->um->activateAccount($newUser);

      if (Yii::app()->user->um->loadUser($newUser->username) != null)
		{
			echo "El usuario {$newUser->username} ya ha sido creado.";
			return;
		}
      //-----------------------------------------------------------------------------
      Yii::app()->user->um->changePassword($newUser,$password); //pwd el rif
      //-----------------------------------------------------------------------------
      if (Yii::app()->user->um->save($newUser))
      {
		   Yii::app()->user->rbac->assign('cliente',$newUser->primaryKey); // asignarlo a cliente
	   }
	   else 
      {
		   $errores = CHtml::errorSummary($newUser);
			fb("no se pudo crear el usuario: ".$errores);
	   }
   }

   public function guardar( )
   {
      $transaction = $this->dbConnection->beginTransaction( );
      try 
      {
         $this->attributes = $_POST;
         if ($this->save( ))
         {
            $this->_guardar_usuario($this->email,$this->rif,$this->email);
            // agregar la firma principal
            $fPrincipal = new Firma('addPrincipal');
            $fPrincipal->attributes = array(
                                             'cliente_id'          => $this->id,
                                             'razon_social'        => $this->razon_social,
                                             'domicilio_fiscal'    => $this->domicilio_fiscal,
                                             'registro_de_comercio'=> $this->registro_de_comercio,
                                             'telefonos'           => $this->telefono_celular." ".$this->telefono_residencial
                                          );
            if ($fPrincipal->save( ))
            {
               //TODO
               $sedeP = new Sede;
               $sedeP->attributes = array(
                                          'descripcion'     => 'Principal',
                                          'firma_id'        => $fPrincipal->id,
                                          'domicilio_fiscal'=> $this->domicilio_fiscal,
                                          'telefonos'       => $this->telefono_celular." ".$this->telefono_residencial
                                        );
               if ($sedeP->save( )) $transaction->commit();
               else $transaction->rollback();
            }
            else {
               $this->addError('registro','fallo al grabar el cliente');
            }
         }
      }
      catch(Exception $e)
      {
         $transaction->rollback();
      }
   }

}

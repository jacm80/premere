<?php

/**
 * This is the model class for table "proveedor".
 *
 * The followings are the available columns in table 'proveedor':
 * @property integer $id
 * @property string $rif
 * @property string $razon_social
 * @property string $domicilio_fiscal
 * @property string $representante
 * @property string $telefono_fijo
 * @property integer $celular
 * @property string $email
 * @property integer $banco_id
 * @property integer $numero_cuenta
 * @property integer $contribuyente_id
 *
 * The followings are the available model relations:
 * @property Contribuyente $contribuyente
 * @property Banco $banco
 */
class Proveedor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Proveedor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'proveedor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('rif, razon_social, domicilio_fiscal, representante, telefono_fijo, celular, email, banco_id, numero_cuenta, contribuyente_id', 'required'),
			array('banco_id, numero_cuenta, contribuyente_id', 'numerical', 'integerOnly'=>true),
			array('rif', 'length', 'max'=>20),
			array('telefono_fijo', 'length', 'max'=>30),
			array('email', 'length', 'max'=>60),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, rif, razon_social, representante, telefono_fijo, celular, email, banco_id, numero_cuenta, contribuyente_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'contribuyente' => array(self::BELONGS_TO, 'Contribuyente', 'contribuyente_id'),
			'banco' => array(self::BELONGS_TO, 'Banco', 'banco_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'rif' => 'Rif',
			'razon_social' => 'Razon Social',
         'domicilio_fiscal'=> 'Domicilio Fiscal',
			'representante' => 'Representante',
			'telefono_fijo' => 'Telefono Fijo',
			'celular' => 'Celular',
			'email' => 'Email',
			'banco_id' => 'Banco',
			'numero_cuenta' => 'Numero Cuenta',
			'contribuyente_id' => 'Contribuyente',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('rif',$this->rif,true);
		$criteria->compare('razon_social',$this->razon_social,true);
		$criteria->compare('representante',$this->representante,true);
		$criteria->compare('telefono_fijo',$this->telefono_fijo,true);
		$criteria->compare('celular',$this->celular);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('banco_id',$this->banco_id);
		$criteria->compare('numero_cuenta',$this->numero_cuenta);
		$criteria->compare('contribuyente_id',$this->contribuyente_id);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getDescripcionById($id)
   {
      $p = Proveedor::model()->findByPk($id);
      return $p->razon_social;
   }

   public static function getAutoComplete($term)
   {
      $data=array();
      $sql="SELECT id,rif,razon_social,representante
               FROM proveedor
               WHERE rif like '%$term%'
                     OR razon_social like '%$term%'
                     OR representante like '%$term%'
               ";
      $clientes = Yii::app()->db->createCommand($sql)->queryAll( );
      foreach($clientes as $c)
      {
         $data[ ]=array(
                       'label'=> $c['rif'].' '.$c['razon_social'],
                       'value'=> $c['razon_social'],
                       'id'   => $c['id'],
                       'field'=> $c['representante']
                     );
      }
      return $data; 
   }

}

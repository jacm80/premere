<?php

/**
 * This is the model class for table "catalogo_cuenta".
 *
 * The followings are the available columns in table 'catalogo_cuenta':
 * @property integer $id
 * @property string $codigo
 * @property string $parent_codigo
 * @property string $tipo
 * @property string $name
 * @property integer $nv
 * @property integer $dv
 * @property integer $snv
 * @property integer $sdv
 */
class CatalogoCuenta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CatalogoCuenta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'catalogo_cuenta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('codigo, parent_codigo, tipo, name, nv, dv, snv, sdv', 'required'),
			array('nv, dv, snv, sdv', 'numerical', 'integerOnly'=>true),
			array('codigo, parent_codigo', 'length', 'max'=>100),
			array('tipo', 'length', 'max'=>2),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, codigo, parent_codigo, tipo, name, nv, dv, snv, sdv', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo' => 'Codigo',
			'parent_codigo' => 'Cuenta Padre',
			'tipo' => 'Tipo',
			'name' => 'Cuenta',
			'nv' => 'Nv',
			'dv' => 'Dv',
			'snv' => 'Snv',
			'sdv' => 'Sdv',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo',$this->codigo,true);
		$criteria->compare('parent_codigo',$this->parent_codigo,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('nv',$this->nv);
		$criteria->compare('dv',$this->dv);
		$criteria->compare('snv',$this->snv);
		$criteria->compare('sdv',$this->sdv);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public function behaviors()
   {
      return array(
                     'nestedInterval'=>'application.extensions.nestedInterval.NestedIntervalBehavior'
                  );
   }

    
   public static function ListaCatalogoCuenta($id)
   {
      $data = array('0'=>'Seleccione');
      $CC = CatalogoCuenta::model()->findByPk($id);
      foreach($CC->children as $c) $data[$c->id] = $c->name;
      return $data;
   }

   public static function listaRoots( )
   {
      $data = array('0'=>'Seleccione');
      $CC = CatalogoCuenta::model()->findAllByAttributes(array('parent_codigo'=>0));
      foreach($CC as $c) $data[$c->id] = $c->name;
      return $data;
   }


}

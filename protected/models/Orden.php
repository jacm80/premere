<?php

/**
 * This is the model class for table "orden".
 *
 * The followings are the available columns in table 'orden':
 * @property integer $id
 * @property integer $numero
 * @property integer $cliente_id
 * @property integer $firma_id
 * @property integer $sede_id
 * @property string $fecha_entrada
 * @property string $fecha_entrega
 * @property string $direcciones_incluidas
 * @property string $telefonos_incluidos
 * @property string $publicidad_incluida
 * @property string $observaciones
 * @property string $abono
 * @property integer $orden_status_id
 * @property integer $forma_pago_id
 * @property integer $banco_id
 * @property double $base_imponible
 * @property double $iva
 * @property double $resta
 * @property double $saldo
 *
 * The followings are the available model relations:
 * @property DetalleOrden[] $detalleOrdens
 * @property Banco $banco
 * @property Cliente $cliente
 * @property FormaPago $formaPago
 * @property OrdenStatus $ordenStatus
 */
class Orden extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Orden the static model class
	 */


   public $rif;
   public $sucursal;

   // para el controller buzon
   public $firma_razon_social;
   public $cliente_razon_social;

   /**/
   public $estatus;
   public $abonoFormaPagoId;
   public $abonoFormaPagoDescripcion;
	public $abonoBancoId;
	public $abonoBancoDescripcion;
   public $abonoNumeroComprobante;
   public $abonoMonto;
   /**/

   public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orden';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
            array('cliente_id, fecha_entrada, fecha_entrega, direcciones_incluidas, 
                   telefonos_incluidos, base_imponible, iva, abono', 'required'),
            array('numero, cliente_id, firma_id, sede_id, orden_status_id', 'numerical', 'integerOnly'=>true),
            array('abono, base_imponible, iva', 'numerical'),
            array('abono', 'length', 'max'=>10),
            // lo que me tenia LOCO
            array('publicidad_incluida, observaciones, fecha_entrega, base_imponible, iva, saldo','safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('numero, rif, firma_razon_social, cliente_id, cliente, estatus, firma_id, sede_id, fecha_entrada, fecha_entrega, 
               direcciones_incluidas, telefonos_incluidos, publicidad_incluida, 
               observaciones, abono, orden_status_id, total', 'safe', 'on'=>'search'),	
            array('id, numero, rif, firma, sucursal, fecha_entrada, fecha_entrega', 'safe', 'on'=>'search_buzon'));
      }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'banco'        => array(self::BELONGS_TO, 'Banco', 'banco_id'),	
         'cliente'      => array(self::BELONGS_TO, 'Cliente', 'cliente_id'),
			'ordenStatus'  => array(self::BELONGS_TO, 'OrdenStatus', 'orden_status_id'),
         // relaciones sin claves foraneas
			'firma'        => array(self::BELONGS_TO, 'Firma', 'firma_id'),
			'sede'         => array(self::BELONGS_TO, 'Sede', 'sede_id'),
         // relacion para las facturas e item de facturas
			'facturas'     => array(self::HAS_MANY, 'Factura', 'orden_id'),
		);
	}

   public function scopes( )
   {
      return array(
         'creadas'   => array('condition'=>'orden_status_id=1'),
         'maquetadas'=> array('condition'=>'orden_status_id=2'),
         'impresas'  => array('condition'=>'orden_status_id=3'),
         'canceladas'=> array('condition'=>'orden_status_id=4'),
      );
   }


	/**
	 * @return array customized attribute labels (name=>label)
	 */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'numero' => 'Numero',
            'cliente_id' => 'Cliente',
            'firma_id' => 'Firma',
            'sede_id' => 'Sede',
            'fecha_entrada' => 'Fecha Entrada',
            'fecha_entrega' => 'Fecha Entrega',
            'direcciones_incluidas' => 'Direcciones Incluidas',
            'telefonos_incluidos' => 'Telefonos Incluidos',
            'publicidad_incluida' => 'Publicidad Incluida',
            'observaciones' => 'Observaciones',
            'abono' => 'Abono',
            'orden_status_id' => 'Orden Status',
            'base_imponible' => 'Base Imponible',
            'iva' => 'Iva',
            'resta' => 'Resta',
            'saldo' => 'Saldo'
        );
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
	   $criteria = new CDbCriteria;
      /////////////////////////////////////////////////////////////////////////////
      //$criteria->distinct = FALSE;
		$criteria->compare('c.numero',$this->id);
      $criteria->compare('rif',$this->rif,TRUE);
      $criteria->compare('f.razon_social',$this->firma_razon_social,TRUE);
		$criteria->compare('orden_status_id',$this->orden_status_id);
		$criteria->compare('fecha_entrada',$this->fecha_entrada,TRUE);
		$criteria->compare('fecha_entrega',$this->fecha_entrega,TRUE);
      //$criteria->params = array(':status'=>'IMPRESA');
      $criteria->alias = 't';

      $criteria->select = 't.id,
                           c.rif, 
                           oe.descripcion AS estatus, 
                           f.razon_social AS firma_razon_social, 
                           t.fecha_entrada, 
                           t.fecha_entrega,
                           t.orden_status_id'; 

      $criteria->join   = 'LEFT JOIN cliente c ON (c.id = t.cliente_id) 
                           LEFT JOIN firma f ON (f.id = t.firma_id) 
                           LEFT JOIN orden_status oe ON (oe.id = t.orden_status_id)'; 
		//$criteria->with   = array('cliente','firma','ordenStatus');
      /////////////////////////////////////////////////////////////////////////////
		return new CActiveDataProvider($this, array('criteria'=>$criteria));
	}


   public function search_buzon( )
   {
	   $criteria = new CDbCriteria;
		$criteria->compare('t.numero',$this->id);
      $criteria->compare('f.razon_social',$this->firma_razon_social,true);
      $criteria->compare('s.descripcion',$this->sucursal,true);
		$criteria->compare('fecha_entrada',$this->fecha_entrada,true);
		$criteria->compare('fecha_entrega',$this->fecha_entrega,true);
      
      if (Yii::app()->user->checkAccess('maquetador'))
            $criteria->addInCondition('orden_status_id',array(1,2));
      if ((Yii::app()->user->checkAccess('admin')) OR (Yii::app()->user->checkAccess('vendedor')))
            $criteria->addInCondition('orden_status_id',array(2,3));

      $criteria->select = "t.numero,
                           c.rif,
                           f.razon_social AS firma_razon_social,
                           s.descripcion  AS sucursal,
                           t.fecha_entrada,
                           t.fecha_entrega,
                           t.orden_status_id";
      $criteria->alias = 't';
      $criteria->join  = "LEFT JOIN cliente  c ON (c.id = t.cliente_id)
                          LEFT JOIN firma    f ON (f.id = t.firma_id)
                          LEFT JOIN sede     s ON (s.id = t.sede_id)";
		//$criteria->with  = array('cliente','firma','sede');      

		return new CActiveDataProvider($this, array('criteria'=>$criteria));
		//return new CActiveDataProvider($buzon);
   }



   public static function getSelectProductos( )
   {
      $data = array('0'=>'Seleccione...');
      $productos = Producto::model()->findAll();
      foreach($productos as $p) $data[$p->id] = $p->descripcion;
      return $data;
   }
   
   public function getOrden( )
   {
      $_orden = array(
                        'id'           => $this->id, 
                        'numero'       => $this->numero, 
                        'razon_social' => $this->cliente->razon_social,
                        'rif'          => $this->cliente->rif,
                        'firma'        => $this->firma->razon_social,
                        'sede'         => $this->sede->descripcion, 
                        'fecha_entrada'=> $this->fecha_entrada,
                        'fecha_entrega'=> $this->fecha_entrega,
                        'direcciones_incluidas' => $this->direcciones_incluidas,
                        'telefonos_incluidos'   => $this->telefonos_incluidos,
                        'publicidad_incluida'   => $this->publicidad_incluida,
                        'observaciones'         => $this->observaciones,
                        'abono'           => $this->abono,
                        'orden_status'    => $this->ordenStatus->descripcion,
                        'forma_pago'      => $this->abonoFormaPagoDescripcion,
                        'banco'           => $this->abonoBancoDescripcion,
                        'base_imponible'  => $this->base_imponible,
                        'iva'             => $this->iva,
                        'total_pagar'     => $this->base_imponible + $this->iva,
                        'resta'           => $this->resta,
                        'saldo'           => $this->saldo
                       );
      return $_orden;
   }

   public function getFacturas( )
   {
      $facturas = $this->facturas;
      $i=0;
      foreach ($facturas as $f)
      {
         $id = $f->id;
         $_factura[$id] = array( 
                                 'id'               => $f->id,
                                 'numero'           => $f->numero,
                                 'usuario_id'       => $f->usuario_id,
                                 'cancelada'        => $f->cancelada,
                                 'fecha_cancelacion'=> $f->fecha_cancelacion,
                                 'firma'            => $f->orden->firma->razon_social,
                                 'rif'              => $f->orden->cliente->rif,
                                 'forma_pago'       => 'falta',
                                 'base_imponible'   => $f->base_imponible,
                                 'iva'              => $f->iva,
                                 'items'            => array()
                                 );
         $items = array( );
         foreach($f->facturaItems as $item)
         {
            $tipo = ($item->producto->documento_fiscal_id>0) ? 'fiscal' : 'papeleria'; 
            $documentos[$tipo][$id] = $_factura[$id]; 
            $items[] =  array(
                              'id'      => $item->id,
                              'producto'=> $item->producto->descripcion,
                              'cantidad'=> $item->cantidad,
                              'precio_unitario'  => $item->precio_unitario,
                              'monto'   => $item->monto,
                              'iva'     => $item->iva,
                              'subtotal'=> $item->iva+$item->monto
                              );
         }
         $documentos[$tipo][$id]['items'] = $items;
      }
      return $documentos;
      //echo HtmlApp::debug($documentos);
   }

   public static function getFacturaPDF($id)
   {
      $fact = Factura::model()->findByPK($id);
      $factura['numero']         = $fact->numero;
      $factura['cliente']        = $fact->orden->cliente->razon_social;
      $factura['rif']            = $fact->orden->cliente->rif;
      $factura['forma_pago']     = 'Efectivo';
      $factura['base_imponible'] = $fact->base_imponible;
      $factura['iva']            = $fact->iva;
      $contribuyente = $fact->orden->cliente->contribuyente->descripcion;
      $factura['items'] = array( ); $i=0;
      foreach ($fact->facturaItems as $item)
      {
         $tipo_doc = $item->producto->documento_fiscal_id;
         $factura['items'][$i] = array(
                                    'cantidad'       => $item->cantidad,
                                    'producto'       => $item->producto->descripcion,
                                    'precio_unitario'=> $item->precio_unitario,
                                    'monto'          => $item->monto
                                    );
         if ($tipo_doc!=0)
         {
            $n = $fact->documentoControls;
            $serie          = $n->serie;
            $identificador  = $n->identificador;
            $inicio_numero  = $n->inicio_numero;
            $fin_numero     = $n->fin_numero;
            $fin_control    = $n->fin_control;
            $inicio_control = $n->inicio_control;
            $factura['items'][$i]['producto'].= "<br />Contribuyente: $contribuyente, Serie: <b>$serie</b>,";
            if ($tipo_doc!=5)
                  $factura['items'][$i]['producto'].= "<br />Numero de Factura del: $inicio_numero Hasta: $fin_numero,<br />";
            $factura['items'][$i]['producto'].= "Numero de Control del: $inicio_control Hasta: $fin_control.";
         }
         $i++;
      }
      return $factura;
   }


   public static function getPagosByOrden($id)
   {
      $arypagos = array();
      $pagos = Pago::model()->findAllByAttributes(array('orden_id'=>$id));
      foreach($pagos as $p)
      {
         $arypagos[ ] = array(
                           'id' => $p->id,
                           'factura_id'  => $p->factura_id,
                           'forma_pago'  => $p->formaPago->descripcion,
                           'numero_comprobante'=>$p->numero_comprobante,
                           'fecha'       => $p->fecha,
                           'banco'       => $p->banco->descripcion,
                           'usuario'     => $p->usuario->username,
                           'descripcion' => $p->descripcion,
                           'monto'       => $p->monto
                           );
      }
      return $arypagos;
   } 

   public static function getProductos($orden_id)
   {
      $item  = array( );
      $Orden = Orden::model()->findByPK($orden_id);
      foreach ($Orden->facturas as $factura)
      {
         foreach ($factura->facturaItems as $i)
         { 
           $numeracion='';
           $tipo_doc = $i->producto->documento_fiscal_id;
           if ($tipo_doc!=0)
           {
            //foreach ($factura->documentoControls as $cnum)
            {
               $numeracion = "Identificador {$factura->documentoControls->identificador}, Serie <strong>{$factura->documentoControls->serie}</strong><br />";
               $numeracion.= "Numero de Documento desde:<strong>{$factura->documentoControls->inicio_numero}</strong> Hasta: 
                                 <strong>{$factura->documentoControls->fin_numero}</strong><br />";
               $numeracion.= "Numero de Control desde:<strong>{$factura->documentoControls->inicio_control}</strong> Hasta: 
                                 <strong>{$factura->documentoControls->fin_control}</strong><br />";
            }
           }
           $item[] = array(
                           'id'              => $i->id,
                           'cantidad'        => $i->cantidad,
                           'producto'        => $i->producto->descripcion,
                           'precio_unitario' => $i->precio_unitario,
                           'monto'           => $i->monto,
                           'iva'             => $i->iva,
                           'numeracion'      => $numeracion
                          ); 
         }
      }
      return $item;
   }

   //////////////////////////////////////////////////
   //       GUARDAR ORDEN DE TRABAJO
   //////////////////////////////////////////////////
   
   private function _setControlNumeracion($f,$tipoDoc,$i)
   {
      $Control = new DocumentoControl;
      $Control->attributes = array(
                  'documento_fiscal_id'=> $tipoDoc,
                  'cliente_id'         => $this->cliente_id,
                  'serie'              => $_POST['DocumentoControl']['serie'][$i],
                  'identificador'      => $_POST['DocumentoControl']['identificador'][$i],
                  'inicio_numero'      => $_POST['DocumentoControl']['inicio_numero'][$i],
                  'fin_numero'         => $_POST['DocumentoControl']['fin_numero'][$i],
                  'inicio_control'     => $_POST['DocumentoControl']['inicio_control'][$i],
                  'fin_control'        => $_POST['DocumentoControl']['fin_control'][$i],
                  'juegos'             => $_POST['DocumentoControl']['juegos'][$i],
                  'cantidad'           => $_POST['DocumentoControl']['cantidad'][$i],
                  'fecha_impresion'    => $this->fecha_entrega,
                  'factura_id'         => $f,
                  );
      $Control->save( );
   }

   private function _setFactura($numfactura,$fiscal)
   {
      $Factura = new Factura;
      $Factura->attributes = array(
                   'numero'           => $numfactura,
                   'fecha_cancelacion'=> null,
                   'orden_id'         => $this->id,
                   'usuario_id'       => 1,
                   'base_imponible'   => 0,
                   'iva'              => 0,
                   'cancelada'        => 0,
                   'fiscal'           => $fiscal
      );
      $Factura->save();
      return $Factura;
   }

   private function _setFacturaItem($fid,$pid,$cantidad,$i)
   {
      $FacturaItem = new FacturaItem;
      $descuento = explode('-',$_POST['DetalleOrden']['descuento_id'][$i]);
      $FacturaItem->attributes = array(
                  'factura_id'      =>   $fid,
                  'producto_id'     =>   $pid,
                  'cantidad'        =>   $cantidad,
                  'precio_unitario' =>   $_POST['DetalleOrden']['precio'][$i],
                  'monto'           =>   $cantidad * $_POST['DetalleOrden']['precio'][$i],
                  'iva'             => (($cantidad * $_POST['DetalleOrden']['precio'][$i]) * getIVA( )),
                  'descuento_id'    =>   $descuento[0]
                  );
      $FacturaItem->save( );
   }

   // echo "n: $i - pid: $pid - T: $tipoDoc <br />";
   
  //private function _getNumeroFactura() { return 1000; }


   private function _getFacturaNOFiscal( )
   {
      $rsFactura = Factura::model()->findByAttributes(array('fiscal'=>0,'orden_id'=>$this->id));
      if ($rsFactura===null) return FALSE;
      else return $rsFactura;
   }

   private function _groupByNOFiscal(&$Factura,$NoFiscal,&$numfactura,$pid,$cantidad,$i)
   { 
      if (!isset($Factura))
      { 
         if ($this->_getFacturaNOFiscal( )) $Factura = $this->_getFacturaNOFiscal( );
         else 
         { 
            $numfactura = succDocumento('FACTURA'); 
            $Factura  = $this->_setFactura($numfactura,0); 
         }
         $NoFiscal = $Factura->id;
         $this->_setFacturaItem($NoFiscal,$pid,$cantidad,$i);
      }
      else 
      {               
         if (isset($NoFiscal)) $this->_setFacturaItem($NoFiscal,$pid,$cantidad,$i);
         else 
         {
            if ($this->_getFacturaNOFiscal( )) $Factura = $this->_getFacturaNOFiscal( );
            else 
            {
               $numfactura = succDocumento('FACTURA'); 
               $Factura  = $this->_setFactura($numfactura,0);
            }
            $NoFiscal = $Factura->id;
            $this->_setFacturaItem($NoFiscal,$pid,$cantidad,$i);
         }
      }
   }
   
   private function _groupByFactura( )
   {
      /*
      $Factura    = NULL;
      $NoFiscal   = NULL;
      $numfactura = NULL;
      */
      if (!isset($_POST['DetalleOrden'])) return;
      $i = 0; 
      foreach($_POST['DetalleOrden']['producto_id'] as $pid)
      {
         $cantidad = $_POST['DocumentoControl']['cantidad'][$i];
         $tipoDoc  = $_POST['DetalleOrden']['documento_fiscal_id'][$i];
         if ($tipoDoc != 0) // Es Documento Fiscal (nueva factura)
         {
            $numfactura = succDocumento('FACTURA'); 
            $Factura = $this->_setFactura($numfactura,1);
            $this->_setControlNumeracion($Factura->id,$tipoDoc,$i);
            $this->_setFacturaItem($Factura->id,$pid,$cantidad,$i);
         }
         else $this->_groupByNOFiscal(&$Factura,&$NoFiscal,&$numfactura,$pid,$cantidad,$i);
         $base_imponible =   $cantidad * $_POST['DetalleOrden']['precio'][$i];
         $iva            = (($cantidad * $_POST['DetalleOrden']['precio'][$i]) * getIVA( ));
         // Calcular los montos por Factura
         $Factura->base_imponible+= $base_imponible;
         $Factura->iva           += $iva;
         $Factura->save( );
         // Calcular los montos por Orden de Trabajo
         $this->base_imponible   += $base_imponible;
         $this->iva              += $iva;
         $i++;
      }
   }

   private function _eliminarPagos( )
   {
      $pagos = Pago::model()->findAllByAttributes(array('orden_id'=>$this->id));
      foreach($pagos as $p) $p->delete( );
   } 

   private function _guardarAbono( )
   {
      $this->_eliminarPagos( );
      $Abono = new Pago;
      $numc = isset($_POST['Pago']['numero_comprobante']) ? $_POST['Pago']['numero_comprobante'] : NULL;
      $Abono->attributes = array(
                                 'orden_id'     => $this->id,
                                 'factura_id'   => NULL,
                                 'forma_pago_id'=> $_POST['Pago']['forma_pago_id'],
                                 'fecha'        => $this->fecha_entrada,
                                 'banco_id'     => 0,
                                 'numero_comprobante'=> $numc,
                                 'usuario_id'   => 1,
                                 'descripcion'  => 'Abono Orden',
                                 'monto'        => $_POST['Orden']['abono']
                                );
      $Abono->save( );
   }

   public function guardarOrden( ) 
   {
      $_orden = $_POST['Orden'];
      $transaction = $this->dbConnection->beginTransaction();
      ///////////////////////////////////////////////////////
      if ($this->isNewRecord)
      {
         $this->numero = succDocumento('ORDEN DE TRABAJO');
         $this->base_imponible  = 0;
         $this->iva             = 0;
         $this->orden_status_id = 1; // CREADA
         $this->fecha_entrada   = date('Y-m-d h:m:i');
         $this->resta           = 0;
      }
      else $this->resta = $this->base_imponible + $this->iva;
      ///////////////////////////////////////////////////////
      //--------------------------
      $this->attributes = $_orden;
      try
      {
         if ($this->save()) $this->_groupByFactura( );
         //-----------------------------------------------------
         // guardar el abono como pago
         if ($_POST['Orden']['abono']>0) $this->_guardarAbono( );
         //-----------------------------------------------------
         $this->resta = ($this->base_imponible + $this->iva) - $this->abono;
         $this->saldo = $this->abono;
         $this->save( );
         $transaction->commit();
      }
      catch(Exception $e)
      {
         echo "<pre>" . $e . "</pre>";
         $transaction->rollback();
      }
   }

   protected function afterFind( ) 
   { 
      if (!empty($this->id)) $this->_setAbono( );
   }

   private function _setAbono( )
   {
      $Abono = Pago::model()->findByAttributes(array('orden_id'=>$this->id,'descripcion'=>'Abono Orden'));
      $this->abonoFormaPagoId          = $Abono->forma_pago_id;
      $this->abonoBancoId              = $Abono->banco->id;
      $this->abonoBancoDescripcion     = $Abono->banco->descripcion;
      $this->abonoNumeroComprobante    = $Abono->numero_comprobante;
      $this->abonoFormaPagoDescripcion = $Abono->formaPago->descripcion;
      $this->abonoMonto                = $Abono->monto;
   }

   public function dataPrintOrden( )
   {
         return  array(
                     'numero'                => sprintf('%06d',$this->numero),
                     'fecha_entrada'         => $this->fecha_entrada,
                     'fecha_entrega'         => $this->fecha_entrega,
                     'rif'                   => $this->cliente->rif,
                     'razon_social'          => $this->cliente->razon_social,
                     'telefonos_incluidos'   => $this->telefonos_incluidos,
                     'contribuyente'         => $this->cliente->contribuyente->descripcion,
                     'tipo_direccion'        => ($this->sede->descripcion=='Principal') ? 'Principal' : 'Sucursal',
                     'direcciones_incluidas' => $this->direcciones_incluidas,
                     'publicidad_incluida'   => $this->telefonos_incluidos,
                     'subtotal'              => $this->base_imponible,
                     'total'                 => $this->base_imponible + $this->iva,
                     'iva'                   => $this->iva,
                     'abono'                 => $this->abonoMonto,
                     'resta'                 => $this->resta,
                     'forma_pago'            => $this->abonoFormaPagoDescripcion,
                     'banco'                 => $this->abonoBancoDescripcion,
                     'numero_comprobante'    => $this->abonoNumeroComprobante,
                     'observaciones'         => $this->observaciones,
                     'items'                 => $this->getProductos($this->id)
                    );
         //fb($this->sede->descripcion,'info');
         //exit();
   }
    
}

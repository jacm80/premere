<?php

/**
 * This is the model class for table "material_conciliacion".
 *
 * The followings are the available columns in table 'material_conciliacion':
 * @property integer $id
 * @property string $fecha
 * @property integer $almacen_id
 * @property integer $inspecciono
 * @property string $observacion
 *
 * The followings are the available model relations:
 * @property Almacen $almacen
 * @property Personal $inspecciono0
 * @property MaterialConciliacionItem[] $materialConciliacionItems
 */
class MaterialConciliacion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaterialConciliacion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'material_conciliacion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, almacen_id, inspecciono, observacion', 'required'),
			array('almacen_id, inspecciono', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha, almacen_id, inspecciono, observacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'almacen' => array(self::BELONGS_TO, 'Almacen', 'almacen_id'),
			'personal' => array(self::BELONGS_TO, 'Personal', 'inspecciono'),
			'Items' => array(self::HAS_MANY, 'MaterialConciliacionItem', 'conciliacion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha' => 'Fecha',
			'almacen_id' => 'Almacen',
			'inspecciono' => 'Inspecciono',
			'observacion' => 'Observacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('almacen_id',$this->almacen_id);
		$criteria->compare('inspecciono',$this->inspecciono);
		$criteria->compare('observacion',$this->observacion,true);
      $criteria->order  = 'id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
   

   private function _actualizarInventario($cm_id,$almacen_id,$e_unidad,$e_detallado)
   {
      $m = Material::model()->findByAttributes(array('catalogo_material_id'=>$cm_id,'almacen_id'=>$almacen_id));
      if ($m===null) 
      { 
         echo "el material no se encontro"; 
         return -1; 
      }
      else
      {
         $m->existencia_unidad    = $e_unidad;
         $m->existencia_detallado = $e_detallado;
         $m->save( );
         return $m->catalogo_material_id;
      }
   }

   public function guardar( )
   {
      $transaction  = $this->dbConnection->beginTransaction( );
      $conciliacion = $_POST['MaterialConciliacion'];
      $conciliacion['fecha'] = date('Y-m-d h:m:i');
      try
      {
         $this->attributes = $conciliacion;
         if( $this->save( ) )
         {
            /**/
            $items = $_POST['items'];
            $i=0;
            foreach ($items['catalogo_material_id'] as $cm_id)
            {
               $pre = 'existencia';
               //--------------------------------------------------------
               $u_sys  = $items[$pre.'_unidad_sistema'][$i];
               $d_sys  = $items[$pre.'_detallado_sistema'][$i];

               $u_real = $items[$pre.'_unidad_real'   ][$i];
               $d_real = $items[$pre.'_detallado_real'][$i];
               //--------------------------------------------------------
               $dif_u  = $items[$pre.'_unidad_sistema'   ][$i] - $u_real;
               $dif_d  = $items[$pre.'_detallado_sistema'][$i] - $d_real;
               //--------------------------------------------------------
               $dif_u *= -1;
               $dif_d *= -1;
               //--------------------------------------------------------
               $cItem = new MaterialConciliacionItem;
               $cItem->attributes = array(
                                          'conciliacion_id'             => $this->id,
                                          'catalogo_material_id'        => $this->_actualizarInventario($cm_id,$this->almacen_id,$u_real,$d_real), 
                                          'existencia_unidad_sistema'   => $u_sys,
                                          'existencia_detallado_sistema'=> $d_sys,
                                          'existencia_unidad_real'      => $u_real,
                                          'existencia_detallado_real'   => $d_real,
                                          'diferencia_unidad'           => $dif_u,
                                          'diferencia_detallado'        => $dif_d
                                         );
               if (!$cItem->save( )) $transaction->rollback( );
               $i++;
            }
            /**/
            $transaction->commit( );
         }
      }
      catch (Exception $e)
      {
         $transaction->rollback( );
      }
   }

}

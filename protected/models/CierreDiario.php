<?php

/**
 * This is the model class for table "cierre_diario".
 *
 * The followings are the available columns in table 'cierre_diario':
 * @property integer $id
 * @property string $fecha
 * @property string $ordenesNuevas
 * @property string $ordenesPagos
 * @property string $gastos
 * @property string $oNuevas_montoRecibidoAB
 * @property string $oNuevas_montoRecibidoTT
 * @property string $oNuevas_montoRecibidoEF
 * @property string $oNuevas_montoRecibidoCH
 * @property string $oNuevas_montoRecibidoBC
 * @property string $oNuevas_montoPendiente
 * @property string $oNuevas_cantidad
 * @property string $oPagos_montoRecibidoTT
 * @property string $oPagos_montoRecibidoEF
 * @property string $oPagos_montoRecibidoCH
 * @property string $oPagos_montoRecibidoBC
 * @property string $oPagos_montoPendiente
 * @property string $oPagos_cantidad
 * @property string $montoEfectivo
 * @property string $montoBanco
 * @property integer $usuario_id
 * @property integer $fecha_actualizacion
 */
class CierreDiario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CierreDiario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cierre_diario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			
         /*
         array('id, fecha, ordenesNuevas, ordenesPagos, oNuevas_montoRecibidoAB, oNuevas_montoRecibidoTT, oNuevas_montoRecibidoEF, oNuevas_montoRecibidoCH, oNuevas_montoRecibidoBC, oNuevas_montoPendiente, oNuevas_cantidad, oPagos_montoRecibidoTT, oPagos_montoRecibidoEF, oPagos_montoRecibidoCH, oPagos_montoRecibidoBC, oPagos_montoPendiente, oPagos_cantidad, montoEfectivo, montoBanco, usuario_id, fecha_actualizacion', 'required'),
			array('id, usuario_id, fecha_actualizacion', 'numerical', 'integerOnly'=>true),
			array('oNuevas_montoRecibidoAB, oNuevas_montoRecibidoTT, oNuevas_montoRecibidoEF, oNuevas_montoRecibidoCH, oNuevas_montoRecibidoBC, oNuevas_montoPendiente, oNuevas_cantidad, oPagos_montoRecibidoTT, oPagos_montoRecibidoEF, oPagos_montoRecibidoCH, oPagos_montoRecibidoBC, oPagos_montoPendiente, oPagos_cantidad, montoEfectivo, montoBanco', 'length', 'max'=>10),
         */

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha, ordenesNuevas, ordenesPagos, gastos, oNuevas_montoRecibidoAB, oNuevas_montoRecibidoTT, oNuevas_montoRecibidoEF, oNuevas_montoRecibidoCH, oNuevas_montoRecibidoBC, oNuevas_montoPendiente, oNuevas_cantidad, oPagos_montoRecibidoTT, oPagos_montoRecibidoEF, oPagos_montoRecibidoCH, oPagos_montoRecibidoBC, oPagos_montoPendiente, oPagos_cantidad, montoEfectivo, montoBanco, usuario_id, fecha_actualizacion', 'safe', 'on'=>'search'),
			array('id, fecha, ordenesNuevas, ordenesPagos, gastos, oNuevas_montoRecibidoAB, oNuevas_montoRecibidoTT, oNuevas_montoRecibidoEF, oNuevas_montoRecibidoCH, oNuevas_montoRecibidoBC, oNuevas_montoPendiente, oNuevas_cantidad, oPagos_montoRecibidoTT, oPagos_montoRecibidoEF, oPagos_montoRecibidoCH, oPagos_montoRecibidoBC, oPagos_montoPendiente, oPagos_cantidad, montoEfectivo, montoBanco, usuario_id, fecha_actualizacion', 'safe')
		);

	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha' => 'Fecha',
			'ordenesNuevas' => 'Ordenes Nuevas',
			'ordenesPagos' => 'Ordenes Pagos',
			'gastos' => 'Gastos',
			'oNuevas_montoRecibidoAB' => 'O Nuevas Monto Recibido Ab',
			'oNuevas_montoRecibidoTT' => 'O Nuevas Monto Recibido Tt',
			'oNuevas_montoRecibidoEF' => 'O Nuevas Monto Recibido Ef',
			'oNuevas_montoRecibidoCH' => 'O Nuevas Monto Recibido Ch',
			'oNuevas_montoRecibidoBC' => 'O Nuevas Monto Recibido Bc',
			'oNuevas_montoPendiente' => 'O Nuevas Monto Pendiente',
			'oNuevas_cantidad' => 'O Nuevas Cantidad',
			'oPagos_montoRecibidoTT' => 'O Pagos Monto Recibido Tt',
			'oPagos_montoRecibidoEF' => 'O Pagos Monto Recibido Ef',
			'oPagos_montoRecibidoCH' => 'O Pagos Monto Recibido Ch',
			'oPagos_montoRecibidoBC' => 'O Pagos Monto Recibido Bc',
			'oPagos_montoPendiente' => 'O Pagos Monto Pendiente',
			'oPagos_cantidad' => 'O Pagos Cantidad',
			'montoEfectivo' => 'Monto Efectivo',
			'montoBanco' => 'Monto Banco',
			'usuario_id' => 'Usuario',
			'fecha_actualizacion' => 'Fecha Actualizacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('ordenesNuevas',$this->ordenesNuevas,true);
		$criteria->compare('ordenesPagos',$this->ordenesPagos,true);
		$criteria->compare('gastos',$this->gastos,true);
		$criteria->compare('oNuevas_montoRecibidoAB',$this->oNuevas_montoRecibidoAB,true);
		$criteria->compare('oNuevas_montoRecibidoTT',$this->oNuevas_montoRecibidoTT,true);
		$criteria->compare('oNuevas_montoRecibidoEF',$this->oNuevas_montoRecibidoEF,true);
		$criteria->compare('oNuevas_montoRecibidoCH',$this->oNuevas_montoRecibidoCH,true);
		$criteria->compare('oNuevas_montoRecibidoBC',$this->oNuevas_montoRecibidoBC,true);
		$criteria->compare('oNuevas_montoPendiente',$this->oNuevas_montoPendiente,true);
		$criteria->compare('oNuevas_cantidad',$this->oNuevas_cantidad,true);
		$criteria->compare('oPagos_montoRecibidoTT',$this->oPagos_montoRecibidoTT,true);
		$criteria->compare('oPagos_montoRecibidoEF',$this->oPagos_montoRecibidoEF,true);
		$criteria->compare('oPagos_montoRecibidoCH',$this->oPagos_montoRecibidoCH,true);
		$criteria->compare('oPagos_montoRecibidoBC',$this->oPagos_montoRecibidoBC,true);
		$criteria->compare('oPagos_montoPendiente',$this->oPagos_montoPendiente,true);
		$criteria->compare('oPagos_cantidad',$this->oPagos_cantidad,true);
		$criteria->compare('montoEfectivo',$this->montoEfectivo,true);
		$criteria->compare('montoBanco',$this->montoBanco,true);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('fecha_actualizacion',$this->fecha_actualizacion);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

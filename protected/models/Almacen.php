<?php

/**
 * This is the model class for table "almacen".
 *
 * The followings are the available columns in table 'almacen':
 * @property integer $id
 * @property string $descripcion
 * @property string $ubicacion
 * @property integer $responsable
 * @property string $fecha_registro
 *
 * The followings are the available model relations:
 * @property Material[] $materials
 */
class Almacen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Almacen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'almacen';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, ubicacion, responsable, fecha_registro', 'required'),
			array('responsable', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descripcion, ubicacion, responsable, fecha_registro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'materials' => array(self::HAS_MANY, 'Material', 'almacen_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'ubicacion' => 'Ubicacion',
			'responsable' => 'Responsable',
			'fecha_registro' => 'Fecha Registro',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('ubicacion',$this->ubicacion,true);
		$criteria->compare('responsable',$this->responsable);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getListAlmacenesJSON( )
   {
      $data = array( );
      $almacenes = Almacen::model()->findAll();
      foreach($almacenes as $a) $data[ ] = array(
                                                'id'=>$a->id,
                                                'descripcion'=>$a->descripcion
                                                );
      return json_encode($data);
   }

   public static function getListAlmacenesAry( )
   {
      $data = array( );
      $almacenes = Almacen::model()->findAll();
      foreach($almacenes as $a) $data[ ] = array(
                                                'id'=>$a->id,
                                                'descripcion'=>$a->descripcion
                                                );
      return $data;
   }

   public static function getListAlmacenes( )
   {
      $data = array(0=>'Seleccione...');
      $almacenes = Almacen::model()->findAll('id>0');
      foreach($almacenes as $a) $data[$a->id] = $a->descripcion;
      return $data;
   }

}

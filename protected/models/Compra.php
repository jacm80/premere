<?php

/**
 * This is the model class for table "compra".
 *
 * The followings are the available columns in table 'compra':
 * @property integer $id
 * @property integer $proveedor_id
 * @property string $fecha_registro
 * @property integer $numero_factura
 * @property string $fecha_emision
 * @property string $fecha_vencimiento
 * @property string $base_imponible
 * @property string $iva
 * @property string $resta
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property FormaPago $formaPago
 * @property Proveedor $proveedor
 */
class Compra extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Compra the static model class
	 */

   public  $monto;
   private $monto_cancelado;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'compra';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('proveedor_id, fecha_registro, numero_factura, fecha_emision, fecha_vencimiento, base_imponible, iva', 'required'),
			array('proveedor_id, numero_factura', 'numerical', 'integerOnly'=>true),
			array('base_imponible, iva, resta', 'length', 'max'=>10),
			array('estatus', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, proveedor_id, fecha_registro, numero_factura, fecha_emision, 
               fecha_vencimiento, base_imponible, iva, resta, 
               estatus', 'safe', 'on'=>'search'),
			array('fecha_registro, resta, estatus','safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'proveedor' => array(self::BELONGS_TO, 'Proveedor', 'proveedor_id'),
			'pagos' => array(self::HAS_MANY, 'CompraPago', 'compra_id'),
			'items' => array(self::HAS_MANY, 'CompraItem', 'compra_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'proveedor_id' => 'Proveedor',
			'fecha_registro' => 'Fecha Registro',
			'numero_factura' => 'Numero Factura',
			'fecha_emision' => 'Fecha Emision',
			'fecha_vencimiento' => 'Fecha Vencimiento',
			'base_imponible' => 'Base Imponible',
			'iva' => 'Iva',
			'resta' => 'Resta',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('proveedor_id',$this->proveedor_id);
		$criteria->compare('fecha_registro',$this->fecha_registro,true);
		$criteria->compare('numero_factura',$this->numero_factura);
		$criteria->compare('fecha_emision',$this->fecha_emision,true);
		$criteria->compare('fecha_vencimiento',$this->fecha_vencimiento,true);
		$criteria->compare('base_imponible',$this->base_imponible,true);
		$criteria->compare('iva',$this->iva,true);
		$criteria->compare('resta',$this->resta,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   private function _actualizarInventario($ary)
   {
      $params = array('catalogo_material_id'=>$ary['catalogo_material_id'],'almacen_id'=>$ary['almacen_id']); 
      $material = Material::model()->findbyAttributes( $params );
      if ($material==null)
      {
         $material = new Material;
         $material->attributes = $ary;
         $material->save( );
      }
      else 
      {
         $material->existencia_unidad += $ary['cantidad'];
         $material->save( );
      }
   }

   private function _guardarItems($items)
   {
      $i=0;
      foreach ($items['id'] as $id)
      {
         $cItem = new CompraItem;
			//array('id, compra_id, material_Id, cantidad, precio_unitario, iva, almacen_id', 'required'),
         $cItem->attributes = array(
                                    'compra_id'      => $this->id,
                                    'material_id'    => $id,
                                    'cantidad'       => $items['cantidad'][$i],    
                                    'precio_unitario'=> $items['precio_unitario'][$i],    
                                    'iva'            => $items['iva'][$i],    
                                    'almacen_id'     => $items['almacen_id'][$i]    
                                    );
         $cItem->save( );
         $this->_actualizarInventario(array(
                                           'catalogo_material_id'=> $items['id'][$i],
                                           'almacen_id'          => $items['almacen_id'][$i],
                                           'cantidad'            => $items['cantidad'][$i],
                                           'punto_reorden'       => 0
                                          )); 
         $i++;
      }
   }

   private function _guardarPagos($items)
   {
      $i = 0;
      //////////////////////////////////////////////////////////////////////////////////  
      foreach($items['monto'] as $monto)
      {
         $pago = new CompraPago;
         $pago->attributes = array(
                                    'fecha'             => date('Y-m-d h:m:i'), //$items['fecha'][$i],
                                    'forma_pago_id'     => $items['forma_pago_id'][$i],
                                    'compra_id'         => $this->id,
                                    'empresa_banco_id'  => $items['banco_id'][$i],
                                    'numero_comprobante'=> $items['numero_comprobante'][$i],
                                    'descripcion'       => $items['descripcion'][$i],
                                    'monto'             => $monto
                                  );
         $this->monto_cancelado += $monto;
         $pago->save(  );
         $i++;
      }
   }

   public function guardar( )
   {
      $_items  = $_POST['Compra']['compra_item'];
      $_compra = $_POST['Compra'];
      $_pagos  = (isset($_POST['Pago'])) ? $_POST['Pago'] : array();
      unset($_compra['compra_item']);
      $_compra['fecha_registro'] = date('Y-m-d');
      $transaction = $this->dbConnection->beginTransaction( );
      $this->attributes = $_compra;
      echo HtmlApp::debug($_POST);
      //return;
      try
      {
         if ($this->save( ))
         {
            $this->_guardarItems($_items);
            /// en caso de que hayan pagos //////////////////////////////////
            if (count($_pagos)>0){ $this->_guardarPagos($_pagos); }
            else $this->estatus='Pendiente';
            /////////////////////////////////////////////////////////////////
            if ( $this->base_imponible + $this->iva > $this->monto_cancelado)
            {
               $this->estatus = 'Pendiente';
            }
            else {
               $this->estatus = 'Cancelado';
            }
            $this->save( );
            $transaction->commit( );
         }
      }
      catch(Exception $e)
      {
         echo "<pre>$e</pre>";
         $transaction->rollback( );
      }

   }

}

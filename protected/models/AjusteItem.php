<?php

/**
 * This is the model class for table "ajuste_item".
 *
 * The followings are the available columns in table 'ajuste_item':
 * @property integer $id
 * @property integer $ajuste_id
 * @property integer $producto_id
 * @property string $base_imponible
 * @property string $iva
 *
 * The followings are the available model relations:
 * @property Producto $producto
 * @property Ajuste $ajuste
 */
class AjusteItem extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AjusteItem the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ajuste_item';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ajuste_id, producto_id, base_imponible, iva', 'required'),
			array('ajuste_id, producto_id', 'numerical', 'integerOnly'=>true),
			array('base_imponible, iva', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ajuste_id, producto_id, base_imponible, iva', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'producto' => array(self::BELONGS_TO, 'Producto', 'producto_id'),
			'ajuste' => array(self::BELONGS_TO, 'Ajuste', 'ajuste_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ajuste_id' => 'Ajuste',
			'producto_id' => 'Producto',
			'base_imponible' => 'Base Imponible',
			'iva' => 'Iva',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ajuste_id',$this->ajuste_id);
		$criteria->compare('producto_id',$this->producto_id);
		$criteria->compare('base_imponible',$this->base_imponible,true);
		$criteria->compare('iva',$this->iva,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "pago".
 *
 * The followings are the available columns in table 'pago':
 * @property integer $id
 * @property integer $orden_id
 * @property integer $factura_id
 * @property integer $forma_pago_id
 * @property string $fecha
 * @property integer $banco_id
 * @property string $numero_comprobante
 * @property integer $usuario_id
 * @property string $descripcion
 * @property string $monto
 *
 * The followings are the available model relations:
 * @property Banco $banco
 * @property Factura $factura
 * @property FormaPago $formaPago
 * @property Orden $orden
 * @property Usuario $usuario
 */
class Pago extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Pago the static model class
	 */


	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pago';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('orden_id, forma_pago_id, fecha, usuario_id, descripcion', 'required'),
			array('orden_id, factura_id, forma_pago_id, banco_id, usuario_id', 'numerical', 'integerOnly'=>true),
			array('monto','numerical'),
			array('numero_comprobante', 'length', 'max'=>20),
			array('descripcion', 'length', 'max'=>100),
         array('orden_id, descripcion, monto','safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, orden_id, factura_id, forma_pago_id, fecha, banco_id, numero_comprobante, usuario_id, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'banco' => array(self::BELONGS_TO, 'Banco', 'banco_id'),
			'factura' => array(self::BELONGS_TO, 'Factura', 'factura_id'),
			'formaPago' => array(self::BELONGS_TO, 'FormaPago', 'forma_pago_id'),
			'orden' => array(self::BELONGS_TO, 'Orden', 'orden_id'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'usuario_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'orden_id' => 'Orden',
			'factura_id' => 'Factura',
			'forma_pago_id' => 'Forma Pago',
			'fecha' => 'Fecha',
			'banco_id' => 'Banco',
			'numero_comprobante' => 'Numero Comprobante',
			'usuario_id' => 'Usuario',
			'descripcion' => 'Descripcion',
         'monto'       => 'Monto'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('orden_id',$this->orden_id);
		$criteria->compare('factura_id',$this->factura_id);
		$criteria->compare('forma_pago_id',$this->forma_pago_id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('banco_id',$this->banco_id);
		$criteria->compare('numero_comprobante',$this->numero_comprobante,true);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

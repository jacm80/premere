<?php

/**
 * This is the model class for table "unidad".
 *
 * The followings are the available columns in table 'unidad':
 * @property integer $id
 * @property string $descripcion
 * @property string $abreviatura
 * @property string $descripcion_detallado
 * @property integer $valor_detallado
 */
class Unidad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Unidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'unidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion, abreviatura, descripcion_detallado, valor_detallado', 'required'),
			array('valor_detallado', 'numerical', 'integerOnly'=>true),
			array('abreviatura', 'length', 'max'=>4),
			array('descripcion_detallado', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descripcion, abreviatura, descripcion_detallado, valor_detallado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
			'abreviatura' => 'Abreviatura',
			'descripcion_detallado' => 'Descripcion Detallado',
			'valor_detallado' => 'Valor Detallado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);
		$criteria->compare('abreviatura',$this->abreviatura,true);
		$criteria->compare('descripcion_detallado',$this->descripcion_detallado,true);
		$criteria->compare('valor_detallado',$this->valor_detallado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function getListaUnidades( )
   {
      $data = array('0'=>'Seleccione');
      $unidades = Unidad::model()->findAll( );
      foreach($unidades as $u) $data[$u->id] = $u->descripcion;
      return $data;
   }


}

<?php

/**
 * This is the model class for table "ajuste".
 *
 * The followings are the available columns in table 'ajuste':
 * @property integer $id
 * @property string $fecha_ajuste
 * @property integer $factura_id
 * @property integer $forma_pago_id
 * @property string $explique
 * @property string $monto_factura
 * @property string $base_imponible
 * @property string $iva
 * @property string $tipo
 * @property integer $user_id
 *
 * The followings are the available model relations:
 * @property CrugeUser $user
 * @property Factura $factura
 * @property FormaPago $formaPago
 * @property AjusteItem[] $ajusteItems
 */
class Ajuste extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Ajuste the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ajuste';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha_ajuste, factura_id, forma_pago_id, explique, monto_factura, base_imponible, iva, tipo, user_id', 'required'),
			array('factura_id, forma_pago_id, user_id', 'numerical', 'integerOnly'=>true),
			array('monto_factura, base_imponible, iva', 'length', 'max'=>10),
			array('tipo', 'length', 'max'=>2),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha_ajuste, factura_id, forma_pago_id, explique, monto_factura, base_imponible, iva, tipo, user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'CrugeUser', 'user_id'),
			'factura' => array(self::BELONGS_TO, 'Factura', 'factura_id'),
			'formaPago' => array(self::BELONGS_TO, 'FormaPago', 'forma_pago_id'),
			'ajusteItems' => array(self::HAS_MANY, 'AjusteItem', 'ajuste_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha_ajuste' => 'Fecha Ajuste',
			'factura_id' => 'Factura',
			'forma_pago_id' => 'Forma Pago',
			'explique' => 'Explique',
			'monto_factura' => 'Monto Factura',
			'base_imponible' => 'Base Imponible',
			'iva' => 'Iva',
			'tipo' => 'Tipo',
			'user_id' => 'User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha_ajuste',$this->fecha_ajuste,true);
		$criteria->compare('factura_id',$this->factura_id);
		$criteria->compare('forma_pago_id',$this->forma_pago_id);
		$criteria->compare('explique',$this->explique,true);
		$criteria->compare('monto_factura',$this->monto_factura,true);
		$criteria->compare('base_imponible',$this->base_imponible,true);
		$criteria->compare('iva',$this->iva,true);
		$criteria->compare('tipo',$this->tipo,true);
		$criteria->compare('user_id',$this->user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
<?php

/**
 * This is the model class for table "gasto_tipo".
 *
 * The followings are the available columns in table 'gasto_tipo':
 * @property integer $id
 * @property string $descripcion
 */
class GastoTipo extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GastoTipo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gasto_tipo';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('descripcion', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   public static function ListaTipoGastos( )
   {
      $data = array('0'=>'Seleccione...');
      $tipos = GastoTipo::model()->findAll();
      foreach ($tipos as $t) $data[$t->id]=$t->descripcion;
      return $data;
   }

   // TEMP
   public static function ListaConceptosGastos( )
   {
      $data = array('0'=>'Seleccione...');
      $conceptos = GastoConcepto::model()->findAll();
      foreach ($conceptos as $c) $data[$c->id]=$c->descripcion;
      return $data;
   }


}

<?php

/**
 * This is the model class for table "material".
 *
 * The followings are the available columns in table 'material':
 * @property integer $id
 * @property integer $catalogo_material_id
 * @property integer $almacen_id
 * @property integer $punto_reorden
 * @property integer $existencia_unidad
 * @property integer $existencia_detallado
 *
 * The followings are the available model relations:
 * @property CompraItem[] $compraItems
 * @property Almacen $almacen
 * @property CatalogoMaterial $catalogoMaterial
 */
class Material extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Material the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'material';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('catalogo_material_id, almacen_id, punto_reorden, existencia_unidad, existencia_detallado', 'required'),
			array('catalogo_material_id, almacen_id, punto_reorden, existencia_unidad, existencia_detallado', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('almacen_id, existencia_unidad, existencia_detallado','safe'),
			//array('id, catalogo_material_id, almacen_id, punto_reorden, existencia_unidad, existencia_detallado', 'safe'),
			array('id, catalogo_material_id, almacen_id, punto_reorden, existencia_unidad, existencia_detallado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'compraItems' => array(self::HAS_MANY, 'CompraItem', 'material_Id'),
			'almacen' => array(self::BELONGS_TO, 'Almacen', 'almacen_id'),
			'catalogoMaterial' => array(self::BELONGS_TO, 'CatalogoMaterial', 'catalogo_material_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'catalogo_material_id' => 'Catalogo Material',
			'almacen_id' => 'Almacen',
			'punto_reorden' => 'Punto Reorden',
			'existencia_unidad' => 'Existencia Unidad',
			'existencia_detallado' => 'Existencia Detallado',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('catalogo_material_id',$this->catalogo_material_id);
		$criteria->compare('almacen_id',$this->almacen_id);
		$criteria->compare('punto_reorden',$this->punto_reorden);
		$criteria->compare('existencia_unidad',$this->existencia_unidad);
		$criteria->compare('existencia_detallado',$this->existencia_detallado);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

  public function getMaterialList( )
  {
      $data = array( );
      $materiales = Material::model()->findAll( );
      foreach($material as $m)
      {
         $data[$m->id] = $m->catalogoMaterial->descripcion;
      }
      return $data;
  }

}

<?php

/**
 * This is the model class for table "material_salida".
 *
 * The followings are the available columns in table 'material_salida':
 * @property integer $id
 * @property string $fecha
 * @property integer $tipo_salida_id
 * @property integer $almacen_id
 * @property integer $entrega
 * @property integer $recibe
 * @property string $observacion
 *
 * The followings are the available model relations:
 * @property MaterialTipoSalida $tipoSalida
 * @property Almacen $almacen
 * @property Personal $entregado
 * @property Personal $recibido
 */

class MaterialSalida extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MaterialSalida the static model class
	 */
	
   public $items; 

   public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'material_salida';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fecha, tipo_salida_id, almacen_id, entrega, recibe, observacion', 'required'),
			array('tipo_salida_id, almacen_id, entrega, recibe', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, fecha, tipo_salida_id, almacen_id, entrega, recibe, observacion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoSalida' => array(self::BELONGS_TO, 'MaterialTipoSalida', 'tipo_salida_id'),
			'almacen' => array(self::BELONGS_TO, 'Almacen', 'almacen_id'),
			'entregado' => array(self::BELONGS_TO, 'Personal', 'entrega'),
			'recibido' => array(self::BELONGS_TO, 'Personal', 'recibe'),
			'Items' => array(self::HAS_MANY, 'MaterialSalidaItem', 'material_salida_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'fecha' => 'Fecha',
			'tipo_salida_id' => 'Tipo Salida',
			'almacen_id' => 'Almacen',
			'entrega' => 'Entrega',
			'recibe' => 'Recibe',
			'observacion' => 'Observacion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('fecha',$this->fecha,true);
		$criteria->compare('tipo_salida_id',$this->tipo_salida_id);
		$criteria->compare('almacen_id',$this->almacen_id);
		$criteria->compare('entrega',$this->entrega);
		$criteria->compare('recibe',$this->recibe);
		$criteria->compare('observacion',$this->observacion,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

   private function _restarInventario($id,$cantidad,$tipo_cantidad)
   {
      $material = Material::model( )->findByPk($id);
      $valor_detallado = $material->catalogoMaterial->unidad->valor_detallado;
      if ($tipo_cantidad==1)
      {
         $material->existencia_unidad-=$cantidad;
      }
      else
      {
         $delta = $material->existencia_detallado-$cantidad;
         if ($delta<0)
         {
            $material->existencia_unidad--;
            $material->existencia_detallado=$valor_detallado+$delta;
         }
         else 
         {
            $material->existencia_detallado-=$cantidad;
         } 
      }
      $material->save( );
      return $id;
   }

   public function guardar( )
   {
      $transaction = $this->dbConnection->beginTransaction( );
      try
      {
         $salida = $_POST['MaterialSalida'];
         $salida['fecha'] = date('Y-m-d');
         $this->attributes = $salida;
         if ($this->save( ))
         {
            $i=0;
            foreach($_POST['detalle']['detalle-id'] as $cm_id)
            {
               $cantidad      = $_POST['detalle']['cantidad'][$i];
               $tipo_cantidad = $_POST['detalle']['tipo_cantidad'][$i];
               $detalle = new MaterialSalidaItem;
               $item = array(
                              'material_salida_id'    => $this->id,
                              'catalogo_material_id'  => $this->_restarInventario($cm_id,$cantidad,$tipo_cantidad),
                              'cantidad'              => $cantidad,
                              'tipo_cantidad'         => $tipo_cantidad
                            );
               $detalle->attributes = $item;
               $detalle->save( );
               if (! $detalle->save( ) ) { $transaction->rollback( ); return; }
               $i++;
            }
            $transaction->commit( );  
         }
      }
      catch(Exception $e){ $transaction->rollback( ); }
   }

}

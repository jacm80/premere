SELECT
   o.id AS numero_orden, 
   c.rif,
   c.razon_social,
   z.razon_social AS firma_comercial,
   s.descripcion AS sucursal,
   o.fecha_entrada,
   o.fecha_entrega,
   p.descripcion AS trabajo,
   dc.serie,
   dc.identificador,
   dc.inicio_numero,
   dc.fin_numero,
   dc.inicio_control,
   dc.fin_control,
   oe.descripcion AS estatus

FROM
   factura_item i
   
   LEFT JOIN factura  f ON (f.id = i.factura_id)
   LEFT JOIN orden    o ON (o.id = f.orden_id)
   LEFT JOIN cliente  c ON (c.id = o.cliente_id)
   LEFT JOIN firma    z ON (z.id = o.firma_id)
   LEFT JOIN sede     s ON (s.id = o.sede_id)
   LEFT JOIN producto p ON (p.id = i.producto_id)
   LEFT JOIN orden_status oe ON (oe.id = o.orden_status_id)
   LEFT JOIN documento_control dc ON (dc.factura_id = f.id)


--  LEFT JOIN banco    b ON (b.id = o.banco_id)

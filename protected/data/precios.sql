DROP EVENT IF EXISTS catalogo_precios;

delimiter |

CREATE EVENT sincro_views
ON SCHEDULE EVERY 5 MINUTE
	COMMENT 'BORRAR TABLA TEMPORAL, CREAR TABLA TEMPORAL, EXTRAER DATA DE LA VISTA'
DO
BEGIN
	DECLARE done INT DEFAULT 0;
   DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

   DROP TABLE IF EXISTS precios_temporal;
	CREATE TABLE precios_temporal 
	(
      producto_id INT AUTO_INCREMENT,
      barcode VARCHAR(255),
      material TEXT,
      marca TEXT,
      unidad TEXT,
      unidad_abreviatura VARCHAR(4),
      precio_unitario DECIMAL(10,2)
	) ENGINE=MyIsam;

   DECLARE cursor_cm CURSOR FOR SELECT 
                                    cm.id,
                                    cm.barcode,
                                    cm.descripcion AS material,
                                    m.descripcion  AS marca,
                                    u.descripcion  AS unidad,
                                    u.abreviatura AS unidad_abreviatura
                                    COALESCE ((SELECT MAX(cp.id) FROM catalogo_material_precio cp WHERE cp.catalogo_material_id=cm.id),0) AS precio_unitario
                                 FROM catalogo_material cm
                                    INNER JOIN marca m ON (cm.marca_id=m.id)
                                    INNER JOIN unidad u ON (cm.unidad_id=u.id)
                                 ORDER BY cm.id;
	REPEAT
      IF NOT done THEN

      END
   UNTIL done END REPEAT;

END 
|

delimiter;

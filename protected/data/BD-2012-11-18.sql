-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 18-11-2012 a las 12:53:05
-- Versión del servidor: 5.5.28
-- Versión de PHP: 5.3.10-1ubuntu3.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `premere_test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banco`
--

CREATE TABLE IF NOT EXISTS `banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `banco`
--

INSERT INTO `banco` (`id`, `descripcion`) VALUES
(0, 'NO APLICA'),
(1, 'BBVA Provincial'),
(2, 'Mercantil'),
(3, 'Banfoandes'),
(4, 'Industrial'),
(5, 'BanPro'),
(6, 'Venezuela'),
(7, '100% Banco'),
(8, 'BNC Nacional de Credito'),
(9, 'Banesco'),
(10, 'BOD'),
(11, 'Sofitasa'),
(12, 'CorpBanca'),
(13, 'Federal'),
(14, 'Caribe'),
(15, 'BFC');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rif` varchar(20) NOT NULL,
  `razon_social` text NOT NULL,
  `representante_legal` text NOT NULL,
  `domicilio_fiscal` text NOT NULL,
  `contribuyente_id` int(11) NOT NULL,
  `cliente_tipo_id` int(11) NOT NULL,
  `telefono_celular` varchar(13) NOT NULL,
  `telefono_residencial` varchar(13) NOT NULL,
  `registro_de_comercio` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `archivo_rif` varchar(30) NOT NULL,
  `archivo_registro_de_comercio` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_cliente_tipo` (`cliente_tipo_id`),
  KEY `fk_cliente_contribuyente` (`contribuyente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `rif`, `razon_social`, `representante_legal`, `domicilio_fiscal`, `contribuyente_id`, `cliente_tipo_id`, `telefono_celular`, `telefono_residencial`, `registro_de_comercio`, `email`, `archivo_rif`, `archivo_registro_de_comercio`) VALUES
(0, 'V-XXXXXXXX-X', 'NO APLICA', 'NO APLICA', 'NO APLICA', 1, 1, 'NO APLICA', 'NO APLICA', 'NO APLICA', 'NOAPLICA@NOAPLICA.COM', 'NO APLICA', 'NO APLICA'),
(1, 'V-15073339-8', 'Juan Antonio Canepa Marquez', 'Juan Antonio Canepa Marquez', 'Los Guasimitos, Calle 3, Casa No. 5', 1, 1, '0424-5812600', '0273-5463123', 'aaa', 'jacanepa@gmail.com', 'aaa', 'bbb'),
(2, 'J-309476427-1', 'Litografia e Impresiones Mundial c.a. (LITIMUNCA)', 'Maria Luisa Canepa', 'Barinas Estado Barinas', 1, 2, '111', '1111', 'aaaa', 'litimunca2@gmail.com', 'aaa', 'bbb');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_tipo`
--

CREATE TABLE IF NOT EXISTS `cliente_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='tipos de clientes' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cliente_tipo`
--

INSERT INTO `cliente_tipo` (`id`, `descripcion`) VALUES
(1, 'Persona Natural'),
(2, 'Compañia Anonima'),
(3, 'Asociaciôn Cooperativa'),
(4, 'Asociacion Civil');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contribuyente`
--

CREATE TABLE IF NOT EXISTS `contribuyente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='los tipos de contribuyentes segun el seniat' AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `contribuyente`
--

INSERT INTO `contribuyente` (`id`, `descripcion`) VALUES
(1, 'Ordinario'),
(2, 'No Sujeto'),
(3, 'Formal');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento`
--

CREATE TABLE IF NOT EXISTS `descuento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(60) NOT NULL,
  `porcentaje` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `descuento`
--

INSERT INTO `descuento` (`id`, `descripcion`, `porcentaje`) VALUES
(0, 'NO APLICA', 0),
(1, '5%', 5),
(2, '10%', 10),
(3, '15%', 15),
(4, '20%', 20),
(5, '25%', 25),
(6, '30%', 30),
(7, 'DONACION (100%)', 100);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_control`
--

CREATE TABLE IF NOT EXISTS `documento_control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `documento_fiscal_id` int(11) NOT NULL,
  `cliente_id` int(11) NOT NULL,
  `serie` varchar(3) NOT NULL,
  `identificador` int(11) NOT NULL,
  `inicio_numero` int(11) NOT NULL,
  `fin_numero` int(11) NOT NULL,
  `inicio_control` int(11) NOT NULL,
  `fin_control` int(11) NOT NULL,
  `juegos` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha_impresion` date NOT NULL,
  `factura_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_documentocontrol_factura` (`factura_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `documento_control`
--

INSERT INTO `documento_control` (`id`, `documento_fiscal_id`, `cliente_id`, `serie`, `identificador`, `inicio_numero`, `fin_numero`, `inicio_control`, `fin_control`, `juegos`, `cantidad`, `fecha_impresion`, `factura_id`) VALUES
(1, 1, 1, 'N/A', 0, 1, 300, 1, 300, 50, 6, '2012-11-17', 1),
(2, 1, 2, 'N/A', 0, 1, 500, 1, 500, 50, 10, '2012-11-17', 3),
(3, 2, 2, 'N/A', 0, 1, 300, 501, 800, 50, 6, '2012-11-24', 5),
(4, 1, 1, 'N/A', 0, 301, 600, 301, 600, 50, 6, '2012-11-18', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `documento_fiscal`
--

CREATE TABLE IF NOT EXISTS `documento_fiscal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `documento_fiscal`
--

INSERT INTO `documento_fiscal` (`id`, `descripcion`) VALUES
(0, 'NINGUNO'),
(1, 'FACTURA'),
(2, 'NOTA DE CREDITO'),
(3, 'NOTA DE DEBITO'),
(4, 'ORDEN DE ENTREGA'),
(5, 'FORMA LIBRE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rif` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `razon_social` text COLLATE utf8_spanish_ci NOT NULL,
  `domicilio_fiscal` text COLLATE utf8_spanish_ci NOT NULL,
  `telefonos` text COLLATE utf8_spanish_ci NOT NULL,
  `representantes` text COLLATE utf8_spanish_ci NOT NULL,
  `registro_de_comercio` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `Resolucion_GTIRA` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `mision` text COLLATE utf8_spanish_ci NOT NULL,
  `vision` text COLLATE utf8_spanish_ci NOT NULL,
  `logotipo` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `rif`, `razon_social`, `domicilio_fiscal`, `telefonos`, `representantes`, `registro_de_comercio`, `Resolucion_GTIRA`, `mision`, `vision`, `logotipo`) VALUES
(1, 'J309478167', 'Litografia e Impresiones Mundial (Litimunca) c.a.', 'Avenida Cruz Paredes No. 8-55, Barinas Estado Barinas', '(0273)5522891', '<ul>\r\n<li>Juana Canepa</li>\r\n<li>Luisa Canepa</li>\r\n<li>Roberto Canepa</li>\r\n<li>Juan Canepa</li>\r\n</li>', '', '', '\r\n	\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.', '\r\n	\r\n\r\nLorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi, qui nunc nobis videntur parum clari, fiant sollemnes in futurum.', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa_banco`
--

CREATE TABLE IF NOT EXISTS `empresa_banco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `banco_id` int(11) NOT NULL,
  `agencia` text NOT NULL,
  `numero` varchar(30) NOT NULL,
  `fecha_apertura` int(11) NOT NULL,
  `saldo` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE IF NOT EXISTS `factura` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `fecha_cancelacion` datetime DEFAULT NULL,
  `orden_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `base_imponible` decimal(10,2) NOT NULL,
  `iva` decimal(10,2) NOT NULL,
  `cancelada` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_factura_orden` (`orden_id`),
  KEY `fk_factura_usuario` (`usuario_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`id`, `numero`, `fecha_cancelacion`, `orden_id`, `usuario_id`, `base_imponible`, `iva`, `cancelada`) VALUES
(1, 1000, NULL, 1, 1, 108.00, 12.96, 0),
(2, 1001, NULL, 1, 1, 90.00, 10.80, 0),
(3, 1000, '2012-11-18 10:11:23', 2, 1, 200.00, 24.00, 1),
(5, 1000, '2012-11-18 10:11:12', 3, 1, 120.00, 14.40, 1),
(6, 1001, '2012-11-18 10:11:12', 3, 1, 480.00, 57.60, 1),
(7, 1000, '2012-11-18 10:11:06', 4, 1, 120.00, 14.40, 1),
(8, 1001, '2012-11-18 10:11:06', 4, 1, 171.00, 20.52, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura_item`
--

CREATE TABLE IF NOT EXISTS `factura_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `factura_id` int(11) NOT NULL,
  `producto_id` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_unitario` decimal(10,2) NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  `iva` decimal(10,2) NOT NULL,
  `descuento_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_facturaitem_factura` (`factura_id`),
  KEY `fk_facturaitem_producto` (`producto_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `factura_item`
--

INSERT INTO `factura_item` (`id`, `factura_id`, `producto_id`, `cantidad`, `precio_unitario`, `monto`, `iva`, `descuento_id`) VALUES
(3, 1, 1, 6, 18.00, 108.00, 12.96, 2),
(4, 2, 5, 100, 0.90, 90.00, 10.80, 2),
(5, 3, 1, 10, 20.00, 200.00, 24.00, 0),
(8, 5, 3, 6, 20.00, 120.00, 14.40, 0),
(9, 6, 5, 200, 0.90, 180.00, 21.60, 2),
(10, 6, 4, 200, 1.50, 300.00, 36.00, 0),
(11, 7, 1, 6, 20.00, 120.00, 14.40, 0),
(12, 8, 4, 50, 1.42, 71.00, 8.52, 1),
(13, 8, 5, 100, 1.00, 100.00, 12.00, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `firma`
--

CREATE TABLE IF NOT EXISTS `firma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) NOT NULL,
  `razon_social` varchar(255) NOT NULL,
  `domicilio_fiscal` text NOT NULL,
  `registro_de_comercio` text NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `archivo_registro_de_comercio` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_firma_cliente` (`cliente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `firma`
--

INSERT INTO `firma` (`id`, `cliente_id`, `razon_social`, `domicilio_fiscal`, `registro_de_comercio`, `telefono`, `archivo_registro_de_comercio`) VALUES
(0, 0, 'NO APLICA', 'NO APLICA', 'NO APLICA', 'NO APLICA', 'NO APLICA'),
(1, 1, 'Juan Antonio Canepa Marquez', 'Los Guasimitos, Calle 3, Casa No. 5', 'N/A', '0424-5812600', 'N/A'),
(2, 1, 'Canepa Software Inc.', 'Guasimitos', '1 Tomo 2B año 2012', '0424-5812600', 'aaa'),
(3, 2, 'Litografia e Impresiones Mundial', 'Barinas', 'aaaa', '111', '111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forma_pago`
--

CREATE TABLE IF NOT EXISTS `forma_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `requiere_comprobante` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `forma_pago`
--

INSERT INTO `forma_pago` (`id`, `descripcion`, `requiere_comprobante`) VALUES
(1, 'Efectivo', 0),
(2, 'Cheque', 1),
(3, 'Tarjeta de Debito', 1),
(4, 'Tarjeta de Credito', 1),
(5, 'Donacion', 0),
(6, 'Deposito Cta. N.XXXXXXXXXX Bco TAL', 1),
(7, 'Transferencia Online Cta. N.XXXXXXX Bco. TAL', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden`
--

CREATE TABLE IF NOT EXISTS `orden` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cliente_id` int(11) NOT NULL,
  `firma_id` int(11) NOT NULL,
  `sede_id` int(11) NOT NULL,
  `fecha_entrada` datetime NOT NULL,
  `fecha_entrega` date NOT NULL,
  `direcciones_incluidas` text NOT NULL,
  `telefonos_incluidos` text NOT NULL,
  `publicidad_incluida` text NOT NULL,
  `observaciones` text NOT NULL,
  `abono` decimal(10,2) NOT NULL,
  `orden_status_id` int(11) NOT NULL,
  `base_imponible` decimal(10,2) NOT NULL,
  `iva` decimal(10,2) NOT NULL,
  `resta` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_orden_cliente` (`cliente_id`),
  KEY `fk_orden_status` (`orden_status_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='solicitud de documentos' AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `orden`
--

INSERT INTO `orden` (`id`, `cliente_id`, `firma_id`, `sede_id`, `fecha_entrada`, `fecha_entrega`, `direcciones_incluidas`, `telefonos_incluidos`, `publicidad_incluida`, `observaciones`, `abono`, `orden_status_id`, `base_imponible`, `iva`, `resta`) VALUES
(1, 1, 1, 4, '2012-11-17 10:11:16', '2012-11-17', 'aaa', 'bbb', 'ccc', 'ddd', 100.00, 1, 198.00, 23.76, 121.76),
(2, 2, 3, 3, '2012-11-17 11:11:29', '2012-11-17', 'aaa', 'bbb', 'ccc', 'ddd', 200.00, 1, 200.00, 24.00, 0.00),
(3, 2, 3, 3, '2012-11-17 11:11:33', '2012-11-24', 'aaa', 'bbb', 'ccc', 'ddd', 100.00, 1, 600.00, 72.00, 0.00),
(4, 1, 0, 0, '2012-11-18 09:11:05', '2012-11-18', 'aaa', 'bbb', 'ccc', 'ddd', 100.00, 1, 291.00, 34.92, 225.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden_status`
--

CREATE TABLE IF NOT EXISTS `orden_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='estatus de una orden para tener un seguimiento' AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `orden_status`
--

INSERT INTO `orden_status` (`id`, `descripcion`) VALUES
(1, 'CREADA'),
(2, 'IMPRESA'),
(3, 'FACTURADA');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago`
--

CREATE TABLE IF NOT EXISTS `pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orden_id` int(11) NOT NULL,
  `factura_id` int(11) NOT NULL,
  `forma_pago_id` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `banco_id` int(11) NOT NULL,
  `numero_comprobante` varchar(20) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  `monto` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `pago`
--

INSERT INTO `pago` (`id`, `orden_id`, `factura_id`, `forma_pago_id`, `fecha`, `banco_id`, `numero_comprobante`, `usuario_id`, `descripcion`, `monto`) VALUES
(1, 1, 0, 1, '2012-11-17 10:11:16', 0, '', 1, 'Abono Orden', 0.00),
(2, 2, 0, 1, '2012-11-17 11:11:29', 0, '', 1, 'Abono Orden', 0.00),
(3, 3, 0, 2, '2012-11-17 11:11:33', 0, '213213', 1, 'Abono Orden', 0.00),
(4, 4, 0, 1, '2012-11-18 09:11:05', 0, '', 1, 'Abono Orden', 0.00),
(5, 2, 0, 1, '2012-11-18 10:11:23', 0, '', 1, 'Cancelar Orden', 24.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE IF NOT EXISTS `producto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `precio_unitario` decimal(10,2) NOT NULL,
  `cantidad_minima` int(11) NOT NULL,
  `documento_fiscal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_producto_df` (`documento_fiscal_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id`, `descripcion`, `precio_unitario`, `cantidad_minima`, `documento_fiscal_id`) VALUES
(1, 'Talonarios Factura 1/18 Original y Copia Papel Quimico 1 Color', 20.00, 2, 1),
(2, 'Talonarios Factura 1/18 Original y Copia Papel Quimico 2 Colores ', 30.50, 5, 1),
(3, 'Talonarios Notas de Credito Tam. 1/18 1 Color papel Quimico', 20.00, 2, 2),
(4, 'Tarjetas de Presentacion medida 20x12 pcs. 1 Color', 1.50, 200, 0),
(5, 'Hojas Membrete Tamaño Carta', 1.00, 300, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto_precio`
--

CREATE TABLE IF NOT EXISTS `producto_precio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto_id` int(11) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `fecha_ajuste` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE IF NOT EXISTS `rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `descripcion`) VALUES
(1, 'administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sede`
--

CREATE TABLE IF NOT EXISTS `sede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `firma_id` int(11) NOT NULL,
  `domicilio_fiscal` text NOT NULL,
  `telefono` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sucursal_firma` (`firma_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `sede`
--

INSERT INTO `sede` (`id`, `descripcion`, `firma_id`, `domicilio_fiscal`, `telefono`) VALUES
(0, 'NO APLICA', 0, 'NO APLICA', 'NO APLICA'),
(1, 'Principal', 2, 'Edificio Canepa 2da. Planta Local 5, Barinas', '0273-5561111'),
(2, 'Socopo', 2, 'Debajo de la mata e mango, Socopo', '0273-5522891'),
(3, 'Principal', 3, 'Barinas', '0273-5522891'),
(4, 'Personal', 1, 'IGUAL', 'IGUAL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol_id` int(11) NOT NULL,
  `personal_id` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `activo` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuario_rol` (`rol_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `username`, `password`, `rol_id`, `personal_id`, `fecha_creacion`, `activo`) VALUES
(1, 'admin', '202cb962ac59075b964b07152d234b70', 1, 1, '2012-10-26 00:00:00', 1);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD CONSTRAINT `fk_cliente_contribuyente` FOREIGN KEY (`contribuyente_id`) REFERENCES `contribuyente` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_cliente_tipo` FOREIGN KEY (`cliente_tipo_id`) REFERENCES `cliente_tipo` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `documento_control`
--
ALTER TABLE `documento_control`
  ADD CONSTRAINT `fk_documentocontrol_factura` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`);

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `fk_factura_orden` FOREIGN KEY (`orden_id`) REFERENCES `orden` (`id`),
  ADD CONSTRAINT `fk_factura_usuario` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `factura_item`
--
ALTER TABLE `factura_item`
  ADD CONSTRAINT `fk_facturaitem_factura` FOREIGN KEY (`factura_id`) REFERENCES `factura` (`id`),
  ADD CONSTRAINT `fk_facturaitem_producto` FOREIGN KEY (`producto_id`) REFERENCES `producto` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `firma`
--
ALTER TABLE `firma`
  ADD CONSTRAINT `fk_firma_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `orden`
--
ALTER TABLE `orden`
  ADD CONSTRAINT `fk_orden_cliente` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`),
  ADD CONSTRAINT `fk_orden_status` FOREIGN KEY (`orden_status_id`) REFERENCES `orden_status` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `producto`
--
ALTER TABLE `producto`
  ADD CONSTRAINT `fk_producto_df` FOREIGN KEY (`documento_fiscal_id`) REFERENCES `documento_fiscal` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `sede`
--
ALTER TABLE `sede`
  ADD CONSTRAINT `fk_sucursal_firma` FOREIGN KEY (`firma_id`) REFERENCES `firma` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `fk_usuario_rol` FOREIGN KEY (`rol_id`) REFERENCES `rol` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

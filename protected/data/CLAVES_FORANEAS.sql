ALTER TABLE cliente 
   ADD CONSTRAINT fk_cliente_tipo FOREIGN KEY (cliente_tipo_id)
   REFERENCES cliente_tipo(cliente_tipo)  ON UPDATE CASCADE;

-- FOREIGN DE ORDEN_TRABAJO
ALTER TABLE orden 
   ADD CONSTRAINT fk_orden_cliente FOREIGN KEY (cliente_id)
   REFERENCES cliente(id)  ON UPDATE RESTRICT;

ALTER TABLE orden 
   ADD CONSTRAINT fk_orden_firma FOREIGN KEY (firma_id)
   REFERENCES firma(id)  ON UPDATE RESTRICT;

ALTER TABLE orden 
   ADD CONSTRAINT fk_orden_sede FOREIGN KEY (sede_id)
   REFERENCES sede(id)  ON UPDATE RESTRICT;

ALTER TABLE orden 
   ADD CONSTRAINT fk_orden_status FOREIGN KEY (orden_status_id)
   REFERENCES orden_status(id) ON UPDATE CASCADE;

ALTER TABLE orden 
   ADD CONSTRAINT fk_orden_banco FOREIGN KEY (banco_id)
   REFERENCES banco(id) ON UPDATE CASCADE;

ALTER TABLE orden 
   ADD CONSTRAINT fk_orden_formapago FOREIGN KEY (forma_pago_id)
   REFERENCES forma_pago(id) ON UPDATE CASCADE;

-- FOREIGN DE ORDEN_ITEM
ALTER TABLE orden_item 
   ADD CONSTRAINT fk_ordenitem_orden FOREIGN KEY (orden_id)
   REFERENCES orden(id) ON UPDATE RESTRICT;

ALTER TABLE orden_item 
   ADD CONSTRAINT fk_ordenitem_producto FOREIGN KEY (producto_id)
   REFERENCES producto(id) ON UPDATE RESTRICT;

-- FOREIGN DE FACTURA
ALTER TABLE factura 
   ADD CONSTRAINT fk_factura_orden FOREIGN KEY (orden_id)
   REFERENCES orden(id) ON UPDATE RESTRICT;

ALTER TABLE factura 
   ADD CONSTRAINT fk_factura_usuario FOREIGN KEY (usuario_id)
   REFERENCES usuario(id) ON UPDATE RESTRICT;

-- FOREIGN DE FACTURA_ITEM
ALTER TABLE factura_item 
   ADD CONSTRAINT fk_facturaitem_factura FOREIGN KEY (factura_id)
   REFERENCES factura(id) ON UPDATE RESTRICT;

ALTER TABLE factura_item 
   ADD CONSTRAINT fk_facturaitem_ordenitem FOREIGN KEY (orden_item_id)
   REFERENCES orden_item(id) ON UPDATE RESTRICT;

-- FOREIGN PARA EL CONTROL DE LOS NUMERO
ALTER TABLE documento_control 
   ADD CONSTRAINT fk_documentocontrol_factura FOREIGN KEY(factura_id) 
   REFERENCES factura(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA QUE REGISTRA LOS PAGOS
ALTER TABLE pago 
   ADD CONSTRAINT fk_pago_orden FOREIGN KEY (orden_id) 
   REFERENCES orden(id) ON DELETE RESTRICT;

ALTER TABLE pago 
   ADD CONSTRAINT fk_pago_factura FOREIGN KEY (factura_id) 
   REFERENCES factura(id) ON DELETE RESTRICT;

ALTER TABLE pago 
   ADD CONSTRAINT fk_pago_forma_pago FOREIGN KEY (forma_pago_id) 
   REFERENCES forma_pago(id) ON DELETE RESTRICT;

ALTER TABLE pago 
   ADD CONSTRAINT fk_pago_usuario FOREIGN KEY (usuario_id) 
   REFERENCES usuario(id) ON DELETE RESTRICT;

ALTER TABLE pago 
   ADD CONSTRAINT fk_pago_banco FOREIGN KEY (banco_id) 
   REFERENCES banco(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA PROVEEDOR
ALTER TABLE proveedor
   ADD CONSTRAINT fk_proveedor_banco FOREIGN KEY (banco_id) 
   REFERENCES banco(id) ON DELETE RESTRICT;

ALTER TABLE proveedor
   ADD CONSTRAINT fk_proveedor_contribuyente FOREIGN KEY (contribuyente_id) 
   REFERENCES contribuyente(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA BENEFICIARIOS
ALTER TABLE beneficiario
   ADD CONSTRAINT fk_beneficiario_banco FOREIGN KEY (banco_id) 
   REFERENCES banco(id) ON DELETE RESTRICT;

ALTER TABLE beneficiario
   ADD CONSTRAINT fk_beneficiario_contribuyente FOREIGN KEY (contribuyente_id) 
   REFERENCES contribuyente(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA GASTO
ALTER TABLE gasto
   ADD CONSTRAINT fk_gasto_concepto FOREIGN KEY (gasto_concepto_id) 
   REFERENCES gasto_concepto(id) ON DELETE RESTRICT;

ALTER TABLE gasto
   ADD CONSTRAINT fk_gasto_forma_pago FOREIGN KEY (forma_pago_id) 
   REFERENCES forma_pago(id) ON DELETE RESTRICT;

ALTER TABLE gasto
   ADD CONSTRAINT fk_gasto_banco FOREIGN KEY (banco_id) 
   REFERENCES banco(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA COMPRA
ALTER TABLE compra
   ADD CONSTRAINT fk_compra_proveedor FOREIGN KEY (proveedor_id) 
   REFERENCES proveedor(id) ON DELETE RESTRICT;

ALTER TABLE compra
   ADD CONSTRAINT fk_compra_forma_pago FOREIGN KEY (forma_pago_id) 
   REFERENCES forma_pago(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA COMPRA_ITEM
ALTER TABLE compra_item
   ADD CONSTRAINT fk_compraitem_compra FOREIGN KEY (compra_id) 
   REFERENCES compra(id) ON DELETE RESTRICT;

ALTER TABLE compra_item
   ADD CONSTRAINT fk_compra_item_material FOREIGN KEY (material_id) 
   REFERENCES material(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA MATERIAL
ALTER TABLE material
   ADD CONSTRAINT fk_material_catalogo FOREIGN KEY (catalogo_material_id) 
   REFERENCES catalogo_material(id) ON DELETE RESTRICT;

ALTER TABLE material
   ADD CONSTRAINT fk_material_almacen FOREIGN KEY (almacen_id) 
   REFERENCES almacen(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA MATERIAL
ALTER TABLE catalogo_material
   ADD CONSTRAINT fk_catalogomaterial_tipo FOREIGN KEY (material_tipo_id) 
   REFERENCES material_tipo(id) ON DELETE RESTRICT;

ALTER TABLE catalogo_material
   ADD CONSTRAINT fk_catalogomaterial_marca FOREIGN KEY (marca_id) 
   REFERENCES marca(id) ON DELETE RESTRICT;

ALTER TABLE catalogo_material
   ADD CONSTRAINT fk_catalogomaterial_unidad FOREIGN KEY (unidad_id) 
   REFERENCES unidad(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA DE MATERIAL_SALIDA
ALTER TABLE material_salida
   ADD CONSTRAINT fk_material_salida_tipo FOREIGN KEY (tipo_salida_id) 
   REFERENCES material_tipo_salida(id) ON DELETE RESTRICT;

ALTER TABLE material_salida
   ADD CONSTRAINT fk_material_salida_almacen FOREIGN KEY (almacen_id) 
   REFERENCES almacen(id) ON DELETE RESTRICT;

ALTER TABLE material_salida
   ADD CONSTRAINT fk_material_salida_entrega FOREIGN KEY (entrega) 
   REFERENCES personal(id) ON DELETE RESTRICT;

ALTER TABLE material_salida
   ADD CONSTRAINT fk_material_salida_recibe FOREIGN KEY (recibe) 
   REFERENCES personal(id) ON DELETE RESTRICT;

-- FOREIGNS PARA LA TABLA DE MATERIAL_SALIDA
ALTER TABLE material_salida_item
   ADD CONSTRAINT fk_salida_item FOREIGN KEY (material_salida_id) 
   REFERENCES material_salida(id) ON DELETE RESTRICT;

ALTER TABLE material_salida_item
   ADD CONSTRAINT fk_salida_catalogo FOREIGN KEY (catalogo_material_id) 
   REFERENCES catalogo_material (id) ON DELETE RESTRICT;

-- FOREINS PARA LA TABLA EMPRESA_BANCOS
ALTER TABLE empresa_banco
   ADD CONSTRAINT fk_empresa_banco FOREIGN KEY (banco_id) 
   REFERENCES banco(id) ON DELETE RESTRICT;

-- FOREINS PARA LA TABLA COMPRA_PAGO
ALTER TABLE compra_pago
   ADD CONSTRAINT fk_compra_pago_compra FOREIGN KEY (compra_id) 
   REFERENCES compra(id) ON DELETE RESTRICT;

ALTER TABLE compra_pago
   ADD CONSTRAINT fk_compra_pago_banco FOREIGN KEY (empresa_banco_id) 
   REFERENCES empresa_banco(id) ON DELETE RESTRICT;

ALTER TABLE compra_pago
   ADD CONSTRAINT fk_comprapago_fpago FOREIGN KEY (forma_pago_id) 
   REFERENCES forma_pago(id) ON DELETE RESTRICT;

ALTER TABLE compra_pago
   ADD CONSTRAINT fk_comprapago_user FOREIGN KEY (user_id) 
   REFERENCES cruge_user(iduser) ON DELETE RESTRICT;

-- FOREINS PARA LA TABLA AJUSTE
ALTER TABLE ajuste
   ADD CONSTRAINT fk_ajuste_factura FOREIGN KEY (factura_id) 
   REFERENCES factura(id) ON DELETE RESTRICT;

ALTER TABLE ajuste
   ADD CONSTRAINT fk_ajuste_formapago FOREIGN KEY (forma_pago_id) 
   REFERENCES forma_pago(id) ON DELETE RESTRICT;

ALTER TABLE ajuste
   ADD CONSTRAINT fk_ajuste_user FOREIGN KEY (user_id) 
   REFERENCES cruge_user(iduser) ON DELETE RESTRICT;

-- FOREINS PARA LA TABLA AJUSTE_ITEM
ALTER TABLE ajuste_item
   ADD CONSTRAINT fk_ajuste_item FOREIGN KEY (ajuste_id) 
   REFERENCES ajuste(id) ON DELETE RESTRICT;

ALTER TABLE ajuste_item
   ADD CONSTRAINT fk_ajuste_producto FOREIGN KEY (producto_id) 
   REFERENCES producto(id) ON DELETE RESTRICT;

-- FOREINS PARA LA TABLA MATERIAL_CONCILIACION
ALTER TABLE material_conciliacion
   ADD CONSTRAINT fk_conciliacion_personal FOREIGN KEY (inspecciono) 
   REFERENCES personal(id) ON DELETE RESTRICT;

ALTER TABLE material_conciliacion
   ADD CONSTRAINT fk_conciliacion_almacen FOREIGN KEY (almacen_id) 
   REFERENCES almacen(id) ON DELETE RESTRICT;


-- FOREINS PARA LA TABLA MATERIAL_CONCILIACION_ITEM
ALTER TABLE material_conciliacion_item
   ADD CONSTRAINT fk_conciliacion_conitem FOREIGN KEY (conciliacion_id) 
   REFERENCES material_conciliacion(id) ON DELETE RESTRICT;

ALTER TABLE material_conciliacion_item
   ADD CONSTRAINT fk_conciliacion_item_catalogo FOREIGN KEY (catalogo_material_id) 
   REFERENCES catalogo_material(id) ON DELETE RESTRICT;

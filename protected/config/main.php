<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Premere System',
   'language'=>'es',
   'sourceLanguage'=>'en',
   'charset'=>'utf-8',
   //'onBeginRequest'=>array('Inicio','preparar'),
   'theme'=>'premere', 
   /**/
   /**/
	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
      'application.modules.cruge.components.*',
		'application.modules.cruge.extensions.crugemailer.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
      'nomina',
      'presupuesto',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'mocomoco',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
         'generatorPaths'=>array('application.modules.gii'),
		),
      'cruge'=>array(
				'tableprefix'=>'cruge_',

				// para que utilice a protected.modules.cruge.models.auth.CrugeAuthDefault.php
				//
				// en vez de 'default' pon 'authdemo' para que utilice el demo de autenticacion alterna
				// para saber mas lee documentacion de la clase modules/cruge/models/auth/AlternateAuthDemo.php
				//
				'availableAuthMethods'=>array('default'),

				'availableAuthModes'=>array('username','email'),
				'baseUrl'=>'http://localhost/premere',

				 // NO OLVIDES PONER EN FALSE TRAS INSTALAR
				 'debug'=>true,
				 'rbacSetupEnabled'=>true,
				 'allowUserAlways'=>true,
				 //'allowUserAlways'=>false,

				// MIENTRAS INSTALAS..PONLO EN: false
				// lee mas abajo respecto a 'Encriptando las claves'
				//
				'useEncryptedPassword' => false,

				// Algoritmo de la función hash que deseas usar
				// Los valores admitidos están en: http://www.php.net/manual/en/function.hash-algos.php
				'hash' => 'md5',

				// a donde enviar al usuario tras iniciar sesion, cerrar sesion o al expirar la sesion.
				//
				// esto va a forzar a Yii::app()->user->returnUrl cambiando el comportamiento estandar de Yii
				// en los casos en que se usa CAccessControl como controlador
				//
				// ejemplo:
				//		'afterLoginUrl'=>array('/site/welcome'),  ( !!! no olvidar el slash inicial / )
				//		'afterLogoutUrl'=>array('/site/page','view'=>'about'),
				//
				'afterLoginUrl'=>null,
				'afterLogoutUrl'=>null,
				'afterSessionExpiredUrl'=>null,

				// manejo del layout con cruge.
				//
				'loginLayout'=>'//layouts/column2',
				'registrationLayout'=>'//layouts/column2',
				'activateAccountLayout'=>'//layouts/column2',
				'editProfileLayout'=>'//layouts/column2',
				// en la siguiente puedes especificar el valor "ui" o "column2" para que use el layout
				// de fabrica, es basico pero funcional.  si pones otro valor considera que cruge
				// requerirá de un portlet para desplegar un menu con las opciones de administrador.
				//
				'generalUserManagementLayout'=>'ui',
			),      
	),

	// application components
	   'components'=>array(
         /*
         'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
         ),
         */
         //
			//  IMPORTANTE:  asegurate de que la entrada 'user' (y format) que por defecto trae Yii
			//               sea sustituida por estas a continuación:
			//
         'user'=>array(
				'allowAutoLogin'=>true,
				'class' => 'application.modules.cruge.components.CrugeWebUser',
				'loginUrl' => array('/cruge/ui/login'),
			),
			'authManager' => array(
				'class' => 'application.modules.cruge.components.CrugeAuthManager',
			),
			'crugemailer'=>array(
				'class' => 'application.modules.cruge.components.CrugeMailer',
				'mailfrom' => 'jacanepa@gmail.com',
				'subjectprefix' => 'Su nueva clave del sistema',
				'debug' => true,
			),
			'format' => array(
				'datetimeFormat'=>"d M, Y h:m:s a",
			),      
         /*
         'user'=>array(
            'class'=>'application.components.test.MiClase'
         ),
         */
         'ePdf' => array(
                        'class'  => 'ext.yii-pdf.EYiiPdf',
                        'params' => array(
                        'mpdf'   => array(
                                          'librarySourcePath' => 'application.vendors.mpdf.*',
                                          'constants'         => array(
                                          '_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
                                          ),
                        'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
                                          /* 
                                          'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
                                          'mode'              => '', //  This parameter specifies the mode of the new document.
                                          'format'            => 'A4', // format A4, A5, ...
                                          'default_font_size' => 10, // Sets the default document font size in points (pt)
                                          'default_font'      => '', // Sets the default font-family for the new document.
                                          'mgl'               => 15, // margin_left. Sets the page margins for the new document.
                                          'mgr'               => 15, // margin_right
                                          'mgt'               => 16, // margin_top
                                          'mgb'               => 16, // margin_bottom
                                          'mgh'               => 9, // margin_header
                                          'mgf'               => 9, // margin_footer
                                          'orientation'       => 'P', // landscape or portrait orientation
                                          )
                                          */
                                          ),
         'HTML2PDF' => array(
                           'librarySourcePath' => 'application.vendors.html2pdf.*',
                           'classFile'         => 'html2pdf.class.php', // For adding to Yii::$classMap
                           /*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
                           'orientation' => 'P', // landscape or portrait orientation
                           'format'      => 'A4', // format A4, A5, ...
                           'language'    => 'en', // language: fr, en, it ...
                           'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
                           'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
                           'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
                           )*/
                        ),
      ),
    ),
   /*
	'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
      */
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
         'showScriptName'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>/id',
			),
		),
      'widgetFactory'=>array(
         'widgets'=>array(
            'CGridView'=>array(
               'cssFile'=>false, 
               'pager'=>array('cssFile'=>false),
               'pagerCssClass'=>'paginator'
            ),
            'CListView'=>array(
               'cssFile'=>false, 
               'pager'=>array('cssFile'=>false),
               'pagerCssClass'=>'paginator'
            ),
            'CDetailView'=>array(
               'cssFile'=>false 
            ),
         ),
      ),

		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
      */

		// uncomment the following to use a MySQL database
		/**/
		'db'=>array(
			//'connectionString' => 'mysql:host=localhost;dbname=imprenta_dev',
			'connectionString' => 'mysql:host=localhost;dbname=premere_test',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '12345',
			'charset' => 'utf8',
         'enableProfiling'=>true
		),
      /*
      'dbxmlgen'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=xmlgenerator',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '12345',
			'charset' => 'utf8',
         //'enableProfiling'=>true,
         'class'   => 'CDbConnection'
		),
		/**/
      'core'=>array(
         'basePath'=>'protected/messages'
      ),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, rbac',
				),
				// uncomment the following to show log messages on web pages
				array(
					'class'         => 'CWebLogRoute',
               'levels'        => 'trace',
               'categories'    => 'vardump',
               'showInFireBug' => true,
				),
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'jacanepa@gmail.com',
	),
);
